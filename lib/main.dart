import 'dart:async';

import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:camera/camera.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';

import 'AppEngine.dart';
import 'Cam.dart';
import 'MainAdmin.dart';
import 'auth/auth_phone.dart';
import 'photo/model/photo_provider.dart';

RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

final galleryController = StreamController<List<String>>.broadcast();
final galleryResultController = StreamController<List<dynamic>>.broadcast();

final selectNotificationSubject = BehaviorSubject<String>();

void main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    cameras = await availableCameras();
  } on CameraException catch (e) {
    logError(e.code, e.description);
  }
  InAppPurchaseConnection.enablePendingPurchases();
  runApp(ChangeNotifierProvider<PhotoProvider>.value(
    value: provider,
    child: MyApp(),
  ));
}

final provider = PhotoProvider();

class MyApp extends StatelessWidget {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  @override
  Widget build(BuildContext c) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Convas",
        color: white,
        theme: ThemeData(fontFamily: 'sfu'),
        navigatorObservers: [
          routeObserver,
          FirebaseAnalyticsObserver(analytics: analytics)
        ],
        home: MainHome());
  }
}

class MainHome extends StatefulWidget {
  @override
  _MainHomeState createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  int _status = 0;
  List<DateTime> _events = [];
  bool _enabled = true;

  @override
  void initState() {
    // TODO: implement initState
    //loadContacts();
    //loadNotify();
    checkUser();
    loadSettings();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loadingLayout();
  }

  checkUser() async {
    await Future.delayed(Duration(milliseconds: 8));
    User user = await FirebaseAuth.instance.currentUser;
    //FirebaseAuth.instance.signOut();
    //return;
    if (user == null) {
      popUpUntil(context, AuthPhoneNumber());
    } else {
      loadLocalUser(user.uid, onInComplete: () {
        popUpUntil(context, AuthPhoneNumber());
      }, onLoaded: () {
//        cacheChat(CHAT_MODE_ENCRYPT, fetch: true);
//        cacheChat(CHAT_MODE_REGULAR, fetch: true, setState: () {
//          chatMessageController.add(true);
//          popUpUntil(context, MainAdmin());
//        });
        popUpUntil(context, MainAdmin());
      });
    }
  }

  loadSettings() {
    Firestore.instance
        .collection(APP_SETTINGS_BASE)
        .document(APP_SETTINGS)
        .get(/*source: Source.cache*/)
        .then((doc) {
      if (!doc.exists) {
        appSettingsModel = new BaseModel();
        appSettingsModel.saveItem(APP_SETTINGS_BASE, false,
            document: APP_SETTINGS);
        return;
      }
      appSettingsModel = BaseModel(doc: doc);
    });
  }

  loadLocalUser(String userId, {onLoaded, onInComplete}) {
    Firestore.instance
        .collection(USER_BASE)
        .document(userId)
        .get()
        .then((doc) async {
      userModel = BaseModel(doc: doc);
      isAdmin = userModel.getBoolean(IS_ADMIN) ||
          userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
          userModel.getString(EMAIL) == "ammaugost@gmail.com";
      if (!userModel.signUpCompleted || !doc.exists) {
        await GoogleSignIn().signOut();
        await FacebookAuth.instance.logOut();
        await FirebaseAuth.instance.signOut();
        userModel = BaseModel();
        onInComplete();
        return;
      }
      onLoaded();
    }).catchError((e) {
      popUpUntil(context, AuthPhoneNumber());
    });
  }
}
