import 'dart:async';
import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:video_player/video_player.dart';

import '../../assets.dart';
import '../../main.dart';

class VideoWidget extends StatefulWidget {
  final AssetEntity entity;

  const VideoWidget({
    Key key,
    @required this.entity,
  }) : super(key: key);

  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {
  VideoPlayerController _controller;
  StreamSubscription sub;
  List<String> visibleIds = [];
  AssetEntity entity;
  String id;
  bool ready = false;
  @override
  void initState() {
    super.initState();
    entity = widget.entity;
    sub = galleryController.stream.listen((List<String> ids) {
      visibleIds = ids;
      bool visible = ids.contains(entity.id);
      if (!_controller.value.initialized) return;
      if (!visible) {
        if (_controller.value.isPlaying && !_controller.value.initialized)
          _controller.pause();
      } else {
        if (!_controller.value.isPlaying &&
            ids.isNotEmpty &&
            ids[ids.length - 1] == entity.id) {
          _controller.play();
          print("Playing...");
        }
      }
    });
    setup();
  }

  setup() async {
    File file = await entity.file;
    if (file == null) return;
    _controller = VideoPlayerController.file(file)
      ..initialize().then((_) {
        if (visibleIds.isNotEmpty &&
            visibleIds[visibleIds.length - 1] == entity.id) _controller.play();
        ready = true;
        setState(() {});
      });
    _controller.setVolume(0);
    _controller.setLooping(true);
  }

  @override
  void dispose() {
    _controller?.dispose();
    sub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return !ready || (!_controller.value.initialized)
        ? (Container(
            child: Center(
            child: Icon(
              Icons.videocam,
              color: black.withOpacity(.3),
            ),
          )))
        : buildVideoPlayer();
  }

  buildVideoPlayer() {
    return GestureDetector(
      onPanStart: (_) {
        if (_controller.value.initialized && !_controller.value.isPlaying)
          _controller.play();
      },
      onPanDown: (_) {
        if (_controller.value.initialized && !_controller.value.isPlaying)
          _controller.play();
      },
      child: Stack(
        fit: StackFit.expand,
        children: [
          Align(
              alignment: Alignment.center,
              child: AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: VideoPlayer(_controller))),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              margin: EdgeInsets.all(5),
              child: Text(
                getTimerText(_controller.value.duration.inSeconds),
                style: textStyle(true, 12, white),
              ),
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              decoration: BoxDecoration(
                  color: black.withOpacity(.5),
                  borderRadius: BorderRadius.all(Radius.circular(25))),
            ),
          )
        ],
      ),
    );
  }
}
