import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/ShowAllMedia.dart';
import 'package:Strokes/ViewChatMedia.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/inputDialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ShareTo.dart';

class ShowProfile extends StatefulWidget {
  final BaseModel personModel;
  final List chatList;
  final int chatMode;
  final bool shared;
  final Map<String, BaseModel> groupMembers;
  ShowProfile(this.personModel, this.chatList, this.chatMode,
      {this.shared = false, this.groupMembers});
  @override
  _ShowProfileState createState() => _ShowProfileState();
}

class _ShowProfileState extends State<ShowProfile> {
  BaseModel theUser;
  bool myProfile = false;
  bool fromChat;
  double pixels = 0;
  List chatList;
  List allChatMedia = [];
  List chatMedia = [];
  List first10 = [];
  Map<String, BaseModel> groupMembers = {};
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    chatList = widget.chatList ?? [];
    theUser = widget.personModel;
    bool isGroup = widget.personModel.getString(GROUP_NAME).isNotEmpty;

    if (theUser.myItem() && !isGroup) {
      theUser = userModel;
      myProfile = true;
    }
    if (isGroup) groupMembers = widget.groupMembers;
    loadMedia();
    refreshPerson();
  }

  @override
  dispose() {
    subs?.cancel();
    super.dispose();
  }

  StreamSubscription subs;

  bool personRefreshed = false;
  refreshPerson() {
    bool isGroup = widget.personModel.getString(GROUP_NAME).isNotEmpty;
    if (isGroup) {
      subs = Firestore.instance
          .collection(widget.chatMode == CHAT_MODE_ENCRYPT
              ? CHAT_IDS_BASE_ENCRYPT
              : CHAT_IDS_BASE)
          .document(widget.personModel.getObjectId())
          .snapshots()
          .listen((event) {
        theUser = BaseModel(doc: event);
        personRefreshed = true;
        setState(() {});
        loadMembers(theUser.getList(PARTIES));
      });
      return;
    }

    subs = Firestore.instance
        .collection(USER_BASE)
        .document(widget.personModel.getUserId())
        .snapshots()
        .listen((event) {
      theUser = BaseModel(doc: event);
      personRefreshed = true;
      setState(() {});
      checkCallingId(theUser.getString(CALLING_ID));
    });
  }

  checkCallingId(String callingId) async {
    if (callingId.isEmpty) return;
    Firestore.instance
        .collection(CALLS_IDS_BASE)
        .document(callingId)
        .get()
        .then((value) {
      if (null == value) {
        theUser
          ..put(CALLING_ID, "")
          ..updateItems();
        setState(() {});
      }
    });
  }

  loadMembers(List parties) {
    for (var id in parties) {
      Firestore.instance.collection(USER_BASE).document(id).get().then((value) {
        if (value.exists) {
          BaseModel model = BaseModel(doc: value);
          groupMembers[model.getObjectId()] = model;
        } else {
          groupMembers.remove(id);
        }
      });
    }
    setState(() {});
  }

  loadMedia() {
    int count = 0;
    for (BaseModel model in chatList) {
      int type = model.getInt(TYPE);
      if (chatRemoved(model)) continue;
      if (type == CHAT_TYPE_TEXT) continue;
      if (type == CHAT_TYPE_REC) continue;
      if (type == CHAT_TYPE_CONTACT) continue;

      allChatMedia.add(model);
      if (type == CHAT_TYPE_DOC) continue;

      chatMedia.add(model);
      if (type == CHAT_TYPE_IMAGE || type == CHAT_TYPE_VIDEO) {
        if (count >= 10) continue;
        first10.add(model);
        count++;
      }
    }
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: white,
      body: page(),
    );
  }

  page() {
    int now = DateTime.now().millisecondsSinceEpoch;
    int lastUpdated = theUser.getInt(TIME_UPDATED);
    int tsDiff = (now - lastUpdated);

    bool notOnline = (tsDiff > (Duration.millisecondsPerMinute * 10));
    bool notTyping = (tsDiff > (Duration.millisecondsPerMinute * 1));
    bool isOnline = theUser.getBoolean(IS_ONLINE) && (!notOnline);
    bool groupChat = theUser.getString(GROUP_NAME).isNotEmpty;
    String phoneNumber = theUser.getString(PHONE_NUMBER);
    bool notInContact = contacts
        .where((e) => e.getString(QUERY) == phoneNumber)
        .toList()
        .isEmpty;
    bool blocked = userModel.getList(BLOCKED).contains(theUser.getUserId());

    return Stack(
      children: [
        ListView(
          padding: EdgeInsets.all(0),
          children: [
            GestureDetector(
              onTap: () {
                pushAndResult(
                    context,
                    ViewImage([
                      groupChat
                          ? theUser.getString(GROUP_PHOTO)
                          : theUser.userImage
                    ], 0),
                    depend: false);
              },
              child: CachedNetworkImage(
                height: getScreenHeight(context) * .4,
                imageUrl: groupChat
                    ? theUser.getString(GROUP_PHOTO)
                    : theUser.userImage,
                width: double.infinity,
                fit: BoxFit.cover,
                placeholder: (c, s) {
                  return Container(
                    color: AppConfig.appColor,
                    height: getScreenHeight(context) * .4,
                    width: double.infinity,
                    alignment: Alignment.center,
                    child: Text(
                      getInitials(
                          theUser.getString(groupChat ? GROUP_NAME : NAME)),
                      style: textStyle(true, 50, white),
                    ),
                  );
                },
              ),
            ),
            addSpace(10),
            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(10),
                color: white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      theUser.getString(groupChat ? GROUP_NAME : NAME),
                      style: textStyle(true, 20, black),
                    ),
                    if (groupChat && groupMembers.isNotEmpty)
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children:
                              List.generate(groupMembers.values.length, (p) {
                            BaseModel model = groupMembers.values.toList()[p];

                            if (model.items.isEmpty) return Container();

                            return GestureDetector(
                              onTap: () {
                                if (model.myItem()) return;
                                pushAndResult(
                                    context, ShowProfile(model, [], 0),
                                    opaque: false, depend: false);
                              },
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: [
                                    CachedNetworkImage(
                                      height: 130,
                                      width: 140,
                                      imageUrl: model.userImage,
                                      fit: BoxFit.cover,
                                      placeholder: (c, s) {
                                        return Container(
                                          color: AppConfig.appColor,
                                          height: 130,
                                          width: 140,
                                          alignment: Alignment.center,
                                          child: Text(
                                            getInitials(model.getString(NAME)),
                                            style: textStyle(true, 50, white),
                                          ),
                                        );
                                      },
                                    ),
                                    addSpace(10),
                                    Text(
                                      model.getString(NAME),
                                      style: textStyle(true, 15, black),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }),
                        ),
                      ),

//                      Container(
//                        height: 180,
//                        alignment: Alignment.centerLeft,
//                        child: ListView.builder(
//                            itemCount: groupMembers.length,
//                            scrollDirection: Axis.horizontal,
//                            itemBuilder: (ctx, p) {
//                              BaseModel model = groupMembers[p];
//                              return GestureDetector(
//                                onTap: () {
//                                  if (model.myItem()) return;
//                                  pushAndResult(
//                                      context, ShowProfile(model, [], 0),
//                                      opaque: false, depend: false);
//                                },
//                                child: Container(
//                                  padding: EdgeInsets.all(5),
//                                  child: Column(
//                                    children: [
//                                      CachedNetworkImage(
//                                        height: 130,
//                                        width: 140,
//                                        imageUrl: model.userImage,
//                                        fit: BoxFit.cover,
//                                        placeholder: (c, s) {
//                                          return Container(
//                                            color: AppConfig.appColor,
//                                            height: 130,
//                                            width: 140,
//                                            alignment: Alignment.center,
//                                            child: Text(
//                                              getInitials(
//                                                  model.getString(NAME)),
//                                              style: textStyle(true, 50, white),
//                                            ),
//                                          );
//                                        },
//                                      ),
//                                      addSpace(10),
//                                      Text(
//                                        model.getString(NAME),
//                                        style: textStyle(true, 15, black),
//                                      )
//                                    ],
//                                  ),
//                                ),
//                              );
//                            }),
//                      ),
                  ],
                )),
            if (allChatMedia.isNotEmpty) addSpace(10),
            if (allChatMedia.isNotEmpty)
              GestureDetector(
                onTap: () {
                  pushAndResult(context, ShowAllMedia(allChatMedia));
                },
                child: Container(
                  color: white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      addSpace(10),
                      Row(
                        children: [
                          addSpaceWidth(10),
                          Text(
                            "Chat Media",
                            style: textStyle(true, 16, black),
                          ),
                          addSpaceWidth(10),
                          Text(
                            "${chatMedia.length}",
                            style: textStyle(true, 16, black.withOpacity(.4)),
                          ),
                          addSpaceWidth(5),
                          Icon(
                            Icons.navigate_next,
                            color: black.withOpacity(.5),
                          )
                        ],
                      ),
                      addSpace(10),
                      if (first10.isNotEmpty)
                        SingleChildScrollView(
                          padding: EdgeInsets.all(0),
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: List.generate(first10.length, (index) {
                              BaseModel model = first10[index];
                              bool isVideo =
                                  model.getInt(TYPE) == CHAT_TYPE_VIDEO;
                              String image = isVideo
                                  ? model.getString(THUMBNAIL_URL)
                                  : model.getString(IMAGE_URL);
                              return Hero(
                                tag: model.getObjectId(),
                                child: GestureDetector(
                                  onTap: () {
                                    pushAndResult(context,
                                        ViewChatMedia(chatMedia, index),
                                        depend: false);
                                  },
                                  child: Container(
                                    width: 100,
                                    height: 100,
                                    color: black.withOpacity(.09),
                                    margin: EdgeInsets.only(left: 10),
                                    child: Stack(
                                      fit: StackFit.expand,
                                      children: [
                                        CachedNetworkImage(
                                          imageUrl: image,
                                          fit: BoxFit.cover,
                                        ),
                                        if (isVideo)
                                          Align(
                                            alignment: Alignment.bottomRight,
                                            child: Container(
                                              margin: EdgeInsets.all(5),
                                              padding: EdgeInsets.fromLTRB(
                                                  5, 2, 5, 2),
                                              decoration: BoxDecoration(
                                                  color: black.withOpacity(.5),
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(25))),
                                              child: Text(
                                                model.getString(VIDEO_LENGTH),
                                                style:
                                                    textStyle(true, 10, white),
                                              ),
                                            ),
                                          )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            }),
                          ),
                        ),
                      if (first10.isNotEmpty) addSpace(10),
                    ],
                  ),
                ),
              ),
            addSpace(10),
            if (!groupChat)
              Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.all(10),
                  color: white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Mobile",
                        maxLines: 1,
                        style: textStyle(false, 14, AppConfig.appColor),
                      ),
                      Text(
                        phoneNumber,
                        style: textStyle(true, 20, black),
                      ),
                    ],
                  )),
            addSpace(10),
            addLine(0.09, black, 0, 0, 0, 0),
            if (widget.shared) ...[
              GestureDetector(
                onTap: () {
                  clickChat(context, widget.chatMode, theUser, replace: true);
                },
                child: Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(15),
                    color: white,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.chat,
                          color: AppConfig.appColor,
                        ),
                        addSpaceWidth(10),
                        Text(
                          "Message Contact",
                          style: textStyle(false, 20, black),
                        ),
                      ],
                    )),
              ),
              addLine(0.09, black, 0, 0, 0, 0),
            ],
            if (notInContact) ...[
              GestureDetector(
                onTap: () {
                  addToContact(theUser);
                },
                child: Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(15),
                    color: white,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.contact_phone,
                          color: AppConfig.appColor,
                        ),
                        addSpaceWidth(10),
                        Text(
                          "Add to Contact",
                          style: textStyle(false, 20, black),
                        ),
                      ],
                    )),
              ),
              addLine(0.09, black, 0, 0, 0, 0),
            ],
            if (!groupChat)
              GestureDetector(
                onTap: () {
                  pushAndResult(
                      context,
                      ShareTo(
                        userContact: theUser,
                      ),
                      depend: false);
                },
                child: Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.all(15),
                    color: white,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.share,
                          color: AppConfig.appColor,
                        ),
                        addSpaceWidth(10),
                        Text(
                          "Share Contact",
                          style: textStyle(false, 20, black),
                        ),
                      ],
                    )),
              ),
            GestureDetector(
              onTap: () {
                String name = theUser.getString(NAME);
                String title = "Block $name";
                String message =
                    "You and $name will no longer be able to have further conversations and view each others status update";

                if (blocked) {
                  title = "Unblock $name";
                  message =
                      "You and $name will now be able to have further conversations and view each others status update";
                }

                showMessage(context, Icons.block, red0, title, message,
                    clickYesText: blocked ? "UNBLOCK" : "BLOCK",
                    clickNoText: "Cancel", onClicked: (_) {
                  if (_ == true) {
                    if (blocked)
                      userModel
                        ..putInList(BLOCKED, theUser.getObjectId(), false)
                        ..updateItems();
                    else
                      performBlocking(theUser);
                    showProgress(true, context, msg: blocked?"Unblocking": "Blocking...");
                    Future.delayed(Duration(seconds: 2), () {
                      showProgress(false, context);
                      showMessage(
                          context,
                          Icons.block,
                          blue0,
                          blocked ? "Unblocked" : "Blocked!",
                          "This person has been ${blocked ? "Unblocked" : "blocked"}. Changes will apply when you restart your App",
                          delayInMilli: 500, onClicked: (_) {
                        Navigator.pop(context);
                      }, cancellable: false);
                    });
                  }
                });
              },
              child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.all(15),
                  color: white,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(
                        Icons.block,
                        color: red,
                      ),
                      addSpaceWidth(10),
                      Text(
                        "Block ${groupChat ? "Group" : "Contact"}",
                        style: textStyle(false, 20, black),
                      ),
                    ],
                  )),
            ),
            addLine(0.09, black, 0, 0, 0, 0),
          ],
        ),
        Container(
            color: black.withOpacity(.2),
            padding: EdgeInsets.only(top: 30, right: 10, left: 10),
            child: Row(
              children: [
                BackButton(
                  color: white,
                ),
                addSpaceWidth(10),
                Text(
                  "Profile",
                  style: textStyle(true, 22, white),
                ),
                Spacer(),
//                IconButton(
//                  icon: Icon(
//                    Icons.more_vert,
//                    color: white,
//                  ),
//                  onPressed: () {},
//                )
              ],
            )),
        if (!groupChat)
          Align(
            alignment: Alignment.centerRight,
            child: Container(
                margin: EdgeInsets.only(right: 10, bottom: 50),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(blurRadius: 5, color: black.withOpacity(.4))
                    ],
                    borderRadius: BorderRadius.circular(8),
                    color: AppConfig.appColor),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.call,
                        color: white,
                      ),
                      onPressed: () {
                        if (theUser.getString(CALLING_ID).isNotEmpty) {
                          showMessage(
                              context,
                              Icons.call_merge,
                              red,
                              "Call Ongoing",
                              "Oops! ${theUser.getString(NAME)} is currently on another call,Try again later");

                          return;
                        }
                        callEngine.placeCall(
                            callerModel: theUser,
                            isVideoCall: false,
                            onCallPlaced: (BaseModel rec) {
                              print(rec.items);
                              BaseModel model = rec;
                              model.saveItem(CALL_BASE, true,
                                  document: rec.getObjectId());
                              int p = callsList.indexWhere((e) =>
                                  e.getObjectId() == model.getObjectId());
                              if (p != -1) {
                                callsList[p] = model;
                              } else {
                                callsList.insert(0, model);
                                //callsList.add(model);
                              }
                              setState(() {});
                            });
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.videocam,
                        color: white,
                      ),
                      onPressed: () {
                        if (theUser.getString(CALLING_ID).isNotEmpty) {
                          showMessage(
                              context,
                              Icons.call_merge,
                              red,
                              "Call Ongoing",
                              "Oops! ${theUser.getString(NAME)} is currently on another call,Try again later");

                          return;
                        }
                        callEngine.placeCall(
                            callerModel: theUser,
                            isVideoCall: true,
                            onCallPlaced: (BaseModel rec) {
                              BaseModel model = rec;
                              model.saveItem(CALL_BASE, true,
                                  document: rec.getObjectId());
                              int p = callsList.indexWhere((e) =>
                                  e.getObjectId() == model.getObjectId());
                              if (p != -1) {
                                callsList[p] = model;
                              } else {
                                callsList.insert(0, model);
                                //callsList.add(model);
                              }
                              setState(() {});
                            });
                      },
                    ),
//                  IconButton(
//                    icon: Icon(
//                      Icons.search,
//                      color: white,
//                    ),
//                    onPressed: () {},
//                  ),
                    IconButton(
                      icon: Icon(
                        Icons.flag,
                        color: white,
                      ),
                      onPressed: () {
                        performFlagged(theUser);
                      },
                    )
                  ],
                )),
          ),
      ],
    );
  }

  functionButton(title, icon, onPressed, {bool small = false}) {
    return Flexible(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FlatButton(
            onPressed: onPressed,
            color: white,
            padding: EdgeInsets.all(15),
            shape: CircleBorder(),
            child: Icon(
              icon,
              size: 20,
            ),
          ),
          addSpace(5),
          Text(
            title,
            style: textStyle(false, 16, white),
          ),
        ],
      ),
    );
  }

  void addToContact(BaseModel theUser) {
    String phoneNumber = theUser.getString(PHONE_NUMBER);
    String name = theUser.getString(NAME);

    yesNoDialog(context, "Add Contact?",
        "Are you sure you want to add $name to your contact list?", () {
      showProgress(true, context, msg: "Adding to Contact");
      Contact contact = Contact(
        phones: [Item(label: name, value: phoneNumber)],
        displayName: name,
        givenName: name,
        familyName: name,
        middleName: name,
      );
      ContactsService.addContact(contact);

//       ContactsService.openContactForm().then((contact) {
//         if(contact==null)return;
//       });

      BaseModel model = BaseModel();
      model.put(OBJECT_ID, theUser.getObjectId());
      model.put(INITIALS, getInitials(name));
      model.put(NAME, name);
      model.put(PHONE_NUMBER, phoneNumber);
      model.put(QUERY, phoneNumber);
      int p = contacts
          .indexWhere((e) => e.getString(QUERY) == model.getString(QUERY));
      if (p == -1) {
        contacts.insert(0, model);
      }
      userModel
        ..putInList(CONTACTS, theUser.getObjectId(), true)
        ..updateItems();
      setState(() {});
      showProgress(false, context);
      showMessage(context, Icons.check, green, "Contact Added!",
          "$name has been added successfully to your Contact List",
          delayInMilli: 1200);
    });
  }

  void performFlagged(BaseModel theUser) {
    if (isAdmin) {
      ShowForAdmin(context, theUser, () {
        setState(() {});
      });
      return;
    }

    showListDialog(context, [
      "Report", /*"Block"*/
    ], (int p) {
      if (p == 0) {
        showListDialog(context, ["Spam", "Fake Profile", "Others Specify"],
            (int p) {
          if (p == 0) {
            createReport(context, theUser, REPORT_TYPE_PROFILE, "Spam");
          }
          if (p == 1) {
            createReport(context, theUser, REPORT_TYPE_PROFILE, "Fake Profile");
          }
          if (p == 2) {
            pushAndResult(
                context,
                inputDialog(
                  "Report",
                  hint: "Write a report",
                ), result: (_) {
              createReport(context, theUser, REPORT_TYPE_PROFILE, _.trim());
            });
          }
        }, title: "Report");
      }
      if (p == 1) {
        showMessage(
            context,
            Icons.block,
            red0,
            "Block ${theUser.getString(NAME)}",
            "This user won't be able to find your profile or connect with you",
            clickYesText: "BLOCK",
            clickNoText: "Cancel", onClicked: (_) {
          if (_ == true) {
            performBlocking(theUser);
            showProgress(true, context, msg: "Blocking...");
            Future.delayed(Duration(seconds: 2), () {
              showProgress(false, context);
              showMessage(context, Icons.block, blue0, "Blocked!",
                  "This person has been blocked. Changes will apply when you restart your App",
                  delayInMilli: 500, onClicked: (_) {
                Navigator.pop(context);
              }, cancellable: false);
            });
          }
        });
      }
    });
  }
}
