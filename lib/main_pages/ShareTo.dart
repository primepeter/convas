import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/ChatMain.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../notificationService.dart';

class ShareTo extends StatefulWidget {
  final List<BaseModel> messages;
  final BaseModel userContact;
  final int chatMode;
  final bool subFor;

  const ShareTo(
      {Key key,
      this.messages,
      this.userContact,
      this.chatMode,
      this.subFor = false})
      : super(key: key);
  @override
  _ShareToState createState() => _ShareToState();
}

class _ShareToState extends State<ShareTo> {
  TextEditingController searchController = TextEditingController();
  bool _showCancel = false;
  FocusNode focusSearch = FocusNode();
  List<BaseModel> contactsList = [];
  List<BaseModel> mainContactsList = [];
  List subs = [];
  bool groupMode = false;
  bool isMessage = false;

  reload() async {
    //contactsList.clear();
    String search = searchController.text.toString().toLowerCase().trim();
    if (search.isNotEmpty) contactsList.clear();
    for (BaseModel model in mainContactsList) {
      bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
      String userName =
          model.getString(isGroup ? GROUP_NAME : NAME).toLowerCase().trim();

      // String contactName = model.getString(NAME).toLowerCase().trim();
      String number = model.getString(PHONE_NUMBER).toLowerCase().trim();
      if (search.isNotEmpty) {
        if (!userName.contains(search)) {
          if (!number.contains(search)) continue;
        }
      }
      int p = contactsList
          .indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p != -1) {
        contactsList[p] = model;
      } else {
        contactsList.add(model);
      }
    }
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isMessage = widget.messages != null;
    loadSharing();
  }

  loadSharing() {
    final keys = otherPersonInfo.keys.toList();

    for (var key in keys) {
      BaseModel model = otherPersonInfo[key];
      if (model == null) continue;
      int p = contactsList.indexWhere((e) => e.getObjectId() == key);
      bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
      if (isGroup && widget.subFor) continue;
      if (p == -1) {
        contactsList.add(model);
      } else {
        contactsList[p] = model;
      }
    }
    mainContactsList.addAll(contactsList);
    setState(() {});
  }

  bool get contactReady => contactSetup && syncSetup;

  @override
  void dispose() {
    // TODO: implement dispose
    for (var sub in subs) sub.cancel();
    super.dispose();
  }

  clickBack() {
    if (groupMode) {
      groupMode = false;
      selectedContacts.clear();
      setState(() {});
      return;
    }
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        clickBack();
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(0, 40, 10, 10),
              color: white,
              child: Row(
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        clickBack();
                      },
                      child: Container(
                        width: 50,
                        height: 30,
                        child: Center(
                            child: Icon(
                          Icons.arrow_back_ios,
                          color: black,
                          size: 20,
                        )),
                      )),
                  Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                          widget.subFor
                              ? "Subscribe for"
                              : groupMode
                                  ? "${selectedContacts.length} Selected Chats"
                                  : "${isMessage ? "Forward" : "Share"} To",
                          style: textStyle(true, 20, black))),
                  addSpaceWidth(10),
                  if (!groupMode && !widget.subFor)
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          groupMode = true;
                        });
                      },
                      child: Container(
                        height: 30,
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(
                                color: black.withOpacity(0.2), width: 1)),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.group_add,
                              color: black.withOpacity(.5),
                              size: 17,
                            ),
                            addSpaceWidth(5),
                            Text(
                              "Select",
                              style: textStyle(true, 13, black),
                            )
                          ],
                        ),
                      ),
                    ),
                  if (groupMode && selectedContacts.isNotEmpty)
                    Container(
                      height: 35,
                      margin: EdgeInsets.only(right: 10),
                      width: 35,
                      child: new RaisedButton(
                          elevation: 1,
                          padding: EdgeInsets.all(0),
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          onPressed: () {
                            for (int i = 0; i < selectedContacts.length; i++) {
                              final user = selectedContacts[i];
                              bool isLast = i == selectedContacts.length - 1;
                              shareToContact(user, true, isLast);
                            }
                          },
                          shape: CircleBorder(),
                          color: AppConfig.appColor,
                          child: Center(
                              child: Icon(
                            Icons.check,
                            color: white,
                          ))),
                    ),
                  if (groupMode) ...[
                    addSpaceWidth(10),
                    GestureDetector(
                      onTap: () {
                        groupMode = false;
                        selectedContacts.clear();
                        setState(() {});
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        child: Icon(
                          Icons.close,
                          color: white,
                          size: 18,
                        ),
                        decoration:
                            BoxDecoration(color: red, shape: BoxShape.circle),
                      ),
                    )
                  ]
                ],
              ),
            ),
            Container(
              height: 45,
              margin: EdgeInsets.fromLTRB(20, contactReady ? 0 : 10, 20, 10),
              decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.circular(25),
                  border: Border.all(color: black.withOpacity(0.2), width: 1)),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  addSpaceWidth(10),
                  Icon(
                    Icons.search,
                    color: black.withOpacity(.5),
                    size: 17,
                  ),
                  addSpaceWidth(10),
                  new Flexible(
                    flex: 1,
                    child: new TextField(
                      textInputAction: TextInputAction.search,
                      textCapitalization: TextCapitalization.sentences,
                      autofocus: false,
                      onSubmitted: (_) {
                        //reload();
                      },
                      decoration: InputDecoration(
                          hintText: "Search name or phone",
                          hintStyle: textStyle(
                            false,
                            18,
                            black.withOpacity(.5),
                          ),
                          border: InputBorder.none,
                          isDense: true),
                      style: textStyle(false, 16, black),
                      controller: searchController,
                      cursorColor: black,
                      cursorWidth: 1,
                      focusNode: focusSearch,
                      keyboardType: TextInputType.text,
                      onChanged: (s) {
                        _showCancel = s.trim().isNotEmpty;
                        setState(() {});
                        reload();
                      },
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        focusSearch.unfocus();
                        _showCancel = false;
                        searchController.text = "";
                      });
                      reload();
                    },
                    child: _showCancel
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                            child: Icon(
                              Icons.close,
                              color: black,
                              size: 20,
                            ),
                          )
                        : new Container(),
                  )
                ],
              ),
            ),

//          addSpace(10),
            Expanded(
                flex: 1,
                child: Builder(builder: (ctx) {
//                if (!contactSetup) return loadingLayout();
                  if (contactsList.isEmpty)
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Image.asset(
                              "assets/icons/contact.png",
                              width: 50,
                              height: 50,
                              color: white,
                            ),
                            Text(
                              "No Active Contact Yet",
                              style: textStyle(true, 20, black),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    );

                  return Container(
                      child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (c, p) {
                      return contactItem(p);
                    },
                    shrinkWrap: true,
                    itemCount: contactsList.length,
                    padding: EdgeInsets.only(top: 0),
                  ));
                }))
          ],
        ),
      ),
    );
  }

  List selectedContacts = [];
  contactItem(int p) {
    BaseModel model = contactsList[p];
    String number = model.getString(PHONE_NUMBER);
    String image = model.userImage;
    bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
    String userName = model.getString(isGroup ? GROUP_NAME : NAME);

    return GestureDetector(
      onTap: () {
        if (!groupMode) return;
        if (selectedContacts.contains(model)) {
          selectedContacts.remove(model);
        } else {
          selectedContacts.add(model);
        }
        setState(() {});
      },
      child: Container(
        decoration: BoxDecoration(color: white, boxShadow: [
          //BoxShadow(color: black.withOpacity(.3), blurRadius: 5)
        ]),
        padding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        margin: EdgeInsets.fromLTRB(0, .5, 0, 0),
        child: Row(
          children: [
            if (groupMode) ...[
              checkBox(selectedContacts.contains(model)),
              addSpaceWidth(10),
            ],
            ClipRRect(
              borderRadius: BorderRadius.circular(35),
              child: Container(
                //margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                width: 65,
                height: 65,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppConfig.appColor,
                    gradient: LinearGradient(colors: [
//                      AppConfig.appColor,
//                      AppConfig.appColor.withOpacity(.8)
                      blue4,
                      blue2,
                    ])),
                child: image.isEmpty
                    ? Center(
                        child: Text(
                          getInitials(userName),
                          style: textStyle(true, 28, white),
                        ),
                      )
                    : CachedNetworkImage(
                        imageUrl: image,
                        fit: BoxFit.cover,
                      ),
              ),
            ),
            addSpaceWidth(10),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    userName,
                    style: textStyle(true, 18, black),
                  ),
                  if (!isGroup)
                    Text(
                      number,
                      style: textStyle(false, 14, black),
                    )
                ],
              ),
            ),
            if (!groupMode)
              FlatButton(
                onPressed: () {
                  if (widget.subFor) {
                    Navigator.pop(context, model);
                    return;
                  }
                  shareToContact(model, false, false);
                },
                padding: EdgeInsets.all(0),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                color: blue6,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.send,
                      color: white,
                      size: 14,
                    ),
                    addSpaceWidth(5),
                    Text(
                      widget.subFor ? "Sub" : "Send",
                      style: textStyle(true, 14, white),
                    ),
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }

  void shareToContact(BaseModel theUser, bool broadcast, bool last) {
    bool isGroup = theUser.getString(GROUP_NAME).isNotEmpty;
    String chatId = createChatId(theUser.getObjectId());
    bool sDate = canShowDate(chatId, 0);
    final String id = getRandomId();
    BaseModel chat = BaseModel();

    if (isMessage) {
      chat = widget.messages[0];
      chat.put(IS_FORWARDED, true);
    } else {
      final mappedContact = mapContactFromModel(widget.userContact);
      chat.put(
          PARTIES,
          isGroup
              ? theUser.getList(PARTIES)
              : [userModel.getObjectId(), theUser.getObjectId()]);
      chat.put(TYPE, CHAT_TYPE_CONTACT);
      chat.put(CONTACT, mappedContact);
    }
    chat.put(SHOW_DATE, sDate);
    chat.put(OBJECT_ID, id);
    chat.put(CHAT_ID, chatId);
    chat.saveItem(CHAT_BASE, true, document: id);

    if (isMessage) {
      Map data = Map();
      data[TYPE] = PUSH_TYPE_CHAT;
      data[OBJECT_ID] = chatId;
      data[TITLE] = getFullName(userModel);
      data[MESSAGE] = chat.getString(MESSAGE);
      NotificationService.sendPush(
          topic: isGroup ? chatId : null,
          token: isGroup ? null : theUser.getString(TOKEN),
          title: getFullName(userModel),
          body: chat.getString(MESSAGE),
          tag: '${userModel.getObjectId()}chat',
          data: data);
    } else {
      final mappedContact = mapContactFromModel(widget.userContact);
      NotificationService.sendPush(
          topic: isGroup ? chatId : null,
          token: isGroup ? null : theUser.getString(TOKEN),
          title: getFullName(userModel),
          body: "Shared Contact",
          tag: '${userModel.getObjectId()}chat',
          data: mappedContact);
    }

    if (!broadcast)
      pushReplaceAndResult(context, ChatMain(chatId, 0, otherPerson: theUser));

    if (last) {
      showMessage(
          context,
          Icons.check,
          green,
          "${isMessage ? "Message Forwarded" : "Contact Shared"}",
          "${isMessage ? "Message has been Forwarded" : "Contact has been Shared"} Successfully",
          delayInMilli: 500, onClicked: (_) {
        Navigator.pop(context);
      });
    }
  }

  mapContactFromModel(BaseModel theUser) {
    final model = BaseModel();
    String number = theUser.getString(PHONE_NUMBER);
    String image = theUser.userImage;
    bool isGroup = theUser.getString(GROUP_NAME).isNotEmpty;
    String userName = theUser.getString(isGroup ? GROUP_NAME : NAME);

    model.put(PHONE_NUMBER, number);
    model.put(USER_IMAGE, image);
    model.put(NAME, userName);
    model.put(USER_ID, theUser.getUserId());
    model.put(OBJECT_ID, theUser.getObjectId());

    return model.items;
  }
}
