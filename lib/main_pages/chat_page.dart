import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/enter_pin.dart';
import 'package:Strokes/main_pages/chat_page_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ShowWallet.dart';

PageController chatPageController = new PageController();
List<BaseModel> notifyList = List();
bool isRefreshing = false;

class MessagesPage extends StatefulWidget {
  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage>
    with AutomaticKeepAliveClientMixin {
  //bool setup = false;

  //git remote add strock https://mtellect@bitbucket.org/primepeter/strock-app.git

  List<StreamSubscription> subs = [];
  int chatMode;
  List messageList;
  List messageDot;
  Map messsageCounter;
  bool modeEdit = false;

  @override
  void initState() {
    super.initState();
    messageDot = chatMode == CHAT_MODE_ENCRYPT
        ? showNewMessageDotEncrypt
        : showNewMessageDot;

    vp.addListener(() {
      messsageCounter = vp.page.toInt() == CHAT_MODE_ENCRYPT
          ? unreadCounterEncrypt
          : unreadCounter;
      setState(() {});
    });
    var sub1 = chatMessageController.stream.listen((_) {
      messsageCounter = vp.page.toInt() == CHAT_MODE_ENCRYPT
          ? unreadCounterEncrypt
          : unreadCounter;

      if (mounted) setState(() {});
    });
    subs.add(sub1);
  }

  @override
  void dispose() {
    for (var s in subs) s.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          page(),
        ],
      ),
    );
  }

  int currentPage = 0;
  final vp = PageController();

  page() {
    //print(lastMessagesEncrypt[0].items);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(4, 4, 0, 4),
          color: white,
          child: Row(
            children: List.generate(2, (p) {
              bool active = currentPage == p;
              String title;
              if (p == 0) title = "Regular";
              if (p == 1) title = "Encrypted";
//              if (p == 2) title = "Group Chat";
              int unread = p == 0
                  ? (null == messsageCounter) ? 0 : messsageCounter.length
                  : /*unreadCounterEncrypt.length*/ 0;

              return Flexible(
                fit: FlexFit.tight,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    IgnorePointer(
                      child: Container(
                          width: double.infinity,
                          //height: 70,
                          color: transparent,
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                title,
                                style: textStyle(
                                    active,
                                    active ? 21 : 18,
                                    p == CHAT_MODE_ENCRYPT &&
                                            unreadCounterEncrypt.length > 1
                                        ? red0
                                        : active
                                            ? AppConfig.appColor
                                            : black.withOpacity(0.5)),
                              ),
//                              if (/*active && */ unread > 0)
//                                Container(
//                                  margin: EdgeInsets.only(top: 2),
//                                  child: Row(
//                                    mainAxisSize: MainAxisSize.min,
//                                    children: [
//                                      Container(
//                                        height: 5,
//                                        width: 5,
//                                        decoration: BoxDecoration(
//                                            color: AppConfig.appColor,
//                                            shape: BoxShape.circle),
//                                      ),
//                                      addSpaceWidth(3),
//                                      Container(
//                                        //margin: EdgeInsets.only(left: 3, right: 3),
//                                        padding: EdgeInsets.only(
//                                            left: 7,
//                                            right: 7,
//                                            top: 3,
//                                            bottom: 3),
//                                        child: Text(
//                                          "$unread",
//                                          style: textStyle(true, 12, white),
//                                        ),
//                                        decoration: BoxDecoration(
//                                            color: AppConfig.appColor,
//                                            shape: BoxShape.circle),
//                                      ),
//                                    ],
//                                  ),
//                                ),
                            ],
                          )),
                    ),
                    InkWell(
                        onTap: () {
                          vp.jumpToPage(p);
                          print("okk");
                        },
                        onDoubleTap: () {
                          if (p == 1) {
                            if (!userModel.isPremium) {
                              showMessage(
                                  context,
                                  Icons.error,
                                  red0,
                                  "Opps Sorry!",
                                  "You cannot view Encrypted messages on ConvasApp until you become a Premium User",
                                  clickYesText: "Go Premium", onClicked: (_) {
                                if (_)
                                  pushAndResult(context, ShowWallet(),
                                      depend: false);
                              });
                              return;
                            }

                            if (showEncrypt) {
                              setState(() {
                                showEncrypt = !showEncrypt;
                              });
                              return;
                            }
                            pushAndResult(context, EnterPin(), depend: false,
                                result: (_) {
                              if (null == _) return;

                              if (_ == userModel.getString(CODE_PIN)) {
                                setState(() {
                                  showEncrypt = !showEncrypt;
                                });
                                return;
                              }

                              if (_ == userModel.getString(FAILED_CODE_PIN)) {
                                wipeEncryptedChat();
                                return;
                              }
                            });
                          }
                        },
                        child: Container(
                          color: black.withOpacity(.01),
                          height: 60,
                        ))
                  ],
                ),
              );
            }),
          ),
        ),
        addSpace(10),
        Container(
          padding: EdgeInsets.fromLTRB(0, 0.2, 0, 0.2),
          decoration: BoxDecoration(color: white, boxShadow: [
            BoxShadow(blurRadius: 4, color: black.withOpacity(.4))
          ]),
        ),
        AnimatedContainer(
          duration: Duration(milliseconds: 500),
          height: isRefreshing ? 30 : 0,
          color: red,
          alignment: Alignment.center,
          child: Text(
            "Refreshing...",
            style: textStyle(true, 16, white),
          ),
        ),
        Expanded(
            flex: 1,
            child: PageView(
                controller: vp,
                onPageChanged: (p) {
                  setState(() {
                    currentPage = p;
                  });
                },
                children: [
                  ChatPageItem(0),
                  ChatPageItem(1),
                ]))
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;

  void wipeEncryptedChat() {
    print("Failed code chat wipe..");
    isRefreshing = true;
    chatSetupEncrypt = false;
    setState(() {});

    for (var msg in lastMessagesEncrypt) {
      BaseModel model = msg;
      String chatId = model.getString(CHAT_ID);
      Firestore.instance
          .collection(CHAT_BASE_ENCRYPT)
          .where(CHAT_ID, isEqualTo: chatId)
          .getDocuments()
          .then((value) {
        for (var doc in value.documents) {
          if (doc.exists) doc.reference.delete();
        }
      });

      Firestore.instance
          .collection(CHAT_IDS_BASE_ENCRYPT)
          .where(CHAT_ID, isEqualTo: chatId)
          .getDocuments()
          .then((value) {
        for (var doc in value.documents) {
          if (doc.exists) doc.reference.delete();
        }
      });
      setState(() {});
    }
    lastMessagesEncrypt.clear();
    chatSetupEncrypt = true;
    isRefreshing = false;
    setState(() {});
  }
}
