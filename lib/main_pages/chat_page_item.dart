import 'dart:async';
import 'dart:convert';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/ChatMain.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/listDialog.dart';
import 'package:Strokes/enter_pin.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ShowWallet.dart';
import 'chat_page.dart';

class ChatPageItem extends StatefulWidget {
  final int chatMode;
  ChatPageItem(this.chatMode);
  @override
  _ChatPageItemState createState() => _ChatPageItemState();
}

class _ChatPageItemState extends State<ChatPageItem> {
  List<StreamSubscription> subs = [];
  int chatMode;
  List messageList;
  List messageDot;
  Map messsageCounter;
  bool modeEdit = false;
  bool delayed = false;

  @override
  void initState() {
    super.initState();
    chatMode = widget.chatMode;
    messageDot = chatMode == CHAT_MODE_ENCRYPT
        ? showNewMessageDotEncrypt
        : showNewMessageDot;
    messsageCounter =
        chatMode == CHAT_MODE_ENCRYPT ? unreadCounterEncrypt : unreadCounter;
    messageList =
        chatMode == CHAT_MODE_ENCRYPT ? lastMessagesEncrypt : lastMessages;
    delayed = true;
    var sub1 = chatMessageController.stream.listen((_) {
      if (mounted) setState(() {});
    });
    var sub2 = editChatController.stream.listen((e) {
      modeEdit = e;
      if (e) modeChatDelete = true;
      if (mounted) setState(() {});
    });
    subs.add(sub1);
    subs.add(sub2);
  }

  @override
  void dispose() {
    for (var s in subs) s.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return page();
  }

  page() {
    return Builder(builder: (ctx) {
      bool encrypted = chatMode == CHAT_MODE_ENCRYPT;

      print(messageList.isEmpty);

      if (!chatSetupRegular && !encrypted) return loadingLayout();
      if (!chatSetupEncrypt && encrypted) return loadingLayout();
      if (!encrypted && messageList.isEmpty ||
          (encrypted && !showEncrypt) ||
          (encrypted && showEncrypt && messageList.isEmpty))
        return Center(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                if (!encrypted)
                  Image.asset(
                    "assets/icons/chat_empty.png",
                    height: 200,
                    fit: BoxFit.cover,
                    //color: black.withOpacity(.3),
                  ),
                if (encrypted)
                  GestureDetector(
                    onDoubleTap: () {
                      if (!userModel.isPremium) {
                        showMessage(context, Icons.error, red0, "Opps Sorry!",
                            "You cannot view Encrypted messages on ConvasApp until you become a Premium User",
                            clickYesText: "Go Premium", onClicked: (_) {
                          if (_)
                            pushAndResult(context, ShowWallet(), depend: false);
                        });
                        return;
                      }

                      pushAndResult(context, EnterPin(), depend: false,
                          result: (_) {
                        if (null == _) return;
                        if (_ == userModel.getString(CODE_PIN)) {
                          setState(() {
                            showEncrypt = !showEncrypt;
                          });
                          return;
                        }
                        if (_ == userModel.getString(FAILED_CODE_PIN)) {
                          wipeEncryptedChat();
                          return;
                        }
                      });
                    },
                    child: Image.asset(
                      "assets/icons/lock.png",
                      height: 50,
                      color: black.withOpacity(.3),
                    ),
                  ),
                /*if (chatMode == CHAT_MODE_GROUP)
                  Image.asset(
                    "assets/icons/chat.png",
                    height: 40,
                    color: black.withOpacity(.3),
                  ),*/
                addSpace(10),
                Text(
                  encrypted
                      ? "No Encrypted Chat Yet"
                      : "Tap the + button to get started.\nMake your Inbox Safe and Casual.",
                  style: textStyle(true, 20, black.withOpacity(.3)),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );

      return Container(
          child: ListView.separated(
        separatorBuilder: (c, p) {
          if (p == messageList.length - 1) return Container();
          return addLine(0.4, black.withOpacity(.09), 100, 0, 0, 0);
        },
        itemBuilder: (c, p) {
          BaseModel model = messageList[p];

          String chatId = model.getString(CHAT_ID);
          String otherPersonId = getOtherPersonId(model);
          print(otherPersonId);
          BaseModel otherPerson =
              otherPersonInfo[chatId] ?? otherPersonInfo[otherPersonId];
          if (otherPerson == null) {
            //messageList.remove(model);
            //chatMessageController.add(true);
            //setState(() {});
            return Container();
          }
          bool isGroup = otherPerson.getString(GROUP_NAME).isNotEmpty;

          String name = isGroup
              ? (otherPerson.getString(GROUP_NAME))
              : getFullName(otherPerson);
          String image = isGroup
              ? (otherPerson.getString(GROUP_PHOTO))
              : otherPerson.userImage;

          return chatItem(
              image, name, model, p == messageList.length - 1, otherPerson);
        },
        shrinkWrap: true,
        itemCount: messageList.length,
        padding: EdgeInsets.only(top: 10),
      ));
    });
  }

  chatItem(String image, String name, BaseModel chatModel, bool last,
      BaseModel otherPerson) {
    int type = chatModel.getInt(TYPE);
    String chatId = chatModel.getString(CHAT_ID);
    bool myItem = chatModel.myItem();

    String hisId = chatId.replaceAll(userModel.getObjectId(), "");
    bool read = chatModel.getList(READ_BY).contains(hisId);
    bool myRead = chatModel.getList(READ_BY).contains(userModel.getObjectId());
    List mutedList = userModel
        .getList(chatMode == CHAT_MODE_ENCRYPT ? MUTED_ENCRYPTED : MUTED);
    List lockedChats = userModel.getList(LOCKED);
    int unread = messsageCounter[chatId] ?? 0;
    bool hasUnread = !myItem && !myRead && unread > 0;
    bool isGroup = otherPerson.getString(GROUP_NAME).isNotEmpty;
    return new InkWell(
      onLongPress: () {
        var options = [
          mutedList.contains(chatId) ? "Unmute Chat" : "Mute Chat",
          "Delete Chat"
        ];
        if (chatMode != 1 && !isGroup) {
          //options.add("Start Encrypted Chat");
          options
              .add(lockedChats.contains(chatId) ? "Unlock Chat" : "Lock Chat");
        }
        pushAndResult(context, listDialog(options), result: (_) {
          if (_ == "Unlock Chat" || _ == "Lock Chat") {
            if (!userModel.isPremium) {
              showMessage(context, Icons.error, red0, "Opps Sorry!",
                  "You cannot lock messages on ConvasApp until you become a Premium User",
                  clickYesText: "Go Premium", onClicked: (_) {
                if (_) pushAndResult(context, ShowWallet(), depend: false);
              });
              return;
            }
            pushAndResult(context, EnterPin(), depend: false, result: (_) {
              if (null == _) return;

              if (_ == userModel.getString(CODE_PIN)) {
                if (lockedChats.contains(chatId)) {
                  lockedChats.remove(chatId);
                } else {
                  lockedChats.add(chatId);
                }
                userModel.put(LOCKED, lockedChats);
                userModel.updateItems();
                setState(() {});
              }
            });
          }
          if (_ == "Mute Chat" || _ == "Unmute Chat") {
            if (mutedList.contains(chatId)) {
              mutedList.remove(chatId);
            } else {
              mutedList.add(chatId);
            }
            userModel.put(
                chatMode == CHAT_MODE_ENCRYPT ? MUTED_ENCRYPTED : MUTED,
                mutedList);
            userModel.updateItems();
            setState(() {});
          }
          if (_ == "Delete Chat") {
            yesNoDialog(context, "Delete Chat?",
                "Are you sure you want to delete this chat?", () {
//              Firestore.instance
//                  .collection(CHAT_IDS_BASE)
//                  .getDocuments()
//                  .then((value) {
//                for (var doc in value.documents) {
//                  BaseModel model = BaseModel(doc: doc);
//                  bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
//                  if (!isGroup) continue;
//                  model.deleteItem();
//                }
//              });
//
//              Firestore.instance
//                  .collection(CHAT_IDS_BASE_ENCRYPT)
//                  .getDocuments()
//                  .then((value) {
//                for (var doc in value.documents) {
//                  BaseModel model = BaseModel(doc: doc);
//                  bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
//                  if (!isGroup) continue;
//                  model.deleteItem();
//                }
//              });
//
//              Firestore.instance
//                  .collection(CHAT_BASE)
//                  .getDocuments()
//                  .then((value) {
//                for (var doc in value.documents) {
//                  BaseModel model = BaseModel(doc: doc);
//                  bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
//                  if (!isGroup) continue;
//                  model.deleteItem();
//                }
//              });
//
//              Firestore.instance
//                  .collection(CHAT_BASE_ENCRYPT)
//                  .getDocuments()
//                  .then((value) {
//                for (var doc in value.documents) {
//                  BaseModel model = BaseModel(doc: doc);
//                  bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
//                  if (!isGroup) continue;
//                  model.deleteItem();
//                }
//              });

              print("done......");
              //setState(() {});
              deleteChat(chatId, chatMode, otherPerson);
            });
          }
          if (_ == "Start Encrypted Chat") {
            if (!userModel.isPremium) {
              showMessage(context, Icons.error, red0, "Opps Sorry!",
                  "You cannot lock start Encrypted Chats on ConvasApp until you become a Premium User",
                  clickYesText: "Go Premium", onClicked: (_) {
                if (_) pushAndResult(context, ShowWallet(), depend: false);
              });
              return;
            }

            clickChat(context, CHAT_MODE_ENCRYPT, otherPerson);
          }
        }, depend: false);
      },
      onTap: () {
        if (modeEdit) {
          if (selectedChats.contains(chatModel)) {
            selectedChats.remove(chatModel);
          } else {
            selectedChats.add(chatModel);
          }
          modeEdit = selectedChats.isNotEmpty;
          if (!modeEdit) modeChatDelete = false;
          homeStateController.add(true);
          setState(() {});
          return;
        }

        if (lockedChats.contains(chatId)) {
          pushAndResult(context, EnterPin(), depend: false, result: (_) {
            if (null == _) return;

            if (_ == userModel.getString(CODE_PIN)) {
              startChat(chatModel, otherPerson);
            }
          });
          return;
        }
        startChat(chatModel, otherPerson);
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
            color: hasUnread ? chat_back : transparent,
            boxShadow: [
              //BoxShadow(color: black.withOpacity(.3), blurRadius: 5)
            ]),
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.fromLTRB(0, 1.5, 10, 0),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            if (modeEdit) ...[
              checkBox(selectedChats.contains(chatModel)),
              addSpaceWidth(10)
            ],
            GestureDetector(
              onTap: () {
//                pushAndResult(
//                    context,
//                    ShowProfile(otherPerson, chatList),
//                    opaque: false,
//                    depend: false
//                );
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(35),
                child: Container(
                  //margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                  width: 65,
                  height: 65,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppConfig.appColor,
                      gradient: LinearGradient(colors: [
//                      AppConfig.appColor,
//                      AppConfig.appColor.withOpacity(.8)
                        blue4,
                        blue2,
                      ])),
                  child: (image.isEmpty)
                      ? Center(
                          child: Text(
                            getInitials(name),
                            style: textStyle(true, 28, white),
                          ),
                        )
                      : CachedNetworkImage(
                          imageUrl: image,
                          fit: BoxFit.cover,
                        ),
                ),
              ),
            ),
            addSpaceWidth(10),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: new Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Text(
                          name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: textStyle(true, 20, black),
                        ),
                      ),
                    ],
                  ),
                  //addSpace(2),
                  if (type == CHAT_TYPE_TEXT &&
                      chatModel.getString(MESSAGE).isEmpty)
                    Container()
                  else
                    Row(
                      children: <Widget>[
                        Flexible(
                          fit: FlexFit.tight,
                          child: Row(
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  margin: EdgeInsets.only(right: 8),
                                  decoration: BoxDecoration(
                                      // color: blue09,
                                      // borderRadius: BorderRadius.circular(5),
                                      // border: Border.all(
                                      //     color: black.withOpacity(.1),
                                      //     width: 1  )
                                      ),
                                  child: new Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(0, 2, 6, 2),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        if (isGroup)
                                          Text("${chatModel.getString(NAME)}: ",
                                              style: textStyle(false, 15,
                                                  black.withOpacity(.8))),
                                        if (myItem && read && !isGroup)
                                          Container(
                                              margin: EdgeInsets.only(right: 5),
                                              child: Icon(
                                                Icons.remove_red_eye,
                                                size: 12,
                                                color: blue0,
                                              )),
                                        if (chatModel.getBoolean(LOCKED) &&
                                            !chatRemoved(chatModel))
                                          Container(
                                              child: Icon(
                                            Icons.lock,
                                            size: 12,
                                            color: blue0,
                                          )),
                                        if (type != CHAT_TYPE_TEXT &&
                                            !chatModel.getBoolean(LOCKED))
                                          Icon(
                                            type == CHAT_TYPE_TEXT
                                                ? Icons.message
                                                : type == CHAT_TYPE_IMAGE
                                                    ? Icons.camera_alt
                                                    : type == CHAT_TYPE_VIDEO
                                                        ? Icons.videocam
                                                        : type == CHAT_TYPE_REC
                                                            ? Icons.mic
                                                            : type ==
                                                                    CHAT_TYPE_CONTACT
                                                                ? Icons.person
                                                                : Icons
                                                                    .library_books,
                                            color: black.withOpacity(.8),
                                            size: 12,
                                          ),
                                        addSpaceWidth(5),
                                        Flexible(
                                          flex: 1,
                                          child: Text(
                                            chatRemoved(chatModel)
                                                ? "This message has been removed"
                                                : chatModel.getBoolean(LOCKED)
                                                    ? "Message Locked"
                                                    : type == CHAT_TYPE_TEXT
                                                        ? chatModel
                                                            .getString(MESSAGE)
                                                        : type ==
                                                                CHAT_TYPE_IMAGE
                                                            ? "Photo"
                                                            : type ==
                                                                    CHAT_TYPE_VIDEO
                                                                ? "Video"
                                                                : type ==
                                                                        CHAT_TYPE_REC
                                                                    ? "Voice Note (${chatModel.getString(AUDIO_LENGTH)})"
                                                                    : type ==
                                                                            CHAT_TYPE_CONTACT
                                                                        ? "New Contact"
                                                                        : "Document",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: textStyle(false, 15,
                                                black.withOpacity(.8)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                ],
              ),
            ),
            Column(
              children: [
                Text(
                  getChatTime(chatModel.getTime()),
                  style: textStyle(false, 12, black.withOpacity(.8)),
                  textAlign: TextAlign.end,
                ),
                addSpace(5),
                // if (hasUnread)
                Row(
                  children: [
                    if (lockedChats.contains(chatId))
                      Icon(
                        Icons.lock,
                        size: 16,
                        color: red0.withOpacity(.5),
                      ),
                    if (mutedList.contains(chatId))
                      Image.asset(
                        ic_mute,
                        width: 18,
                        height: 18,
                        color: black.withOpacity(.5),
                      ),
                    if (hasUnread)
                      Container(
                        width: 25,
                        height: 25,
                        decoration: BoxDecoration(
                            color: AppConfig.appColor,
//                            borderRadius: BorderRadius.circular(5),
                            shape: BoxShape.circle,
                            border: Border.all(color: white, width: 2)),
                        child: Center(
                            child: Text(
                          "${unread > 9 ? "9+" : unread}",
                          style: textStyle(true, 12, white),
                        )),
                      ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  startChat(BaseModel chatModel, otherPerson) {
    String chatId = chatModel.getString(CHAT_ID);
    messsageCounter.remove(chatId);
    messageDot.removeWhere((id) => id == chatId);
    setState(() {});
    pushAndResult(
        context,
        ChatMain(
          chatId,
          chatMode,
          otherPerson: otherPerson,
        ), result: (_) {
      setState(() {});
    });
  }

  deleteChat(String chatId, int chatMode, BaseModel otherPerson) async {
    bool isGroup = otherPerson.getString(GROUP_NAME).isNotEmpty;
    if (isGroup) otherPersonInfo.remove(chatId);
    messageList.removeWhere((bm) => bm.getString(CHAT_ID) == chatId);
    stopListening.add(chatId);

    List deletedChats = userModel.getList(DELETED_CHAT);
    int p = deletedChats.indexWhere((m) => m[CHAT_ID] == chatId);
    Map deleteItem = Map();
    deleteItem[CHAT_ID] = getRealChatId(chatId, chatMode);
    deleteItem[TIME] = DateTime.now().millisecondsSinceEpoch;
    if (p == -1) {
      deletedChats.add(deleteItem);
    } else {
      deletedChats[p] = deleteItem;
    }
    userModel.put(DELETED_CHAT, deletedChats);
    userModel.updateItems();
    if (mounted) setState(() {});

    SharedPreferences pref = await SharedPreferences.getInstance();
    List<String> deletedList = pref.getStringList(DELETED_CHAT) ?? [];
    String realId = getRealChatId(chatId, chatMode);
    int index = deletedList.indexWhere((b) => b.contains(realId));
    String chatString = "$realId$SPACE${DateTime.now().millisecondsSinceEpoch}";
    if (index != -1) {
      deletedList[index] = chatString;
    } else {
      deletedList.add(chatString);
    }
    pref.setStringList(DELETED_CHAT, deletedList);

    final mainMessageList = [];
    String key =
        chatMode == CHAT_MODE_ENCRYPT ? CHAT_IDS_BASE_ENCRYPT : CHAT_IDS_BASE;
    for (var bm in messageList) {
      final model = bm;
      model..remove(UPDATED_AT)..remove(CREATED_AT);
      List deletedIds = userModel.getList(DELETED_CHAT);
      if (deletedIds.contains(model.getObjectId())) continue;
      mainMessageList.add(model.items);
    }

    final encodedChat = jsonEncode(mainMessageList);
    pref.remove(key);
    pref.setString(key, encodedChat);

    if (chatMode == CHAT_MODE_ENCRYPT) lastMessagesEncrypt = messageList;
    if (chatMode != CHAT_MODE_ENCRYPT) lastMessages = messageList;
//    cacheChat(
//      chatMode,
//    );

    if (isGroup) {
      int partyCount = otherPerson.getList(PARTIES).length;
      if (partyCount == 1) {
        otherPerson.deleteItem();
      } else {
        otherPerson
          ..putInList(PARTIES, userModel.getUserId(), false)
          ..updateItems();
      }
      //push a message to the group saying user has left the group
      String userName = userModel.getString(NAME);
      //postGeneralMessage("$userName has left", otherPerson,chatMode);
      //return;
    }
  }

  void wipeEncryptedChat() {
    print("Failed code chat wipe..");
    isRefreshing = true;
    chatSetupEncrypt = false;
    setState(() {});

    for (var msg in lastMessagesEncrypt) {
      BaseModel model = msg;
      String chatId = model.getString(CHAT_ID);
      Firestore.instance
          .collection(CHAT_BASE_ENCRYPT)
          .where(CHAT_ID, isEqualTo: chatId)
          .getDocuments()
          .then((value) {
        for (var doc in value.documents) {
          if (doc.exists) doc.reference.delete();
        }
      });

      Firestore.instance
          .collection(CHAT_IDS_BASE_ENCRYPT)
          .where(CHAT_ID, isEqualTo: chatId)
          .getDocuments()
          .then((value) {
        for (var doc in value.documents) {
          if (doc.exists) doc.reference.delete();
        }
      });
      setState(() {});
    }
    lastMessagesEncrypt.clear();
    chatSetupEncrypt = true;
    isRefreshing = false;
    setState(() {});
  }
}
