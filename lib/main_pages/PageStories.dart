import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/ShowStatus.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dashed_circle/dashed_circle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:timeago/timeago.dart' as timeago;

PageController chatPageController = new PageController();
List<BaseModel> notifyList = List();

class PageStories extends StatefulWidget {
  @override
  _PageStoriesState createState() => _PageStoriesState();
}

var subs = [];

class _PageStoriesState extends State<PageStories>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  List<List<BaseModel>> storyList = [];
  bool setup = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setup = false;
    refreshStories();
    var refreshSub = homeRefreshController.stream.listen((b) {
      refreshStories();
    });
    subs.add(refreshSub);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    for (var sub in subs) sub.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: chat_back,
      body: page(),
    );
  }

  bool hasUnreadStory() {
    bool hasUnread = false;
    for (List<BaseModel> list in storyList) {
      for (BaseModel bm in list) {
        if (bm.myItem()) continue;
        if (!bm.getList(SHOWN).contains(userModel.getObjectId())) {
          hasUnread = true;
          break;
        }
      }
      if (hasUnread) break;
    }
    return hasUnread;
  }

  refreshStories() {
    for (BaseModel model in allStoryList) {
      int listIndex = storyList.indexWhere((List<BaseModel> models) =>
          models[0].getString(USER_ID) == model.getString(USER_ID));

      //Fetch the story list...
      List<BaseModel> stories = listIndex == -1 ? [] : storyList[listIndex];
      int storyIndex =
          stories.indexWhere((bm) => bm.getObjectId() == model.getObjectId());
      if (storyIndex == -1) {
        stories.add(model);
      }
      stories.sort((bm1, bm2) => bm1.getTime().compareTo(bm2.getTime()));

      if (listIndex == -1) {
        storyList.add(stories);
      } else {
        storyList[listIndex] = stories;
      }
    }

    //Split the list
    List<BaseModel> myList = [];
    List<List<BaseModel>> unreadList = [];
    List<List<BaseModel>> readList = [];
    for (List list in storyList) {
      bool myItem = list.isEmpty
          ? false
          : (list[0].getUserId() == userModel.getObjectId());
      if (myItem) {
        myList = list;
        continue;
      }
      int unread = list.indexWhere(
          (bm) => !bm.getList(SHOWN).contains(userModel.getObjectId()));
      if (unread != -1) {
        unreadList.add(list);
      } else {
        readList.add(list);
      }
    }
    unreadList.sort((list1, list2) => list2[list2.length - 1]
        .getTime()
        .compareTo(list1[list1.length - 1].getTime()));
    readList.sort((list1, list2) => list2[list2.length - 1]
        .getTime()
        .compareTo(list1[list1.length - 1].getTime()));
    //sort the stories
    //storyList.sort((listA, listB)=>listA);
    storyList.clear();
    if (myList.isNotEmpty) storyList.add(myList);
    storyList.addAll(unreadList);
    storyList.addAll(readList);
    setup = true;
    if (mounted) setState(() {});
  }

  RefreshController refreshController = RefreshController();

  page() {
    if (!setup) return loadingLayout();

    if (allStoryList.isEmpty)
      return Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/icons/chat_bg.png"),
                    fit: BoxFit.cover)),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image.asset("assets/icons/stories.png",
                      width: 50, height: 50, color: black.withOpacity(.3)),
                  Text(
                    "No Stories Yet",
                    style: textStyle(true, 20, black.withOpacity(.3)),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ],
      );

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: 0.7,
      ),
      itemBuilder: (c, p) {
        int likedCount = 0;
        List<BaseModel> list = storyList[p];
        BaseModel model = list[list.length - 1];
        bool myItem = model.getUserId() == userModel.getObjectId();
        String image = model.getString(STORY_IMAGE);
        String text = model.getString(STORY_TEXT);
        String mColor = model.getString(COLOR_KEY);
        final dateT = DateTime.fromMillisecondsSinceEpoch(model.getTime());
        String timeAgo = timeago.format(dateT);
        String name = model.getString(NAME);
        bool readAll = true;
        for (BaseModel bm in list) {
          likedCount = likedCount + bm.getList(LIKED).length;
          if (bm.myItem()) continue;
          if (!bm.getList(SHOWN).contains(userModel.getObjectId())) {
            readAll = false;
            break;
          }
        }

        double screenWidth = MediaQuery.of(context).size.width;
        return Container(
          width: screenWidth / 4,
          height: screenWidth / 4,
          margin: EdgeInsets.all(10),
          child: Stack(
            children: [
              Opacity(
                opacity: model.myItem() || !readAll ? 1 : (.4),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () {
                        int startAt = list.lastIndexWhere((bm) => bm
                            .getList(SHOWN)
                            .contains(userModel.getObjectId()));
                        startAt = startAt + 1;
                        startAt = startAt > list.length - 1 ? 0 : startAt;
                        pushAndResult(context, ShowStatus(list, startAt),
                            result: (_) {
                          if (_ is String && _.isNotEmpty) {
                            if (list.length == 1) {
                              storyList.removeAt(p);
                            } else {
                              list.removeWhere((bm) => _ == bm.getObjectId());
                            }
                            allStoryList
                                .removeWhere((bm) => _ == bm.getObjectId());
                          }
                          setState(() {});
                        }, depend: false);
                      },
                      child: Container(
                        width: screenWidth / 4,
                        height: screenWidth / 4,
                        color: transparent,
                        child: Stack(
                          fit: StackFit.expand,
                          children: <Widget>[
                            DashedCircle(
                              dashes: list.length == 1 ? 0 : list.length,
                              color: blue0,
                              strokeWidth: 1,
                              gapSize: 5,
                              child: Card(
                                elevation: 0,
                                clipBehavior: Clip.antiAlias,
                                shape: CircleBorder(
                                    /*side: BorderSide(color: black.withOpacity(.2),width: 1)*/),
                                child: image.isNotEmpty
                                    ? CachedNetworkImage(
                                        imageUrl: image,
                                        fit: BoxFit.cover,
                                        width: 70,
                                        height: 70,
                                      )
                                    : (Container(
                                        width: 70,
                                        height: 70,
                                        decoration: BoxDecoration(
                                            color: getColorForKey(mColor),
                                            shape: BoxShape.circle),
                                        child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.all(5),
                                            child: Text(
                                              text,
                                              style: textStyle(true, 18, white),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      )),
                              ),
                            ),
                            if (likedCount > 0 && myItem)
                              Align(
                                alignment: Alignment.topRight,
                                child: new Container(
                                  //margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
//                            width: 65,
                                  height: 25, width: 40,
                                  decoration: BoxDecoration(
                                      color: black.withOpacity(.8),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(25))),
                                  child: Center(
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          likedCount > 9 ? "9+" : "$likedCount",
                                          style: textStyle(true, 12, white),
                                        ),
                                        if (likedCount <= 9) addSpaceWidth(5),
//                              Image.asset(
//                                heart,
//                                color: red0,
//                                width: 10,
//                                height: 10,
//                              ),
                                        Icon(
                                          Icons.visibility,
                                          size: 14,
                                          color: white,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                    addSpace(10),
                    Text(
                      model.myItem() ? "Your Story" : name,
                      style: textStyle(true, 14, black),
                    ),
                    Text(
                      timeAgo,
                      style: textStyle(false, 12, black),
                    ),
                  ],
                ),
              ),
              if (!myItem && readAll)
                Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                            border: Border.all(color: white, width: 2),
                            shape: BoxShape.circle,
                            color: green),
                        child: Icon(
                          Icons.check,
                          size: 15,
                          color: white,
                        )))
            ],
          ),
        );
      },
      shrinkWrap: true,
      padding: EdgeInsets.only(bottom: 100),
      itemCount: storyList.length,
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => false;
}
