import 'dart:convert';
import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/ChatMain.dart';
import 'package:Strokes/GroupInfoDialog.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../enter_pin.dart';

class ContactsPage extends StatefulWidget {
  final bool groupMode;
  final bool shareMode;
  ContactsPage({this.groupMode = false, this.shareMode = false});
  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {
  TextEditingController searchController = TextEditingController();
  bool _showCancel = false;
  FocusNode focusSearch = FocusNode();
  List<BaseModel> contactsList = [];
  List subs = [];
  bool groupMode = false;

  reload() async {
    //contactsList.clear();
    String search = searchController.text.toString().toLowerCase().trim();
    if (search.isNotEmpty) contactsList.clear();
    for (BaseModel model in contacts) {
      String contactName = model.getString(NAME).toLowerCase().trim();
      String number = model.getString(PHONE_NUMBER).toLowerCase().trim();
      if (search.isNotEmpty) {
        if (!contactName.contains(search)) {
          if (!number.contains(search)) continue;
        }
      }
      int p = contactsList
          .indexWhere((e) => e.getObjectId() == model.getObjectId());
      if (p != -1) {
        contactsList[p] = model;
      } else {
        contactsList.add(model);
      }
    }
    contactsList.sort((a, b) => b
        .getBoolean(IS_CONVAS)
        .toString()
        .compareTo(a.getBoolean(IS_CONVAS).toString()));
    final myContacts =
        contactsList.where((e) => e.getBoolean(IS_CONVAS)).toList();
    userModel
      ..put(CONTACTS, myContacts.map((e) => e.getUserId()).toList())
      ..updateItems();
    if (contacts.isNotEmpty) updateStoryParties(null);
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(userModel.getUserId());
    print(userModel.getList(CONTACTS));
    groupMode = widget.groupMode;
    var contactSub = contactsController.stream.listen((model) {
      addToContacts(model);
    });
    subs.add(contactSub);
    reload();
    //refreshScreen();
    if (contactReady) {
      SharedPreferences.getInstance().then((pref) {
        pref.setString(
            CONTACTS, jsonEncode(contactsList.map((e) => e.items).toList()));
      });
    }
    syncContacts();
    loadSharing();
  }

  loadSharing() {
    bool canShare = widget.shareMode;
    if (!canShare) return;
  }

  addToContacts(BaseModel model) {
    int p = contactsList.indexWhere(
        (e) => e.getString(PHONE_NUMBER) == model.getString(PHONE_NUMBER));
    if (p != -1) {
      contactsList[p] = model;
    } else {
      contactsList.add(model);
    }
    contactsList.sort((a, b) => b
        .getBoolean(IS_CONVAS)
        .toString()
        .compareTo(a.getBoolean(IS_CONVAS).toString()));
    //if (mounted)
    setState(() {});
  }

  syncContacts() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (!mounted) return;
    //if (!contactReady) return;
    //Query firebase for record
    for (int p = 0; p < contacts.length; p++) {
      BaseModel model = contacts[p];
      String query = model.getString(PHONE_NUMBER);
      bool onConvas = model.getBoolean(IS_CONVAS);
      bool synced = model.getBoolean(SYNCED);
      model.put(SYNCED, true);
      if (onConvas && !synced) {
        userModel
          ..putInList(CONTACT, model.getUserId(), true)
          ..updateItems(updateTime: false);
      }

      if (synced) continue;
      QuerySnapshot shots = await Firestore.instance
          .collection(USER_BASE)
          .where(PHONE_NUMBER, isEqualTo: query)
          .limit(1)
          .getDocuments();

      if (shots.documents.isEmpty) {
        int index = contactsList.indexWhere(
            (e) => e.getString(PHONE_NUMBER) == model.getString(PHONE_NUMBER));
        if (model.myItem()) continue;
        bool exists = index != -1;
        if (exists) {
          contactsList[index] = model;
          userModel
            ..putInList(CONTACT, model.getUserId(), true)
            ..updateItems();
          if (contacts.isNotEmpty) updateStoryParties(null);
        }
        continue;
      }

      final doc = shots.documents[0];
      BaseModel user = BaseModel(doc: doc);
      if (user.myItem()) continue;
      bool connected = user.getList(CONTACTS).contains(userModel.getUserId());
      model.put(USER_IMAGE, user.userImage);
      model.put(USERNAME, user.getString(NAME));
      model.put(IS_CONVAS, true);
      model.put(OBJECT_ID, user.getObjectId());
      model.put(USER_ID, user.getObjectId());
      model.put(CONNECTED, connected);
      if (user.myItem()) continue;
      int index = contactsList.indexWhere(
          (e) => e.getString(PHONE_NUMBER) == model.getString(PHONE_NUMBER));
      bool exists = index != -1;
      if (exists) {
        contactsList.removeAt(index);
        contactsList.insert(0, model);
      }
      if (mounted) setState(() {});
      if (p % 10 == 0) {
        pref.setString(
            CONTACTS, jsonEncode(contactsList.map((e) => e.items).toList()));
      }
    }
    syncSetup = true;
    if (contacts.isNotEmpty) updateStoryParties(null);
    if (mounted) setState(() {});

    if (contactReady)
      pref.setString(
          CONTACTS, jsonEncode(contactsList.map((e) => e.items).toList()));
  }

  bool get contactReady => contactSetup && syncSetup;

  @override
  void dispose() {
    // TODO: implement dispose
    for (var sub in subs) sub.cancel();
    super.dispose();
  }

  clickBack() {
    if (groupMode) {
      groupMode = false;
      selectedContacts.clear();
      setState(() {});
      return;
    }
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        clickBack();
        return false;
      },
      child: Scaffold(
        backgroundColor: white,
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(0, 40, 10, 10),
              color: white,
              child: Row(
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        clickBack();
                      },
                      child: Container(
                        width: 50,
                        height: 30,
                        child: Center(
                            child: Icon(
                          Icons.arrow_back_ios,
                          color: black,
                          size: 20,
                        )),
                      )),
                  Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                          groupMode
                              ? "${selectedContacts.length} / ${GROUP_MAX} Members"
                              : "${contacts.length} Contacts",
                          style: textStyle(true, 20, black))),
                  addSpaceWidth(10),
                  if (!contactReady)
                    Container(
                      width: 15,
                      height: 15,
                      child: CircularProgressIndicator(
                        //value: 20,
                        valueColor: AlwaysStoppedAnimation<Color>(app_blue),
                        strokeWidth: 2,
                      ),
                    ),
                  addSpaceWidth(10),
                  if (!groupMode)
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          groupMode = true;
                        });
                      },
                      child: Container(
                        height: 30,
//                  width: 100,
//                  margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(
                                color: black.withOpacity(0.2), width: 1)),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.group_add,
                              color: black.withOpacity(.5),
                              size: 17,
                            ),
                            addSpaceWidth(5),
                            Text(
                              "Group",
                              style: textStyle(true, 13, black),
                            )
                          ],
                        ),
                      ),
                    ),
                  if (groupMode && selectedContacts.isNotEmpty)
                    Container(
                      height: 35,
                      margin: EdgeInsets.only(right: 10),
                      width: 35,
                      child: new RaisedButton(
                          elevation: 1,
                          padding: EdgeInsets.all(0),
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          onPressed: () {
                            pushAndResult(context, GroupInfoDialog(),
                                result: (_) {
                              Future.delayed(Duration(milliseconds: 500), () {
                                createGroup(_);
                              });
                            }, depend: false);
                          },
                          shape: CircleBorder(),
                          color: AppConfig.appColor,
                          child: Center(
                              child: Icon(
                            Icons.check,
                            color: white,
                          ))),
                    ),
                  //if (!groupMode)
//                    Container(
//                      height: 30,
//                      width: 50,
//                      child: new FlatButton(
//                          padding: EdgeInsets.all(0),
//                          materialTapTargetSize:
//                              MaterialTapTargetSize.shrinkWrap,
//                          onPressed: () {},
//                          child: Center(
//                              child: Icon(
//                            Icons.more_vert,
//                            color: black,
//                          ))),
//                    ),
                ],
              ),
            ),
            AnimatedContainer(
              height: contactReady ? 0 : 40,
              duration: Duration(milliseconds: 500),
              width: double.infinity,
              child: Center(
                  child: contactReady
                      ? Container()
                      : Text(
                          "Convas is sycning your contacts please wait...",
                          style: textStyle(true, 14, white),
                        )),
              color: black.withOpacity(.5),
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            ),
            Container(
              height: 45,
              margin: EdgeInsets.fromLTRB(20, contactReady ? 0 : 10, 20, 10),
              decoration: BoxDecoration(
                  color: white,
                  borderRadius: BorderRadius.circular(25),
                  border: Border.all(color: black.withOpacity(0.2), width: 1)),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  addSpaceWidth(10),
                  Icon(
                    Icons.search,
                    color: black.withOpacity(.5),
                    size: 17,
                  ),
                  addSpaceWidth(10),
                  new Flexible(
                    flex: 1,
                    child: new TextField(
                      textInputAction: TextInputAction.search,
                      textCapitalization: TextCapitalization.sentences,
                      autofocus: false,
                      onSubmitted: (_) {
                        //reload();
                      },
                      decoration: InputDecoration(
                          hintText: "Search name or phone",
                          hintStyle: textStyle(
                            false,
                            18,
                            black.withOpacity(.5),
                          ),
                          border: InputBorder.none,
                          isDense: true),
                      style: textStyle(false, 16, black),
                      controller: searchController,
                      cursorColor: black,
                      cursorWidth: 1,
                      focusNode: focusSearch,
                      keyboardType: TextInputType.text,
                      onChanged: (s) {
                        _showCancel = s.trim().isNotEmpty;
                        setState(() {});
                        reload();
                      },
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        focusSearch.unfocus();
                        _showCancel = false;
                        searchController.text = "";
                      });
                      reload();
                    },
                    child: _showCancel
                        ? Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
                            child: Icon(
                              Icons.close,
                              color: black,
                              size: 20,
                            ),
                          )
                        : new Container(),
                  )
                ],
              ),
            ),

//          addSpace(10),
            Expanded(
                flex: 1,
                child: Builder(builder: (ctx) {
//                if (!contactSetup) return loadingLayout();
                  if (contactsList.isEmpty)
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Image.asset(
                              "assets/icons/contact.png",
                              width: 50,
                              height: 50,
                              color: white,
                            ),
                            Text(
                              "No Contact Yet",
                              style: textStyle(true, 20, black),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    );

                  return Container(
                      child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (c, p) {
                      return contactItem(p);
                    },
                    shrinkWrap: true,
                    itemCount: contactsList.length,
                    padding: EdgeInsets.only(top: 0),
                  ));
                }))
          ],
        ),
      ),
    );
  }

  List selectedContacts = [];
  contactItem(int p) {
    BaseModel model = contactsList[p];
    bool isConvas = model.getBoolean(IS_CONVAS);
    String contactName = model.getString(NAME);
    String userName = model.getString(USERNAME);
    String number = model.getString(PHONE_NUMBER);
    String image = model.userImage;

    return GestureDetector(
      onTap: () {
        if (!isConvas) return;
        if (!groupMode) return;

        if (selectedContacts.contains(model)) {
          selectedContacts.remove(model);
        } else {
          selectedContacts.add(model);
        }
        setState(() {});
      },
      child: Container(
        decoration: BoxDecoration(color: white, boxShadow: [
          //BoxShadow(color: black.withOpacity(.3), blurRadius: 5)
        ]),
        padding: EdgeInsets.fromLTRB(10, 8, 10, 8),
        margin: EdgeInsets.fromLTRB(0, .5, 0, 0),
        child: Row(
          children: [
            if (groupMode && isConvas)
              checkBox(selectedContacts.contains(model)),
            if (groupMode && isConvas) addSpaceWidth(10),
            ClipRRect(
              borderRadius: BorderRadius.circular(35),
              child: Container(
                //margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                width: 65,
                height: 65,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppConfig.appColor,
                    gradient: LinearGradient(colors: [
//                      AppConfig.appColor,
//                      AppConfig.appColor.withOpacity(.8)
                      blue4,
                      blue2,
                    ])),
                child: image.isEmpty
                    ? Center(
                        child: Text(
                          getInitials(isConvas ? userName : contactName),
                          style: textStyle(true, 28, white),
                        ),
                      )
                    : CachedNetworkImage(
                        imageUrl: image,
                        fit: BoxFit.cover,
                      ),
              ),
            ),

            /*    Container(
              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
              width: 60,
              height: 60,
              child: Card(
                color: black.withOpacity(.1),
                elevation: 0,
                clipBehavior: Clip.antiAlias,
                shape: CircleBorder(
                    side: BorderSide(color: black.withOpacity(.2), width: .9)),
                child: CachedNetworkImage(
                  imageUrl: image,
                  fit: BoxFit.cover,
                  placeholder: (c, s) {
                    return Container(
                      width: 60,
                      height: 60,
                      child: Center(
                        child: Text(
                          initials,
                          style: textStyle(true, 18, black.withOpacity(.6)),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),*/
            addSpaceWidth(10),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    isConvas ? userName : contactName,
                    style: textStyle(true, 18, black),
                  ),
                  Text(
                    number,
                    style: textStyle(false, 14, black),
                  )
                ],
              ),
            ),
            //if (!groupMode || (groupMode && !isConvas))
            if (!groupMode)
              FlatButton(
                onPressed: () {
                  if (isConvas) {
                    showListDialog(
                      context,
                      [
                        "Normal Chat",
                        if (userModel.isPremium) "Encrypted Chat"
                      ],
                      (_) {
                        if (_ == 0) {
                          clickChat(context, CHAT_MODE_REGULAR, model,
                              replace: true);
                          return;
                        }

                        if (_ == 1) {
                          pushAndResult(context, EnterPin(), depend: false,
                              result: (_) {
                            if (null == _) return;

                            if (_ == userModel.getString(CODE_PIN)) {
                              clickChat(context, CHAT_MODE_ENCRYPT, model,
                                  replace: true);
                            }
                          });
                        }
                      },
                      showTitle: false,
                    );
                  } else {
                    String appLink = appSettingsModel.getString(APP_LINK_IOS);
                    if (Platform.isAndroid) {
                      appLink = appSettingsModel.getString(APP_LINK_ANDROID);
                    }
                    Share.share(
                        "Join the ConvasApp Community,keep messages secured always.\n $appLink");
                  }
                },
                padding: EdgeInsets.all(0),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                color: isConvas ? blue6 : AppConfig.appColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      isConvas ? Icons.chat : Icons.email,
                      color: white,
                      size: 13,
                    ),
                    addSpaceWidth(5),
                    Text(
                      isConvas ? "Chat" : "Invite",
                      style: textStyle(true, 12, white),
                    ),
                  ],
                ),
              ),
//            if (isConvas) ...[
//              addSpaceWidth(5),
//              FlatButton(
//                onPressed: () {
//                  pushAndResult(context, EnterPin(), depend: false,
//                      result: (_) {
//                        if (null == _) return;
//                        if (_ == userModel.getString(CODE_PIN)) {
//                          Future.delayed(Duration(milliseconds: 100), () {
//                            clickChat(
//                              context,
//                              CHAT_MODE_ENCRYPT,
//                              model,
//                            );
//                          });
//                        }
//                      });
//                },
//                padding: EdgeInsets.all(0),
//                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
//                color: red,
//                shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(25)),
//                child: Row(
//                  mainAxisSize: MainAxisSize.min,
//                  children: [
//                    Icon(
//                      Icons.lock,
//                      color: white,
//                      size: 13,
//                    ),
//                    addSpaceWidth(5),
//                    Text(
//                      "Encrypt",
//                      style: textStyle(true, 12, white),
//                    ),
//                  ],
//                ),
//              ),
//            ]
          ],
        ),
      ),
    );
  }

  createGroup(List _) {
    showProgress(true, context, msg: "Creating Group");
    String photo = _[0];
    String name = _[1];
    String desc = _[2];
    bool encrypted = _[3];

    uploadFile(photo.isEmpty ? null : File(photo), (res, error) {
      showProgress(false, context);
      if (error != null) {
        showMessage(
            context, Icons.error, red0, "Error Occurred", error.toString(),
            delayInMilli: 500, clickYesText: "Retry", onClicked: (_) {
          if (_ == true) {
            Future.delayed(Duration(milliseconds: 500), () {
              createGroup(_);
            });
          }
        });
        return;
      }
      BaseModel groupModel = BaseModel();
      String key = encrypted ? CHAT_IDS_BASE_ENCRYPT : CHAT_IDS_BASE;
      String chatId = getRandomId();
      List ids = [];
      selectedContacts.forEach((model) {
        ids.add(model.getObjectId());
      });
      ids.add(userModel.getObjectId());
      groupModel.put(PARTIES, ids);
      groupModel.put(GROUP_NAME, name);
      groupModel.put(GROUP_PHOTO, res);
      groupModel.put(GROUP_DESCRIPTION, desc);
      groupModel.saveItem(key, true, document: chatId);
      firebaseMessaging.subscribeToTopic(chatId);
      showProgress(false, context);
      Future.delayed(Duration(milliseconds: 1200), () {
        postGeneralMessage("${userModel.getString(NAME)} created $name group",
            groupModel, encrypted ? 1 : 0);
        pushReplaceAndResult(
            context,
            ChatMain(
              chatId,
              encrypted ? CHAT_MODE_ENCRYPT : CHAT_MODE_REGULAR,
              otherPerson: groupModel,
            ),
            depend: false);
      });
    });
  }
}
