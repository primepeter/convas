import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/PaymentSubAndroid.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeAgo;

class ShowWallet extends StatefulWidget {
  @override
  _ShowWalletState createState() => _ShowWalletState();
}

class _ShowWalletState extends State<ShowWallet> {
  BaseModel personModel;
  bool myProfile = false;
  bool fromChat;

  final formatCurrency =
      new NumberFormat.currency(decimalDigits: 2, symbol: "");

  List<BaseModel> subHistory = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSubHistory();
  }

  bool subLoaded = false;

  loadSubHistory() {
    Firestore.instance
        .collection(TRANSACTION_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .getDocuments()
        .then((docs) {
      for (var doc in docs.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (model.getType() != TYPE_WALLET_SUB) continue;
        int p = subHistory
            .indexWhere((e) => e.getObjectId() == model.getObjectId());
        if (p != -1)
          subHistory[p] = model;
        else
          subHistory.add(model);
      }
      subLoaded = true;
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: page(),
    );
  }

  page() {
    return Container(
      child: Column(
        children: [
          Container(
            color: white,
            padding: EdgeInsets.only(top: 35, right: 10, left: 10, bottom: 0),
            child: Row(
              children: [
                BackButton(
                  color: black,
                  onPressed: () {
                    Navigator.pop(context, "");
                  },
                ),
                Text(
                  "Wallet",
                  style: textStyle(true, 25, black),
                ),
                Spacer(),
              ],
            ),
          ),
          Container(
            color: white,
            padding: EdgeInsets.only(
              left: 10,
              right: 10,
            ),
            margin: EdgeInsets.only(top: 10, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Container(
                        width: 85,
                        height: 85,
                        child: Stack(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(40),
                              child: Container(
                                width: 80,
                                height: 80,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: AppConfig.appColor,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/icons/empty_bg.png"),
                                        fit: BoxFit.cover),
                                    gradient: LinearGradient(colors: [
                                      blue4,
                                      blue2,
                                    ])),
                                child: (userModel.userImage.isEmpty)
                                    ? Center(
                                        child: Text(
                                          getInitials(
                                              userModel.getString(NAME)),
                                          style: textStyle(true, 30, white),
                                        ),
                                      )
                                    : CachedNetworkImage(
                                        imageUrl: userModel.userImage,
                                        fit: BoxFit.cover,
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      addSpaceWidth(10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text.rich(TextSpan(children: [
                            TextSpan(
                              text: "Wallet Balance ",
                              style: textStyle(true, 16, black.withOpacity(.5)),
                            ),
                            TextSpan(
                              text:
                                  //"${currency(context).currencyName}"
                                  "NGN${formatCurrency.format(userModel.accountBalance)}",
                              style: textStyle(true, 25, black),
                            ),
                          ])),
                          Text.rich(TextSpan(children: [
                            TextSpan(
                              text: "Account Premium ",
                              style: textStyle(true, 14, black.withOpacity(.5)),
                            ),
                            TextSpan(
                              text: "${userModel.isPremium}",
                              style: textStyle(true, 16, black),
                            ),
                          ])),
                          if (userModel.getInt(SUBSCRIPTION_EXPIRY) != 0)
                            Text.rich(TextSpan(children: [
                              TextSpan(
                                text: "Account Expiry ",
                                style:
                                    textStyle(true, 14, black.withOpacity(.5)),
                              ),
                              TextSpan(
                                text:
                                    "${formatTransDay(userModel.getInt(SUBSCRIPTION_EXPIRY))}",
                                style: textStyle(true, 16, black),
                              ),
                            ]))
                        ],
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    color: white,
                    padding: EdgeInsets.only(
                      left: 10,
                      right: 10,
                    ),
                    margin: EdgeInsets.only(top: 10, bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        fieldItem(Icons.add_circle_outline, blue, "Fund Wallet",
                            () {
                          fundWallet(context);
                        }),
                        fieldItem(Icons.person, brown, "Subscribe for me", () {
                          pushAndResult(context, PaymentSubAndroid(),
                              depend: false);
                        }),
                        fieldItem(Icons.people, green, "Subscribe for others",
                            () {
                          pushAndResult(
                              context,
                              PaymentSubAndroid(
                                forAnother: true,
                              ),
                              depend: false);
                        }),
                      ],
                    ),
                  ),
                  addSpace(15),
                  ...List.generate(subHistory.length, (p) {
                    BaseModel model = subHistory[p];
                    String title = "";

                    String image = model.userImage;
                    String name = model.getString(NAME);
                    String number = model.getString(PHONE_NUMBER);

                    int months;
                    if (model.getInt(PREMIUM_INDEX) == 0) months = 1;
                    if (model.getInt(PREMIUM_INDEX) == 1) months = 6;
                    if (model.getInt(PREMIUM_INDEX) == 2) months = 12;

                    double amount = model.getDouble(AMOUNT);
                    bool myItem = true;

                    return Container(
                      decoration: BoxDecoration(
                          color: white,
                          border: Border(
                              bottom: BorderSide(
                                  width: .5, color: black.withOpacity(.09)))),
                      padding: EdgeInsets.all(15),
                      margin: EdgeInsets.only(bottom: 2),
                      child: Row(
                        children: [
                          Flexible(
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(22),
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppConfig.appColor,
                                        gradient: LinearGradient(colors: [
                                          blue4,
                                          blue2,
                                        ])),
                                    child: image.isEmpty
                                        ? Center(
                                            child: Text(
                                              getInitials(name),
                                              style: textStyle(true, 14, white),
                                            ),
                                          )
                                        : CachedNetworkImage(
                                            imageUrl: image,
                                            fit: BoxFit.cover,
                                          ),
                                  ),
                                ),
                                addSpaceWidth(10),
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        name,
                                        style: textStyle(true, 18, black),
                                      ),
                                      Text(
                                        number,
                                        style: textStyle(
                                            false, 15, black.withOpacity(.5)),
                                      ),
                                      Text(
                                        timeAgo.format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                model.getTime())),
                                        style: textStyle(
                                            false, 13, black.withOpacity(.5)),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                "N${formatCurrency.format(amount)}",
                                style: textStyle(true, 16, black),
                              ),
                              Text(
                                "$months Months",
                                style:
                                    textStyle(false, 15, black.withOpacity(.5)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  fieldItem(icon, color, title, onPressed,
      {bool value = false, bool showSwitch = false}) {
    return Column(
      children: [
        GestureDetector(
          onTap: onPressed,
          child: Container(
            //height: 40,
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                        BorderSide(width: .5, color: black.withOpacity(.09)))),
            padding: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: color,
                      // borderRadius: BorderRadius.circular(8)
                    ),
                    child: Icon(
                      icon,
                      color: white,
                      size: 18,
                    )),
                addSpaceWidth(10),
                Text(
                  title,
                  style: textStyle(false, 16, black),
                ),
                Spacer(),
                if (showSwitch)
                  CupertinoSwitch(
                      activeColor: AppConfig.appColor,
                      value: value,
                      onChanged: (b) {
                        onPressed();
                      }),
              ],
            ),
          ),
        ),
        //addLine(0.09, black, 0, 10, 0, 10),
      ],
    );
  }

  settingsItem(
      {String title,
      String sub,
      bool active = false,
      bool showSwitch = false,
      bool showCall = false,
      onTap}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: textStyle(false, 12, black.withOpacity(.6)),
                ),
                Text(
                  sub,
                  style: textStyle(false, 18, black),
                ),
              ],
            ),
          ),
          addSpaceWidth(10),
          if (showCall)
            Row(
              children: [
                new Container(
                  height: 30,
                  width: 40,
                  child: new FlatButton(
                      padding: EdgeInsets.all(0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {},
                      child: Center(
                          child: Icon(
                        Icons.videocam,
                        size: 20,
                        color: black,
                      ))),
                ),
                new Container(
                  height: 30,
                  width: 40,
                  child: new FlatButton(
                      padding: EdgeInsets.all(0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {},
                      child: Center(
                          child: Icon(
                        Icons.call,
                        size: 20,
                        color: black,
                      ))),
                ),
              ],
            ),
          if (showSwitch)
            CupertinoSwitch(
                activeColor: AppConfig.appColor,
                value: true,
                onChanged: (b) {}),
        ],
      ),
    );
  }

  void savePhoto(File croppedFile) {
    uploadFile(croppedFile, (res, err) {
      if (null != err) {
        savePhoto(croppedFile);
        return;
      }
      userModel
        ..put(USER_IMAGE, res)
        ..updateItems();
      setState(() {});
    });
  }
}

NumberFormat currency(BuildContext context) {
  Locale locale = Localizations.localeOf(context);
  var format = NumberFormat.simpleCurrency(locale: locale.toString());
  return format;
}

String formatTransDay(int t) {
  final date = DateTime.fromMillisecondsSinceEpoch(t);
  return new DateFormat("EE MMMM d yyyy").format(date);
}
