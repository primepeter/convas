import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/admin/AppAdmin.dart';
import 'package:Strokes/app/app.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/inputDialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:share/share.dart';

import 'ShowWallet.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  BaseModel personModel;
  bool myProfile = false;
  bool fromChat;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: page(),
    );
  }

  page() {
    return Container(
      child: SingleChildScrollView(
        child: Container(
          //margin: EdgeInsets.only(top: 0, right: 10, left: 10, bottom: 20),
          child: Column(
            children: [
              Container(
                color: white,
//                padding: EdgeInsets.all(10),
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                ),
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          GestureDetector(
                            onLongPress: () {
                              if (!isAdmin) return;
                              pushAndResult(context, AppAdmin(), depend: false);
                            },
                            onTap: () async {
                              openGallery(context, onPicked: (_) {
                                if (_.isEmpty) return;
                                Future.delayed(Duration(milliseconds: 100),
                                    () async {
                                  List files = [];
                                  String path = _[0].file.path;
                                  File croppedFile =
                                      await ImageCropper.cropImage(
                                    sourcePath: path,
                                  );
                                  if (croppedFile != null)
                                    savePhoto(croppedFile);
                                });
                              });

//                              await provider.refreshGalleryList();
//                              final item = provider.list[0];
//                              pushAndResult(
//                                  context,
//                                  CustomGalleryContentListPage(
//                                    path: item,
//                                    singleMode: true,
//                                    topTitle: "Set Profile Photo",
//                                  ),
//                                  depend: false, result: (_) {
//                                if (_.isEmpty) return;
//                                Future.delayed(Duration(milliseconds: 100),
//                                    () async {
//                                  List files = [];
//                                  String path = _[0].path;
//                                  File croppedFile =
//                                      await ImageCropper.cropImage(
//                                    sourcePath: path,
//                                  );
//                                  if (croppedFile != null)
//                                    savePhoto(croppedFile);
//                                });
//                              });
                            },
                            child: Container(
                              width: 85,
                              height: 85,
                              child: Stack(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(40),
                                    child: Container(
                                      width: 80,
                                      height: 80,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppConfig.appColor,
                                          gradient: LinearGradient(colors: [
                                            blue4,
                                            blue2,
                                          ])),
                                      child: (userModel.userImage.isEmpty)
                                          ? Center(
                                              child: Text(
                                                getInitials(
                                                    userModel.getString(NAME)),
                                                style:
                                                    textStyle(true, 30, white),
                                              ),
                                            )
                                          : CachedNetworkImage(
                                              imageUrl: userModel.userImage,
                                              fit: BoxFit.cover,
                                            ),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Container(
                                        height: 30,
                                        width: 30,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: white, width: 2),
                                            color: AppConfig.appColor,
                                            shape: BoxShape.circle),
                                        child: Icon(
                                          Icons.edit,
                                          color: white,
                                          size: 15,
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                          /*Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                            width: 80,
                            height: 80,
                            child: Card(
                              color: black.withOpacity(.1),
                              elevation: 0,
                              clipBehavior: Clip.antiAlias,
                              shape: CircleBorder(
                                  side: BorderSide(
                                      color: black.withOpacity(.2), width: .9)),
                              child: (userModel.userImage.isEmpty)
                                  ? Center(
                                      child: Text(
                                        getInitials(userModel.getString(NAME)),
                                        style: textStyle(true, 14, white),
                                      ),
                                    )
                                  : CachedNetworkImage(
                                      imageUrl: userModel.userImage,
                                      fit: BoxFit.cover,
                                    ),
                            ),
                          ),*/
                          addSpaceWidth(10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                userModel.getString(NAME),
                                style: textStyle(true, 22, black),
                              ),
                              Text(
                                userModel.getString(PHONE_NUMBER),
                                style:
                                    textStyle(false, 18, black.withOpacity(.7)),
                              )
                            ],
                          ),
                          Spacer(),
//                          Icon(
//                            Icons.navigate_next,
//                            size: 30,
//                            color: black.withOpacity(.7),
//                          )
                        ],
                      ),
                    ),
                    //addLine(0.09, black, 0, 10, 0, 10),
//                    GestureDetector(
//                      onTap: () async {
//                        await provider.refreshGalleryList();
//                        final item = provider.list[0];
//                        pushAndResult(
//                            context,
//                            CustomGalleryContentListPage(
//                              path: item,
//                              singleMode: true,
//                              topTitle: "Set Profile Photo",
//                            ),
//                            depend: false, result: (_) {
//                          if (_.isEmpty) return;
//                          Future.delayed(Duration(milliseconds: 100), () async {
//                            List files = [];
//                            String path = _[0].path;
//                            File croppedFile = await ImageCropper.cropImage(
//                              sourcePath: path,
//                            );
//                            if (croppedFile != null) savePhoto(croppedFile);
//                          });
//                        });
//                      },
//                      child: Container(
//                        //height: 40,
//                        padding: EdgeInsets.all(15),
//                        alignment: Alignment.centerLeft,
//                        decoration: BoxDecoration(
//                            border: Border(
//                                bottom: BorderSide(
//                                    width: .5, color: black.withOpacity(.09)))),
//                        child: Text(
//                          "Set Profile Photo",
//                          style: textStyle(true, 16, AppConfig.appColor),
//                        ),
//                      ),
//                    ),
                    //addLine(0.09, black, 0, 10, 0, 10),
                    //addSpace(10),
                    GestureDetector(
                      onTap: () {
                        pushAndResult(
                            context,
                            inputDialog(
                              "Set Username",
                              message: userModel.getString(NAME),
                            ),
                            depend: false, result: (_) {
                          if (_ == null) return;
                          userModel
                            ..put(NAME, _)
                            ..updateItems();
                          setState(() {});
                        });
                      },
                      child: Container(
                        //height: 40,
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: .5, color: black.withOpacity(.09)))),
                        child: Text(
                          "Set Username",
                          style: textStyle(true, 16, AppConfig.appColor),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                color: white,
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                ),
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //fieldItem(Icons.call, green, "Recent calls", () {}),
                    fieldItem(
                        Icons.notifications_active, pink3, "Notifications", () {
                      bool value = userModel.getBoolean(PUSH_NOTIFICATION);
                      userModel
                        ..put(PUSH_NOTIFICATION, !value)
                        ..updateItems();
                      setState(() {});
                    },
                        value: userModel.getBoolean(PUSH_NOTIFICATION),
                        showSwitch: true),
                    if (appSettingsModel.getBoolean(WALLET_ENABLED))
                      fieldItem(Icons.account_balance_wallet, orange0, "Wallet",
                          () {
                        pushAndResult(context, ShowWallet(), depend: false);
                      }),
                    fieldItem(Icons.person, blue, "Tell a friend", () {
                      String appLink = appSettingsModel.getString(APP_LINK_IOS);
                      if (Platform.isAndroid) {
                        appLink = appSettingsModel.getString(APP_LINK_ANDROID);
                      }
                      Share.share(
                          "Join the ConvasApp Community,keep messages secured always.\n $appLink");
                    }),
                  ],
                ),
              ),
              Container(
                color: white,
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                ),
                margin: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    fieldItem(Icons.help, dark_green0, "FAQ", () {
                      String link = appSettingsModel.getString(FAQ_LINK);
                      if (link.isEmpty) return;
                      openLink(link);
                    }),
                    fieldItem(Icons.lock, pink5, "Privacy Policy", () {
                      String link = appSettingsModel.getString(PRIVACY_LINK);
                      if (link.isEmpty) return;
                      openLink(link);
                    }),
                    fieldItem(Icons.lock, brown, "Terms & Condition", () {
                      String link = appSettingsModel.getString(TERMS_LINK);
                      if (link.isEmpty) return;
                      openLink(link);
                    }),
                  ],
                ),
              ),
              Container(
                color: white,
//                padding: EdgeInsets.all(10),
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                ),
                margin: EdgeInsets.only(top: 10, bottom: 10),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    fieldItem(Icons.delete_forever, red, "Delete Account", () {
                      deleteAccount(context);
                    }),
                    fieldItem(Icons.exit_to_app, red, "Logout", () {
                      clickLogout(context);
                    }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  fieldItem(icon, color, title, onPressed,
      {bool value = false, bool showSwitch = false}) {
    return Column(
      children: [
        GestureDetector(
          onTap: onPressed,
          child: Container(
            //height: 40,
            decoration: BoxDecoration(
                border: Border(
                    bottom:
                        BorderSide(width: .5, color: black.withOpacity(.09)))),
            padding: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: color,
                      // borderRadius: BorderRadius.circular(8)
                    ),
                    child: Icon(
                      icon,
                      color: white,
                      size: 18,
                    )),
                addSpaceWidth(10),
                Text(
                  title,
                  style: textStyle(false, 16, black),
                ),
                Spacer(),
                if (showSwitch)
                  CupertinoSwitch(
                      activeColor: AppConfig.appColor,
                      value: value,
                      onChanged: (b) {
                        onPressed();
                      }),
              ],
            ),
          ),
        ),
        //addLine(0.09, black, 0, 10, 0, 10),
      ],
    );
  }

  settingsItem(
      {String title,
      String sub,
      bool active = false,
      bool showSwitch = false,
      bool showCall = false,
      onTap}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: textStyle(false, 12, black.withOpacity(.6)),
                ),
                Text(
                  sub,
                  style: textStyle(false, 18, black),
                ),
              ],
            ),
          ),
          addSpaceWidth(10),
          if (showCall)
            Row(
              children: [
                new Container(
                  height: 30,
                  width: 40,
                  child: new FlatButton(
                      padding: EdgeInsets.all(0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {},
                      child: Center(
                          child: Icon(
                        Icons.videocam,
                        size: 20,
                        color: black,
                      ))),
                ),
                new Container(
                  height: 30,
                  width: 40,
                  child: new FlatButton(
                      padding: EdgeInsets.all(0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      onPressed: () {},
                      child: Center(
                          child: Icon(
                        Icons.call,
                        size: 20,
                        color: black,
                      ))),
                ),
              ],
            ),
          if (showSwitch)
            CupertinoSwitch(
                activeColor: AppConfig.appColor,
                value: true,
                onChanged: (b) {}),
        ],
      ),
    );
  }

  void savePhoto(File croppedFile) {
    uploadFile(croppedFile, (res, err) {
      if (null != err) {
        savePhoto(croppedFile);
        return;
      }
      userModel
        ..put(USER_IMAGE, res)
        ..updateItems();
      setState(() {});
    });
  }
}
