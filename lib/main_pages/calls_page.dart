import 'dart:async';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

PageController chatPageController = new PageController();
List<BaseModel> notifyList = List();

class CallsPage extends StatefulWidget {
  @override
  _CallsPageState createState() => _CallsPageState();
}

class _CallsPageState extends State<CallsPage>
    with AutomaticKeepAliveClientMixin {
  List<StreamSubscription> subs = [];

  bool modeEdit = false;

  @override
  void initState() {
    super.initState();
    startRefreshingMessages();
    var sub2 = editCallController.stream.listen((e) {
      modeEdit = e;
      if (e) modeCallDelete = true;
      if (mounted) setState(() {});
    });
    subs.add(sub2);
  }

  @override
  void dispose() {
    for (var s in subs) s.cancel();
    super.dispose();
  }

  startRefreshingMessages() {
    Future.delayed(Duration(seconds: 2), () {
      if (mounted) setState(() {});
      startRefreshingMessages();
    });
  }

  //git remote add strock https://mtellect@bitbucket.org/primepeter/strock-app.git

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: chat_back,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[page()],
      ),
    );
  }

  page() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
            flex: 1,
            child: Builder(builder: (ctx) {
              if (!callsSetup2) return loadingLayout();
              if (callsList.isEmpty)
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset(
                          "assets/icons/multimedia.png",
                          width: 50,
                          height: 50,
                          color: black.withOpacity(.3),
                        ),
                        Text(
                          "No Calls Yet",
                          style: textStyle(true, 20, black.withOpacity(.3)),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                );

              return Container(
                  child: ListView.builder(
                itemBuilder: (c, p) {
                  BaseModel model = callsList[p];

                  String chatId = model.getString(CHAT_ID);
                  String otherPersonId = getOtherPersonId(model);
                  BaseModel otherPerson = otherPersonInfo[otherPersonId];
                  if (otherPerson == null) return Container();

                  String name = getFullName(otherPerson);
                  String image = otherPerson.userImage;

                  return chatItem(image, name, model,
                      p == lastMessages.length - 1, otherPerson);
                },
                shrinkWrap: true,
                itemCount: callsList.length,
                padding: EdgeInsets.only(top: 10),
              ));
            }))
      ],
    );
  }

  chatItem(String image, String name, BaseModel chatModel, bool last,
      BaseModel otherPerson) {
    int type = chatModel.getInt(TYPE);
    String chatId = chatModel.getString(CHAT_ID);
    bool myItem = chatModel.myItem();

    String hisId = chatId.replaceAll(userModel.getObjectId(), "");
    bool read = chatModel.getList(READ_BY).contains(hisId);
    bool myRead = chatModel.getList(READ_BY).contains(userModel.getObjectId());
    List mutedList = userModel.getList(MUTED);
    int unread = unreadCounter[chatId] ?? 0;

    return new InkWell(
      onLongPress: () {},
      onTap: () {
        if (modeEdit) {
          if (selectedCalls.contains(chatModel)) {
            selectedCalls.remove(chatModel);
          } else {
            selectedCalls.add(chatModel);
          }
          modeEdit = selectedCalls.isNotEmpty;
          if (!modeEdit) modeCallDelete = false;
          homeStateController.add(true);
          setState(() {});
          return;
        }
      },
      //margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Container(
        decoration: BoxDecoration(color: white, boxShadow: []),
        padding: EdgeInsets.all(8),
        //margin: EdgeInsets.fromLTRB(0, 1.5, 10, 0),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            if (modeEdit) ...[
              checkBox(selectedCalls.contains(chatModel)),
              addSpaceWidth(10)
            ],
            Container(
              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
              width: 80,
              height: 80,
              child: Card(
                color: black.withOpacity(.1),
                elevation: 0,
                clipBehavior: Clip.antiAlias,
                shape: CircleBorder(
                    side: BorderSide(color: black.withOpacity(.2), width: .9)),
                child: CachedNetworkImage(
                  imageUrl: image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            addSpaceWidth(10),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: new Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    //"Emeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                    name,
                    maxLines: 1, overflow: TextOverflow.ellipsis,
                    style: textStyle(true, 20, black),
                  ),
                  //addSpace(5),
                  Row(
                    children: [
                      callType(chatModel),
                      addSpaceWidth(10),
                      Text(
                        timeago.format(DateTime.fromMillisecondsSinceEpoch(
                            chatModel.getTime())),
                        style: textStyle(false, 12, black.withOpacity(.8)),
                        textAlign: TextAlign.end,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            IconButton(
                onPressed: () {
                  callEngine.placeCall(
                      callerModel: otherPerson,
                      isVideoCall: chatModel.getBoolean(CALL_MODE),
                      onCallPlaced: (BaseModel rec) {
                        BaseModel model = rec;
                        model.saveItem(CALL_BASE, true,
                            document: rec.getObjectId());
                        int p = callsList.indexWhere(
                            (e) => e.getObjectId() == model.getObjectId());
                        if (p != -1) {
                          callsList[p] = model;
                        } else {
                          callsList.insert(0, model);
                        }

                        setState(() {});
                      });
                },
                icon: Icon(chatModel.getBoolean(CALL_MODE)
                    ? Icons.videocam
                    : Icons.call)),
            addSpaceWidth(10)
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  callType(BaseModel chatModel) {
    if (chatModel.myItem()) {
      return Container(
        height: 18,
        width: 18,
        decoration: BoxDecoration(color: AppConfig.appColor),
        padding: EdgeInsets.all(2),
        alignment: Alignment.center,
        child: Icon(
          Icons.arrow_upward,
          color: white,
          size: 15,
        ),
      );
    }

    if (!chatModel.myItem() && chatModel.getBoolean(IS_MISSED)) {
      return Container(
        height: 18,
        width: 18,
        decoration: BoxDecoration(color: red),
        padding: EdgeInsets.all(2),
        alignment: Alignment.center,
        child: Icon(
          Icons.close,
          color: white,
          size: 15,
        ),
      );
    }

    return Container(
      height: 18,
      width: 18,
      decoration: BoxDecoration(color: green),
      padding: EdgeInsets.all(2),
      alignment: Alignment.center,
      child: Icon(
        Icons.arrow_downward,
        color: white,
        size: 15,
      ),
    );
  }
}
