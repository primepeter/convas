import 'dart:io';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/assets.dart';
import 'package:flutter/material.dart';

import 'main_pages/ShowWallet.dart';

class GroupInfoDialog extends StatefulWidget {
  @override
  _GroupInfoDialogState createState() => _GroupInfoDialogState();
}

class _GroupInfoDialogState extends State<GroupInfoDialog> {
  TextEditingController groupName = TextEditingController();
  TextEditingController descController = TextEditingController();
  String photo = "";
  bool encrypted = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: transparent,
        body: Stack(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: Container(
                    color: black.withOpacity(.7),
                  )),
            ),
            page()
          ],
        ));
  }

  page() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 40, 20, 20),
      child: Center(
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15))),
            clipBehavior: Clip.antiAlias,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  width: double.infinity,
                  color: default_white,
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                  child: Center(
                      child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      InkWell(
                        onTap: () {
                          getSingleCroppedImage(context, onPicked: (path) {
                            photo = path;
                            setState(() {});
                          });
                        },
                        child: Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          width: 70,
                          height: 70,
                          child: Card(
                            color: app_blue,
                            elevation: 0,
                            clipBehavior: Clip.antiAlias,
                            shape: CircleBorder(
                                side: BorderSide(
                                    color: black.withOpacity(.1), width: .5)),
                            child: photo.isNotEmpty
                                ? (Image.file(
                                    File(photo),
                                    width: 70,
                                    height: 70,
                                    fit: BoxFit.cover,
                                  ))
                                : Center(
                                    child: Icon(
                                      Icons.add_a_photo,
                                      color: white,
//                                  size: 15,
                                    ),
                                  ),
                          ),
                        ),
                      ),
                      Text(
                        "Group Logo",
                        style: textStyle(true, 16, black),
                      ),
                    ],
                  )),
                ),
                AnimatedContainer(
                  duration: Duration(milliseconds: 500),
                  width: double.infinity,
                  height: errorText.isEmpty ? 0 : 40,
                  color: red0,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Center(
                      child: Text(
                    errorText,
                    style: textStyle(true, 16, white),
                  )),
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: Container(
                    color: white,
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            inputTextView("Group Name", groupName,
                                isNum: false),
                            inputTextView(
                                "Description (Optional)", descController,
                                isNum: false, maxLine: 2),
                            Center(
                              child: GestureDetector(
                                onTap: () {
                                  if (!userModel.isPremium) {
                                    showMessage(
                                        context,
                                        Icons.error,
                                        red0,
                                        "Opps Sorry!",
                                        "You cannot create an Encrypted Group on ConvasApp until you become a Premium User",
                                        clickYesText: "Go Premium",
                                        onClicked: (_) {
                                      if (_)
                                        pushAndResult(context, ShowWallet(),
                                            depend: false);
                                    });
                                    return;
                                  }
                                  setState(() {
                                    encrypted = !encrypted;
                                  });
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: default_white,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(25))),
                                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Icon(
                                        Icons.lock,
                                        size: 14,
                                        color: red0,
                                      ),
                                      addSpaceWidth(5),
                                      Text(
                                        "Encryted Chat",
                                        style: textStyle(true, 14, red0),
                                      ),
                                      addSpaceWidth(5),
                                      checkBox(encrypted,
                                          backColor: white, checkColor: red0)
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: FlatButton(
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0)),
                      color: app_blue,
                      onPressed: () {
                        String name = groupName.text.trim();
                        String desc = descController.text.trim();
                        if (name.isEmpty) {
                          showError("Enter Group Name");
                          return;
                        }
                        Navigator.pop(context, [photo, name, desc, encrypted]);
                      },
                      child: Text(
                        "Create Group",
                        style: textStyle(true, 16, white),
                      )),
                ),
              ],
            )),
      ),
    );
  }

  String errorText = "";
  showError(String text) {
    errorText = text;
    setState(() {});
    Future.delayed(Duration(seconds: 1), () {
      errorText = "";
      setState(() {});
    });
  }
}
