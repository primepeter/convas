import 'dart:async';
import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:animated_widgets/animated_widgets.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_incall/flutter_incall.dart';

class CallScreen extends StatefulWidget {
  final bool isVideoCall;
  final bool callAnswered;
  final bool inComing;
  final BaseModel callModel;
  final BaseModel callerModel;

  const CallScreen({
    this.inComing = false,
    this.isVideoCall = false,
    this.callAnswered = false,
    @required this.callModel,
    @required this.callerModel,
  });

  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  static final _users = <int>[];
  final _infoStrings = <String>[];
  bool muted = false;
  bool isVideoCall = false;
  static const APP_ID = "40c37c3e06f840449394b70b93b177d5";

  String callId;
  BaseModel callModel;
  BaseModel callerModel;
  bool callAnswered = false;
  AudioPlayer audioBeepController = AudioPlayer();
  List<StreamSubscription> subs = [];
  bool hideViews = false;
  Timer ringTimer;
  int timerCounter = 0;

  @override
  void dispose() {
    _users.clear();
    AgoraRtcEngine.leaveChannel();
    AgoraRtcEngine.destroy();
    for (var sub in subs) sub?.cancel();
    ringTimer?.cancel();
    if (timerCounter < 59) {
      userModel
        ..put(CALLING_ID, "")
        ..updateItems();
      callerModel
        ..put(CALLING_ID, "")
        ..updateItems();
      Firestore.instance
          .collection(CALL_BASE)
          .document(callId)
          .get()
          .then((value) {
        BaseModel model = BaseModel(doc: value);
        model.put(IS_MISSED, true);
        model.updateItems();
      });
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    callModel = widget.callModel;
    callerModel = widget.callerModel;
    isVideoCall = widget.isVideoCall;
    callAnswered = widget.callAnswered;
    callId = widget.callModel.getObjectId();
    if (widget.inComing) {
      widget.callModel
        ..put(CALL_STATUS, CALL_STATUS_ACCEPTED)
        ..updateItems();
    }
    initialize();
    loadBeeper();
    listenToUser();
    listenToCall();
    audioBeepController.onPlayerCompletion.listen((event) {
      if (mounted) {
        if (!callAnswered) loadBeeper();
      }
    });
  }

  startRingTimer() {
    if (!callModel.myItem()) return;
    ringTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (timerCounter == 59) {
        timer.cancel();
        Firestore.instance
            .collection(CALL_BASE)
            .document(callId)
            .get()
            .then((value) {
          BaseModel model = BaseModel(doc: value);
          model.put(IS_MISSED, true);
          model.updateItems();
          hangUpCall();
        });
        return;
      }
      timerCounter = timerCounter + 1;
      setState(() {});
    });
  }

  void hangUpCall() {
    print("hangup clicked");
    ringTimer?.cancel();
    audioBeepController?.dispose();
    callModel
      ..put(CALL_DURATION, timerCounter)
      ..updateItems();
    if (!widget.inComing) callEngine.endCall();
    Navigator.pop(context);
  }

  void autoHidePanels({int delay = 2}) {
    Future.delayed(Duration(seconds: delay), () {
      hideViews = true;
      setState(() {});
    });
  }

  bool beepReady = false;
  IncallManager callManager = IncallManager();

  loadBeeper() async {
    if (!mounted) return;
    if (callAnswered) return;
    if (!callModel.myItem()) return;
    if (Platform.isIOS) {
      callManager.startRingback();
      return;
    }

    File beep = await loadFile('assets/sounds/ring.mp3', "ring.mp3");
    audioBeepController.play(beep.path, isLocal: true);
    if (mounted) setState(() {});
  }

  listenToUser() async {
    Future.delayed(Duration(milliseconds: 15), () {
      var sub = Firestore.instance
          .collection(USER_BASE)
          .document(callerModel.getUserId())
          .snapshots()
          .listen((doc) {
        callerModel = BaseModel(doc: doc);
        setState(() {});
      });
      subs.add(sub);
    });
  }

  listenToCall() async {
    Future.delayed(Duration(milliseconds: 25), () {
      var sub = Firestore.instance
          .collection(CALLS_IDS_BASE)
          .document(callId)
          .snapshots()
          .listen((doc) {
        BaseModel model = BaseModel(doc: doc);
        final status = model.getString(CALL_STATUS);
        print("Esther status $status");
        print("Esther doc ${doc.data}");
        bool declined = status == CALL_STATUS_DECLINED;
        bool answered = status == CALL_STATUS_ACCEPTED;

        if (declined || !doc.exists) {
          if (Platform.isIOS) {
            provider.performEndCallAction = (e) {
              e.fulfillWithDateEnded(DateTime.now());
            };
          }

          hangUpCall();
          return;
        }

        if (answered) {
          callManager.stopRingback();
          callAnswered = answered;
          audioBeepController?.stop();
          callManager = IncallManager();
          createTimer(true);
          setState(() {});
        }
        callModel = model;
        setState(() {});
      });
      subs.add(sub);
    });
  }

  int time = 0;
  String timeText = "";

  createTimer(bool create) {
//    if (create) {
//      time = 60;
//    }
//    if (time < 0) {
//      if (mounted)
//        setState(() {
//          timeText = "";
//        });
//      return;
//    }
    timeText = getTimeText();
    Future.delayed(Duration(seconds: 1), () {
      time++;
      if (mounted) setState(() {});
      createTimer(false);
    });
  }

  getTimeText() {
    if (time <= 0) return "";
    return "${time >= 60 ? "01" : "00"}:${time % 60}";
  }

  initialize() async {
    if (APP_ID.isEmpty) {
      setState(() {
        _infoStrings.add(
          'APP_ID missing, please provide your APP_ID in settings.dart',
        );
        _infoStrings.add('Agora Engine is not starting');
      });
      return;
    }

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);
    await AgoraRtcEngine.setParameters(
        '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
    await AgoraRtcEngine.joinChannel(
        null, callId /*userModel.getString(CALLING_ID)*/, null, 0);
  }

  /// Create agora sdk instance and initialize
  _initAgoraRtcEngine() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.enableAudio();
    if (isVideoCall) await AgoraRtcEngine.enableVideo();
  }

  /// Add agora event handlers
  _addAgoraEventHandlers() {
    AgoraRtcEngine.onError = (dynamic code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    };

    AgoraRtcEngine.onJoinChannelSuccess = (
      String channel,
      int uid,
      int elapsed,
    ) {
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    };

    AgoraRtcEngine.onLeaveChannel = () {
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    };

    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      setState(() {
        final info = 'userJoined: $uid';
        _infoStrings.add(info);
        _users.add(uid);
      });
    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      setState(() {
        final info = 'userOffline: $uid';
        _infoStrings.add(info);
        _users.remove(uid);
      });
    };

    AgoraRtcEngine.onFirstRemoteVideoFrame = (
      int uid,
      int width,
      int height,
      int elapsed,
    ) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    };
  }

  void onCallAnswered() {
    setState(() {
      callAnswered = !callAnswered;
    });
    initialize();
  }

  void onCallEnd() {
    Navigator.pop(context);
  }

  void onToggleMute() {
    setState(() {
      muted = !muted;
    });
    AgoraRtcEngine.muteLocalAudioStream(muted);
  }

  void onSwitchCamera() {
    AgoraRtcEngine.switchCamera();
  }

  void onSwitchCall() {
    if (isVideoCall) {
      AgoraRtcEngine.disableVideo();
    } else {
      AgoraRtcEngine.enableVideo();
    }
    isVideoCall = !isVideoCall;
    setState(() {});
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<AgoraRenderWidget> list = [
      AgoraRenderWidget(0, local: true, preview: true),
    ];
    _users.forEach((int uid) => list.add(AgoraRenderWidget(uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget videoPanel() {
    final views = _getRenderViews();
    print(views.length);

    return Stack(
      children: [
        if (views.length == 1) views[0] else views[1],
        if (views.length > 1)
          Positioned(
              bottom: 100,
              right: 15,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  height: 150,
                  width: 100,
                  child: views[0],
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: white, width: 2)),
                ),
              ))
      ],
    );

    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow([views[0]]),
            _expandedVideoRow([views[1]])
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container();
  }

  /// Toolbar layout
  Widget toolPanel() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: EdgeInsets.all(15),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: white.withOpacity(.09),
            borderRadius: BorderRadius.circular(15)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: List.generate(5, (p) {
            var icon = Icons.call_end;
            Color bgColor = red;
            Color iconColor = white;

            if (p == 0) {
              icon = muted ? Icons.mic_off : Icons.mic;
              bgColor = white;
              iconColor = black;
            }

            if (p == 1) {
              icon = Icons.chat;
              bgColor = white;
              iconColor = black;
            }

            if (p == 3) {
              icon = isVideoCall ? Icons.videocam_off : Icons.videocam;
              bgColor = white;
              iconColor = black;
            }
            if (p == 4) {
              icon = Icons.switch_camera;
              bgColor = white;
              iconColor = black;
            }
            return Flexible(
              child: ShakeAnimatedWidget(
                enabled: false,
                duration: Duration(milliseconds: 1500),
                shakeAngle: Rotation.deg(
                  z: 40,
                ),
                curve: Curves.linear,
                child: Container(
                  margin: EdgeInsets.all(5),
                  height: 60,
                  width: 60,
                  child: MaterialButton(
                    onPressed: () {
                      if (p == 0) {
                        onToggleMute();
                        return;
                      }
                      if (p == 1) {
                        return;
                      }
                      if (p == 2) {
                        hangUpCall();
                        return;
                      }
                      if (p == 3) {
                        onSwitchCall();
                        return;
                      }
                      if (p == 4) {
                        onSwitchCamera();
                        return;
                      }
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Icon(
                        icon,
                        color: iconColor,
                      ),
                    ),
                    color: bgColor,
                    padding: EdgeInsets.all(0),
                    //materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  /// Info panel to show logs
  Widget infoPanel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 48),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: _infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              if (_infoStrings.isEmpty) {
                return null;
              }
              return Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 3,
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 5,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.yellowAccent,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          _infoStrings[index],
                          style: TextStyle(color: Colors.blueGrey),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  /// Profile Panel of the other user
  Widget panelTop() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        //alignment: Alignment.topCenter,
        padding: EdgeInsets.only(top: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              //mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,

              children: [
                Image.asset(
                  ic_launcher,
                  height: 30,
                  width: 30,
                  fit: BoxFit.cover,
                ),
                addSpaceWidth(10),
                Text(
                  '${AppConfig.appName.toUpperCase()} VOICE CALL',
                  style: textStyle(false, 14, white.withOpacity(.7)),
                ),
              ],
            ),
            addSpace(5),
          ],
        ),
      ),
    );
  }

  Widget PanelCenter() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            userImageItem(context, callerModel, size: 180, strokeSize: 1),
            addSpace(10),
            Text(
              callerModel.getString(NAME),
              style: textStyle(true, 25, white),
            ),
            //addSpace(5),
            Text(
              callStatusText(),
              style: textStyle(false, 16, white.withOpacity(.7)),
            )
          ],
        ),
      ),
    );
  }

  callStatusText() {
//    return timeText;
    String status = callModel.getString(CALL_STATUS);
    bool online = callerModel.getBoolean(IS_ONLINE);
    if (status == CALL_STATUS_RINGING) return online ? "RINGING" : "CALLING";
    if (status == CALL_STATUS_ENDED) return "ENDED";
    if (status == CALL_STATUS_DECLINED) return "DECLINED";
    return timeText;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      body: Stack(
        children: <Widget>[
          if (isVideoCall)
            videoPanel()
          else
            Container(
              color: AppConfig.appColor,
            ),
          //infoPanel(),
          GestureDetector(
            onTap: () {
              if (!callAnswered) return;
              hideViews = false;
              setState(() {});
              autoHidePanels();
            },
            child: Container(
              color: transparent,
            ),
          ),

//          if (hideViews) ...[toolPanel(), profilePanel()]
          ...[
            toolPanel(), panelTop(), PanelCenter(),
            //infoPanel()
          ],
        ],
      ),
    );
  }
}
