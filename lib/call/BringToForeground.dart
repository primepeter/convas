import 'package:flutter/services.dart';

class BringToForeground {
  static const MethodChannel _channel = const MethodChannel('channel.john');

  static void bringAppToForeground() {
    _channel.invokeMethod("bringAppToForeground");
  }
}
