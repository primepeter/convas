import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/AppPermissions.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/call/CallScreen.dart';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:animated_widgets/animated_widgets.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_incall/flutter_incall.dart';

class PickUpScreen extends StatefulWidget {
  //final bool calling;
  final bool isVideoCall;
  final BaseModel callIdModel;
  final BaseModel callerModel;
  const PickUpScreen({
    //this.calling = true,
    this.isVideoCall = false,
    this.callIdModel,
    this.callerModel,
  });

  @override
  _PickUpScreenState createState() => _PickUpScreenState();
}

class _PickUpScreenState extends State<PickUpScreen> {
  static final _users = <int>[];
  final _infoStrings = <String>[];
  bool muted = false;
  bool isVideoCall = false;
  static const APP_ID = "40c37c3e06f840449394b70b93b177d5";
  BaseModel callModel;
  BaseModel callerModel;
  bool callAnswered = false;

  List<StreamSubscription> subs = [];

  @override
  void dispose() {
    _users.clear();
    for (var sub in subs) sub?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    callModel = widget.callIdModel;
    callerModel = widget.callerModel;
    isVideoCall = widget.isVideoCall;
    startRinging();
    listenToUser();
    listenToCall();
    initialize();
  }

  IncallManager callManager = IncallManager();
  startRinging() {
    callManager.startRingtone(RingtoneUriType.DEFAULT, 'default', 30);
    var callSubs = hangUpController.stream.listen((bool p) {
      if (p) onCallDeclined();
    });
    subs.add(callSubs);
  }

  void onCallDeclined() async {
    await callManager.stopRingtone();
    await callManager.stop();
    callModel
      ..put(STATUS, CALL_STATUS_DECLINED)
      ..updateItems();
    Future.delayed(Duration(milliseconds: 60), () {
      Navigator.pop(context);
    });
  }

  void onCallAnswered() async {
    bool granted = await AppPermissions.cameraAndMicrophonePermissionsGranted();
    if (!granted) return;
    await callManager.stopRingtone();
    await callManager.stop();
    Future.delayed(Duration(milliseconds: 10), () {
      pushReplaceAndResult(
          context,
          CallScreen(
            inComing: true,
            callerModel: callerModel,
            callModel: callModel,
            isVideoCall: isVideoCall,
          ),
          depend: false);
    });
  }

  listenToUser() async {
    Future.delayed(Duration(milliseconds: 15), () {
      var sub = Firestore.instance
          .collection(USER_BASE)
          .document(callerModel.getUserId())
          .snapshots()
          .listen((doc) {
        callerModel = BaseModel(doc: doc);
        setState(() {});
      });
      subs.add(sub);
    });
  }

  listenToCall() async {
    Future.delayed(Duration(milliseconds: 15), () {
      var sub = Firestore.instance
          .collection(CALLS_IDS_BASE)
          .document(callModel.getObjectId())
          .snapshots()
          .listen((doc) {
        if (!doc.exists) {
          onCallDeclined();
          return;
        }
        callModel = BaseModel(doc: doc);
        final status = callModel.getString(STATUS);
        bool callDeclined = status == CALL_STATUS_DECLINED;
        callAnswered = status == CALL_STATUS_ACCEPTED;
        if (callDeclined) {
          onCallDeclined();
          return;
        }

        setState(() {});
      });
      subs.add(sub);
    });
  }

  bool _joined = false;
  int _remoteUid = null;
  bool _switch = false;

  initialize() async {
    if (APP_ID.isEmpty) {
      setState(() {
        _infoStrings.add(
          'APP_ID missing, please provide your APP_ID in settings.dart',
        );
        _infoStrings.add('Agora Engine is not starting');
      });
      return;
    }

    await _initAgoraRtcEngine();
    //_addAgoraEventHandlers();
    await AgoraRtcEngine.enableWebSdkInteroperability(true);
    await AgoraRtcEngine.setParameters(
        '''{\"che.video.lowBitRateStreamParameter\":{\"width\":320,\"height\":180,\"frameRate\":15,\"bitRate\":140}}''');
  }

  /// Create agora sdk instance and initialize
  _initAgoraRtcEngine() async {
    await AgoraRtcEngine.create(APP_ID);
    await AgoraRtcEngine.disableAudio();
    if (isVideoCall) await AgoraRtcEngine.enableVideo();
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<AgoraRenderWidget> list = [
      AgoraRenderWidget(0, local: true, preview: true),
    ];
    _users.forEach((int uid) => list.add(AgoraRenderWidget(uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Video layout wrapper
  Widget videoPanel() {
    final views = _getRenderViews();
    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow([views[0]]),
            _expandedVideoRow([views[1]])
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container();
  }

  /// Toolbar layout
  Widget toolPanel() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: EdgeInsets.all(15),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: white.withOpacity(.09),
            borderRadius: BorderRadius.circular(15)),
        child: Row(
          //mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(2, (p) {
            var icon = Icons.call;
            Color bgColor = green;

            if (p == 1) {
              icon = Icons.call_end;
              bgColor = red;
            }

            return Flexible(
              child: ShakeAnimatedWidget(
                enabled: p == 0,
                duration: Duration(milliseconds: 1500),
                shakeAngle: Rotation.deg(
                  z: 40,
                ),
                curve: Curves.linear,
                child: Container(
                  margin: EdgeInsets.all(5),
                  height: 80,
                  width: 80,
                  child: MaterialButton(
                    onPressed: () {
                      if (p == 0) onCallAnswered();
                      if (p == 1) onCallDeclined();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      child: Icon(
                        icon,
                        color: white,
                        size: 30,
                      ),
                    ),
                    color: bgColor,
                    padding: EdgeInsets.all(0),
                    shape: CircleBorder(),
//                    shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }

  /// Info panel to show logs
  Widget infoPanel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 48),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: _infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              if (_infoStrings.isEmpty) {
                return null;
              }
              return Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 3,
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 5,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.yellowAccent,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          _infoStrings[index],
                          style: TextStyle(color: Colors.blueGrey),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  /// Profile Panel of the other user
  Widget profilePanel() {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        //alignment: Alignment.topCenter,
        padding: EdgeInsets.only(top: 40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              //mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,

              children: [
                Image.asset(
                  ic_launcher,
                  height: 30,
                  width: 30,
                  fit: BoxFit.cover,
                ),
                addSpaceWidth(10),
                Text(
                  '${AppConfig.appName.toUpperCase()} VOICE CALL',
                  style: textStyle(false, 14, white.withOpacity(.7)),
                ),
              ],
            ),
            addSpace(5),
            userImageItem(context, callerModel, size: 80, strokeSize: 1),
            addSpace(10),
            Text(
              callerModel.getString(NAME),
              style: textStyle(true, 25, white),
            ),
            //addSpace(5),
            Text(
              "INCOMING CALL",
              style: textStyle(false, 16, white.withOpacity(.7)),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      body: Stack(
        children: <Widget>[
          if (isVideoCall)
            videoPanel()
          else
            Container(
              color: AppConfig.appColor,
            ),
          //infoPanel(),
          toolPanel(),
          profilePanel()
        ],
      ),
    );
  }
}
