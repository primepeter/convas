import 'dart:async';
import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/notificationService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_incall/flutter_incall.dart';
import 'package:video_player/video_player.dart';

import 'CallScreen.dart';
import 'PickUpScreen.dart';

bool onCall = false;

class CallEngine {
  BaseModel callModel;
  BaseModel callerModel;
  bool isVideoCall;
  String callId;
  List<StreamSubscription> subs = List();
  static CallEngine get instance => CallEngine();
  BuildContext context;

  IncallManager callManager = new IncallManager();
  VideoPlayerController beepController;

  initCallEngine(BuildContext context, {@required callBack}) {
    this.context = context;

    var sub = Firestore.instance
        .collection(CALL_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .orderBy(TIME, descending: true)
        .snapshots()
        .listen((shots) {
      for (var changes in shots.documentChanges) {
        BaseModel model = BaseModel(doc: changes.document);
        String callId = model.getObjectId();
        int p = callsList.indexWhere((e) => e.getObjectId() == callId);

        if (changes.type == DocumentChangeType.removed) {
          callsList.removeWhere((e) => e.getObjectId() == callId);
          callBack();
          continue;
        }
        String otherPersonId = getOtherPersonId(model);
        loadOtherPerson(otherPersonId, 0, callBack: () {
          if (null != callBack) callBack();
        });
        if (p != -1) {
          callsList[p] = model;
        } else {
          callsList.add(model);
        }
      }
      callsSetup2 = true;
      if (null != callBack) callBack();
    });

    if (appSettingsModel.getBoolean(FREEZE_IN_APP_CALL)) return;

    var sub1 = Firestore.instance
        .collection(CALLS_IDS_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      for (var changes in shots.documentChanges) {
        callModel = BaseModel(doc: changes.document);
        final status = callModel.getString(CALL_STATUS);
        bool callDeclined = status == CALL_STATUS_DECLINED;
        bool callAnswered = status == CALL_STATUS_ACCEPTED;
        bool myItem = callModel.myItem();
        String otherPersonId = getOtherPersonId(callModel);
        loadOtherPerson(otherPersonId, 0, callBack: () {
          if (null != callBack) callBack();
        });
        BaseModel otherPerson = otherPersonInfo[otherPersonId];
        callerModel = otherPerson;

        if (changes.type == DocumentChangeType.removed) {
          if (!callModel.myItem() && onCall) hangUpController.add(true);
          endCall();
          continue;
        }

        if (callDeclined && !myItem) continue;
        if (callDeclined && myItem) {
          endCall();
          hangUpController.add(true);
          continue;
        }
        if (myItem) continue;
        //if (Platform.isIOS) continue;
        if (callAnswered) {
          callAnsweredController.add(true);
          continue;
        }
        String callId = callModel.getObjectId();
        if (userModel.getList(BLOCKED).contains(callId)) continue;

        Future.delayed(Duration(milliseconds: 15), () {
          if (null == otherPerson) return;
          if (onCall) return;
          onCall = true;
          pushAndResult(
              context,
              PickUpScreen(
                callIdModel: callModel,
                callerModel: otherPerson,
                isVideoCall: callModel.getBoolean(CALL_MODE),
              ),
              depend: false);
        });
      }
      callsSetup = true;
      if (null != callBack) callBack();
    });
    subs.add(sub1);
    subs.add(sub);
  }

  disposeCallEngine() {
    //beepController?.dispose();
    //callManager?.stop();
    for (var s in subs) s?.cancel();
  }

  endCall() {
    onCall = false;
    if (Platform.isIOS) return;
    userModel
      ..put(CALLING_ID, "")
      ..updateItems();
    callerModel
      ..put(CALLING_ID, "")
      ..updateItems();
    Future.delayed(Duration(milliseconds: 10), () {
      callModel.deleteItem();
    });
  }

  rejectCall() {}

  pickCall() {}

  placeCall({
    @required BaseModel callerModel,
    @required bool isVideoCall,
    Function onCallPlaced,
  }) {
    this.callerModel = callerModel;
    callId = getRandomId();
    Map data = Map();
    data[TYPE] = PUSH_TYPE_INCOMING_CALL;
    data[OBJECT_ID] = callId;
    data[TITLE] = getFullName(userModel);
    data[MESSAGE] = "Incoming Call";
    data[CALL_MODE] = isVideoCall;

    bool deviceIsIOS = callerModel.getInt(PLATFORM) == IOS;

    // if (deviceIsIOS)
    //   pushCallIOS(
    //       callId: callId,
    //       callerName: userModel.getString(NAME),
    //       apnToken: callerModel.getString(CALL_TOKEN),
    //       isVideo: isVideoCall);
    // else
    NotificationService.sendPush(
        token: callerModel.getString(TOKEN),
        title: getFullName(userModel),
        body: "Incoming Call ${isVideoCall ? "(Video)" : ""}",
        tag: '${callId}chat',
        data: data);
    callModel = BaseModel();
    callModel.put(PARTIES, [userModel.getUserId(), callerModel.getUserId()]);
    callModel.put(OBJECT_ID, callId);
    callModel.put(CALL_STATUS, CALL_STATUS_RINGING);
    callModel.put(CALL_MODE, isVideoCall);
    callModel.saveItem(CALLS_IDS_BASE, true, document: callId);
    callerModel
      ..put(CALLING_ID, callId)
      ..updateItems();
    userModel
      ..put(CALLING_ID, callId)
      ..updateItems();
    if (null != onCallPlaced) onCallPlaced(callModel);
    pushAndResult(
        context,
        CallScreen(
          callerModel: callerModel,
          callModel: callModel,
          isVideoCall: isVideoCall,
        ),
        depend: false);
  }

  pushCallIOS(
      {String callId, String callerName, String apnToken, bool isVideo}) async {
    print("Before call callId $callId caller $callerName video $isVideo");
    if (appSettingsModel.getBoolean(FREEZE_IN_APP_CALL)) return;

    Dio().post("https://us-central1-convasapp.cloudfunctions.net/makeCall",
        data: {
          "data": {
            "callToken": "$apnToken",
            "callId": "$callId",
            "callerId": "${userModel.getUserId()}",
            "callerName": "$callerName",
            "isVideo": "$isVideo"
          },
        }).then((response) {
      print("call rep callId $callId caller $callerName video $isVideo");

      print("call resp ${response.data}");
    }).catchError((e) {
      print("call error callId $callId caller $callerName video $isVideo");

      print("call error $e");
    });
  }
}
