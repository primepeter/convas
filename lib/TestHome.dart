import 'package:Strokes/AppEngine.dart';
import 'package:flutter/material.dart';

import 'assets.dart';

class TestHome extends StatefulWidget {
  @override
  _TestHomeState createState() => _TestHomeState();
}

class _TestHomeState extends State<TestHome>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 4, vsync: this);
    tabController.addListener(() {
      print(tabController.offset);
    });
  }

  List tabTitles = [
    Icons.camera,
    "CHATS",
    "STATUS",
    "CALLS",
  ];

  topBar() {
    return AppBar(
      title: Text("WhatsApp"),
      centerTitle: false,
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.search),
        ),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.more_vert),
        ),
      ],
      bottom: tabX(),
    );
  }

  tabX() {
    final Animation<double> animation = CurvedAnimation(
      parent: tabController.animation,
      curve: Curves.fastOutSlowIn,
    );

    final Color fixColor = Colors.transparent;
    final Color fixSelectedColor = white; //Theme.of(context).accentColor;
    final ColorTween selectedColorTween =
        ColorTween(begin: fixColor, end: fixSelectedColor);
    final ColorTween previousColorTween =
        ColorTween(begin: fixSelectedColor, end: fixColor);

    return PreferredSize(
      preferredSize: Size.fromHeight(60),
      child: AnimatedBuilder(
          animation: animation,
          builder: (ctx, c) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(tabTitles.length, (index) {
                final size = getScreenWidth(context) / tabTitles.length;
                return _buildTabIndicator(index, tabController,
                    selectedColorTween, previousColorTween, size);
              }),
              /*children: tabTitles.map((e) {
                if (e is IconData)
                  return Container(
                    width: 60,
                    height: 50,
                    child: Icon(
                      e,
                      color: Colors.white,
                    ),
                  );

                return Flexible(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 4,
                    child: Text(
                      e,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                );
              }).toList(),*/
            );
          }),
    );
  }

  Widget _buildTabIndicator(
      int tabIndex,
      TabController tabController,
      ColorTween selectedColorTween,
      ColorTween previousColorTween,
      double indicatorSize) {
    Color background;
    if (tabController.indexIsChanging) {
      // The selection's animation is animating from previousValue to value.
      final double t = 1.0 - _indexChangeProgress(tabController);
      if (tabController.index == tabIndex)
        background = selectedColorTween.lerp(t);
      else if (tabController.previousIndex == tabIndex)
        background = previousColorTween.lerp(t);
      else
        background = selectedColorTween.begin;
    } else {
      // The selection's offset reflects how far the TabBarView has / been dragged
      // to the previous page (-1.0 to 0.0) or the next page (0.0 to 1.0).
      final double offset = tabController.offset;
      if (tabController.index == tabIndex) {
        background = selectedColorTween.lerp(1.0 - offset.abs());
      } else if (tabController.index == tabIndex - 1 && offset > 0.0) {
        background = selectedColorTween.lerp(offset);
      } else if (tabController.index == tabIndex + 1 && offset < 0.0) {
        background = selectedColorTween.lerp(-offset);
      } else {
        background = selectedColorTween.begin;
      }
    }
    return Flexible(
      child: Container(
        color: background,
        width: indicatorSize,
        height: 5,
      ),
    );
    return TabPageSelectorIndicator(
      backgroundColor: background,
      borderColor: selectedColorTween.end,
      size: indicatorSize,
    );
  }

  double _indexChangeProgress(TabController controller) {
    final double controllerValue = controller.animation.value;
    final double previousIndex = controller.previousIndex.toDouble();
    final double currentIndex = controller.index.toDouble();

    // The controller's offset is changing because the user is dragging the
    // TabBarView's PageView to the left or right.
    if (!controller.indexIsChanging)
      return (currentIndex - controllerValue).abs().clamp(0.0, 1.0) as double;

    // The TabController animation's value is changing from previousIndex to currentIndex.
    return (controllerValue - currentIndex).abs() /
        (currentIndex - previousIndex).abs();
  }

  pageBody() {
    return TabBarView(
      controller: tabController,
      children: List.generate(tabController.length, (p) {
        String value = tabTitles[p] is IconData ? "Camera" : tabTitles[p];
        return Container(
          alignment: Alignment.center,
          child: Text(
            value,
            style: TextStyle(fontSize: 40),
          ),
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: topBar(),
      body: pageBody(),
    );
  }
}
