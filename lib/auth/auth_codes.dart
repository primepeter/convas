import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app/dotsIndicator.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:flutter/material.dart';

import 'auth_fail_code.dart';
import 'auth_otp.dart';
import 'auth_pin.dart';
import 'auth_profile.dart';

final pc = PageController();
int currentPage = 0;
final nameController = TextEditingController();
String phoneNumber = '';
String email = '';

// bool oldUser = false;

class AuthCodes extends StatefulWidget {
  final String email;
  final String phoneNumber;

  const AuthCodes({Key key, this.phoneNumber, this.email}) : super(key: key);
  @override
  _AuthCodesState createState() => _AuthCodesState();
}

class _AuthCodesState extends State<AuthCodes> {
  @override
  void initState() {
    super.initState();
    phoneNumber = widget.phoneNumber;
    email = widget.email;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          addSpace(40),
          BackButton(),
          Padding(
            padding: const EdgeInsets.only(left: 25),
            child: Container(
              padding: EdgeInsets.all(1),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: black.withOpacity(.7)),
              child: DotsIndicator(
                dotsCount: 4,
                position: currentPage,
                decorator: DotsDecorator(
                  size: const Size.square(5.0),
                  color: white,
                  activeColor: AppConfig.appColor,
                  activeSize: const Size(10.0, 7.0),
                  activeShape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)),
                ),
              ),
            ),
          ),
          Flexible(
              child: PageView(
            controller: pc,
            physics: NeverScrollableScrollPhysics(),
            onPageChanged: (p) {
              setState(() {
                currentPage = p;
              });
            },
            children: [AuthOtp(), AuthProfile(), AuthPin(), AuthFailCode()],
          ))
        ],
      ),
    );
  }
}
