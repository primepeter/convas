import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/app/navigation.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'auth_codes.dart';

class AuthFailCode extends StatefulWidget {
  @override
  _AuthFailCodeState createState() => _AuthFailCodeState();
}

class _AuthFailCodeState extends State<AuthFailCode>
    with AutomaticKeepAliveClientMixin {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final failCodeController = TextEditingController();
  final errorController = StreamController<ErrorAnimationType>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      failCodeController.text = userModel.getString(FAILED_CODE_PIN);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(backgroundColor: white, key: scaffoldKey, body: page1());
  }

  page1() {
    return ListView(
      padding: EdgeInsets.all(25),
      children: [
        Text.rich(TextSpan(children: [
          TextSpan(
              text: "Please enter your ", style: textStyle(false, 25, black)),
          TextSpan(text: "Fail Code Pin ", style: textStyle(true, 25, black)),
          TextSpan(text: "for ", style: textStyle(false, 25, black)),
          TextSpan(text: "Encrypted Chats", style: textStyle(true, 25, black)),
        ])),
        addSpace(10),
        Container(
          //padding: const EdgeInsets.all(25),
          decoration: BoxDecoration(),
          alignment: Alignment.center,
          child: Text(
            "Note: This code will be used to delete your encrypted chats in cases of duress. You cannot use the same code used for your pin code",
            textAlign: TextAlign.left,
            style: textStyle(true, 16, black.withOpacity(0.7)),
          ),
        ),
        addSpace(20),
        Container(
//          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            //color: black.withOpacity(.05),
            borderRadius: BorderRadius.circular(10),
          ),
          child: PinCodeTextField(
            length: 4,
            obsecureText: false,
            animationType: AnimationType.fade,
            textInputType: TextInputType.number,
            autoDisposeControllers: false,
            errorAnimationController: errorController,
            pinTheme: PinTheme(
                shape: PinCodeFieldShape.circle,
                borderRadius: BorderRadius.circular(25),
                fieldHeight: 40,
                fieldWidth: 40,
                inactiveFillColor: black.withOpacity(.3),
                disabledColor: black.withOpacity(.02),
                inactiveColor: black.withOpacity(.02),
                selectedColor: black.withOpacity(.02),
                activeFillColor: black.withOpacity(.02),
                activeColor: black.withOpacity(.02),
                selectedFillColor: black.withOpacity(.5)),
            animationDuration: Duration(milliseconds: 300),
            backgroundColor: transparent,
            enableActiveFill: true,
            controller: failCodeController,
            onCompleted: (v) {},
            onChanged: (value) {},
            beforeTextPaste: (text) {
              return true;
            },
          ),
        ),
        addSpace(20),
        Container(
          height: 50,
          child: FlatButton(
              onPressed: () {
                bool isInValid =
                    failCodeController.text == userModel.getString(CODE_PIN);
                if (failCodeController.text.isEmpty) {
                  toast(scaffoldKey, "Please enter your fail code pin!");
                  errorController.add(ErrorAnimationType.shake);

                  return;
                }
                if (isInValid) {
                  toast(scaffoldKey,
                      "Please enter you cannot use your pin code as your fail code pin!");
                  failCodeController.clear();
                  errorController.add(ErrorAnimationType.shake);
                  return;
                }

                showProgress(true, context, msg: "Saving Profile...");
                userModel
                  ..put(FAILED_CODE_PIN, failCodeController.text)
                  ..put(SIGNUP_COMPLETED, true)
                  ..updateItems();
                Future.delayed(Duration(seconds: 3), () {
                  popUpUntil(context, MainAdmin());
                });
              },
              padding: EdgeInsets.all(15),
              color: AppConfig.appColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Center(
                  child: Text("FINISH", style: textStyle(true, 16, white)))),
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
