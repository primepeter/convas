import 'dart:io';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'auth_codes.dart';

class AuthProfile extends StatefulWidget {
  @override
  _AuthProfileState createState() => _AuthProfileState();
}

class _AuthProfileState extends State<AuthProfile>
    with AutomaticKeepAliveClientMixin {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameController.text = userModel.getString(NAME);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(backgroundColor: white, key: scaffoldKey, body: page1());
  }

  page1() {
    return ListView(
      padding: EdgeInsets.all(25),
      children: [
        Text.rich(TextSpan(children: [
          TextSpan(text: "Please setup ", style: textStyle(false, 25, black)),
          TextSpan(text: "Your Profile ", style: textStyle(true, 25, black)),
          TextSpan(text: "for ", style: textStyle(false, 25, black)),
          TextSpan(text: "Convas", style: textStyle(true, 25, black)),
        ])),
        addSpace(30),
        Container(
          decoration: BoxDecoration(
              color: black.withOpacity(.05),
              border: Border.all(color: black.withOpacity(.0)),
              borderRadius: BorderRadius.circular(25)),
//          padding: EdgeInsets.all(8),
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: TextField(
            controller: nameController,
            style: textStyle(false, 20, black),
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
                //contentPadding: EdgeInsets.all(0),
                suffixIcon: SizedBox(
                    //width: 50,
                    child: Center(widthFactor: 0.0, child: Icon(Icons.info))),
                isDense: true,
                hintText: "Enter a display name",
                border: InputBorder.none),
            maxLines: 1,
          ),
        ),
        addSpace(30),
        GestureDetector(
          onTap: () {
            getSingleCroppedImage(context, onPicked: (_) {
              userModel
                ..put(USER_IMAGE, _)
                ..updateItems();
              setState(() {});
            });
          },
          child: Container(
            height: 200,
            width: double.infinity,
//            decoration: BoxDecoration(
//              color: black.withOpacity(.05),
//              borderRadius: BorderRadius.circular(10),
//            ),
            child: Card(
              clipBehavior: Clip.antiAlias,
              elevation: 0,
              color: default_white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Stack(
                children: [
                  if (!userModel.userImage.contains("http") ||
                      userModel.userImage.isEmpty)
                    Container(
//                    padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      child: userModel.userImage.isEmpty
                          ? Icon(
                              Icons.person,
                              size: 60,
                              color: black.withOpacity(.2),
                            )
                          : Image.file(
                              File(userModel.userImage),
                              fit: BoxFit.cover,
                              width: double.infinity,
                              height: 200,
                            ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    )
                  else
                    CachedNetworkImage(
                      imageUrl: userModel.userImage,
                      alignment: Alignment.topCenter,
                      width: double.infinity,
                      height: 200,
                      fit: BoxFit.cover,
                      placeholder: (c, s) {
                        return placeHolder(
                          200,
                          width: double.infinity,
                        );
                      },
                    )
                ],
              ),
            ),
          ),
        ),
        addSpace(40),
        Container(
          height: 50,
          child: FlatButton(
              onPressed: () {
                if (nameController.text.isEmpty) {
                  toast(scaffoldKey, "Please enter your display name!");
                  return;
                }
                // if (userModel.userImage.isEmpty) {
                //   toast(scaffoldKey, "Please add a display image");
                //   return;
                // }
                userModel
                  ..put(NAME, nameController.text)
                  ..updateItems();
                pc.animateToPage(2,
                    duration: Duration(milliseconds: 100),
                    curve: Curves.easeInToLinear);
              },
              color: AppConfig.appColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Center(
                  child: Text("CONTINUE", style: textStyle(true, 16, white)))),
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
