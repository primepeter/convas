import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app/app.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:masked_controller/mask.dart';
import 'package:masked_controller/masked_controller.dart';
import 'package:otp/otp.dart';
import 'package:twilio_flutter/twilio_flutter.dart';

import '../basemodel.dart';
import 'auth_codes.dart';

const String TWILIO_SSID = 'ACfd192b68b73bdbeb942d15d78787ea16';
const String TWILIO_TOKEN = '8a4c61213db6d9fd1471e57533dcdcbe';

TwilioFlutter twilioFlutter = TwilioFlutter(
    accountSid:
        "ACfd192b68b73bdbeb942d15d78787ea16", // replace *** with Account SID
    authToken:
        "8a4c61213db6d9fd1471e57533dcdcbe", // replace xxx with Auth Token
    twilioNumber: '+12057406266' // replace .... with Twilio Number
    );

String currentOTP = '';

String generateOTP() {
  return OTP.generateTOTPCodeString(
      "JBSWY3DPEHPK3PXP", DateTime.now().millisecondsSinceEpoch);
  //return DateTime.now().millisecondsSinceEpoch.toString().substring(0, 6);
}

class AuthPhoneNumber extends StatefulWidget {
  @override
  _AuthPhoneNumberState createState() => _AuthPhoneNumberState();
}

class _AuthPhoneNumberState extends State<AuthPhoneNumber> {
  final emailController = TextEditingController();
  final mobileController = MaskedController(mask: Mask(mask: 'NNN-NNN-NNNNN'));

  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Stack(
        children: [
          Container(
            height: 250,
            color: AppConfig.appColor,
            padding: EdgeInsets.all(25),
            alignment: Alignment.center,
            child: Text.rich(TextSpan(children: [
              TextSpan(
                  text: "ConvasApp will send an ",
                  style: textStyle(false, 25, white)),
              TextSpan(text: "SMS Code ", style: textStyle(true, 25, white)),
              TextSpan(text: "to Verify ", style: textStyle(false, 25, white)),
              TextSpan(
                  text: "Your Phone Number", style: textStyle(true, 25, white)),
            ])),
          ),
          page()
        ],
      ),
    );
  }

  page() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 200, right: 20, left: 20, bottom: 0),
            decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(25),
                boxShadow: [
                  BoxShadow(blurRadius: 4, color: black.withOpacity(.3))
                ]),
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(15),
                          child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              addSpace(10),
//                    Text.rich(TextSpan(children: [
//                      TextSpan(text: "Login ", style: textStyle(true, 18, black.withOpacity(.5))),
//                      TextSpan(
//                          text: "with your phone number",
//                          style: textStyle(false, 18, black.withOpacity(.5)))
//                    ])),
                              Text(
                                "Your Phone Number",
                                style: textStyle(true, 20, AppConfig.appColor),
                              ),
                              addSpace(20),
                              phoneInput(),
                              addSpace(10),
                              emailInput(),
                              addSpace(10),
                            ],
                          ),
                        ),
                        FlatButton(
                            onPressed: () {
                              verifyPhoneNumber();
                            },
                            padding: EdgeInsets.all(15),
                            color: AppConfig.appColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(25),
                              bottomLeft: Radius.circular(25),
                            )),
                            child: Center(
                                child: Text("NEXT",
                                    style: textStyle(true, 16, white))))
                      ])
                ]),
          ),
          Container(
            padding: const EdgeInsets.all(25),
            decoration: BoxDecoration(),
            alignment: Alignment.center,
            child: Text(
              "ConvasApp will not share your number with any third-Party",
              textAlign: TextAlign.center,
              style: textStyle(true, 14, black.withOpacity(0.4)),
            ),
          ),
        ],
      ),
    );
  }

  phoneInput() {
    return Container(
        height: 60,
        decoration: BoxDecoration(
            color: black.withOpacity(.02),
            border: Border.all(color: black.withOpacity(.1)),
            borderRadius: BorderRadius.circular(25)),
        padding: EdgeInsets.all(8),
        child: Row(
          children: [
            GestureDetector(
              onTap: () {
                pushAndResult(context, CountryChooser(), result: (Countries _) {
                  setState(() {
                    country = _;
                  });
                }, depend: false);
              },
              child: Row(children: [
                addSpaceWidth(5),
                Image.asset(country.countryFlag, height: 20, width: 20),
                Icon(
                  Icons.keyboard_arrow_down,
                  color: black.withOpacity(.4),
                ),
              ]),
            ),
            Flexible(
                child: TextField(
              controller: mobileController,
              style: textStyle(false, 23, black),
              textAlignVertical: TextAlignVertical.center,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  hintStyle: textStyle(false, 18, black.withOpacity(.4)),
                  hintText: "708-888-3333",
                  prefixIcon: SizedBox(
                      //width: 50,
                      child: Center(
                          widthFactor: 0.0,
                          child: Text(country.countryCode,
                              style: textStyle(true, 18, black)))),
                  isDense: true,
                  border: InputBorder.none),
            ))
          ],
        ));
  }

  emailInput() {
    return Container(
        height: 60,
        decoration: BoxDecoration(
            color: black.withOpacity(.02),
            border: Border.all(color: black.withOpacity(.1)),
            borderRadius: BorderRadius.circular(25)),
        padding: EdgeInsets.all(8),
        child: TextField(
          controller: emailController,
          style: textStyle(false, 23, black),
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
              contentPadding: EdgeInsets.all(0),
              hintStyle: textStyle(false, 18, black.withOpacity(.4)),
              hintText: "you@email.com",
              prefixIcon: SizedBox(
                  //width: 50,
                  child: Center(widthFactor: 0.0, child: Icon(Icons.email))),
              isDense: true,
              border: InputBorder.none),
        ));
  }

  verifyPhoneNumber() async {
    if (mobileController.text.isEmpty) {
      toast(scaffoldKey, "Please enter your phone number!");
      return;
    }
    if (emailController.text.isEmpty) {
      toast(scaffoldKey, "Please enter your email address!");
      return;
    }
    String number = mobileController.unmaskedText;
    String newNum = number;
    if (number.startsWith("0")) newNum = number.substring(1);
    String phoneNumber = '${country.countryCode}$newNum';
    String emailAddress = emailController.text;
    //showProgress(true, context, msg: "Please wait...", cancellable: true);
    print("Resp valeee");
    FirebaseFirestore.instance
        .collection(USER_BASE)
        .where(PHONE_NUMBER, isEqualTo: phoneNumber)
        .limit(1)
        .get()
        .then((value) {
      print("Resp ${value.docs.length}");

      if (value.docs.isEmpty) {
        showProgress(false, context);
        Future.delayed(Duration(milliseconds: 10), () {
          pushAndResult(
              context,
              AuthCodes(
                email: emailAddress,
                phoneNumber: phoneNumber,
              ));
        });
      } else {
        BaseModel model = BaseModel(doc: value.docs[0]);
        if ((model.getEmail() != emailAddress) && model.getEmail().isNotEmpty) {
          showProgress(false, context);
          showMessage(
              context, Icons.error, red0, "Error", "Invalid Login Credentials",
              delayInMilli: 1500, cancellable: true);
          return;
        }
        showProgress(false, context);
        Future.delayed(Duration(milliseconds: 800), () {
          pushAndResult(
              context,
              AuthCodes(
                email: emailAddress,
                phoneNumber: phoneNumber,
              ));
        });
      }
    }).catchError(onError);
  }

  onError(e) {
    showProgress(false, context);
    showMessage(context, Icons.error, red0, "Error", e.message,
        delayInMilli: 1500, cancellable: true);
  }
}
