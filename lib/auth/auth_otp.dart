import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app/EmailService.dart';
import 'package:Strokes/app/app.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'auth_codes.dart';
import 'auth_phone.dart';

class AuthOtp extends StatefulWidget {
  @override
  _AuthOtpState createState() => _AuthOtpState();
}

class _AuthOtpState extends State<AuthOtp> with AutomaticKeepAliveClientMixin {
  int time = 0;
  String timeText = "";
  int forceResendingToken = 0;
  bool verifying = false;
  String verificationId = "";
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final otpController = TextEditingController();
  final errorController = StreamController<ErrorAnimationType>();
  //final emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(milliseconds: 30), () {
      //showProgress(true, context, msg: "Verifying Number");
      clickVerify();
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(backgroundColor: white, key: scaffoldKey, body: page1());
  }

  page1() {
    return ListView(
      padding: EdgeInsets.all(25),
      children: [
        Text.rich(TextSpan(children: [
          TextSpan(
              text: "Please enter the ", style: textStyle(false, 25, black)),
          TextSpan(text: "Code ", style: textStyle(true, 25, black)),
          TextSpan(text: "to Verify ", style: textStyle(false, 25, black)),
          TextSpan(
              text: "Your Phone Number", style: textStyle(true, 25, black)),
        ])),
        addSpace(15),
        Container(
            padding: EdgeInsets.all(0),
            decoration: BoxDecoration(
              //color: black.withOpacity(.05),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Text(
                "We have sent you an SMS with a code to the number $phoneNumber")),
        addSpace(30),
        Container(
          alignment: Alignment.center,
          child: Opacity(
            opacity: timeText.isEmpty ? 1 : (.5),
            child: Container(
              height: 40,
              //width: 105,
              child: FlatButton(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25),
                      side:
                          BorderSide(color: white.withOpacity(.5), width: .5)),
                  color: AppConfig.appColor,
                  onPressed: () {
                    if (timeText.isEmpty) {
                      showProgress(true, context, msg: "Verifying Number");
                      clickVerify();
                    }

                    pc.animateToPage(0,
                        duration: Duration(milliseconds: 100),
                        curve: Curves.easeInToLinear);
                  },
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "Send a new code",
                        style: textStyle(true, 12, white),
                      ),
                      if (timeText.isNotEmpty)
                        Text(
                          timeText,
                          style: textStyle(false, 10, white),
                        ),
                    ],
                  )),
            ),
          ),
        ),
        addSpace(10),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            //color: black.withOpacity(.05),
            borderRadius: BorderRadius.circular(10),
          ),
          child: PinCodeTextField(
            length: 6,
            obsecureText: false,
            animationType: AnimationType.fade,
            textInputType: TextInputType.number,
            autoDisposeControllers: false,
            errorAnimationController: errorController,
            pinTheme: PinTheme(
                shape: PinCodeFieldShape.circle,
                borderRadius: BorderRadius.circular(25),
                fieldHeight: 40,
                fieldWidth: 40,
                inactiveFillColor: black.withOpacity(.3),
                disabledColor: black.withOpacity(.02),
                inactiveColor: black.withOpacity(.02),
                selectedColor: black.withOpacity(.02),
                activeFillColor: black.withOpacity(.02),
                activeColor: black.withOpacity(.02),
                selectedFillColor: black.withOpacity(.5)),
            animationDuration: Duration(milliseconds: 300),
            backgroundColor: transparent,
            enableActiveFill: true,
            controller: otpController,
            onCompleted: (v) {},
            onChanged: (value) {},
            beforeTextPaste: (text) {
              return true;
            },
          ),
        ),
        addSpace(20),
        FlatButton(
            onPressed: () {
              checkCode();
            },
            padding: EdgeInsets.all(15),
            color: AppConfig.appColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: Center(
                child: Text("VERIFY", style: textStyle(true, 16, white)))),
//        if (timeText.isEmpty) ...[
//          Container(
//              padding: EdgeInsets.all(15),
//              margin: EdgeInsets.all(15),
//              decoration: BoxDecoration(
//                color: red,
//                borderRadius: BorderRadius.circular(10),
//              ),
//              alignment: Alignment.centerLeft,
//              child: Text(
//                "Didnt recieve any OTP?, Enter email to recieve OTP",
//                style: textStyle(false, 14, white),
//              )),
//          addSpace(10),
//          Container(
//            decoration: BoxDecoration(
//                color: black.withOpacity(.05),
//                border: Border.all(color: black.withOpacity(.0)),
//                borderRadius: BorderRadius.circular(25)),
////          padding: EdgeInsets.all(8),
//            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
//            child: TextField(
//              controller: emailController,
//              style: textStyle(false, 20, black),
//              textAlignVertical: TextAlignVertical.center,
//              decoration: InputDecoration(
//                  //contentPadding: EdgeInsets.all(0),
//                  suffixIcon: SizedBox(
//                      //width: 50,
//                      child: Center(widthFactor: 0.0, child: Icon(Icons.info))),
//                  isDense: true,
//                  hintText: "Enter Email",
//                  border: InputBorder.none),
//              maxLines: 1,
//            ),
//          ),
//          addSpace(20),
//          FlatButton(
//              onPressed: () {
//                if (emailController.text.isEmpty) {
//                  toast(scaffoldKey, "Please enter Email Address");
//                  return;
//                }
//                showProgress(true, context, msg: "Processing...");
//                EmailService.sendEmailTemp(currentOTP, emailController.text,
//                    onSent: () {
//                  showProgress(false, context);
//                  createTimer(true);
//                }, onError: (e) {
//                  showMessage(
//                      context, Icons.warning, red0, "OTP ERR", e.message,
//                      delayInMilli: 600);
//                });
//              },
//              padding: EdgeInsets.all(15),
//              color: AppConfig.appColor,
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(25)),
//              child: Center(
//                  child: Text("SEND OTP", style: textStyle(true, 16, white)))),
//        ]
      ],
    );
  }

  clickVerify() {
    currentOTP = generateOTP();
    EmailService.sendEmailTemp(currentOTP, email, onSent: () {
      print(email);
    }, onError: (e) {
      showMessage(context, Icons.warning, red0, "OTP ERR", e.message,
          delayInMilli: 600);
    });
    twilioFlutter
        .sendSMS(
            toNumber: phoneNumber,
            messageBody: "Your ONE-TIME-OTP for ConvasApp is $currentOTP")
        .then((value) {
      createTimer(true);
      //showProgress(false, context);
    });

    // return;
    // verifyNumber(context,
    //     phoneNumber: phoneNumber,
    //     onVerified: startAcctCreation,
    //     codeSent: (s, [x]) {
    //       createTimer(true);
    //       showProgress(false, context);
    //       verificationId = s;
    //       print("verification $s x $x");
    //       forceResendingToken = x;
    //       if (mounted) setState(() {});
    //       showProgress(false, context, cancellable: true);
    //       // pc.nextPage(
    //       //     duration: Duration(milliseconds: 100),
    //       //     curve: Curves.easeInToLinear);
    //     },
    //     codeAutoRetrievalTimeout: (s) {},
    //     onFailed: (e) {
    //       otpController.clear();
    //       showProgress(false, context, cancellable: true);
    //       showMessage(context, Icons.warning, red0, "OTP ERR", e.message,
    //           delayInMilli: 600);
    //     },
    //     onError: (e) {
    //       otpController.clear();
    //       showProgress(false, context, cancellable: true);
    //       showMessage(context, Icons.warning, red0, "OTP ERR", e.message,
    //           delayInMilli: 600);
    //     });
  }

  createTimer(bool create) {
    if (create) {
      time = 60;
    }
    if (time < 0) {
      if (mounted)
        setState(() {
          timeText = "";
        });
      return;
    }
    timeText = getTimeText();
    Future.delayed(Duration(seconds: 1), () {
      time--;
      if (mounted) setState(() {});
      createTimer(false);
    });
  }

  getTimeText() {
    if (time <= 0) return "";
    return "${time >= 60 ? "01" : "00"}:${time % 60}";
  }

  checkCode() async {
    String code = otpController.text.replaceAll(" ", "");
    if (code.isEmpty) {
      toast(scaffoldKey, "Please enter OTP Code!");
      errorController.add(ErrorAnimationType.shake);

      return;
    }

    if (code != currentOTP) {
      otpController.clear();
      showProgress(false, context, cancellable: true);
      showMessage(context, Icons.warning, red0, "OTP ERR",
          "OTP code entered is Invalid.",
          delayInMilli: 600);
      return;
    }
    showProgress(true, context, msg: "Verifying OTP");
    String authKey = phoneNumber + "@convasapp.com";
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: authKey, password: authKey)
        .then((value) {
      Firestore.instance
          .collection(USER_BASE)
          .document(value.user.uid)
          .get()
          .then((doc) {
        userModel = BaseModel(doc: doc);
        showProgress(false, context);
        pc.animateToPage(1,
            duration: Duration(milliseconds: 100),
            curve: Curves.easeInToLinear);
      }).catchError(onError);
    }).catchError((e) {
      if (e.message.contains("no user record")) {
        FirebaseAuth.instance
            .createUserWithEmailAndPassword(email: authKey, password: authKey)
            .then((result) {
          userModel
            ..put(USER_ID, result.user.uid)
            ..put(OBJECT_ID, result.user.uid)
            ..put(COUNTRY, country.countryName)
            ..put(COUNTRY_CODE, country.countryCode)
            ..put(PHONE_NUMBER, phoneNumber)
            ..put(EMAIL, email)
            ..saveItem(USER_BASE, false, document: result.user.uid,
                onComplete: () {
              showProgress(false, context);
              pc.animateToPage(1,
                  duration: Duration(milliseconds: 100),
                  curve: Curves.easeInToLinear);
            });
        });
        return;
      }
      onError(e);
    });

    // return;
    // showProgress(true, context, msg: "Verifying OTP");
    // AuthCredential credential = PhoneAuthProvider.getCredential(
    //     verificationId: verificationId, smsCode: code);
    // await startAcctCreation(credential);
  }

  startAcctCreation(AuthCredential phoneAuthCredential) async {
    FirebaseAuth.instance
        .signInWithCredential(phoneAuthCredential)
        .then((result) {
      Firestore.instance
          .collection(USER_BASE)
          .document(result.user.uid)
          .get()
          .then((doc) {
        // if (doc.exists) {
        //   userModel = BaseModel(doc: doc);
        //   popUpUntil(context, MainAdmin());
        // } else {
        //    userModel = BaseModel(doc: doc);
        //   userModel
        //     ..put(USER_ID, result.user.uid)
        //     ..put(OBJECT_ID, result.user.uid)
        //     ..put(COUNTRY, country.countryName)
        //     ..put(COUNTRY_CODE, country.countryCode)
        //     ..put(PHONE_NUMBER, phoneNumber)
        //     ..saveItem(USER_BASE, false, onComplete: () {
        //       pc.animateToPage(1,
        //           duration: Duration(milliseconds: 100),
        //           curve: Curves.easeInToLinear);
        //     });
        // }
        userModel = BaseModel(doc: doc);
        userModel
          ..put(USER_ID, result.user.uid)
          ..put(OBJECT_ID, result.user.uid)
          ..put(COUNTRY, country.countryName)
          ..put(COUNTRY_CODE, country.countryCode)
          ..put(PHONE_NUMBER, phoneNumber)
          ..saveItem(USER_BASE, false, document: result.user.uid,
              onComplete: () {
            showProgress(false, context);
            pc.animateToPage(1,
                duration: Duration(milliseconds: 100),
                curve: Curves.easeInToLinear);
          });
      }).catchError(onError);
    }).catchError(onError);
  }

  onError(e) {
    showProgress(false, context);
    errorController.add(ErrorAnimationType.shake);
    showMessage(context, Icons.error, red0, "Error", e.message,
        delayInMilli: 950, cancellable: true);
  }

  @override
  bool get wantKeepAlive => true;
}
