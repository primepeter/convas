import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'auth_codes.dart';

class AuthPin extends StatefulWidget {
  @override
  _AuthPinState createState() => _AuthPinState();
}

class _AuthPinState extends State<AuthPin> with AutomaticKeepAliveClientMixin {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final pinController = TextEditingController();
  final errorController = StreamController<ErrorAnimationType>();
  @override
  void initState() {
    super.initState();
    pinController.text = userModel.getString(CODE_PIN);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(backgroundColor: white, key: scaffoldKey, body: page1());
  }

  page1() {
    return ListView(
      padding: EdgeInsets.all(25),
      children: [
        Text.rich(TextSpan(children: [
          TextSpan(
              text: "Please enter your ", style: textStyle(false, 25, black)),
          TextSpan(text: "Pin Code ", style: textStyle(true, 25, black)),
          TextSpan(text: "for ", style: textStyle(false, 25, black)),
          TextSpan(text: "Encrypted Chats", style: textStyle(true, 25, black)),
        ])),
        addSpace(10),
        Container(
          //padding: const EdgeInsets.all(25),
          decoration: BoxDecoration(),
          alignment: Alignment.center,
          child: Text(
            "Note: This code will be used to access your encrypted chat. It is important that you use a code that you can remember",
            textAlign: TextAlign.left,
            style: textStyle(true, 16, black.withOpacity(0.7)),
          ),
        ),
        addSpace(20),
        Container(
//          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            //color: black.withOpacity(.05),
            borderRadius: BorderRadius.circular(10),
          ),
          child: PinCodeTextField(
            length: 4,
            obsecureText: false,
            animationType: AnimationType.fade,
            textInputType: TextInputType.number,
            pinTheme: PinTheme(
                shape: PinCodeFieldShape.circle,
                borderRadius: BorderRadius.circular(25),
                fieldHeight: 40,
                fieldWidth: 40,
                inactiveFillColor: black.withOpacity(.3),
                disabledColor: black.withOpacity(.02),
                inactiveColor: black.withOpacity(.02),
                selectedColor: black.withOpacity(.02),
                activeFillColor: black.withOpacity(.02),
                activeColor: black.withOpacity(.02),
                selectedFillColor: black.withOpacity(.5)),
            animationDuration: Duration(milliseconds: 300),
            backgroundColor: transparent,
            enableActiveFill: true,
            controller: pinController,
            onCompleted: (v) {},
            onChanged: (value) {},
            beforeTextPaste: (text) {
              return true;
            },
          ),
        ),
        addSpace(20),
        Container(
          height: 50,
          child: FlatButton(
              onPressed: () {
                if (pinController.text.isEmpty) {
                  toast(scaffoldKey, "Please enter your code pin!");
                  errorController.add(ErrorAnimationType.shake);
                  return;
                }
                userModel
                  ..put(CODE_PIN, pinController.text)
                  ..updateItems();
                pc.animateToPage(3,
                    duration: Duration(milliseconds: 100),
                    curve: Curves.easeInToLinear);
              },
              padding: EdgeInsets.all(15),
              color: AppConfig.appColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Center(
                  child: Text("CONTINUE", style: textStyle(true, 16, white)))),
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
