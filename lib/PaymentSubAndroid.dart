import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'AppEngine.dart';
import 'app_config.dart';
import 'assets.dart';
import 'basemodel.dart';
import 'main_pages/ShareTo.dart';

class PaymentSubAndroid extends StatefulWidget {
  final bool forAnother;

  const PaymentSubAndroid({Key key, this.forAnother = false}) : super(key: key);
  @override
  _PaymentSubAndroidState createState() => _PaymentSubAndroidState();
}

class _PaymentSubAndroidState extends State<PaymentSubAndroid> {
  PageController pc = PageController();
  int currentPage = 0;
  BaseModel package = appSettingsModel.getModel(FEATURES_PREMIUM);
  BaseModel otherPerson;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      body: Stack(
        children: [
          Container(
            //padding: EdgeInsets.fromLTRB(0, 50, 0, 10),
            alignment: Alignment.topCenter,
            decoration: BoxDecoration(color: AppConfig.appColor),
            height: 150,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            width: 50,
                            height: 30,
                            child: Center(
                                child: Icon(
                              Icons.keyboard_backspace,
                              color: white,
                              size: 20,
                            )),
                          )),
                      GestureDetector(
                        onTap: () {},
                        child: Center(
                            child: Text(
                          "Subscriptions${widget.forAnother ? " (Others)" : ""}",
                          //style: textStyle(true, 20, black)
                          style: textStyle(true, 25, white),
                        )),
                      ),
                      addSpaceWidth(10),
                      Spacer(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 120),
            decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20))),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                  child: Card(
                    color: black.withOpacity(0.2),
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: List.generate(3, (p) {
                        String title = p == 0
                            ? "1 Month"
                            : p == 1 ? "6 Months" : "12 Months";
                        bool selected = p == currentPage;
                        return Flexible(
                          child: GestureDetector(
                            onTap: () {
                              pc.animateToPage(p,
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.ease);
                            },
                            child: Container(
                                margin: EdgeInsets.all(4),
                                height: 30,
                                decoration: !selected
                                    ? null
                                    : BoxDecoration(
                                        color: selected ? white : transparent,
                                        borderRadius:
                                            BorderRadius.circular(25)),
                                child: Center(
                                    child: Text(
                                  title,
                                  style: textStyle(
                                      true,
                                      16,
                                      selected
                                          ? black
                                          : (white.withOpacity(.7))),
                                  textAlign: TextAlign.center,
                                ))),
                          ),
                          fit: FlexFit.tight,
                        );
                      }),
                    ),
                  ),
                ),
                //addSpace(30),
                Flexible(
                  child: PageView.builder(
                      controller: pc,
                      onPageChanged: (p) {
                        setState(() {
                          currentPage = p;
                        });
                      },
                      itemCount: 3,
                      itemBuilder: (ctx, p) {
                        final fee = package.getList(PREMIUM_FEES)[currentPage];
                        final features = package.getString(FEATURES).split("&");

                        return SingleChildScrollView(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              addSpace(20),
                              Center(
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      border: Border.all(
                                          color: black.withOpacity(0.5))),
                                  child: Text.rich(TextSpan(children: [
                                    TextSpan(
                                        text:
                                            //"${currency(context).currencyName}"
                                            "NGN$fee",
                                        style: textStyle(true, 50, black)),
                                    TextSpan(
                                        text: appSettingsModel
                                            .getString(APP_CURRENCY),
                                        style: textStyle(false, 20, black)),
                                  ])),
                                ),
                              ),
//                              Container(
//                                decoration: BoxDecoration(
//                                    color: red,
//                                    borderRadius: BorderRadius.circular(8)),
//                                padding: EdgeInsets.all(10),
//                                margin: EdgeInsets.all(10),
//                                child: Row(
//                                  children: [
//                                    Icon(
//                                      Icons.info,
//                                      color: white,
//                                    ),
//                                    addSpaceWidth(10),
//                                    Flexible(
//                                      child: Text(
//                                        "Note: The Price would be converted to your country's currency at checkout",
//                                        style: textStyle(false, 14, white),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ),
                              Center(
                                child: Text(
                                  "Features",
                                  style: textStyle(true, 23, black),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 15, vertical: 15),
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children:
                                        List.generate(features.length, (p) {
                                      return Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 5, vertical: 5),
                                        padding: EdgeInsets.all(8),
                                        decoration: BoxDecoration(
                                            border: Border(
                                                bottom: BorderSide(
                                                    color: black
                                                        .withOpacity(.05)))),
                                        child: Row(
                                          children: [
                                            Container(
                                              height: 30,
                                              width: 30,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  color: green,
                                                  shape: BoxShape.circle),
                                              child: Icon(
                                                Icons.check,
                                                color: white,
                                              ),
                                            ),
                                            addSpaceWidth(20),
                                            Flexible(
                                              child: Text(features[p],
                                                  style: textStyle(
                                                    false,
                                                    18,
                                                    black.withOpacity(0.6),
                                                  )),
                                            )
                                          ],
                                        ),
                                      );
                                    })),
                              ),

                              if (widget.forAnother)
                                GestureDetector(
                                  onTap: () {
                                    pushAndResult(
                                        context,
                                        ShareTo(
                                          subFor: true,
                                        ),
                                        depend: false, result: (_) {
                                      if (null == _) return;
                                      otherPerson = _;
                                      setState(() {});
                                    });
                                  },
                                  child: Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.all(25),
                                      decoration: BoxDecoration(
                                          color: black.withOpacity(.09),
                                          borderRadius:
                                              BorderRadius.circular(15)),
                                      child: Column(
                                        children: [
                                          if (null == otherPerson)
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                    "Tap to select a user to subscribe for"),
                                                Icon(Icons.navigate_next)
                                              ],
                                            )
                                          else
                                            contactItem()
                                        ],
                                      )),
                                ),

                              Container(
                                padding: EdgeInsets.only(left: 25, right: 25),
                                child: FlatButton(
                                  onPressed: () {
                                    bool forAnother = widget.forAnother;
                                    double balance = userModel.accountBalance -
                                        num.parse(fee);

                                    if (null == otherPerson && forAnother) {
                                      showMessage(
                                        context,
                                        Icons.error,
                                        red0,
                                        "Opps Sorry!",
                                        "You have to select a user you would want to subscribe for on ConvasApp funds",
                                      );
                                      return;
                                    }

                                    if (balance.isNegative) {
                                      showMessage(
                                          context,
                                          Icons.error,
                                          red0,
                                          "Opps Sorry!",
                                          "You have Insufficient funds in your wallet.Add funds into your wallet",
                                          clickYesText: "Fund Wallet",
                                          onClicked: (_) {
                                        if (_) fundWallet(context);
                                      });
                                      return;
                                    }

                                    int months;
                                    if (p == 0) months = 1;
                                    if (p == 1) months = 6;
                                    if (p == 2) months = 12;

                                    final liveMode = appSettingsModel
                                        .getBoolean(RIVE_IS_LIVE);
                                    Duration howLong;
                                    if (liveMode) {
                                      howLong = Duration(days: 30 * months);
                                    } else {
                                      howLong = Duration(minutes: 10);
                                    }
                                    int duration = DateTime.now()
                                        .add(howLong)
                                        .millisecondsSinceEpoch;
                                    print(
                                        "Sub-Expiring in ${DateTime.fromMillisecondsSinceEpoch(duration)}");

                                    if (forAnother) {
                                      int d = DateTime
                                              .fromMillisecondsSinceEpoch(
                                                  otherPerson.getInt(
                                                      SUBSCRIPTION_EXPIRY))
                                          .add(Duration(milliseconds: duration))
                                          //.add(Duration(days: 30 * months))
                                          .millisecondsSinceEpoch;

                                      userModel
                                        ..put(ACCOUNT_BALANCE, balance)
                                        ..updateItems();
                                      otherPerson
                                        ..put(
                                            ACCOUNT_TYPE, ACCOUNT_TYPE_PREMIUM)
                                        ..put(PREMIUM_INDEX, p)
                                        ..put(AMOUNT, num.parse(fee))
                                        ..put(SUBSCRIPTION_EXPIRY, d)
                                        ..updateItems();

                                      String id = getRandomId();
                                      final model = BaseModel();
                                      model
                                        ..put(OBJECT_ID, id)
                                        ..put(PREMIUM_INDEX, p)
                                        ..put(STATUS, APPROVED)
                                        ..put(TYPE, TYPE_WALLET_SUB)
                                        ..put(AMOUNT, num.parse(fee))
                                        ..put(PARTIES, [
                                          userModel.getUserId(),
                                          otherPerson.getUserId()
                                        ]);

                                      model.saveItem(TRANSACTION_BASE, true,
                                          document: id, onComplete: () {
                                        showMessage(
                                            context,
                                            Icons.check,
                                            green,
                                            "Process Successful",
                                            "You have successfully Upgraded ${otherPerson.getString(NAME)} to a Premium ConvasApp.",
                                            onClicked: (_) {
                                          otherPerson = null;
                                          setState(() {});
                                          Navigator.pop(context);
                                        });
                                      });

                                      return;
                                    }

                                    int d = DateTime.fromMillisecondsSinceEpoch(
                                            userModel
                                                .getInt(SUBSCRIPTION_EXPIRY))
                                        .add(Duration(milliseconds: duration))
                                        .millisecondsSinceEpoch;

                                    userModel
                                      ..put(ACCOUNT_BALANCE, balance)
                                      ..put(ACCOUNT_TYPE, ACCOUNT_TYPE_PREMIUM)
                                      ..put(PREMIUM_INDEX, p)
                                      ..put(AMOUNT, num.parse(fee))
                                      ..put(SUBSCRIPTION_EXPIRY, d);
                                    userModel.updateItems();

                                    String id = getRandomId();
                                    final model = BaseModel();
                                    model
                                      ..put(OBJECT_ID, id)
                                      ..put(PREMIUM_INDEX, p)
                                      ..put(STATUS, APPROVED)
                                      ..put(TYPE, TYPE_WALLET_SUB)
                                      ..put(AMOUNT, num.parse(fee))
                                      ..put(PARTIES, [userModel.getUserId()]);

                                    model.saveItem(TRANSACTION_BASE, true,
                                        document: id, onComplete: () {
                                      showMessage(
                                          context,
                                          Icons.check,
                                          green,
                                          "Process Successful",
                                          "You have successfully Upgraded to a Premium ConvasApp User,Now you enjoy the benefits!",
                                          onClicked: (_) {
                                        if (_) Navigator.pop(context);
                                      });
                                    });
                                  },
                                  padding: EdgeInsets.all(20),
                                  color: AppConfig.appColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25)),
                                  child: Center(
                                      child: Text(
                                    "SUBSCRIBE",
                                    style: textStyle(true, 18, white),
                                  )),
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  contactItem() {
    BaseModel model = otherPerson;
    String number = model.getString(PHONE_NUMBER);
    String image = model.userImage;
    bool isGroup = model.getString(GROUP_NAME).isNotEmpty;
    String userName = model.getString(isGroup ? GROUP_NAME : NAME);

    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: white,
          boxShadow: [
            //BoxShadow(color: black.withOpacity(.3), blurRadius: 5)
          ]),
      padding: EdgeInsets.fromLTRB(10, 8, 10, 8),
      margin: EdgeInsets.fromLTRB(0, .5, 0, 0),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(35),
            child: Container(
              //margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
              width: 65,
              height: 65,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppConfig.appColor,
                  gradient: LinearGradient(colors: [
                    blue4,
                    blue2,
                  ])),
              child: image.isEmpty
                  ? Center(
                      child: Text(
                        getInitials(userName),
                        style: textStyle(true, 28, white),
                      ),
                    )
                  : CachedNetworkImage(
                      imageUrl: image,
                      fit: BoxFit.cover,
                    ),
            ),
          ),
          addSpaceWidth(10),
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userName,
                  style: textStyle(true, 18, black),
                ),
                if (!isGroup)
                  Text(
                    number,
                    style: textStyle(false, 14, black),
                  )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
