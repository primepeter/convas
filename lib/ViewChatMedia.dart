import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ViewChatMedia extends StatefulWidget {
  List chatMedia;
  int defPosition;
  ViewChatMedia(this.chatMedia, this.defPosition);

  @override
  _ViewChatMediaState createState() => _ViewChatMediaState();
}

class _ViewChatMediaState extends State<ViewChatMedia> {
  List chatMedia;
  PageController pageController;
  int currentPage;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    chatMedia = widget.chatMedia;
    currentPage = widget.defPosition;
    pageController = PageController(initialPage: currentPage);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      body: page(),
    );
  }

  page() {
    return Stack(
      children: [
        PageView.builder(
          itemBuilder: (c, p) {
            BaseModel model = chatMedia[p];
            bool isVideo = model.getType() == CHAT_TYPE_VIDEO;
            String image = isVideo
                ? model.getString(THUMBNAIL_URL)
                : model.getString(IMAGE_URL);
            return Hero(
              tag: model.getObjectId(),
              child: Stack(
                fit: StackFit.expand,
                children: [
                  CachedNetworkImage(
                    imageUrl: image,
                    height: MediaQuery.of(context).size.height,
                  ),
                  if (isVideo)
                    PlayVideo(
                      model.getObjectId(),
                      model.getString(VIDEO_URL),
                      showBack: false,
                    )
                ],
              ),
            );
          },
          controller: pageController,
          itemCount: chatMedia.length,
          onPageChanged: (p) {
            setState(() {
              currentPage = p;
            });
          },
        ),
        Container(
            padding: EdgeInsets.only(top: 35, left: 10),
            child: BackButton(
              color: white,
            ))
      ],
    );
  }
}
