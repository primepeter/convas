library app;

import 'dart:io';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/assets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:photo/photo.dart';

import 'countries.dart';
part 'countryChooser.dart';
// part 'dotsIndicator.dart';
// part 'gridCollage.dart';
// part 'infoDialog.dart';
// part 'inputDialog.dart';
// part 'listDialog.dart';
// part 'messageDialog.dart';
// part 'navigation.dart';
// part 'notificationService.dart';
// part 'placeChooser.dart';
// part 'preview_image.dart';
// part 'progressDialog.dart';
// part 'rating.dart';
// part 'unicons.dart';

class Countries {
  final String countryName;
  final String countryFlag;
  final String countryCode;

  Countries({this.countryName, this.countryFlag, this.countryCode});
}

List<Countries> getCountries() {
  return countryMap
      .map((c) => Countries(
          countryName: c["Name"],
          countryCode: '+${c["Code"]}',
          countryFlag: 'flags/${c["ISO"]}.png'))
      .toList();
}

Countries country =
    getCountries().singleWhere((e) => e.countryName == 'Nigeria');

openGallery(BuildContext context,
    {bool singleMode = false,
    PickType type = PickType.all,
    int maxSelection,
    @required onPicked(List<PhotoGallery> _)}) {
  PhotoPicker.pickAsset(
          thumbSize: 250,
          context: context,
          provider: I18nProvider.english,
          pickType: PickType.onlyImage,
          maxSelected: singleMode ? 1 : maxSelection,
          rowCount: 3)
      .then((value) async {
    List<PhotoGallery> paths = [];
    for (var v in value) {
      final gallery = PhotoGallery(galleryId: v.id, file: await v.file);
      paths.add(gallery);
    }
    onPicked(paths);
  });
  return;
}

class PhotoGallery {
  final String galleryId;
  final File file;
  final File thumbFilePath;
  final bool isVideo;

  PhotoGallery({
    @required this.galleryId,
    @required this.file,
    this.isVideo,
    this.thumbFilePath,
  });
}
