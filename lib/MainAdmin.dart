import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:io' as io;
import 'dart:ui';

import 'package:Strokes/AppPermissions.dart';
import 'package:Strokes/app/app.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_callkit_voximplant/flutter_callkit_voximplant.dart';
import 'package:flutter_voip_push_notification/flutter_voip_push_notification.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:synchronized/synchronized.dart';

import 'AppEngine.dart';
import 'ChatMain.dart';
import 'ReportMain.dart';
import 'ShowPhotos.dart';
import 'StoryText.dart';
import 'admin/CallService.dart';
import 'app_config.dart';
import 'call/CallEngine.dart';
import 'call/CallScreen.dart';
import 'main.dart';
import 'main_pages/MyProfile.dart';
import 'main_pages/PageStories.dart';
import 'main_pages/calls_page.dart';
import 'main_pages/chat_page.dart';
import 'main_pages/contacts_page.dart';

Map unreadCounter = Map();
Map unreadCounterEncrypt = Map();
Map otherPersonInfo = Map();
List<BaseModel> allStoryList = new List();
final firebaseMessaging = FirebaseMessaging();
final voipPush = FlutterVoipPushNotification();
//IncallManager callManager = IncallManager();

CallService _callService = CallService();
FCXPlugin plugin = FCXPlugin();
FCXProvider provider = FCXProvider();
FCXCallController callController = FCXCallController();

final chatMessageController = StreamController<bool>.broadcast();
final homeStateController = StreamController<bool>.broadcast();
final editChatController = StreamController<bool>.broadcast();
final editCallController = StreamController<bool>.broadcast();
final homeRefreshController = StreamController<bool>.broadcast();
final uploadingController = StreamController<String>.broadcast();
final contactsController = StreamController<BaseModel>.broadcast();
final contactStateController = StreamController<bool>.broadcast();
final hangUpController = StreamController<bool>.broadcast();
final callAnsweredController = StreamController<bool>.broadcast();

List<BaseModel> selectedChats = [];
List<BaseModel> selectedCalls = [];

bool modeCallDelete = false;
bool modeChatDelete = false;

List connectCount = [];
List<String> stopListening = List();
List<BaseModel> lastMessages = List();
List<BaseModel> lastMessagesEncrypt = List();
bool showEncrypt = false;
bool storiesSetup = false;
bool chatSetupRegular = false;
bool chatSetupEncrypt = false;
List showNewMessageDot = [];
List showNewMessageDotEncrypt = [];
bool showNewNotifyDot = false;
List newStoryIds = [];
String visibleChatId;
bool itemsLoaded = false;
List peopleList = [];

List<BaseModel> contacts = List();
//List<BaseModel> contactsList = [];
bool contactSetup = false;
bool syncSetup = false;

List<BaseModel> stories = List();
bool storiesSetUp = false;

List<BaseModel> callsList = List();
bool callsSetup = false;
bool callsSetup2 = false;

CallEngine callEngine = CallEngine.instance;

Future<dynamic> onBackgroundMessage(Map<String, dynamic> message) {
  print("onBackground: $message");
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}

class MainAdmin extends StatefulWidget {
  @override
  _MainAdminState createState() => _MainAdminState();
}

class _MainAdminState extends State<MainAdmin>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin {
  PageController peoplePageController = PageController();
  List<StreamSubscription> subs = List();
  int timeOnline = 0;
  String noInternetText = "";

  String flashText = "";
  bool setup = false;
  bool settingsLoaded = false;
  String uploadingText;
  int peopleCurrentPage = 0;
  bool tipHandled = false;
  bool tipShown = true;
  bool _configured;
  String currentCallId = "Empty";

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    for (var sub in subs) {
      sub.cancel();
    }
    callEngine.disposeCallEngine();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    peopleList.clear();
    itemsLoaded = false;
    WidgetsBinding.instance.addObserver(this);
    peopleList.add(BaseModel());
    Future.delayed(Duration(seconds: 1), () {
      createUserListener();
    });
    var uploadingSub = uploadingController.stream.listen((text) {
      setState(() {
        uploadingText = text;
      });
    });
    var stateSub = homeStateController.stream.listen((event) {
      setState(() {});
    });
    var sub = galleryResultController.stream.listen((_) async {
      if (_.isEmpty) return;

      Future.delayed(Duration(milliseconds: 100), () async {
        List files = [];
        for (File f in _) {
          File croppedFile = await ImageCropper.cropImage(
            sourcePath: f.path,
          );
          if (croppedFile != null) files.add(croppedFile);
        }
        pushAndResult(context, ShowPhoto(files), result: (List models) {
          saveStories(models);
        }, depend: false);
      });
    });
    subs.add(uploadingSub);
    subs.add(stateSub);
    subs.add(sub);
  }

  int minTime = DateTime.now().millisecondsSinceEpoch;
  loadItems({bool manually = false}) async {
    QuerySnapshot shots = userModel.isMale()
        ? await Firestore.instance
            .collection(USER_BASE)
            .where(GENDER, isEqualTo: FEMALE)
            .orderBy(TIME_UPDATED, descending: true)
            .startAt([minTime])
            .limit(PEOPLE_MAX_LOAD)
            .getDocuments()
        : await Firestore.instance
            .collection(WIFE_BASE)
            .where(WIFE_ID, isEqualTo: userModel.getObjectId())
            .orderBy(TIME, descending: true)
            .startAt([minTime])
            .limit(PEOPLE_MAX_LOAD)
            .getDocuments();

    if (shots != null) {
      List newItems = [];
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        int time = model.getInt(userModel.isMale() ? TIME_UPDATED : TIME);
        minTime = time < minTime ? time : minTime;

        if (appSettingsModel
            .getList(DISABLED)
            .contains(model.getString(USER_ID))) continue;
        if (isBlocked(model)) continue;
        if (userModel.getList(LOVE_IDS).contains(model.getString(USER_ID)))
          continue;
        int seenCount = await getSeenCount(getProfileKey(model));
        if (seenCount > 1) continue;
        /*if(userModel.getList(VIEWS_IDS).contains(model.getString(USER_ID))){
        if(userModel.getList(SEEN_PEOPLE).contains(getProfileKey(model)))continue;
      }*/

        int index =
            peopleList.indexWhere((b1) => b1.getUserId() == model.getUserId());
        int index1 =
            newItems.indexWhere((b1) => b1.getUserId() == model.getUserId());
        if (index == -1 && index1 == -1) {
          newItems.add(model);
        }
      }

      if (newItems.isNotEmpty) {
        peopleList.insertAll(peopleList.length - 1, newItems);
        okLayout(manually);
      }

      if (newItems.isEmpty && shots.documents.length == PEOPLE_MAX_LOAD) {
        loadItems(manually: manually);
        return;
      }

      okLayout(manually);
    } else {
      itemsLoaded = true;
      setState(() {});
    }
  }

  checkTip() {
    if (!tipHandled &&
        peopleCurrentPage != peopleList.length - 1 &&
        peopleList.length > 1) {
      tipHandled = true;
      Future.delayed(Duration(seconds: 1), () async {
        SharedPreferences pref = await SharedPreferences.getInstance();
        tipShown = pref.getBool("swipe_tipx") ?? false;
        if (!tipShown) {
          pref.setBool("swipe_tipx", true);
          setState(() {});
        }
      });
    }
  }

  okLayout(bool manually) {
    checkTip();
    itemsLoaded = true;
    if (mounted) setState(() {});
    Future.delayed(Duration(milliseconds: 1000), () {
//      if(manually)pageScrollController.add(-1);
      if (manually) peoplePageController.jumpToPage(peopleCurrentPage);
    });
  }

  Future<int> getSeenCount(String id) async {
    var pref = await SharedPreferences.getInstance();
    List<String> list = pref.getStringList(SHOWN) ?? [];
    int index = list.indexWhere((s) => s.contains(id));
    if (index != -1) {
      String item = list[index];
      var parts = item.split(SPACE);
      int seenCount = int.parse(parts[1].trim());
      return seenCount;
    }
    return 0;
  }

  void createUserListener() async {
    User user =  FirebaseAuth.instance.currentUser;
    if (user != null) {
      var userSub = Firestore.instance
          .collection(USER_BASE)
          .document(user.uid)
          .snapshots()
          .listen((shot) async {
        if (shot != null) {
          User user =  FirebaseAuth.instance.currentUser;
          if (user == null) return;

          userModel = BaseModel(doc: shot);
          isAdmin = userModel.getBoolean(IS_ADMIN) ||
              userModel.getString(EMAIL) == "johnebere58@gmail.com" ||
              userModel.getString(EMAIL) == "ammaugost@gmail.com";
          loadBlocked();

          if (!settingsLoaded) {
            settingsLoaded = true;
            loadSettings();
          }
        }
      });
      subs.add(userSub);
    }
  }

  loadReset() {
    final liveMode = appSettingsModel.getBoolean(RIVE_IS_LIVE);
    if (!isAdmin && userModel.subscriptionExpired)
      userModel
        ..put(ACCOUNT_TYPE, 0)
        ..updateItems();
    print("UserModel ${userModel.subscriptionExpired}");
    return;
  }

  loadSettings() async {
    var settingsSub = Firestore.instance
        .collection(APP_SETTINGS_BASE)
        .document(APP_SETTINGS)
        .snapshots()
        .listen((shot) {
      if (shot != null) {
        appSettingsModel = BaseModel(doc: shot);
        List banned = appSettingsModel.getList(BANNED);
        if (banned.contains(userModel.getObjectId()) ||
            banned.contains(userModel.getString(DEVICE_ID)) ||
            banned.contains(userModel.getEmail())) {
          io.exit(0);
        }

        String genMessage = appSettingsModel.getString(GEN_MESSAGE);
        int genMessageTime = appSettingsModel.getInt(GEN_MESSAGE_TIME);

        if (userModel.getInt(GEN_MESSAGE_TIME) != genMessageTime &&
            genMessageTime > userModel.getTime()) {
          userModel.put(GEN_MESSAGE_TIME, genMessageTime);
          userModel.updateItems(updateTime: false);

          String title = !genMessage.contains("+")
              ? "Announcement!"
              : genMessage.split("+")[0].trim();
          String message = !genMessage.contains("+")
              ? genMessage
              : genMessage.split("+")[1].trim();
          showMessage(context, Icons.info, blue0, title, message);
        }

        if (!setup) {
          setup = true;
          blockedIds.addAll(userModel.getList(BLOCKED));
          loadReset();
          onResume();
          loadItems();
          loadNotification();
          loadMessages(CHAT_MODE_REGULAR);
          loadMessages(CHAT_MODE_ENCRYPT);
          configureCallKit();
          setupPush();
          loadBlocked();
          updatePackage();
          chkUpdate();
          checkPhoto();
          loadContacts();
          loadStory();
          callEngine.initCallEngine(context, callBack: () {
            setState(() {});
          });
        }
      }
    });
    subs.add(settingsSub);
  }

  loadContacts() async {
    bool granted = await AppPermissions.contactPermissionGranted();
    if (!granted) return;
    final pref = await SharedPreferences.getInstance();
    //pref.clear();
    List local = jsonDecode(pref.getString(CONTACTS) ?? '[]');
    contacts = List.from(local)
        .map((e) => BaseModel(items: e))
        .toList()
        .where((e) => !e.myItem())
        .toList();
    if (contacts.isNotEmpty) {
      for (var con in contacts) contactsController.add(con);
      contactSetup = true;
      setState(() {});
    }

    //Fetch device Contact
    final con = await ContactsService.getContacts(
        withThumbnails: false, photoHighResolution: false);

    //Add my Contact to the list where it doesn't exist at all
    for (int i = 0; i < con.toList().length; i++) {
      await Future.delayed(
        Duration(milliseconds: 60),
      );
      var c = con.toList()[i];
      String initials = c.initials();
      String contactName = c.displayName;
      if (c.phones.toList().isEmpty) continue;
      String number = c.phones.toList()[0].value;
      String queryBy = getPhoneNumber(number);
      bool hasCode = queryBy.startsWith("+");

      if (!hasCode) {
        String newNum = queryBy;
        if (queryBy.startsWith("0")) newNum = queryBy.substring(1);
        queryBy = userModel.getString(COUNTRY_CODE) + newNum;
      }

      String id = queryBy.hashCode.toString();
      BaseModel model = BaseModel();
      model.put(OBJECT_ID, id);
      model.put(INITIALS, initials);
      model.put(NAME, contactName);
      model.put(PHONE_NUMBER, queryBy);
      model.put(QUERY, queryBy);
      final checker =
          contacts.where((e) => e.getString(PHONE_NUMBER) == queryBy).toList();
      bool addContact = checker.length == 0;
      if (addContact) {
        print("${checker.length} $queryBy");
        addToContactList(model);
      }
      if (i % 10 == 0) {
        pref.setString(
            CONTACTS, jsonEncode(contacts.map((e) => e.items).toList()));
      }
    }
    contactSetup = true;
    if (mounted) setState(() {});
    print("Contact set $contactSetup");
    pref.setString(CONTACTS, jsonEncode(contacts.map((e) => e.items).toList()));
  }

  String getPhoneNumber(String num) {
    return num.replaceAll("-", "")
        .replaceAll("(", "")
        .replaceAll(")", "")
        .replaceAll(" ", "");
  }

  addToContactList(BaseModel model,
      {bool insert = false, bool ignoring = false}) {
    int p = contacts.indexWhere(
        (e) => e.getString(PHONE_NUMBER) == model.getString(PHONE_NUMBER));

    if (p != -1) {
      if (insert) {
        contacts.removeAt(p);
        contacts.insert(0, model);
      } else if (!ignoring) contacts[p] = model;
    } else {
      contacts.add(model);
    }
    contactsController.add(model);
    setState(() {});
  }

  checkPhoto() {
    String myPhoto = userModel.userImage;
    if (myPhoto.isEmpty) return;
    bool local = !myPhoto.contains("http");
    if (!local) return;
    uploadFile(File(myPhoto), (r, e) {
      if (null != e) {
        return;
      }
      userModel
        ..put(USER_IMAGE, r)
        ..updateItems();
    });
  }

  chkUpdate() async {
    int version = appSettingsModel.getInt(VERSION_CODE);
    PackageInfo pack = await PackageInfo.fromPlatform();
    String v = pack.buildNumber;
    int myVersion = int.parse(v);
    if (myVersion < version) {
      pushAndResult(context, UpdateLayout(), opaque: false);
    }
  }

  handleMessage(var message) {
    final dynamic data = message['data'];
    if (data != null) {
      String type = data[TYPE];
      String id = data[OBJECT_ID];
      if (type != null) {
        if (type == PUSH_TYPE_CHAT && visibleChatId != id) {
          pushAndResult(
              context,
              ChatMain(
                id,
                0,
                otherPerson: null,
              ));
        }
      }
    }
  }

  configureCallKit() async {
    return;
    if (Platform.isAndroid) return;

    try {
      await callController.configure();
      await provider.configure(FCXProviderConfiguration(
        'ExampleLocalizedName',
      ));

      plugin.didDisplayIncomingCall = (uuid, update) async {
        print("CallerID $uuid");
        handleIncomingCall(callId: uuid);
      };

      provider.performEndCallAction = (e) async {
        if (incomingCallSet) handleCallEnded();
        e.fulfill();
      };

      provider.performStartCallAction = (startCallAction) async {
        await startCallAction.fulfill();
      };

      provider.performAnswerCallAction = (e) async {
        print("Incoming call connected @ base!!");
        handleIncomingCall(callId: e.uuid, handle: true);
        e.fulfill();
      };

      callController.callObserver.callChanged = (call) async {
        if (call.hasEnded) handleCallEnded();

        if (call.hasConnected) {
          print("Incoming call connected!!");
        }
      };
    } catch (_) {}
  }

  handleCallEnded() {
    if (!incomingCallSet) return;
    callModel
      ..put(STATUS, CALL_STATUS_DECLINED)
      ..updateItems();
    callerModel
      ..put(CALLING_ID, "")
      ..updateItems();
    userModel
      ..put(CALLING_ID, "")
      ..updateItems();
    callModel = null;
    callerModel = null;
    incomingCallSet = false;
    if (mounted) setState(() {});
  }

  BaseModel callModel;
  BaseModel callerModel;
  bool incomingCallSet = false;

  void handleIncomingCall({@required String callId, bool handle = false}) {
    if (handle && incomingCallSet) {
      onCallAnswered();
      return;
    }

    Firestore.instance
        .collection(CALLS_IDS_BASE)
        .document(callId.toLowerCase())
        .get()
        .then((value) {
      callModel = BaseModel(doc: value);
      print("caller loaded ${value.data}");

      setState(() {});
      String callerId = getOtherPersonId(callModel);
      loadOtherPerson(callerId, 0, callBack: () {
        callerModel = otherPersonInfo[callerId];
        incomingCallSet = true;
        print("caller loaded ${callerModel.getString(NAME)}");
        setState(() {});
        if (handle) onCallAnswered();
      });
    });
  }

  void onCallAnswered() async {
    bool granted = await AppPermissions.cameraAndMicrophonePermissionsGranted();
    if (!granted) return;
    onCall = true;
    callModel
      ..put(STATUS, CALL_STATUS_ACCEPTED)
      ..updateItems();
    Future.delayed(Duration(milliseconds: 10), () {
      pushAndResult(
          context,
          CallScreen(
            inComing: true,
            callerModel: callerModel,
            callModel: callModel,
            isVideoCall: callModel.getBoolean(CALL_MODE),
          ),
          depend: false);
    });
  }

  setupPush() async {
    firebaseMessaging.configure(
      //onBackgroundMessage: Platform.isAndroid ? onBackgroundMessage : null,
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        handleMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        handleMessage(message);
      },
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: false));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    if (userModel.isAdminItem()) {
      firebaseMessaging.subscribeToTopic('admin');
    }

    firebaseMessaging.subscribeToTopic('all');
    firebaseMessaging.getToken().then((String token) async {
      List myTopics = List.from(userModel.getList(TOPICS));

      if (userModel.isAdminItem() && !myTopics.contains('admin')) {
        myTopics.add('admin');
      }
      if (!myTopics.contains('all')) myTopics.add('all');
      print("push $token");
      userModel.put(TOPICS, myTopics);
      userModel.put(TOKEN, token);
      //firebaseMessaging.subscribeToTopic(token);
      userModel.updateItems();
    });
    return;
    if (Platform.isIOS) {
      await voipPush.requestNotificationPermissions();
      voipPush.onTokenRefresh.listen((token) {
        print("callToken $token");
        userModel.put(PLATFORM, IOS);
        userModel.put(CALL_TOKEN, token);
        userModel.updateItems();
      });
      voipPush.configure(
          onMessage: (bool isLocal, Map<String, dynamic> payload) {
//        print("received on foreground payload: $payload, isLocal=$isLocal");
//        incomingCallPayload = payload;
//        handleIncomingCall(
//            callId: payload["callId"], callerId: payload["callerId"]);
//        setState(() {});
        return null;
      }, onResume: (bool isLocal, Map<String, dynamic> payload) {
//        print("received on background payload: $payload, isLocal=$isLocal");
//        incomingCallPayload = payload;
//        handleIncomingCall(
//            callId: payload["callId"], callerId: payload["callerId"]);
//        setState(() {});
        return null;
      });
    }
  }

  void onPause() {
    //cacheChat(CHAT_MODE_REGULAR);
    //cacheChat(CHAT_MODE_ENCRYPT);
    if (userModel == null) return;
    int prevTimeOnline = userModel.getInt(TIME_ONLINE);
    int timeActive = (DateTime.now().millisecondsSinceEpoch) - timeOnline;
    int newTimeOnline = timeActive + prevTimeOnline;
    userModel.put(IS_ONLINE, false);
    userModel.put(TIME_ONLINE, newTimeOnline);
    userModel.updateItems(updateTime: false);
    timeOnline = 0;
  }

  void onResume() async {
    if (userModel == null) return;

    timeOnline = DateTime.now().millisecondsSinceEpoch;
    userModel.put(IS_ONLINE, true);
    userModel.put(
        PLATFORM, Platform.isAndroid ? ANDROID : Platform.isIOS ? IOS : WEB);
    if (!userModel.getBoolean(NEW_APP)) {
      userModel.put(NEW_APP, true);
    }
    userModel.updateItems(updateTime: false);

    Future.delayed(Duration(seconds: 2), () {
      checkLaunch();
    });
  }

  Future<void> checkLaunch() async {
    const platform = const MethodChannel("channel.john");
    try {
      if (Platform.isAndroid) {
        final callIntent = await platform.invokeMethod('callIntent');
        //print("Response on Resume $callIntent");
        if (null == callIntent) return;
        final decodedCall = Map.from(jsonDecode(callIntent));
        BaseModel model = BaseModel(items: decodedCall);
        final caller = model.getModel("caller");
        final call = model.getModel("call");
//        pushAndResult(
//            context,
//            PickUpScreen(
//              callerModel: caller,
//              callIdModel: call,
//              isVideoCall: call.getBoolean(IS_VIDEO),
//            ));
      }

      Map response = await platform
          .invokeMethod('launch', <String, String>{'message': ""});
      int type = response[TYPE];
      String chatId = response[CHAT_ID];
      if (type == LAUNCH_CHAT) {
        pushAndResult(context, ChatMain(chatId, 0, otherPerson: null));
      }

      if (type == LAUNCH_REPORTS) {
        pushAndResult(context, ReportMain());
      }
    } catch (e) {}
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    print(state);
    if (state == AppLifecycleState.paused) {
      onPause();
    }
    if (state == AppLifecycleState.resumed) {
      onResume();
    }

    if (state == AppLifecycleState.inactive) {
      //SharedPreferences.getInstance().then((value) => value.clear());
    }

    super.didChangeAppLifecycleState(state);
  }

  Future<bool> isMessageDeleted(
      String chatId, int chatMode, int theTime) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    List<String> deletedList = pref.getStringList(DELETED_CHAT) ?? [];
    String realId = getRealChatId(chatId, chatMode);
    int index = deletedList.indexWhere((b) => b.contains(realId));
    if (index != -1) {
      String chatString = deletedList[index];
      String timeText = chatString.split(SPACE)[1];
      int time = int.parse(timeText);
      if (time > theTime) return true;
    }

    List deletedChats = userModel.getList(DELETED_CHAT);
    int deleteIndex = deletedChats
        .indexWhere((m) => m[CHAT_ID] == getRealChatId(chatId, chatMode));
    if (deleteIndex != -1) {
      Map deleteItem = deletedChats[deleteIndex];
      int deleteTime = deleteItem[TIME];
      if (deleteTime > theTime) return true;
    }
    return false;
  }

  List loadedIds = [];
  loadMessages(int chatMode) async {
    var lock = Lock();
    List messageList =
        chatMode == CHAT_MODE_ENCRYPT ? lastMessagesEncrypt : lastMessages;
    List messageDot = chatMode == CHAT_MODE_ENCRYPT
        ? showNewMessageDotEncrypt
        : showNewMessageDot;

    await lock.synchronized(() async {
      var sub = Firestore.instance
          .collection(chatMode == CHAT_MODE_ENCRYPT
              ? CHAT_IDS_BASE_ENCRYPT
              : CHAT_IDS_BASE)
          .where(PARTIES, arrayContains: userModel.getObjectId())
          .orderBy(TIME, descending: true)
          .snapshots()
          .listen((shots) {
        for (DocumentSnapshot doc in shots.documents) {
          BaseModel chatIdModel = BaseModel(doc: doc);
          String chatId = chatIdModel.getObjectId();
          if (loadedIds.contains(getRealChatId(chatId, chatMode))) continue;
          loadedIds.add(getRealChatId(chatId, chatMode));
          bool isGroup = chatIdModel.getString(GROUP_NAME).isNotEmpty;
          if (isGroup) {
            otherPersonInfo[chatId] = chatIdModel;
          } else {
            String otherPersonId = getOtherPersonId(chatIdModel);
            loadOtherPerson(otherPersonId, chatMode, callBack: () {
              if (mounted) setState(() {});
            });
          }
          BaseModel dummyModel = BaseModel();
          dummyModel.put(CHAT_ID, chatId);
          dummyModel.put(TIME, DateTime.now().millisecondsSinceEpoch);
          int index = messageList.indexWhere(
              (bm) => bm.getString(CHAT_ID) == dummyModel.getString(CHAT_ID));
          if (index == -1) {
            if (isGroup)
              messageList.insert(0, dummyModel);
            else
              messageList.add(dummyModel);
          }

          var sub = Firestore.instance
              .collection(
                  chatMode == CHAT_MODE_ENCRYPT ? CHAT_BASE_ENCRYPT : CHAT_BASE)
              .where(PARTIES, arrayContains: userModel.getUserId())
              .where(CHAT_ID, isEqualTo: chatId)
              .orderBy(TIME, descending: true)
              .limit(1)
              .snapshots()
              .listen((shots) async {
            if (shots.documents.isNotEmpty) {
              BaseModel cModel = BaseModel(doc: (shots.documents[0]));
              bool deleted =
                  await isMessageDeleted(chatId, chatMode, cModel.getTime());
              if (isBlocked(null, userId: getOtherPersonId(cModel)) ||
                  deleted) {
                messageList.removeWhere(
                    (bm) => bm.getString(CHAT_ID) == cModel.getString(CHAT_ID));
                chatMessageController.add(true);
                return;
              }
            }
            if (stopListening.contains(getRealChatId(chatId, chatMode))) return;
            for (DocumentSnapshot doc in shots.documents) {
              BaseModel model = BaseModel(doc: doc);
              String chatId = model.getString(CHAT_ID);
              List hiddenChat = model.getList(HIDDEN);
              if (hiddenChat.contains(userModel.getUserId())) continue;

              int index = messageList.indexWhere(
                  (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
              if (index == -1) {
                messageList.add(model);
              } else {
                messageList[index] = model;
              }

              if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                  !model.myItem() &&
                  visibleChatId != model.getString(CHAT_ID)) {
                try {
                  if (!messageDot.contains(chatId)) messageDot.add(chatId);
                  setState(() {});
                } catch (E) {
                  if (!messageDot.contains(chatId)) messageDot.add(chatId);
                  setState(() {});
                }
                countUnread(chatId, chatMode);
              }
            }

            try {
              messageList
                  .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
            } catch (E) {}
          });

          subs.add(sub);
        }
        Future.delayed(Duration(milliseconds: 200), () {
          if (chatMode == CHAT_MODE_REGULAR) chatSetupRegular = true;
          if (chatMode == CHAT_MODE_ENCRYPT) chatSetupEncrypt = true;
          if (mounted) setState(() {});
        });
      });
      subs.add(sub);
    });
  }

  loadConnects() async {
    var lock = Lock();
    await lock.synchronized(() async {
      var sub = Firestore.instance
          .collection(CONNECTS_BASE)
          .where(PARTIES, arrayContains: userModel.getObjectId())
          .snapshots()
          .listen((shots) {
        for (DocumentSnapshot doc in shots.documents) {
          BaseModel connect = BaseModel(doc: doc);
          if (!connect.getList(READ_BY).contains(userModel.getObjectId())) {
            if (!connectCount.contains(connect.getObjectId()))
              connectCount.add(connect.getObjectId());
          }
        }
        if (mounted) setState(() {});
      });
      subs.add(sub);
    });
  }

  countUnread(String chatId, int chatMode) async {
    var lock = Lock();
    lock.synchronized(() async {
      int count = 0;
      QuerySnapshot shots = await Firestore.instance
          .collection(
              chatMode == CHAT_MODE_ENCRYPT ? CHAT_BASE_ENCRYPT : CHAT_BASE)
          .where(CHAT_ID, isEqualTo: chatId)
          .getDocuments();

      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          count++;
        }
      }
      if (chatMode == CHAT_MODE_REGULAR) unreadCounter[chatId] = count;
      if (chatMode == CHAT_MODE_ENCRYPT) unreadCounterEncrypt[chatId] = count;
      chatMessageController.add(true);
    });
  }

  loadNotification() async {
    var sub = Firestore.instance
        .collection(NOTIFY_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .limit(1)
        .orderBy(TIME_UPDATED, descending: true)
        .snapshots()
        .listen((shots) {
      //toastInAndroid(shots.documents.length.toString());
      for (DocumentSnapshot d in shots.documents) {
        BaseModel model = BaseModel(doc: d);
        /*int p = nList
            .indexWhere((bm) => bm.getObjectId() == model.getObjectId());
        if (p == -1) {
          nList.add(model);
        } else {
          nList[p] = model;
        }*/

        if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
            !model.myItem()) {
          showNewNotifyDot = true;
          setState(() {});
        }
      }
      /*nList.sort((bm1, bm2) =>
          bm2.getInt(TIME_UPDATED).compareTo(bm1.getInt(TIME_UPDATED)));*/
    });

    subs.add(sub);
    //notifySetup = true;
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
      onWillPop: () {
        //backThings();
        exit(0);
        return;
      },
      child: Scaffold(
          //floatingActionButton: FloatingActionButton(),
          body: Stack(children: [
        page(),
      ])),
    );
  }

  int currentPage = 2;
  final vp = PageController(initialPage: 2);

  String get headerTitle {
    if (currentPage == 0) return "Stories";
    if (currentPage == 1) return "Calls";
    if (currentPage == 2) return "Chats";

    return "Profile";
  }

  page() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(15, 40, 15, 10),
          color: white,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  if (currentPage != 3 && currentPage != 1)
                    GestureDetector(
                      onTap: () {
                        if (currentPage == 0) {
                          shareStoryImages();
                          return;
                        }

                        if (currentPage == 2) {
                          pushAndResult(context, ContactsPage(groupMode: true),
                              depend: false);
                          return;
                        }
                      },
                      child: Container(
                        height: 30,
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        decoration: BoxDecoration(
                            color: white,
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(
                                color: black.withOpacity(0.2), width: 1)),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              currentPage == 0
                                  ? Icons.camera_alt
                                  /*: currentPage == 1
                                      ? Icons.call*/
                                  : Icons.group_add,
                              color: black.withOpacity(.5),
                              size: 17,
                            ),
                            addSpaceWidth(5),
                            Text(
                              currentPage == 0
                                  ? "Photos"
                                  : /*currentPage == 1 ? "Call" :*/ "Group",
                              style: textStyle(true, 16, black),
                            )
                          ],
                        ),
                      ),
                    ),
                  //Spacer(),
                  //addSpaceWidth(14),
                  if (currentPage != 3)
                    Row(
                      children: [
                        if ((currentPage == 2 && selectedChats.isNotEmpty) ||
                            (currentPage == 1 && selectedCalls.isNotEmpty)) ...[
                          Container(
                            height: 30,
                            width: 50,
                            child: new FlatButton(
                                padding: EdgeInsets.all(0),
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                onPressed: () {
                                  yesNoDialog(
                                      context,
                                      "Delete ${currentPage == 1 ? "Calls" : "Chats"}",
                                      "Are your sure you want to delete your ${currentPage == 1 ? "Calls" : "Chat"}",
                                      () {
                                    if (currentPage == 1) {
                                      for (var model in selectedCalls) {
                                        model
                                          ..putInList(PARTIES,
                                              userModel.getUserId(), false)
                                          ..updateItems();
                                      }
                                      editCallController.add(false);
                                      modeCallDelete = false;
                                      selectedCalls.clear();
                                      setState(() {});
                                    }

                                    if (currentPage == 2) {
                                      for (var model in selectedChats) {
                                        model
                                          ..put(DELETED, true)
                                          ..updateItems();
                                        lastMessages.removeWhere((e) =>
                                            e.getObjectId() ==
                                            model.getObjectId());

                                        lastMessagesEncrypt.removeWhere((e) =>
                                            e.getObjectId() ==
                                            model.getObjectId());
                                      }
                                      editChatController.add(false);
                                      selectedChats.clear();
                                      modeChatDelete = false;
                                      setState(() {});
                                    }
                                  });
                                },
                                child: Center(
                                    child: Icon(
                                  Icons.delete_sweep,
                                  color: black,
                                ))),
                          ),
                          Container(
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: red,
                              shape: BoxShape.circle,
                            ),
                            child: Text(
                                (currentPage == 1
                                        ? selectedCalls.length
                                        : currentPage == 2
                                            ? selectedChats.length
                                            : "")
                                    .toString(),
                                style: textStyle(true, 16, white)),
                          ),
                        ] else
                          GestureDetector(
                            onTap: () {
                              if (currentPage == 0) {
                                pushAndResult(context, StoryText(),
                                    result: (BaseModel model) {
                                  uploadItem(
                                    uploadingController,
                                    "Uploading Story...",
                                    "Story uploaded successfully",
                                    model,
                                  );
                                }, depend: false);
                              }
                              if (currentPage == 1)
                                editCallController.add(true);
                              if (currentPage == 2)
                                editChatController.add(true);
                              setState(() {});
                            },
                            child: Container(
                              height: 30,
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              decoration: BoxDecoration(
                                  color: white,
                                  borderRadius: BorderRadius.circular(25),
                                  border: Border.all(
                                      color: black.withOpacity(0.2), width: 1)),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(
                                    Icons.edit,
                                    color: black.withOpacity(.5),
                                    size: 17,
                                  ),
                                  addSpaceWidth(5),
                                  Text(
                                    currentPage == 0 ? "Text" : "Edit",
                                    style: textStyle(true, 16, black),
                                  )
                                ],
                              ),
                            ),
                          ),
                        if (modeCallDelete && currentPage == 1 ||
                            modeChatDelete && currentPage == 2) ...[
                          addSpaceWidth(10),
                          GestureDetector(
                            onTap: () {
                              if (currentPage == 1) {
                                editCallController.add(false);
                                selectedCalls.clear();
                                modeCallDelete = false;
                              }
                              if (currentPage == 2) {
                                editChatController.add(false);
                                selectedChats.clear();
                                modeChatDelete = false;
                              }
                              setState(() {});
                            },
                            child: Container(
                              height: 20,
                              width: 20,
                              child: Icon(
                                Icons.close,
                                color: white,
                                size: 14,
                              ),
                              decoration: BoxDecoration(
                                  color: red, shape: BoxShape.circle),
                            ),
                          )
                        ]
                      ],
                    ),
                ],
              ),
              Text(headerTitle, style: textStyle(true, 28, black)),
            ],
          ),
        ),
        AnimatedContainer(
          height: uploadingText == null ? 0 : 40,
          duration: Duration(milliseconds: 500),
          width: double.infinity,
          child: Center(
              child: uploadingText == null
                  ? Container()
                  : Text(
                      uploadingText,
                      style: textStyle(true, 14, white),
                    )),
          color: black.withOpacity(.5),
          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        ),
        Flexible(
          child: Stack(
            children: [
              PageView(
                controller: vp,
                onPageChanged: (p) {
                  setState(() {
                    currentPage = p;
                  });
                },
                children: [
                  PageStories(),
                  CallsPage(),
                  MessagesPage(),
                  MyProfile()
                ],
              ),
              //pageFloaters()
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          color: white,
          child: Row(
              children: List.generate(5, (p) {
            int at = p > 1 ? p - 1 : p;

            bool active = currentPage == at;
            bool isProfile = p == 4;
            String imageUrl = userModel.userImage;
            bool isLocal = !imageUrl.contains("http");
            bool isIcon = true;
            var icon;
            Color color = black.withOpacity(active ? 1 : 0.4);
            double size =
                active ? (isProfile ? 32 : 28) : (isProfile ? 25 : 22);
            bool isCenter = p == 2;
            if (p == 0) {
              icon = "assets/icons/stories.png";
              isIcon = false;
            }
            if (p == 1) {
              icon = "assets/icons/multimedia.png";
              isIcon = false;
            }
            if (p == 3) {
              icon = "assets/icons/chat.png";
              isIcon = false;
            }
            return Flexible(
              child: GestureDetector(
                onTap: () {
                  if (isCenter) {
                    pushAndResult(context, ContactsPage(), depend: false);
                    return;
                  }
                  vp.jumpToPage(at);
                },
                child: Container(
                    padding: EdgeInsets.all(8),
                    width: getScreenWidth(context) / 4,
                    color: transparent,
                    alignment: Alignment.center,
                    child: isCenter
                        ? MaterialButton(
                            color: AppConfig.appColor,
                            shape: CircleBorder(),
                            padding: EdgeInsets.all(12),
                            elevation: 12,
                            onPressed: () {
                              pushAndResult(context, ContactsPage(),
                                  depend: false);
                            },
                            child: Icon(Icons.add, color: white))
                        : Container(
                            height: size,
                            width: size,
                            child: Stack(
                              children: [
                                if (isProfile) ...[
                                  Opacity(
                                    opacity: active ? 1 : 0.6,
                                    child: Builder(
                                      builder: (ctx) {
                                        return ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(35),
                                          child: Container(
                                            //margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                            width: 65,
                                            height: 65,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: AppConfig.appColor,
                                                gradient:
                                                    LinearGradient(colors: [
//                      AppConfig.appColor,
//                      AppConfig.appColor.withOpacity(.8)
                                                  blue4,
                                                  blue2,
                                                ])),
                                            child: userModel.userImage.isEmpty
                                                ? Center(
                                                    child: Text(
                                                      getInitials(userModel
                                                          .getString(NAME)),
                                                      style: textStyle(
                                                          true, 15, white),
                                                    ),
                                                  )
                                                : CachedNetworkImage(
                                                    imageUrl:
                                                        userModel.userImage,
                                                    fit: BoxFit.cover,
                                                  ),
                                          ),
                                        );

//                                  if (imageUrl.isEmpty)
//                                    return ClipRRect(
//                                      borderRadius: BorderRadius.circular(
//                                          isProfile ? size / 2 : 0),
//                                      child: Container(
//                                        height: size,
//                                        width: size,
//                                        decoration: BoxDecoration(
//                                            shape: BoxShape.circle,
//                                            color: AppConfig.appColor,
//                                            gradient: LinearGradient(colors: [
////                      AppConfig.appColor,
////                      AppConfig.appColor.withOpacity(.8)
//                                              blue4,
//                                              blue2,
//                                            ])),
//                                        alignment: Alignment.center,
//                                        child: Text(
//                                          getInitials(
//                                            userModel.getString(NAME),
//                                          ),
//                                          style: textStyle(active, 12, black),
//                                        ),
//                                      ),
//                                    );
//
//                                  if (isLocal)
//                                    return ClipRRect(
//                                      borderRadius: BorderRadius.circular(
//                                          isProfile ? size / 2 : 0),
//                                      child: Image.file(
//                                        File(
//                                          imageUrl,
//                                        ),
//                                        fit: BoxFit.cover,
//                                        height: size,
//                                        width: size,
//                                      ),
//                                    );
//
//                                  return ClipRRect(
//                                    borderRadius: BorderRadius.circular(
//                                        isProfile ? size / 2 : 0),
//                                    child: CachedNetworkImage(
//                                      imageUrl: imageUrl,
//                                      fit: BoxFit.cover,
//                                      height: size,
//                                      width: size,
//                                    ),
//                                  );
                                      },
                                    ),
                                  )
                                ] else ...[
                                  if (isIcon)
                                    Align(
                                        alignment: Alignment.center,
                                        child: Icon(
                                          icon,
                                          color: color,
                                          size: size,
                                        ))
                                  else
                                    Image.asset(
                                      icon,
                                      color: color,
                                      fit: BoxFit.cover,
                                      height: size,
                                      width: size,
                                    )
                                ],
                                if (unreadCounter.isNotEmpty && p == 2)
                                  Container(
                                    height: 10,
                                    width: 10,
                                    decoration: BoxDecoration(
                                        color: AppConfig.appColor,
                                        shape: BoxShape.circle),
                                  )
                              ],
                            ),
                          )),
              ),
            );
          })),
        )
      ],
    );
  }

  pageFloaters() {
    if (currentPage > 2)
      return Container(
        height: 0,
      );
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
          height: currentPage != 2 ? 150 : 100,
          padding: EdgeInsets.all(12),
          margin: EdgeInsets.only(bottom: 10),
          child: Row(children: [
//            if (currentPage == 2)
//              Flexible(
//                  child: Container(
//                      padding: EdgeInsets.all(12),
//                      decoration: BoxDecoration(
//                          color: AppConfig.appGreen,
//                          borderRadius: BorderRadius.circular(20)),
//                      child: Row(
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        children: [
//                          Flexible(
//                            child: Row(
//                              children: [
//                                Container(
//                                    height: 50,
//                                    width: 50,
//                                    child: Center(
//                                        child: Text("3",
//                                            style: textStyle(true, 18,
//                                                black.withOpacity(.8)))),
//                                    decoration: BoxDecoration(
//                                        color: white,
//                                        borderRadius:
//                                            BorderRadius.circular(10))),
//                                addSpaceWidth(15),
//                                Flexible(
//                                  child: Column(
//                                    crossAxisAlignment:
//                                        CrossAxisAlignment.start,
//                                    mainAxisAlignment: MainAxisAlignment.center,
//                                    children: [
//                                      Text("Active users",
//                                          style: textStyle(
//                                              true, 16, black.withOpacity(.8))),
//                                      Text("Maugost,Mtellect,Jevss",
//                                          style: textStyle(
//                                              true, 12, black.withOpacity(.8))),
//                                    ],
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Icon(Icons.dialpad, color: white)
//                        ],
//                      )))
//            else
            Spacer(),
            Column(
              children: [
                if (currentPage == 0) ...[
                  MaterialButton(
                      color: AppConfig.appColor,
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(12),
                      elevation: 12,
                      onPressed: () {
                        //pushAndResult(context, StoryText(), depend: false);
                        pushAndResult(context, StoryText(),
                            result: (BaseModel model) {
                          uploadItem(
                            uploadingController,
                            "Uploading Story...",
                            "Story uploaded successfully",
                            model,
                          );
                        }, depend: false);
                      },
                      child: Icon(Icons.edit, color: white)),
                  addSpace(10),
                  MaterialButton(
                      color: AppConfig.appColor,
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(20),
                      elevation: 12,
                      onPressed: () {
                        shareStoryImages();
                      },
                      child: Icon(Icons.camera_alt, color: white)),
                ],
                if (currentPage == 1) ...[
                  MaterialButton(
                      color: AppConfig.appColor,
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(12),
                      elevation: 12,
                      onPressed: () {},
                      child: Icon(Icons.delete_sweep, color: white)),
                  addSpace(10),
                  MaterialButton(
                      color: AppConfig.appColor,
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(20),
                      elevation: 12,
                      onPressed: () async {
//                        NotificationService.sendPush(
//                            topic: "com.convas",
//                            token:
//                                "eBcGtBq110FyoTtkusbHqF:APA91bFCgyJTj5hoX9K0ntxpdj4BMMyiLEYm7sQNjLVq3IpKrVZhm_ukQ5UPcyTKFnxomfK-Tg4iRaDJogSOzBaQnr9ukMWW5CZd9ysMBDCInOxpitBj-X_2_LaysIEkxf8anqrrZiCr",
//                            data: {});

                        //callEngine.pushCallIOS();
//                        return;

                        Dio.Dio().post(
                            "https://us-central1-convasapp.cloudfunctions.net/makeCall",
                            data: {
                              "data": {
                                "callToken":
                                    "0d2006103a9b54b7899a98c45a31ac516eb46f8e4a588c0e8f590dc28827de77"
                              }
                            }).then((response) {
                          showMessage(context, Icons.check, green, "Response",
                              response.data.toString());
                        }).catchError((e) {
                          print(e);
                          showMessage(
                              context, Icons.check, red, "Error", e.toString());
                        });
                      },
                      child: Icon(Icons.add_call, color: white)),
                ],
//                if (currentPage == 2) ...[
//                  Spacer(),
//                  MaterialButton(
//                      color: AppConfig.appColor,
//                      shape: CircleBorder(),
//                      padding: EdgeInsets.all(12),
//                      elevation: 12,
//                      onPressed: () {
//                        pushAndResult(context, ContactsPage(), depend: false);
//                      },
//                      child: Icon(Icons.add, color: white))
//                ]
              ],
            )
          ])),
    );
  }

  backThings() {
    if (userModel != null && !userModel.getBoolean(HAS_RATED)) {
      showMessage(context, Icons.star, blue0, "Rate Us",
          "Enjoying the App? Please support us with 5 stars",
          clickYesText: "RATE APP", clickNoText: "Later", onClicked: (_) {
        if (_ == true) {
          if (appSettingsModel == null ||
              appSettingsModel.getString(PACKAGE_NAME).isEmpty) {
            onPause();
            Future.delayed(Duration(seconds: 1), () {
              io.exit(0);
            });
          } else {
            rateApp();
          }
        } else {
          onPause();
          Future.delayed(Duration(seconds: 1), () {
            io.exit(0);
          });
        }
      });
      return;
    }
    onPause();
    Future.delayed(Duration(seconds: 1), () {
      io.exit(0);
    });
  }

  loadStory() async {
    //await Future.delayed(Duration(seconds: 2));
    var storySub = Firestore.instance
        .collection(STORY_BASE)
        .where(PARTIES, arrayContains: userModel.getUserId())
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .snapshots()
        .listen((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.documents) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStoryList
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStoryList.add(model);
          added = true;
          if (!model.myItem() &&
              !model.getList(SHOWN).contains(userModel.getObjectId())) {
            if (!newStoryIds.contains(model.getObjectId()))
              newStoryIds.add(model.getObjectId());
          }
        } else {
          allStoryList[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
    var myStorySub = Firestore.instance
        .collection(STORY_BASE)
        .where(USER_ID, isEqualTo: userModel.getObjectId())
        .where(TIME,
            isGreaterThan: (DateTime.now().millisecondsSinceEpoch -
                (Duration.millisecondsPerDay * 1)))
        .snapshots()
        .listen((shots) {
      bool added = false;
      for (DocumentSnapshot shot in shots.documents) {
        if (!shot.exists) continue;
        BaseModel model = BaseModel(doc: shot);
        if (isBlocked(model)) continue;
        int index = allStoryList
            .indexWhere(((bm) => bm.getObjectId() == model.getObjectId()));
        if (index == -1) {
          allStoryList.add(model);
          added = true;
        } else {
          allStoryList[index] = model;
        }
      }
      homeRefreshController.add(true);
    });
    subs.add(storySub);
    subs.add(myStorySub);
  }

  loadBlocked() async {
    var lock = Lock();
    lock.synchronized(() async {
      QuerySnapshot shots = await Firestore.instance
          .collection(USER_BASE)
          .where(BLOCKED, arrayContains: userModel.getObjectId())
          .getDocuments();

      for (DocumentSnapshot doc in shots.documents) {
        BaseModel model = BaseModel(doc: doc);
        String uId = model.getObjectId();
        String deviceId = model.getString(DEVICE_ID);
        if (!blockedIds.contains(uId)) blockedIds.add(uId);
        if (deviceId.isNotEmpty) if (!blockedIds.contains(deviceId))
          blockedIds.add(deviceId);
      }
    }, timeout: Duration(seconds: 10));
  }

  shareStoryImages() async {
    openGallery(context, onPicked: (_) {
      if (_.isEmpty) return;
      Future.delayed(Duration(milliseconds: 100), () async {
        List files = [];
        for (var f in _) {
          File croppedFile = await ImageCropper.cropImage(
            sourcePath: f.file.path,
          );
          if (croppedFile != null) files.add(croppedFile);
        }
        if (files.isEmpty) return;
        pushAndResult(context, ShowPhoto(files), result: (List models) {
          saveStories(models);
        }, depend: false);
      });
    });

//    await provider.refreshGalleryList();
//    final item = provider.list[0];
//    pushAndResult(
//        context,
//        CustomGalleryContentListPage(
//          path: item,
//        ),
//        depend: false, result: (_) {
//      if (_.isEmpty) return;
//      Future.delayed(Duration(milliseconds: 100), () async {
//        List files = [];
//        for (File f in _) {
//          File croppedFile = await ImageCropper.cropImage(
//            sourcePath: f.path,
//          );
//          if (croppedFile != null) files.add(croppedFile);
//        }
//        pushAndResult(context, ShowPhoto(files), result: (List models) {
//          saveStories(models);
//        }, depend: false);
//      });
//    });
  }

  saveStories(List models) async {
    if (models.isEmpty) {
      uploadingController.add("Uploading Successful");
      Future.delayed(Duration(seconds: 1), () {
        uploadingController.add(null);
      });
      return;
    }
    uploadingController.add("Uploading Story");

    BaseModel model = models[0];
    String image = model.getString(STORY_IMAGE);
    uploadFile(File(image), (res, error) {
      if (error != null) {
        saveStories(models);
        return;
      }
      model.put(STORY_IMAGE, res);
      model.put(PARTIES, userModel.getList(CONTACTS));
      model.saveItem(STORY_BASE, true);
      models.removeAt(0);
      saveStories(models);
    });
  }

  @override
  bool get wantKeepAlive => true;
}

class UpdateLayout extends StatelessWidget {
  BuildContext con;
  @override
  Widget build(BuildContext context) {
    String features = appSettingsModel.getString(NEW_FEATURE);
    if (features.isNotEmpty) features = "* $features";
    bool mustUpdate = appSettingsModel.getBoolean(MUST_UPDATE);
    con = context;
    return WillPopScope(
      onWillPop: () {},
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
              child: Container(
                color: black.withOpacity(.6),
              )),
          Container(
            padding: EdgeInsets.all(15),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (isAdmin) {
                        Navigator.pop(con);
                      }
                    },
                    child: Icon(
                      Icons.update,
                      color: red0,
                      size: 60,
                    ),
                  ),
                  addSpace(10),
                  Text(
                    "New Update Available",
                    style: textStyle(true, 22, white),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(10),
                  Text(
                    features.isEmpty
                        ? "Please update your App to proceed"
                        : features,
                    style: textStyle(false, 16, white.withOpacity(.5)),
                    textAlign: TextAlign.center,
                  ),
                  addSpace(15),
                  Container(
                    height: 40,
                    width: double.infinity,
                    child: FlatButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        color: blue3,
                        onPressed: () {
                          rateApp();
                        },
                        child: Text(
                          "UPDATE",
                          style: textStyle(true, 14, white),
                        )),
                  ),
                  addSpace(15),
                  if (!mustUpdate)
                    Container(
                      height: 40,
                      child: FlatButton(
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          color: red0,
                          onPressed: () {
                            Navigator.pop(con);
                          },
                          child: Text(
                            "Later",
                            style: textStyle(true, 14, white),
                          )),
                    )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
