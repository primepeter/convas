//import 'dart:io';
//
//import 'package:Strokes/AppEngine.dart';
//import 'package:Strokes/ChatMain.dart';
//import 'package:Strokes/MyProfile1.dart';
//import 'package:Strokes/assets.dart';
//import 'package:Strokes/basemodel.dart';
//import 'package:Strokes/dialogs/listDialog.dart';
//import 'package:cached_network_image/cached_network_image.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:firebase_storage/firebase_storage.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter_text_highlight/flutter_text_highlight.dart';
//import 'package:intl/intl.dart';
//import 'package:loading_indicator/loading_indicator.dart';
//import 'package:video_player/video_player.dart';
//
//class ChatUtils {
//  var onReplyTapped;
//  BaseModel otherPersonModel;
//  double elevation;
//
//  ChatUtils({@required onReplyTapped(String id), @required otherPersonModel,double elevation=0}) {
//    this.onReplyTapped = onReplyTapped;
//    this.otherPersonModel = otherPersonModel;
//    this.elevation = elevation;
//  }
//
//  setOtherPerson(otherPersonModel) {
//    this.otherPersonModel = otherPersonModel;
//  }
//
//  incomingChatText(context, BaseModel chat) {
//    if (chat.getBoolean(DELETED)) {
//      return incomingChatDeleted(context, chat);
//    }
//    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
//      return Container();
//    }
//
//    String message = chat.getString(MESSAGE);
//
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    Map<String, HighlightedWord> words = {
//      searchMessage: HighlightedWord(
//        onTap: () {},
//        textStyle: textStyle(true, 17, blue0),
//      ),
//    };
//    return new Stack(
//      children: <Widget>[
//        new GestureDetector(
//          onLongPress: () {
//            showChatOptions(context, chat);
//          },
//          child: Container(
//              margin: EdgeInsets.fromLTRB(60, 0, 60, 15),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: [
//                  if (replyData.items.isNotEmpty)
//                    chatReplyWidget(
//                      replyData,
//                    ),
//                  Card(
//                    clipBehavior: Clip.antiAlias,
//                    color: default_white,
//                    elevation: elevation,
//                    /*shadowColor: black.withOpacity(.3),*/
//                    shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.only(
//                      topRight: Radius.circular(25),
//                      topLeft: Radius.circular(0),
//                      bottomLeft: Radius.circular(25),
//                      bottomRight: Radius.circular(25),
//                    )),
//                    child: new Container(
//                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
//                      decoration: BoxDecoration(
////                    color: white,
//                          borderRadius: BorderRadius.circular(10)),
//                      child: Column(
//                        mainAxisSize: MainAxisSize.min,
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        children: <Widget>[
//                          TextHighlight(
//                            text:
//                                message, // You need to pass the string you want the highlights
//                            words: words, // Your dictionary words
//                            // textStyle: TextStyle( // You can set the general style, like a Text()
//                            //   fontSize: 20.0,
//                            //   color: Colors.black,
//                            // ),
//                            textStyle: textStyle(false, 17, black),
//                            textAlign: TextAlign
//                                .justify, // You can use any attribute of the RichText widget
//                          ),
////                        Text(
////                          message,
////                          style: textStyle(false, 17, black),
////                        ),
//                          addSpace(3),
//                          Text(
//                            /*timeAgo.format(
//                            DateTime.fromMillisecondsSinceEpoch(
//                                chat.getTime()),
//                            locale: "en_short")*/
//                            getChatTime(chat.getInt(TIME)),
//                            style: textStyle(false, 12, black.withOpacity(.3)),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                ],
//              )),
//        ),
//        userImageItem(context)
//      ],
//    );
//  }
//
//  incomingChatDeleted(context, BaseModel chat) {
//    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
//      return Container();
//    }
//    return new Stack(
//      children: <Widget>[
//        GestureDetector(
//          onLongPress: () {
//            showChatOptions(context, chat, deletedChat: true);
//          },
//          child: Container(
//              margin: EdgeInsets.fromLTRB(60, 0, 60, 15),
//              child: Card(
//                clipBehavior: Clip.antiAlias,
//                color: default_white,
//                elevation: elevation,
//                /*shadowColor: black.withOpacity(.3),*/
//                shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.only(
//                  topRight: Radius.circular(25),
//                  topLeft: Radius.circular(0),
//                  bottomLeft: Radius.circular(25),
//                  bottomRight: Radius.circular(25),
//                )),
//                child: new Container(
//                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
//                  decoration: BoxDecoration(
////                    color: white,
//                      borderRadius: BorderRadius.circular(10)),
//                  child: Column(
//                    mainAxisSize: MainAxisSize.min,
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      Row(
//                        mainAxisSize: MainAxisSize.min,
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        children: <Widget>[
//                          Text(
//                            "Deleted",
//                            style: textStyle(false, 15, black),
//                          ),
//                          addSpaceWidth(5),
//                          Icon(
//                            Icons.info,
//                            color: red0,
//                            size: 17,
//                          )
//                        ],
//                      ),
//                      /*addSpace(3),
//                Text(
//                  */ /*timeAgo.format(
//                        DateTime.fromMillisecondsSinceEpoch(
//                            chat.getTime()),
//                        locale: "en_short")*/ /*
//                  getChatTime(chat.getInt(TIME)),
//                  style: textStyle(false, 12, black.withOpacity(.3)),
//                ),*/
//                    ],
//                  ),
//                ),
//              )),
//        ),
//        userImageItem(context)
//      ],
//    );
//  }
//
//  incomingChatTyping(context, BaseModel chat, {@required bool typing}) {
//    return new Stack(
//      children: <Widget>[
//        Container(
//            height: 40,
//            margin: EdgeInsets.fromLTRB(60, 0, 60, 15),
//            child: Column(
//              children: [
//                Card(
//                  clipBehavior: Clip.antiAlias, color: default_white,
//                  elevation: elevation, //shadowColor: black.withOpacity(.3),
//                  shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.only(
//                    topRight: Radius.circular(25),
//                    topLeft: Radius.circular(0),
//                    bottomLeft: Radius.circular(25),
//                    bottomRight: Radius.circular(25),
//                  )),
//                  child: new Container(
//                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
//                    decoration: BoxDecoration(
////                  color: white,
//                        borderRadius: BorderRadius.circular(10)),
//                    child: Column(
//                      mainAxisSize: MainAxisSize.min,
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//
//
//
//
//                        Row(
//                          mainAxisSize: MainAxisSize.min,
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          children: <Widget>[
//
//Container(
//                width: 50,
//                height: 10,
//                child: LoadingIndicator(
//                  indicatorType: Indicator.ballBeat,
//                  color: black.withOpacity(.5),
//                )),
//
//                            // Container(
//                            //   width: 5,
//                            //   height: 5,
//                            //   decoration: BoxDecoration(
//                            //       color: blue0, shape: BoxShape.circle),
//                            // ),
//                            // addSpaceWidth(5),
//                            // Container(
//                            //   width: 4,
//                            //   height: 4,
//                            //   decoration: BoxDecoration(
//                            //       color: blue0, shape: BoxShape.circle),
//                            // ),
//                            // addSpaceWidth(5),
//                            // Container(
//                            //   width: 3,
//                            //   height: 3,
//                            //   decoration: BoxDecoration(
//                            //       color: blue0, shape: BoxShape.circle),
//                            // ),
//                            addSpaceWidth(5),
//                            if (!typing)
//                              Icon(
//                                Icons.mic,
//                                color: blue0,
//                                size: 12,
//                              )
//                          ],
//                        ),
//                      ],
//                    ),
//                  ),
//                ),
//              ],
//            )),
//        userImageItem(context)
//      ],
//    );
//  }
//
//  outgoingChatText(context, BaseModel chat) {
//    if (chat.getBoolean(DELETED)) {
//      return Container();
//    }
//    String message = chat.getString(MESSAGE);
//
//    String chatId = chat.getString(CHAT_ID);
//    chatId = getOtherPersonId(chat);
//    bool read = chat.getList(READ_BY).contains(chatId);
//
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    Map<String, HighlightedWord> words = {
//      searchMessage: HighlightedWord(
//        onTap: () {},
//        textStyle: textStyle(true, 17, blue0),
//      ),
//    };
//    return new GestureDetector(
//      onLongPress: () {
//        showChatOptions(context, chat);
//      },
//      child: Container(
//        margin: EdgeInsets.fromLTRB(60, 0, 20, 15),
//        child: Column(
//          mainAxisSize: MainAxisSize.min,
//          crossAxisAlignment: CrossAxisAlignment.end,
//          children: [
//            if (replyData.items.isNotEmpty)
//              chatReplyWidget(
//                replyData,
//              ),
//            Card(
//              //elevation: 5,
//              //shadowColor: black.withOpacity(.3),
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.only(
//                topRight: Radius.circular(0),
//                topLeft: Radius.circular(25),
//                bottomLeft: Radius.circular(25),
//                bottomRight: Radius.circular(25),
//              )),
//              child: new Container(
//                margin: const EdgeInsets.fromLTRB(15, 10, 20, 10),
//                child: Column(
//                  mainAxisSize: MainAxisSize.min,
//                  crossAxisAlignment: CrossAxisAlignment.end,
//                  children: <Widget>[
//                    TextHighlight(
//                      text:
//                          message, // You need to pass the string you want the highlights
//                      words: words, // Your dictionary words
//                      // textStyle: TextStyle( // You can set the general style, like a Text()
//                      //   fontSize: 20.0,
//                      //   color: Colors.black,
//                      // ),
//                      textStyle: textStyle(false, 17, black),
//                      textAlign: TextAlign
//                          .justify, // You can use any attribute of the RichText widget
//                    ),
////                  Text(
////                    message,
////                    style: textStyle(false, 17, black),
////                  ),
////              addSpace(3),
////              Text(
////                getChatTime(chat.getInt(TIME)),
////                style: textStyle(false, 12, black.withOpacity(.3)),
////              ),
//                  ],
//                ),
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  incomingChatDoc(context, BaseModel chat, bool exists, onComplete) {
//    if (chat.getBoolean(DELETED)) {
//      return incomingChatDeleted(context, chat);
//    }
//    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
//      return Container();
//    }
//
//    String fileUrl = chat.getString(FILE_URL);
//    String fileName = chat.getString(FILE_NAME);
//    String size = chat.getString(FILE_SIZE);
//    String ext = chat.getString(FILE_EXTENSION);
//    bool downloading = upOrDown.contains(chat.getObjectId());
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
////  return Container();
//    return new Stack(
//      children: <Widget>[
//        Opacity(
//          opacity: fileUrl.isEmpty ? (.4) : 1,
//          child: new GestureDetector(
//            onLongPress: () {
//              showChatOptions(context, chat);
//            },
//            onTap: () async {
//              if (fileUrl.isEmpty || !exists) return;
//
//              /*if (!exists) {
//              downloadChatFile(chat, false, onComplete);
//              return;
//            }*/
//
//              String fileName =
//                  "${chat.getObjectId()}.${chat.getString(FILE_EXTENSION)}";
//              File file = await getDirFile(fileName);
//              await openTheFile(file.path);
//            },
//            child: Container(
//              width: 200,
//              margin: EdgeInsets.fromLTRB(60, 0, 0, 15),
//              child: Column(
//                mainAxisSize: MainAxisSize.min,
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: [
//                  if (replyData.items.isNotEmpty)
//                    chatReplyWidget(
//                      replyData,
//                    ),
//                  new Card(
//                    clipBehavior: Clip.antiAlias,
//                    color: default_white,
//                    elevation: elevation,
//                    /*shadowColor: black.withOpacity(.3),*/
//                    shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.only(
//                      topRight: Radius.circular(25),
//                      topLeft: Radius.circular(0),
//                      bottomLeft: Radius.circular(25),
//                      bottomRight: Radius.circular(25),
//                    )),
//                    child: Column(
//                      mainAxisSize: MainAxisSize.min,
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        new Container(
//                          width: 250,
//                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
//                          margin: EdgeInsets.all(5),
//                          decoration: BoxDecoration(
////                        color: black.withOpacity(.2),
//                              borderRadius: BorderRadius.circular(5)),
//                          child: Row(
//                            crossAxisAlignment: CrossAxisAlignment.center,
//                            children: <Widget>[
//                              //addSpaceWidth(10),
//                              Image.asset(
//                                getExtImage(ext),
//                                width: 20,
//                                height: 20,
//                              ),
//                              addSpaceWidth(10),
//                              new Flexible(
//                                flex: 1,
//                                fit: FlexFit.tight,
//                                child: Column(
//                                  crossAxisAlignment: CrossAxisAlignment.start,
//                                  mainAxisSize: MainAxisSize.min,
//                                  children: <Widget>[
//                                    Text(
//                                      fileName,
//                                      maxLines: 1,
//                                      //overflow: TextOverflow.ellipsis,
//                                      style: textStyle(false, 14, black),
//                                    ),
//                                    addSpace(3),
//                                    Text(
//                                      size,
//                                      maxLines: 1,
//                                      overflow: TextOverflow.ellipsis,
//                                      style: textStyle(
//                                          false, 12, black.withOpacity(.5)),
//                                    ),
//                                  ],
//                                ),
//                              ),
//                              //addSpaceWidth(5),
//                              exists || fileUrl.isEmpty
//                                  ? Container()
//                                  : Container(
//                                      width: 27,
//                                      height: 27,
//                                      margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
//                                      decoration: BoxDecoration(
//                                          shape: BoxShape.circle,
//                                          color: blue0,
//                                          border: Border.all(
//                                              width: 1, color: blue0)),
//                                      child: GestureDetector(
//                                        onTap: () {
//                                          if (!downloading) {
//                                            downloadChatFile(chat, onComplete);
//                                          }
//                                        },
//                                        child: (!downloading)
//                                            ? Container(
//                                                width: 27,
//                                                height: 27,
//                                                child: Icon(
//                                                  Icons.arrow_downward,
//                                                  color: white,
//                                                  size: 15,
//                                                ),
//                                              )
//                                            : Container(
//                                                margin: EdgeInsets.all(3),
//                                                child:
//                                                    CircularProgressIndicator(
//                                                  //value: 20,
//                                                  valueColor:
//                                                      AlwaysStoppedAnimation<
//                                                          Color>(white),
//                                                  strokeWidth: 2,
//                                                ),
//                                              ),
//                                      ),
//                                    )
//                              //addSpaceWidth(5),
//                            ],
//                          ),
//                        ),
////                  addSpace(3),
////                  Padding(
////                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
////                    child: Text(
////                      getChatTime(chat.getInt(TIME)),
////                      style: textStyle(false, 12, black.withOpacity(.3)),
////                    ),
////                  ),
//                      ],
//                    ),
//                  ),
//                ],
//              ),
//            ),
//          ),
//        ),
//        userImageItem(context)
//      ],
//    );
//  }
//
//  outgoingChatDoc(context, BaseModel chat, onSaved) {
//    if (chat.getBoolean(DELETED)) {
//      return Container();
//    }
//    String fileName = chat.getString(FILE_NAME);
//    String size = chat.getString(FILE_SIZE);
//    String ext = chat.getString(FILE_EXTENSION);
//    bool uploading = upOrDown.contains(chat.getObjectId());
//    String filePath = chat.getString(FILE_PATH);
//    String fileUrl = chat.getString(FILE_URL);
//
//    String chatId = chat.getString(CHAT_ID);
//    chatId = getOtherPersonId(chat);
//    bool read = chat.getList(READ_BY).contains(chatId);
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    return new GestureDetector(
//      onLongPress: () {
//        //long pressed chat...
//        showChatOptions(context, chat);
//      },
//      onTap: () async {
//        if (fileUrl.isEmpty) return;
//
//        await openTheFile(filePath);
//      },
//      child: Container(
//        margin: EdgeInsets.fromLTRB(40, 0, 20, 15),
//        width: 200,
//        child: Column(
//          mainAxisSize: MainAxisSize.min,
//          crossAxisAlignment: CrossAxisAlignment.end,
//          children: [
//            if (replyData != null)
//              chatReplyWidget(
//                replyData,
//              ),
//            new Card(
//              color: read ? blue3 : blue0,
//              clipBehavior: Clip.antiAlias,
//              elevation: elevation,
//              /*shadowColor: black.withOpacity(.3),*/
//              shape: RoundedRectangleBorder(
//                  borderRadius: chat.myItem()
//                      ? BorderRadius.only(
//                          bottomLeft: Radius.circular(25),
//                          bottomRight: Radius.circular(25),
//                          topLeft: Radius.circular(25),
//                          topRight: Radius.circular(0),
//                        )
//                      : BorderRadius.only(
//                          bottomLeft: Radius.circular(25),
//                          bottomRight: Radius.circular(25),
//                          topLeft: Radius.circular(25),
//                          topRight: Radius.circular(0),
//                        )),
//              child: Column(
//                mainAxisSize: MainAxisSize.min,
//                crossAxisAlignment: CrossAxisAlignment.end,
//                children: <Widget>[
//                  new Container(
//                    width: 250,
//                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
//                    margin: EdgeInsets.all(5),
//                    decoration: BoxDecoration(
////                  color: black.withOpacity(.2),
//                        borderRadius: BorderRadius.circular(5)),
//                    child: Row(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: <Widget>[
//                        //addSpaceWidth(10),
//                        fileUrl.isNotEmpty
//                            ? Container()
//                            : new Container(
//                                width: 27,
//                                height: 27,
//                                margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
//                                decoration: BoxDecoration(
//                                    shape: BoxShape.circle,
//                                    color: blue0.withOpacity(.3),
//                                    border: Border.all(width: 1, color: blue0)),
//                                child: GestureDetector(
//                                  onTap: () {
//                                    if (!uploading) {
//                                      saveChatFile(
//                                          chat, FILE_PATH, FILE_URL, onSaved);
//                                      onSaved();
//                                    }
//                                  },
//                                  child: uploading
//                                      ? Container(
//                                          margin: EdgeInsets.all(3),
//                                          child: CircularProgressIndicator(
//                                            //value: 20,
//                                            valueColor:
//                                                AlwaysStoppedAnimation<Color>(
//                                                    white),
//                                            strokeWidth: 2,
//                                          ),
//                                        )
//                                      : Container(
//                                          width: 27,
//                                          height: 27,
//                                          child: Icon(
//                                            Icons.arrow_upward,
//                                            color: white,
//                                            size: 15,
//                                          ),
//                                        ),
//                                )),
//
//                        new Flexible(
//                          flex: 1,
//                          fit: FlexFit.tight,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.end,
//                            mainAxisSize: MainAxisSize.min,
//                            children: <Widget>[
//                              Text(
//                                fileName,
//                                maxLines: 1,
//                                //overflow: TextOverflow.ellipsis,
//                                style: textStyle(true, 14, white),
//                              ),
//                              addSpace(3),
//                              Text(
//                                size,
//                                maxLines: 1,
//                                overflow: TextOverflow.ellipsis,
//                                style:
//                                    textStyle(false, 12, white.withOpacity(.5)),
//                              ),
//                            ],
//                          ),
//                        ),
//                        //addSpaceWidth(5),
//
//                        addSpaceWidth(10),
//                        Image.asset(
//                          getExtImage(ext),
//                          width: 20,
//                          height: 20,
//                        ),
//
//                        //addSpaceWidth(5),
//                      ],
//                    ),
//                  ),
//                  //addSpace(3),
////            Padding(
////              padding: const EdgeInsets.fromLTRB(15, 0, 10, 10),
////              child: Text(
////                getChatTime(chat.getInt(TIME)),
////                style: textStyle(false, 12, white.withOpacity(.3)),
////              ),
////            ),
//                ],
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  incomingChatImage(context, BaseModel chat) {
//    if (chat.getBoolean(DELETED)) {
//      return incomingChatDeleted(context, chat);
//    }
//    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
//      return Container();
//    }
//    //List images = chat.getList(IMAGES);
//    String firstImage = chat.getString(IMAGE_URL);
//
////  return Container();
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    return new Stack(
//      children: <Widget>[
//        new GestureDetector(
//          onLongPress: () {
//            //long pressed chat...
//            showChatOptions(context, chat);
//          },
//          onTap: () {
//            if (firstImage.isEmpty) return;
//            pushAndResult(context, ViewImage([firstImage], 0));
//          },
//          child: Container(
//            width: 250,
////            height: 200,
//            margin: EdgeInsets.fromLTRB(65, 0, 40, 15),
//            child: Column(
//              mainAxisSize: MainAxisSize.min,
//              crossAxisAlignment: CrossAxisAlignment.start,
//              children: [
//                if (replyData != null)
//                  chatReplyWidget(
//                    replyData,
//                  ),
//                Container(
//                  width: 250,
//                  height: 200,
//                  child: new Card(
//                    clipBehavior: Clip.antiAlias,
//                    color: default_white,
//                    elevation: elevation,
//                    /*shadowColor: black.withOpacity(.3),*/
//                    shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.only(
//                      topRight: Radius.circular(25),
//                      topLeft: Radius.circular(0),
//                      bottomLeft: Radius.circular(25),
//                      bottomRight: Radius.circular(25),
//                    )),
//                    child: Stack(
//                      fit: StackFit.expand,
//                      children: <Widget>[
//                        CachedNetworkImage(
//                            imageUrl: firstImage,
//                            width: 200,
//                            height: 200,
//                            fit: BoxFit.cover,
//                            placeholder: (c, p) {
//                              return placeHolder(200, width: double.infinity);
//                            }),
//                        Align(
//                          alignment: Alignment.bottomCenter,
//                          child: gradientLine(height: 40),
//                        ),
//                        Align(
//                            alignment: Alignment.bottomLeft,
//                            child: Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: Text(
//                                getChatTime(chat.getInt(TIME)),
//                                style:
//                                    textStyle(false, 12, white.withOpacity(.3)),
//                              ),
//                            )),
//                      ],
//                    ),
//                  ),
//                ),
//              ],
//            ),
//          ),
//        ),
//        userImageItem(context)
//      ],
//    );
//  }
//
//  outgoingChatImage(context, BaseModel chat, onSaved) {
//    if (chat.getBoolean(DELETED)) {
//      return Container();
//    }
//    String imageUrl = chat.getString(IMAGE_URL);
//    String imagePath = chat.getString(IMAGE_PATH);
//    bool uploading = upOrDown.contains(chat.getObjectId());
//
//    String chatId = chat.getString(CHAT_ID);
//    chatId = getOtherPersonId(chat);
//    bool read = chat.getList(READ_BY).contains(chatId);
//
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    return new GestureDetector(
//      onLongPress: () {
//        //long pressed chat...
//        showChatOptions(context, chat);
//      },
//      onTap: () {
//        if (imageUrl.isEmpty) return;
//        pushAndResult(context, ViewImage([imageUrl], 0));
//      },
//      child: Container(
//        margin: EdgeInsets.fromLTRB(40, 0, 20, 15),
//        width: 250,
//        child: Column(
//          mainAxisSize: MainAxisSize.min,
//          crossAxisAlignment: CrossAxisAlignment.end,
//          children: [
//            if (replyData != null)
//              chatReplyWidget(
//                replyData,
//              ),
//            Container(
////            width: 290,
//              height: 200,
//              child: new Card(
//                color: blue0,
//                clipBehavior: Clip.antiAlias,
//                margin: EdgeInsets.all(0),
//                elevation: elevation,
//                /*shadowColor: black.withOpacity(.3),*/
//                shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.only(
//                  bottomLeft: Radius.circular(25),
//                  bottomRight: Radius.circular(25),
//                  topLeft: Radius.circular(25),
//                  topRight: Radius.circular(0),
//                )),
//                child: Stack(
//                  fit: StackFit.expand,
//                  children: <Widget>[
//                    imageUrl.isEmpty
//                        ? Image.file(
//                            File(imagePath),
//                            fit: BoxFit.cover,
//                          )
//                        : CachedNetworkImage(
//                            imageUrl: imageUrl,
//                            fit: BoxFit.cover,
//                            placeholder: (c, p) {
//                              return placeHolder(200, width: double.infinity);
//                            }),
//                    Align(
//                      alignment: Alignment.bottomCenter,
//                      child: gradientLine(height: 40),
//                    ),
//                    Align(
//                        alignment: Alignment.bottomRight,
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: Text(
//                            getChatTime(chat.getInt(TIME)),
//                            style: textStyle(false, 12, white.withOpacity(.3)),
//                          ),
//                        )),
//                    imageUrl.isNotEmpty
//                        ? Container()
//                        : Align(
//                            alignment: Alignment.center,
//                            child: GestureDetector(
//                              onTap: () {
//                                if (!uploading) {
//                                  saveChatFile(
//                                      chat, IMAGE_PATH, IMAGE_URL, onSaved);
//                                  onSaved();
//                                }
//                              },
//                              child: Container(
//                                width: 40,
//                                height: 40,
//                                decoration: BoxDecoration(
//                                    color: black.withOpacity(.9),
//                                    border: Border.all(color: white, width: 1),
//                                    shape: BoxShape.circle),
//                                child: uploading
//                                    ? Container(
//                                        margin: EdgeInsets.all(5),
//                                        child: CircularProgressIndicator(
//                                          //value: 20,
//                                          valueColor:
//                                              AlwaysStoppedAnimation<Color>(
//                                                  white),
//                                          strokeWidth: 2,
//                                        ),
//                                      )
//                                    : Center(
//                                        child: Icon(
//                                          Icons.arrow_upward,
//                                          color: white,
//                                          size: 20,
//                                        ),
//                                      ),
//                              ),
//                            ),
//                          )
//                  ],
//                ),
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  incomingChatVideo(context, BaseModel chat) {
//    if (chat.getBoolean(DELETED)) {
//      return incomingChatDeleted(context, chat);
//    }
//    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
//      return Container();
//    }
//
//    String videoUrl = chat.getString(VIDEO_URL);
//    String thumb = chat.getString(THUMBNAIL_URL);
//    String videoLenght = chat.getString(VIDEO_LENGTH);
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    return new Stack(
//      children: <Widget>[
//        Opacity(
//          opacity: videoUrl.isEmpty ? (.4) : 1,
//          child: new GestureDetector(
//            onLongPress: () {
//              showChatOptions(context, chat);
//              //long pressed chat...
//            },
//            child: Container(
//              width: 250,
//              margin: EdgeInsets.fromLTRB(65, 0, 40, 15),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: [
//                  if (replyData != null)
//                    chatReplyWidget(
//                      replyData,
//                    ),
//                  new Card(
//                    clipBehavior: Clip.antiAlias,
//                    color: default_white,
//                    elevation: elevation,
//                    /*shadowColor: black.withOpacity(.3),*/
//                    shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.only(
//                      topRight: Radius.circular(25),
//                      topLeft: Radius.circular(0),
//                      bottomLeft: Radius.circular(25),
//                      bottomRight: Radius.circular(25),
//                    )),
//                    child: Column(
//                      mainAxisSize: MainAxisSize.min,
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        new Container(
//                            color: blue0.withOpacity(.1),
//                            width: 250,
//                            height: 150,
//                            child: new GestureDetector(
//                                onTap: () {
//                                  if (videoUrl.isEmpty) return;
//
//                                  pushAndResult(context,
//                                      PlayVideo(chat.getObjectId(), videoUrl));
//                                },
//                                child: new Stack(
//                                  children: <Widget>[
//                                    thumb.isEmpty
//                                        ? Container()
//                                        : CachedNetworkImage(
//                                            imageUrl: thumb,
//                                            fit: BoxFit.cover,
//                                            width: double.infinity,
//                                            height: 250,
//                                          ),
//                                    Center(
//                                      child: new Container(
//                                        width: 40,
//                                        height: 40,
//                                        decoration: BoxDecoration(
//                                            color: black.withOpacity(.9),
//                                            border: Border.all(
//                                                color: white, width: 1),
//                                            shape: BoxShape.circle),
//                                        child: videoUrl.isNotEmpty
//                                            ? Center(
//                                                child: Icon(
//                                                  Icons.play_arrow,
//                                                  color: white,
//                                                  size: 20,
//                                                ),
//                                              )
//                                            : Container(),
//                                      ),
//                                    ),
//                                    new Column(
//                                      crossAxisAlignment:
//                                          CrossAxisAlignment.end,
//                                      children: <Widget>[
//                                        Expanded(flex: 1, child: Container()),
//                                        Row(
//                                          crossAxisAlignment:
//                                              CrossAxisAlignment.end,
//                                          children: <Widget>[
//                                            Padding(
//                                              padding:
//                                                  const EdgeInsets.fromLTRB(
//                                                      8, 0, 0, 8),
//                                              child: Text(
//                                                getChatTime(chat.getInt(TIME)),
//                                                style: textStyle(false, 12,
//                                                    white.withOpacity(.3)),
//                                              ),
//                                            ),
//                                            Flexible(
//                                                flex: 1, child: Container()),
//                                            Container(
//                                              margin: EdgeInsets.all(10),
//                                              decoration: BoxDecoration(
//                                                  color: black.withOpacity(.9),
//                                                  borderRadius:
//                                                      BorderRadius.circular(
//                                                          10)),
//                                              child: Padding(
//                                                padding:
//                                                    const EdgeInsets.fromLTRB(
//                                                        8, 4, 8, 4),
//                                                child: Text(videoLenght,
//                                                    style: textStyle(
//                                                        false, 12, white)),
//                                              ),
//                                            ),
//                                          ],
//                                        ),
//                                      ],
//                                    )
//                                  ],
//                                ))),
//                        /* addSpace(3),
//                      Padding(
//                        padding: const EdgeInsets.fromLTRB(15, 5, 15, 10),
//                        child: Text(
//                          getChatTime(chat.getInt(TIME)),
//                          style: textStyle(false, 12, black.withOpacity(.3)),
//                        ),
//                      ),*/
//                      ],
//                    ),
//                  ),
//                ],
//              ),
//            ),
//          ),
//        ),
//        userImageItem(context)
//      ],
//    );
//  }
//
//  outgoingChatVideo(context, BaseModel chat, onSaved) {
//    if (chat.getBoolean(DELETED)) {
//      return Container();
//    }
//    String videoUrl = chat.getString(VIDEO_URL);
//    String videoPath = chat.getString(VIDEO_PATH);
//    String thumbPath = chat.getString(THUMBNAIL_PATH);
//    String thumb = chat.getString(THUMBNAIL_URL);
//    String videoLenght = chat.getString(VIDEO_LENGTH);
//    bool uploading = upOrDown.contains(chat.getObjectId());
//
//    String chatId = chat.getString(CHAT_ID);
//    chatId = getOtherPersonId(chat);
//    bool read = chat.getList(READ_BY).contains(chatId);
//
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    return new GestureDetector(
//      onLongPress: () {
//        showChatOptions(context, chat);
//        //long pressed chat...
//      },
//      child: Container(
//        margin: EdgeInsets.fromLTRB(40, 0, 20, 15),
//        width: 250,
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.end,
//          children: [
//            if (replyData != null)
//              chatReplyWidget(
//                replyData,
//              ),
//            new Card(
//              color: blue0,
//              clipBehavior: Clip.antiAlias,
//              elevation: elevation,
//              /*shadowColor: black.withOpacity(.3),*/
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.only(
//                bottomLeft: Radius.circular(25),
//                bottomRight: Radius.circular(25),
//                topLeft: Radius.circular(25),
//                topRight: Radius.circular(0),
//              )),
//              child: Column(
//                mainAxisSize: MainAxisSize.min,
//                crossAxisAlignment: CrossAxisAlignment.end,
//                children: <Widget>[
//                  new Container(
//                      color: blue0.withOpacity(.1),
//                      width: 250,
//                      height: 150,
//                      child: new GestureDetector(
//                          onTap: () {
//                            if (videoUrl.isNotEmpty) {
//                              pushAndResult(
//                                  context,
//                                  PlayVideo(
//                                    chat.getObjectId(),
//                                    videoUrl,
//                                    videoFile: File(videoPath),
//                                  ));
//                            } else {
//                              if (!uploading) {
//                                saveChatVideo(chat, onSaved);
//                                onSaved();
//                              }
//                            }
//                          },
//                          child: new Stack(
//                            fit: StackFit.expand,
//                            children: <Widget>[
//                              thumb.isEmpty
//                                  ? Image.file(
//                                      File(thumbPath),
//                                      fit: BoxFit.cover,
//                                    )
//                                  : CachedNetworkImage(
//                                      imageUrl: thumb,
//                                      fit: BoxFit.cover,
//                                      placeholder: (c, p) {
//                                        return placeHolder(250,
//                                            width: double.infinity);
//                                      }),
//                              /*
//                            thumb.isEmpty
//                                ? Container()
//                                : CachedNetworkImage(
//                                    imageUrl: thumb,
//                                    fit: BoxFit.cover,
//                                    width: double.infinity,
//                                    height: 250,
//                                  ),*/
//                              Center(
//                                child: new Container(
//                                  width: 40,
//                                  height: 40,
//                                  decoration: BoxDecoration(
//                                      color: black.withOpacity(.9),
//                                      border:
//                                          Border.all(color: white, width: 1),
//                                      shape: BoxShape.circle),
//                                  child: videoUrl.isNotEmpty
//                                      ? Center(
//                                          child: Icon(
//                                            Icons.play_arrow,
//                                            color: white,
//                                            size: 20,
//                                          ),
//                                        )
//                                      : (!uploading)
//                                          ? Center(
//                                              child: Icon(
//                                                Icons.arrow_upward,
//                                                color: white,
//                                                size: 20,
//                                              ),
//                                            )
//                                          : Container(
//                                              margin: EdgeInsets.all(5),
//                                              child: CircularProgressIndicator(
//                                                //value: 20,
//                                                valueColor:
//                                                    AlwaysStoppedAnimation<
//                                                        Color>(white),
//                                                strokeWidth: 2,
//                                              ),
//                                            ),
//                                ),
//                              ),
//                              new Column(
//                                crossAxisAlignment: CrossAxisAlignment.end,
//                                children: <Widget>[
//                                  Expanded(flex: 1, child: Container()),
//                                  Row(
//                                    crossAxisAlignment: CrossAxisAlignment.end,
//                                    children: <Widget>[
//                                      Container(
//                                        margin: EdgeInsets.all(10),
//                                        decoration: BoxDecoration(
//                                            color: black.withOpacity(.9),
//                                            borderRadius:
//                                                BorderRadius.circular(10)),
//                                        child: Padding(
//                                          padding: const EdgeInsets.fromLTRB(
//                                              8, 4, 8, 4),
//                                          child: Text(videoLenght,
//                                              style:
//                                                  textStyle(false, 12, white)),
//                                        ),
//                                      ),
//                                      Flexible(flex: 1, child: Container()),
//                                      Padding(
//                                        padding: const EdgeInsets.fromLTRB(
//                                            0, 0, 8, 8),
//                                        child: Text(
//                                          getChatTime(chat.getInt(TIME)),
//                                          style: textStyle(
//                                              false, 12, white.withOpacity(.3)),
//                                        ),
//                                      ),
//                                    ],
//                                  ),
//                                ],
//                              )
//                            ],
//                          ))),
//                  //addSpace(3),
//                ],
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  userImageItem(context) {
//    return new GestureDetector(
//      onTap: () {
////      pushAndResult(
////          context,
////          MyProfile1(
////            otherPersonModel,fromChat: true,
////          ),opaque: false);
//      },
//      child: new Container(
//        decoration: BoxDecoration(
//          border: Border.all(width: 2, color: white),
//          shape: BoxShape.circle,
//        ),
//        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
//        width: 40,
//        height: 40,
//        child: Stack(
//          children: <Widget>[
//            Card(
//              margin: EdgeInsets.all(0),
//              shape: CircleBorder(),
//              clipBehavior: Clip.antiAlias,
//              color: transparent,
//              elevation: .5,
//              child: Stack(
//                children: <Widget>[
//                  Container(
//                    width: 40,
//                    height: 40,
//                    color: blue0,
//                    child: Center(
//                        child: Icon(
//                      Icons.person,
//                      color: white,
//                      size: 15,
//                    )),
//                  ),
//                  CachedNetworkImage(
//                    width: 40,
//                    height: 40,
//                    imageUrl: otherPersonModel.getString(USER_IMAGE),
//                    fit: BoxFit.cover,
//                  ),
//                ],
//              ),
//            ),
//            if (isOnline(otherPersonModel))
//              Align(
//                alignment: Alignment.bottomRight,
//                child: Container(
//                  width: 10,
//                  height: 10,
//                  decoration: BoxDecoration(
//                    shape: BoxShape.circle,
//                    border: Border.all(color: white, width: 2),
//                    color: red0,
//                  ),
//                ),
//              ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  saveChatFile(BaseModel model, String pathKey, String urlKey, onSaved) {
//    upOrDown.add(model.getObjectId());
//    String path = model.getString(pathKey);
//    uploadFile(File(path), (_, error) {
//      upOrDown.removeWhere((s) => s == model.getObjectId());
//      if (error != null) {
//        return;
//      }
//      model.put(urlKey, _);
//      model.updateItems();
//      onSaved();
//    });
//  }
//
//  saveChatVideo(BaseModel model, onSaved) {
//    String thumb = model.getString(THUMBNAIL_PATH);
//    String videoPath = model.getString(VIDEO_PATH);
//    String thumbUrl = model.getString(THUMBNAIL_URL);
//    String videoUrl = model.getString(VIDEO_URL);
//
//    bool uploadingThumb = thumbUrl.isEmpty;
//
//    if (videoUrl.isNotEmpty) {
//      onSaved();
//      return;
//    }
//
//    upOrDown.add(model.getObjectId());
//
//    uploadFile(File(uploadingThumb ? thumb : videoPath), (_, error) {
//      upOrDown.removeWhere((s) => s == model.getObjectId());
//      if (error != null) {
//        return;
//      }
//      model.put(uploadingThumb ? THUMBNAIL_URL : VIDEO_URL, _);
//      model.updateItems();
//      saveChatVideo(model, onSaved);
//    });
//  }
//
//  downloadChatFile(BaseModel model, onComplete) async {
//    String fileName =
//        "${model.getObjectId()}.${model.getString(FILE_EXTENSION)}";
//    File file = await getDirFile(fileName);
//    upOrDown.add(model.getObjectId());
//    onComplete();
//
//    QuerySnapshot shots = await Firestore.instance
//        .collection(REFERENCE_BASE)
//        .where(FILE_URL, isEqualTo: model.getString(FILE_URL))
//        .limit(1)
//        .getDocuments();
//    if (shots.documents.isEmpty) {
//      upOrDown.removeWhere((s) => s == model.getObjectId());
//      onComplete();
//    } else {
//      for (DocumentSnapshot doc in shots.documents) {
//        if (!doc.exists || doc.data.isEmpty) continue;
//        BaseModel model = BaseModel(doc: doc);
//        String ref = model.getString(REFERENCE);
//        StorageReference storageReference =
//            FirebaseStorage.instance.ref().child(ref);
//        storageReference.writeToFile(file).future.then((_) {
//          //toastInAndroid("Download Complete");
//          upOrDown.removeWhere((s) => s == model.getObjectId());
//          onComplete();
//        }, onError: (error) {
//          upOrDown.removeWhere((s) => s == model.getObjectId());
//          onComplete();
//        }).catchError((error) {
//          upOrDown.removeWhere((s) => s == model.getObjectId());
//          onComplete();
//        });
//
//        break;
//      }
//    }
//  }
//
//  showChatOptions(context, BaseModel chat, {bool deletedChat = false}) {
//    int type = chat.getInt(TYPE);
//    pushAndResult(
//        context,
//        listDialog(type == CHAT_TYPE_TEXT && !deletedChat
//            ? ["Copy", "Delete"]
//            : ["Delete"]),
//        opaque: false, result: (_) {
//      if (_ == "Copy") {
//        //ClipboardManager.copyToClipBoard(chat.getString(MESSAGE));
//      }
//      if (_ == "Delete") {
//        if (chat.myItem()) {
//          chat.put(DELETED, true);
//          chat.updateItems();
//        } else {
//          List hidden = List.from(chat.getList(HIDDEN));
//          hidden.add(userModel.getObjectId());
//          chat.put(HIDDEN, hidden);
//          chat.updateItems();
//        }
//      }
//    }, depend: false);
//  }
//
//  VideoPlayerController recAudioController;
//  String currentPlayingAudio;
//
//  bool recPlayEnded = false;
//  List noFileFound = [];
//  getChatAudioWidget(context, BaseModel chat, onEdited(bool removed)) {
//    if (chat.getBoolean(DELETED)) {
//      if (!chat.myItem()) {
//        return incomingChatDeleted(context, chat);
//      }
//      return Container();
//    }
////    return Container();
//    String audioUrl = chat.getString(AUDIO_URL);
//    String audioPath = chat.getString(AUDIO_PATH);
//    String audioLength = chat.getString(AUDIO_LENGTH);
//    bool uploading = upOrDown.contains(chat.getObjectId());
//    bool noFile = noFileFound.contains(chat.getObjectId());
//    bool currentPlay = currentPlayingAudio == chat.getObjectId();
//    bool isPlaying = recAudioController != null &&
//        recAudioController.value.initialized &&
//        recAudioController.value.isPlaying;
//    var parts = audioPath.split("/");
//    String baseFileName = parts[parts.length - 1];
//    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//    return Container(
////    height: chat.myItem()?null:40,
//      width: chat.myItem() ? null : 250,
//      margin: EdgeInsets.fromLTRB(0, 0, 0, chat.myItem() ? 15 : 10),
//      child: Stack(
//        children: [
//          Align(
//            alignment: Alignment.centerRight,
//            child: Opacity(
//              opacity: audioUrl.isEmpty && !chat.myItem() ? (.4) : 1,
//              child: new GestureDetector(
//                onTap: () async {
//                  if (audioUrl.isEmpty && !chat.myItem()) return;
//                  if (uploading) return;
//                  if (noFile) {
//                    showMessage(context, Icons.error, red0, "File not found",
//                        "This file no longer exist on your device");
//                    return;
//                  }
//
//                  if (!chat.myItem()) {
//                    String path = await localPath;
//                    File file =
//                        File("$path/${chat.getObjectId()}$baseFileName");
//                    print("File Path: ${file.path}");
//                    bool exists = await file.exists();
//                    if (!exists) {
//                      upOrDown.add(chat.getObjectId());
//                      onEdited(false);
//                      downloadFile(file, audioUrl, (e) {
//                        upOrDown.removeWhere(
//                            (element) => element == chat.getObjectId());
//                        fileThatExists.add(chat.getObjectId());
//                        onEdited(false);
//                      });
//                      return;
//                    } else {
//                      audioPath = file.path;
//                    }
//                  }
//
//                  if (currentPlayingAudio == chat.getObjectId()) {
//                    if (recAudioController != null &&
//                        recAudioController.value.initialized) {
//                      if (recAudioController.value.isPlaying) {
//                        recAudioController.pause();
//                      } else {
//                        currentPlayingAudio = chat.getObjectId();
//                        recAudioController.play();
//                        recPlayEnded = false;
//                        onEdited(false);
//                      }
//                    }
//                  } else {
//                    /*if (recAudioController != null) {
//                    await recAudioController.pause();
//                    await recAudioController.dispose();
//                    recAudioController = null;
//                  }*/
//                    try {
//                      await recAudioController.pause();
//                    } catch (e) {}
//                    try {
//                      await recAudioController.dispose();
//                    } catch (e) {}
//                    recAudioController = null;
//
//                    recAudioController =
//                        VideoPlayerController.file(File(audioPath));
//                    recAudioController.addListener(() async {
//                      if (recAudioController != null) {
//                        int currentTime =
//                            recAudioController.value.position.inSeconds;
//                        int fullTime = getSeconds(chat.getString(AUDIO_LENGTH));
//                        print("FullTime $fullTime CurrentTime $currentTime");
//                        if (currentTime >= fullTime - 1 && currentTime != 0) {
//                          if (recPlayEnded) return;
//                          recPlayEnded = true;
//                          await recAudioController.pause();
//                          await recAudioController.seekTo(Duration(seconds: 0));
//                          print("Play Finished");
//                          onEdited(false);
//                          /* Future.delayed(Duration(milliseconds: 200),()async{
//                            currentPlayingAudio="";
//                            recAudioController=null;
//                          });*/
//                        }
//                      }
//                    });
//                    recAudioController.initialize().then((value) {
//                      currentPlayingAudio = chat.getObjectId();
//                      recAudioController.play();
//                      recPlayEnded = false;
//                      onEdited(false);
//                    }).catchError((e) {
//                      showMessage(context, Icons.error, red0, "Audio Error",
//                          "This audio recording is corrupted");
//                    });
//                  }
//                },
//                onLongPress: () {
//                  showChatOptions(context, chat);
//                },
//                child: Container(
//                  width: 200,
//                  margin: EdgeInsets.fromLTRB(65, 0, chat.myItem() ? 20 : 0, 0),
//                  child: Column(
//                    crossAxisAlignment: !chat.myItem()
//                        ? (CrossAxisAlignment.start)
//                        : CrossAxisAlignment.end,
//                    mainAxisSize: MainAxisSize.min,
//                    children: [
//                      if (replyData != null)
//                        chatReplyWidget(
//                          replyData,
//                        ),
//                      new Container(
//                        height: 30,
//                        width: 200,
//                        color: transparent,
//                        child: Card(
//                          clipBehavior: Clip.antiAlias,
//                          elevation: elevation,
//                          /*shadowColor: black.withOpacity(.3),*/
//                          shape: RoundedRectangleBorder(
//                              borderRadius: chat.myItem()
//                                  ? BorderRadius.only(
//                                      bottomLeft: Radius.circular(25),
//                                      bottomRight: Radius.circular(25),
//                                      topLeft: Radius.circular(25),
//                                      topRight: Radius.circular(0),
//                                    )
//                                  : BorderRadius.only(
//                                      bottomLeft: Radius.circular(25),
//                                      bottomRight: Radius.circular(25),
//                                      topLeft: Radius.circular(0),
//                                      topRight: Radius.circular(25),
//                                    )),
//                          margin: EdgeInsets.all(0),
//                          color: isPlaying && currentPlay ? blue3 : blue0,
//                          child: Stack(
//                            fit: StackFit.expand,
//                            children: [
//                              /*LinearProgressIndicator(
//                                value:currentPlay?(playPosition / 100):0,
//                                backgroundColor: transparent,
//                                valueColor:
//                                AlwaysStoppedAnimation<Color>(black.withOpacity(.7)),
//                              ),*/
//                              Row(
//                                children: [
//                                  addSpaceWidth(10),
//                                  if (uploading)
//                                    Container(
//                                      width: 14,
//                                      height: 14,
//                                      child: CircularProgressIndicator(
//                                        //value: 20,
//                                        valueColor:
//                                            AlwaysStoppedAnimation<Color>(
//                                                white),
//                                        strokeWidth: 2,
//                                      ),
//                                    ),
//                                  if (!uploading)
//                                    chat.myItem()
//                                        ? (Icon(
//                                            currentPlay && isPlaying
//                                                ? (Icons.pause)
//                                                : Icons.play_circle_filled,
//                                            color: white,
//                                          ))
//                                        : (Icon(
//                                            !fileThatExists.contains(
//                                                    chat.getObjectId())
//                                                ? Icons.file_download
//                                                : currentPlay && isPlaying
//                                                    ? (Icons.pause)
//                                                    : Icons.play_circle_filled,
//                                            color: white,
//                                          )) /*FutureBuilder(
//                                  builder: (c,d){
//                                    if(!d.hasData)return Container();
//                                    bool exists = d.data;
//                                    return Icon(
//                                      !exists?Icons.file_download:currentPlay && isPlaying?
//                                      (Icons.pause):Icons.play_circle_filled,
//                                      color: white,
//                                    );
//                                  },future: checkLocalFile("${chat.getObjectId()}$baseFileName"),
//                                )*/
//                                  ,
//                                  Flexible(
//                                    fit: FlexFit.tight,
//                                    child: Container(
//                                      height: 2,
//                                      width: double.infinity,
//                                      decoration: BoxDecoration(
//                                          color: white,
//                                          borderRadius: BorderRadius.all(
//                                              Radius.circular(5))),
//                                    ),
//                                  ),
//                                  Container(
//                                    padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
//                                    decoration: BoxDecoration(
//                                        color: white,
//                                        borderRadius: BorderRadius.all(
//                                            Radius.circular(25))),
//                                    child: Text(
//                                      audioLength,
//                                      style: textStyle(false, 12, blue0),
//                                    ),
//                                  ),
//                                  addSpaceWidth(5),
//                                  if (noFile)
//                                    Container(
//                                        padding: EdgeInsets.all(1),
//                                        decoration: BoxDecoration(
//                                            color: white,
//                                            shape: BoxShape.circle),
//                                        child: Icon(
//                                          Icons.error,
//                                          color: red0,
//                                          size: 18,
//                                        )),
//                                  addSpaceWidth(10),
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//            ),
//          ),
//          if (!chat.myItem()) userImageItem(context)
//        ],
//      ),
//    );
//  }
//
//  chatReplyWidget(BaseModel chat, {onRemoved, bool fitScreen = false}) {
//    if (chat.items.isEmpty) return Container();
//    String text = chat.getString(MESSAGE);
//    int type = chat.getType();
//    if (type == CHAT_TYPE_DOC) text = "Document";
//    if (type == CHAT_TYPE_IMAGE) text = "Photo";
//    if (type == CHAT_TYPE_VIDEO)
//      text = "Video (${chat.getString(VIDEO_LENGTH)})";
//    if (type == CHAT_TYPE_REC)
//      text = "Voice Message (${chat.getString(AUDIO_LENGTH)})";
//    var icon;
//    if (type == CHAT_TYPE_DOC) icon = Icons.assignment;
//    if (type == CHAT_TYPE_IMAGE) icon = Icons.photo;
//    if (type == CHAT_TYPE_VIDEO) icon = Icons.videocam;
//    if (type == CHAT_TYPE_REC) icon = Icons.mic;
//
//    String image = "";
//    if (type == CHAT_TYPE_IMAGE) image = chat.getString(IMAGE_URL);
//    if (type == CHAT_TYPE_VIDEO) image = chat.getString(THUMBNAIL_URL);
//
//    return GestureDetector(
//      onTap: () {
//        if (onReplyTapped != null) onReplyTapped(chat.getObjectId());
//      },
//      child: Container(
////    width: 100,
//          width: fitScreen ? double.infinity : null,
//          child: Card(
//            clipBehavior: Clip.antiAlias,
//            color: default_white,
//            elevation: 0,
//            shape: RoundedRectangleBorder(
//                borderRadius: BorderRadius.all(Radius.circular(5)),
//                side: BorderSide(color: black.withOpacity(.1), width: .5)),
//            child: Container(
//              decoration: BoxDecoration(
//                  border: Border(left: BorderSide(color: blue3, width: 5))),
//              child: Row(
//                mainAxisSize: fitScreen ? MainAxisSize.max : MainAxisSize.min,
//                children: [
//                  Flexible(
//                    fit: fitScreen ? FlexFit.tight : FlexFit.loose,
//                    child: Padding(
//                      padding: EdgeInsets.fromLTRB(
//                          10,
//                          onRemoved == null ? 10 : 0,
//                          onRemoved == null ? 10 : 0,
//                          image.isEmpty ? 10 : 0),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        mainAxisSize: MainAxisSize.min,
//                        children: [
//                          Row(
//                            mainAxisSize: MainAxisSize.min,
//                            children: [
//                              Flexible(
//                                  fit: !fitScreen
//                                      ? FlexFit.loose
//                                      : FlexFit.tight,
//                                  child: Text(
//                                    chat.myItem()
//                                        ? "YOU"
//                                        : chat.getString(FIRST_NAME),
//                                    style: textStyle(
//                                        true, 12, black.withOpacity(.5)),
//                                  )),
//                              if (onRemoved != null && image.isEmpty)
//                                Container(
//                                  width: 30,
//                                  height: 25,
//                                  child: FlatButton(
//                                      padding: EdgeInsets.all(0),
//                                      onPressed: () {
//                                        onRemoved();
//                                      },
//                                      child: Icon(
//                                        Icons.close,
//                                        size: 15,
//                                        color: black.withOpacity(.5),
//                                      )),
//                                )
//                            ],
//                          ),
//                          Row(
//                            mainAxisSize: MainAxisSize.min,
//                            children: [
//                              if (icon != null)
//                                Icon(
//                                  icon,
//                                  size: 14,
//                                  color: black.withOpacity(.3),
//                                ),
//                              addSpaceWidth(3),
//                              Flexible(
//                                child: Text(
//                                  text,
//                                  style: textStyle(false, 14, black),
//                                  maxLines: 2,
//                                  overflow: TextOverflow.ellipsis,
//                                ),
//                              ),
//                              addSpaceWidth(10),
//                            ],
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                  if (image.isNotEmpty)
//                    Container(
//                      width: 50,
//                      height: 50,
//                      child: Stack(
//                        fit: StackFit.expand,
//                        children: [
//                          CachedNetworkImage(
//                            imageUrl: image,
//                            fit: BoxFit.cover,
//                          ),
//                          if (onRemoved != null)
//                            Align(
//                              alignment: Alignment.topRight,
//                              child: Container(
//                                width: 16,
//                                height: 16,
//                                margin: EdgeInsets.all(2),
//                                child: FlatButton(
//                                    padding: EdgeInsets.all(0),
//                                    onPressed: () {
//                                      onRemoved();
//                                    },
//                                    shape: CircleBorder(),
//                                    color: white.withOpacity(.5),
//                                    child: Icon(
//                                      Icons.close,
//                                      size: 12,
//                                      color: black.withOpacity(.5),
//                                    )),
//                              ),
//                            )
//                        ],
//                      ),
//                    )
//                ],
//              ),
//            ),
//          )),
//    );
//  }
//}
