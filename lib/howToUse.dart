import 'package:Strokes/basemodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class HowToUse {
  String USER_BASE = "userBase";
  String USER_NAME = "userName";
  String EMAIL = "email";
  String PASSWORD = "password";
  String PATH = "docPAth";
  String ITEM_ADDED = "itemAdded";

  addingToModel() {
    // call the baseModel
    BaseModel model = BaseModel();
    model.put(EMAIL, "ammaugost@gmail.com");
    model.put(PASSWORD, "12334566");
    model.put(USER_NAME, "ammaugost");
    //save to the userbase collection
    model.saveItem(USER_BASE, false, document: PATH);
  }

  queryFromFirebase() {
    Firestore.instance.collection(USER_BASE).document(PATH).get().then((value) {
      BaseModel model = BaseModel(doc: value);
      String email = model.getString(EMAIL);
      String pass = model.getString(PASSWORD);
      String userName = model.getString(USER_NAME);

      //i can still update the database by saying after adding items to the model
      model.put(ITEM_ADDED, "fresh");
      model.updateItems();
    });
  }
}
