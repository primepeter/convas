import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/PreSendVideo.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:Strokes/dialogs/listDialog.dart';
import 'package:Strokes/enter_pin.dart';
import 'package:Strokes/main_pages/ShareTo.dart';
import 'package:Strokes/notificationService.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:emoji_picker/emoji_picker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:filesize/filesize.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_size/flutter_keyboard_size.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_sound/flauto.dart';
import 'package:flutter_sound/flutter_sound_recorder.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:path/path.dart' as pathLib;
import 'package:path_provider/path_provider.dart' show getTemporaryDirectory;
import 'package:permission_handler/permission_handler.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:shimmer/shimmer.dart';
import 'package:synchronized/synchronized.dart';
import 'package:vibration/vibration.dart';
import 'package:video_player/video_player.dart';

import 'MainAdmin.dart';
import 'main_pages/ShowProfile.dart';
import 'main_pages/ShowWallet.dart';

List<String> upOrDown = List();
List<String> noFileFound = List();
String visibleChatId = "";
List<String> fileThatExists = List();
String searchMessage = "";

class ChatMain extends StatefulWidget {
  String chatId;
  BaseModel otherPerson;
  int chatMode;
  bool groupChat;
  ChatMain(this.chatId, this.chatMode, {this.otherPerson, this.groupChat});

  @override
  _ChatMainState createState() => _ChatMainState();
}

class _ChatMainState extends State<ChatMain>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  TextEditingController messageController = new TextEditingController();
  final ItemScrollController messageListController = ItemScrollController();
  List<BaseModel> chatList = List();

  BaseModel replyModel;
  FocusNode messageNode = FocusNode();
  String message = "";
  String chatId;
  BaseModel otherPerson;
  bool setup = false;

  bool showEmoji = false;
  bool keyboardVisible = false;
  bool amTyping = false;
  Timer timerType;
  String lastTypedText = "";
  int lastTyped = 0;

  final scaffoldKey = GlobalKey<ScaffoldState>();

  double elevation = 0;
  double xPosition = -1;
  double yPosition = -1;
  bool tappingDown = false;
  int blinkPosition = -1;
  int chatMode;
//  double recButtonSize = 40;

  double sendSize = 40;
  var sendIcon = Icons.mic;
  VideoPlayerController typingSoundController;
  VideoPlayerController messageSoundController;
  bool searchMode = false;

  TextEditingController searchController = TextEditingController();
  FocusNode focusSearch = FocusNode();
  List<int> searchPositions = [];
  bool hasSearched = false;
  int searchPositionIndex = 0;
//  BaseModel replyModel;

//  ChatUtils chatUtils;
  bool canSound = true;
  String chatDatabaseName;

  int messageDelay = 0;
  bool lockChat = false;
  bool groupChat;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    searchMessage = "";
    loadSound();
    loadRecorder();
    groupChat = widget.groupChat;
    chatMode = widget.chatMode;
    chatDatabaseName =
        chatMode == CHAT_MODE_ENCRYPT ? CHAT_BASE_ENCRYPT : CHAT_BASE;
    chatId = widget.chatId;
    visibleChatId = chatId;
    otherPerson = widget.otherPerson ?? BaseModel();
    groupChat = otherPerson.getString(GROUP_NAME).isNotEmpty;
    setState(() {});
    startup();

    timerType = new Timer.periodic(Duration(seconds: 1), (_) {
      if (!keyboardVisible) {
        if (amTyping) {
          setState(() {
            amTyping = false;
            updateTyping(false);
          });
        }
        return;
      }
      String newText = messageController.text;
      if (newText.trim().isEmpty) return;

      int now = DateTime.now().millisecondsSinceEpoch;

      if (newText != (lastTypedText)) {
        if (!amTyping) {
          amTyping = true;
          updateTyping(true);
          setState(() {});
        }
      } else {
        if ((now - lastTyped) > 2000) {
          if (amTyping) {
            amTyping = false;
            updateTyping(false);
            setState(() {});
          }
        }
      }

      lastTypedText = newText;
    });
    KeyboardVisibility.onChange.listen((bool visible) {
      keyboardVisible = visible;
      print("Keyboard Visible $visible");
      if (!visible) {
        amTyping = false;
        updateTyping(false);
        setState(() {});
      }
      if (showEmoji && visible) {
        showEmoji = false;
        setState(() {});
      }
    });
    handleTimedMessages();
    var audioSub = recAudioController.onPlayerCompletion.listen((event) {
      setState(() {
        isPlaying = false;
      });
    });
    subs.add(audioSub);
  }

  loadSound() async {
    File bubble = await loadFile('assets/sounds/typing.m4a', "typing.m4a");
    File bubble1 = await loadFile('assets/sounds/sent.m4a', "sent.m4a");
    typingSoundController = VideoPlayerController.file(bubble);
    messageSoundController = VideoPlayerController.file(bubble1);
    typingSoundController.initialize();
    messageSoundController.initialize();
  }

  startup() async {
    await Future.delayed(Duration(milliseconds: 8));
    if (widget.otherPerson != null) {
      loadChat();
      refreshOtherUser();
      return;
    }

    if (null == chatId || chatId.isEmpty) return;
    DocumentSnapshot doc = await Firestore.instance
        .collection(chatMode == CHAT_MODE_ENCRYPT
            ? CHAT_IDS_BASE_ENCRYPT
            : CHAT_IDS_BASE)
        .document(chatId)
        .get();
    if (!doc.exists) return;
    BaseModel model = BaseModel(doc: doc);
    groupChat = model.getString(GROUP_NAME).isNotEmpty;
    if (groupChat) {
      otherPerson = model;
      loadChat();
      refreshOtherUser();
      return;
    }

    String otherId = getOtherPersonId(model);
    if (otherId.isEmpty) return;
    DocumentSnapshot person =
        await Firestore.instance.collection(USER_BASE).document(otherId).get();
    if (!person.exists) return;

    otherPerson = BaseModel(doc: person);
    loadChat();
    refreshOtherUser();
  }

  refreshGroup() {
    if (null == chatId || chatId.isEmpty) return;
    String base =
        (chatMode == CHAT_MODE_ENCRYPT ? CHAT_IDS_BASE_ENCRYPT : CHAT_IDS_BASE);
    print(base);
    StreamSubscription<DocumentSnapshot> sub = Firestore.instance
        .collection(base)
        .document(widget.otherPerson.getObjectId())
        .snapshots()
        .listen((shot) {
      print(shot.data);
      otherPerson = BaseModel(doc: shot);
      if (otherPerson.getList(BANNED).contains(userModel.getObjectId())) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.pop(context);
        });
      }
      int now = DateTime.now().millisecondsSinceEpoch;
      int lastUpdated = otherPerson.getInt(TIME_UPDATED);
      int tsDiff = (now - lastUpdated);
      bool notTyping = (tsDiff > (Duration.millisecondsPerMinute * 1));
      bool notRecording = (tsDiff > (Duration.millisecondsPerMinute * 5));

      typingUser = otherPerson.getMap(TYPING_USER);
      recordingUser = otherPerson.getMap(REC_USER);
      isTyping = typingUser.isNotEmpty &&
          (!notTyping) &&
          typingUser[USER_ID] != userModel.getUserId();
      isRecording = recordingUser.isNotEmpty &&
          (!notRecording) &&
          recordingUser[USER_ID] != userModel.getUserId();

      if (mounted) setState(() {});
      if (isTyping) {
        playTypingSound();
      } else if (typingSoundController != null &&
          typingSoundController.value.initialized) {
        typingSoundController.pause();
      }
      loadMembers(otherPerson.getList(PARTIES));
    });

    subs.add(sub);
  }

  Map<String, BaseModel> groupMembers = {};
  loadMembers(List parties) {
    for (var id in parties) {
      Firestore.instance.collection(USER_BASE).document(id).get().then((value) {
        BaseModel model = BaseModel(doc: value);
        groupMembers[model.getObjectId()] = model;
        setState(() {});
      });
    }
    setState(() {});
  }

  refreshOtherUser() async {
    if (groupChat) {
      refreshGroup();
      return;
    }

    String otherPersonId = otherPerson.getUserId();
    if (otherPersonId.isEmpty) return;

    StreamSubscription<DocumentSnapshot> sub = Firestore.instance
        .collection(USER_BASE)
        .document(otherPersonId)
        .snapshots()
        .listen((shot) {
      if (shot == null) return;
      otherPerson = BaseModel(doc: shot);

//      if (userModel.getList(BLOCKED).contains(otherPersonId)) {
//        Future.delayed(Duration(seconds: 1), () {
//          Navigator.pop(context);
//        });
//      }
      int now = DateTime.now().millisecondsSinceEpoch;
      int lastUpdated = otherPerson.getInt(TIME_UPDATED);
      int tsDiff = (now - lastUpdated);
      bool notOnline = (tsDiff > (Duration.millisecondsPerMinute * 10));
      bool notTyping = (tsDiff > (Duration.millisecondsPerMinute * 1));
      isOnline = otherPerson.getBoolean(IS_ONLINE) && (!notOnline);
      isTyping = otherPerson.getString(TYPING_ID) == chatId && (!notTyping);
      bool notRecording = (tsDiff > (Duration.millisecondsPerMinute * 5));
      isRecording = otherPerson.getString(REC_ID) == chatId && (!notRecording);
      if (mounted) setState(() {});

      /*if(typingController!=null && typingController.value.initialized) {
        if (isTyping) {
          typingController.play();
        }else{
          typingController.pause();
        }
      }*/
      if (isTyping) {
        playTypingSound();
      } else if (typingSoundController != null &&
          typingSoundController.value.initialized) {
        typingSoundController.pause();
      }
    });

    subs.add(sub);
  }

  List audioSetupList = [];
  List<StreamSubscription> subs = List();
  void loadChat() async {
    if (null == chatId || chatId.isEmpty) return;
    StreamSubscription<QuerySnapshot> sub = Firestore.instance
        .collection(chatDatabaseName)
        .where(CHAT_ID, isEqualTo: chatId)
        .orderBy(TIME, descending: false)
        .snapshots()
        .listen((shots) async {
      for (DocumentChange docChange in shots.documentChanges) {
        if (docChange.type == DocumentChangeType.added &&
            loadedChatIds.contains(docChange.document.documentID)) continue;
        DocumentSnapshot doc = docChange.document;
        BaseModel chat = BaseModel(doc: doc);
        addChatToList(chat);
        final readBy = chat.getList(READ_BY);
        bool hasRead = readBy.contains(userModel.getString(USER_ID));
        if (!chat.myItem() && !hasRead) {
          readBy.add(userModel.getString(USER_ID));
          chat.put(READ_BY, readBy);
          chat.updateItems();
        }
      }
      refreshChatDates();
      setup = true;
      chatMessageController.add(true);
      if (mounted) setState(() {});
    });

    subs.add(sub);
  }

  handleTimedMessages() {
    if (!mounted) return;
    Future.delayed(Duration(seconds: 15), () async {
      //print("Handling Timed Messages");
      for (BaseModel model in chatList) {
        if (model.getInt(DELAY) == 0) continue;

        int now = DateTime.now().millisecondsSinceEpoch;
        int time = model.getInt(TIME);
        int delay = model.getInt(DELAY);
        int showTime = time + (delay * Duration.millisecondsPerMinute);
        if (now >= (showTime)) {
          model.put(DELAY, 0);
          model.put(DELETED, true);
          model.updateItems();
          /*BaseModel chat = new BaseModel(items: model.items);
          chat.put(DELAY, 0);
          chat.put(TIME, showTime);
          chat.put(TIME_UPDATED, showTime);
          chat.put(CREATED_AT, DateTime.fromMillisecondsSinceEpoch(showTime));
          chat.put(UPDATED_AT, DateTime.fromMillisecondsSinceEpoch(showTime));
          chat.saveItem(chatDatabaseName, false,
              document: chat.getObjectId(), ignoreTime: true);*/
        }
      }
      handleTimedMessages();
    });
  }

  List loadedChatIds = [];
  void addChatToList(BaseModel chat) async {
    //chatList.insert(0, chat);
    var lock = Lock();
    await lock.synchronized(() {
      int p = chatList
          .indexWhere((BaseModel c) => chat.getObjectId() == c.getObjectId());
      if (p != -1) {
        if (chatList[p].getTime() != chat.getTime()) {
          chatList.removeAt(p);
          chatList.insert(0, chat);
        } else {
          chatList[p] = chat;
        }
        print("updating chat at $p");
      } else {
        if (!chat.myItem()) playNewMessageSound();
        chatList.insert(0, chat);
//        if(mounted)setState(() {});
        if (!loadedChatIds.contains(chat.getObjectId()))
          loadedChatIds.add(chat.getObjectId());
        if (chat.getInt(TYPE) == CHAT_TYPE_DOC ||
            chat.getInt(TYPE) == CHAT_TYPE_REC) {
          checkIfFileExists(chat, false);
        }
      }
    });
  }

  int lastNewMessagePlayed = 0;
  playNewMessageSound() async {
    if (visibleChatId != chatId) return;
    int now = DateTime.now().millisecondsSinceEpoch;
    int diff = now - lastNewMessagePlayed;
    if (diff < 1000) return;
    lastNewMessagePlayed = now;

    if (messageSoundController == null) return;
    if (!messageSoundController.value.initialized) return;

    messageSoundController.setLooping(true);
    messageSoundController.setVolume(.1);
    messageSoundController.play();
    Future.delayed(Duration(seconds: 1), () {
      messageSoundController.pause();
    });
  }

  int lastTypingPlayed = 0;
  playTypingSound() async {
    if (visibleChatId != chatId) return;
    int now = DateTime.now().millisecondsSinceEpoch;
    int diff = now - lastTypingPlayed;
    if (diff < 2000) return;
    lastTypingPlayed = now;
    typingSoundController?.setLooping(true);
    typingSoundController?.setVolume(.1);
    typingSoundController?.play();
  }

//  List checkedList = [];
  checkIfFileExists(BaseModel model, bool notify) async {
    if (fileThatExists.contains(model.getObjectId())) return;

    if (model.getInt(TYPE) == CHAT_TYPE_REC) {
      String audioPath = model.getString(AUDIO_PATH);
      var parts = audioPath.split("/");
      String baseFileName = parts[parts.length - 1];
      bool exists = await checkLocalFile("${model.getObjectId()}$baseFileName");
      if (exists) fileThatExists.add(model.getObjectId());
      if (mounted) setState(() {});
      return;
    }

    String fileName =
        "${model.getObjectId()}.${model.getString(FILE_EXTENSION)}";
    File file = await getDirFile(fileName);
    bool fileExists = await file.exists();
    if (fileExists) {
      fileThatExists.add(model.getObjectId());
    }

    if (/*notify &&*/ mounted) setState(() {});
  }

  handleAudio(BaseModel model) async {
    var lock = Lock();
    await lock.synchronized(() {
      if (!audioSetupList.contains(model.getObjectId())) {
        audioSetupList.add(model.getObjectId());
        if (model.getString(AUDIO_URL).isEmpty && model.myItem()) {
          saveChatAudio(model);
        }
      }
    });
  }

  saveChatAudio(BaseModel chat) async {
    String audioPath = chat.getString(AUDIO_PATH);
    bool exists = await File(audioPath).exists();
    if (!exists) {
      noFileFound.add(chat.getObjectId());
      setState(() {});
      return;
    }
    upOrDown.add(chat.getObjectId());
    uploadFile(File(audioPath), (url, error) {
      upOrDown.removeWhere((s) => s == chat.getObjectId());
      if (error != null) {
        setState(() {});
        return;
      }
      chat.put(AUDIO_URL, url);
      chat.updateItems();
      setState(() {});
    });
  }

  List timePositions = List();
  int lastRefreshed = 0;
  int refreshedCounter = 0;
  refreshChatDates() async {
    int now = DateTime.now().millisecondsSinceEpoch;
    int diff = now - lastRefreshed;
    if (diff < 10000) return;
    lastRefreshed = now;
    timePositions.clear();
    if (refreshedCounter > 2) return;
    refreshedCounter++;
    print("Finding Time...");
    List<BaseModel> newList = [];
    for (BaseModel chat in chatList) {
      if (chatRemoved(chat)) continue;
      newList.add(chat);
    }
    print("New List ${newList.length}");
    for (int i = newList.length - 1; i >= 0; i--) {
      BaseModel chat = newList[i];
      int prevIndex = i + 1;
      BaseModel prevChat =
          prevIndex > newList.length - 1 ? null : newList[prevIndex];
      if (prevChat != null) {
        bool sameDay = isSameDay(chat.getTime(), prevChat.getTime());
        if (!sameDay) {
          timePositions.add(prevChat.getObjectId());
        }
//      timePositions.add(prevChat.documentId);
      }
    }
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    try {
      flutterSoundRecorder?.stopRecorder();
      releaseFlauto();
    } catch (err) {
      print('stopRecorder error: $err');
    }
    if (timerType != null) timerType.cancel();
//    updateTyping(false);

    for (StreamSubscription sub in subs) {
      sub.cancel();
    }
    WidgetsBinding.instance.removeObserver(this);
    typingSoundController?.dispose();
    messageSoundController?.dispose();

    super.dispose();
  }

  Future<void> releaseFlauto() async {
//    try {
//      await flutterSoundRecorder.closeAudioSession();
//    } catch (e) {
//      print('Released unsuccessful');
//      print(e);
//    }
  }

  ScreenHeight screenHeight;
  @override
  Widget build(BuildContext c) {
    return WillPopScope(
      onWillPop: () {
        if (searchMode) {
          setState(() {
            searchController.text = "";
            searchMode = false;
          });
          return Future.value(false);
        }
        if (showEmoji) {
          showEmoji = false;
          setState(() {});
          return Future.value(false);
          ;
        }
        visibleChatId = "";
        updateTyping(false);
        Navigator.pop(context, true);
        return Future.value(true);
      },
      child: KeyboardDismisser(
        gestures: [
          GestureType.onTap,
          GestureType.onPanUpdateDownDirection,
        ],
        child: KeyboardSizeProvider(
          smallSize: 500.0,
          child: Consumer<ScreenHeight>(
              builder: (context, ScreenHeight sh, child) {
            this.screenHeight = sh;
            return Scaffold(
                resizeToAvoidBottomInset: true,
                key: scaffoldKey,
                backgroundColor: white,
                body: Stack(
                  fit: StackFit.expand,
                  children: [
                    page(),
                    if (tooShort)
                      Align(
                        alignment: Alignment.bottomRight,
                        child: IgnorePointer(
                          ignoring: true,
                          child: Container(
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            margin: EdgeInsets.fromLTRB(0, 0, 10, 70),
                            decoration: BoxDecoration(
                                color: black.withOpacity(.8),
                                borderRadius: BorderRadius.circular(10)),
                            child: Text(
                              "Hold to start recording",
                              style: textStyle(true, 12, white),
                            ),
                          ),
                        ),
                      ),
                    if (recording && !canCancelRecording())
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                          margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                          child: Text(
                            recordTimerText,
                            style: textStyle(true, 20, AppConfig.appColor),
                          ),
                        ),
                      ),
                    if (message.isEmpty)
                      Positioned(
                        top: screenHeight.screenHeight -
                            (screenHeight.keyboardHeight) -
                            (77),
                        left: xPosition == -1
                            ? getScreenWidth(context) - (200)
                            : xPosition - 170,
//                  width: 80,
//                  height: 80,
                        child: Opacity(
                          opacity: !recording ? 0 : 1,
                          child: Row(
                            children: <Widget>[
                              IgnorePointer(
                                ignoring: true,
                                child: Container(
                                  margin: EdgeInsets.only(right: 10, top: 10),
                                  child: Shimmer.fromColors(
                                    baseColor: Colors.red,
                                    highlightColor: Colors.yellow,
                                    direction: ShimmerDirection.rtl,
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.arrow_back_ios,
                                          color: black.withOpacity(.5),
                                        ),
                                        addSpaceWidth(5),
                                        Text(
                                          "Slide to Cancel",
                                          style: textStyle(
                                              false, 14, black.withOpacity(.5)),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onPanDown: (u) async {
                                  holdToRecord(u);
                                },
                                onPanStart: (u) {
                                  holdToRecord(u);
                                },
                                onPanEnd: (u) {
                                  tappingDown = false;
                                  stopRecording();
                                },
                                onPanCancel: () {
                                  tappingDown = false;
                                  stopRecording();
                                },
                                onPanUpdate: (u) {
                                  if (stopUpdatingPosition) return;
                                  xPosition = u.globalPosition.dx;
                                  yPosition = u.globalPosition.dy;
                                  setState(() {});
                                  if (canCancelRecording() && recording)
                                    Future.delayed(Duration(milliseconds: 100),
                                        () {
                                      stopUpdatingPosition = true;
                                      cancelled = true;
                                      stopRecording();
                                    });
                                },
                                child: Container(
                                  height: 80,
                                  width: 80,
                                  decoration: BoxDecoration(
                                      color: AppConfig.appColor,
                                      shape: BoxShape.circle),
                                  child: Center(
                                    child: Icon(
                                      Icons.mic,
                                      color: white,
                                      size: 50,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
//                    if (showOverlay)
//                      Align(
//                          alignment: Alignment.bottomCenter,
//                          child: Stack(
//                            children: [
//                              GestureDetector(
//                                onTap: () {
//                              showOverlay = false;
//                              setState(() {});
//                                },
//                                child: Container(
//                              color: black.withOpacity(.1),
//                                ),
//                              ),
//                              Container(
//                                margin: EdgeInsets.only(
//                                    right: 10, left: 10, bottom: 80),
//                                //height: 90,
//                                padding: EdgeInsets.all(10),
//                                width: double.infinity,
//                                decoration: BoxDecoration(
//                                    color: white,
//                                    borderRadius: BorderRadius.circular(15)),
//                                //color: red,
//                                child: Row(
//                                    mainAxisAlignment:
//                                    MainAxisAlignment.spaceEvenly,
//                                    children: List.generate(3, (p) {
//                                      String title = "Camera";
//                                      var icon = Icons.camera_alt;
//
//                                      if (p == 1) {
//                                        title = "Video";
//                                        icon = Icons.videocam;
//                                      }
//
//                                      if (p == 2) {
//                                        title = "Stickers";
//                                        icon = Icons.insert_emoticon;
//                                      }
//
//                                      return Flexible(
//                                        child: Column(
//                                          mainAxisAlignment:
//                                          MainAxisAlignment.center,
//                                          children: [
//                                            Container(
//                                              height: 50,
//                                              width: 50,
//                                              child: Icon(icon),
//                                              decoration: BoxDecoration(
//                                                  color: black.withOpacity(.1),
//                                                  shape: BoxShape.circle),
//                                            ),
//                                            addSpace(5),
//                                            Text("Camera")
//                                          ],
//                                        ),
//                                      );
//                                    })),
//                              ),
//                            ],
//                          ))
                  ],
                ));
          }),
        ),
      ),
    );
  }

  int shownChatsCount = 10;
  int countIncrement = 10;

  Map typingUser = {};
  Map recordingUser = {};
  bool isTyping = false;
  bool isRecording = false;
  bool isOnline = false;

  page() {
    List mutedList = List.from(userModel
        .getList(chatMode == CHAT_MODE_ENCRYPT ? MUTED_ENCRYPTED : MUTED));
    List lockedChats = List.from(userModel.getList(LOCKED));
    bool isMuted = mutedList.contains(chatId);
    bool blocked = otherPerson.getList(BLOCKED).contains(userModel.getUserId());
    bool iBlocked =
        userModel.getList(BLOCKED).contains(otherPerson.getUserId());
    return Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (!searchMode)
          Container(
            padding: EdgeInsets.fromLTRB(10, 40, 10, 10),
            color: white,
            child: Stack(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          width: 85,
                          height: 30,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(
                                Icons.arrow_back_ios,
                                color: black,
                                size: 20,
                              ),
                              addSpaceWidth(5),
                              Text("Chats",
                                  maxLines: 1,
                                  style: textStyle(true, 18, black)),
                            ],
                          ),
                        )),
                    Flexible(
                      fit: FlexFit.tight,
                      child: GestureDetector(
                        onTap: () {
                          pushAndResult(
                              context,
                              ShowProfile(
                                otherPerson,
                                chatList,
                                chatMode,
                                groupMembers: groupMembers,
                              ),
                              opaque: false,
                              depend: false);
                        },
                        child: Container(
//                          color:black,
                            child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                                otherPerson
                                    .getString(groupChat ? GROUP_NAME : NAME),
                                maxLines: 1,
                                style: textStyle(true, 18, black)),
                            if (!groupChat)
                              Text(
                                isOnline
                                    ? "Online"
                                    : getLastSeen(otherPerson) ?? "Offline",
                                maxLines: 1,
                                style:
                                    textStyle(false, 14, black.withOpacity(.7)),
                              )
                          ],
                        )),
                      ),
                    ),
                    InkWell(
                        onTap: () {
                          var options = [
                            "Search",
                            mutedList.contains(chatId)
                                ? "Unmute Chat"
                                : "Mute Chat",
                          ];
                          if (chatMode != 1) {
                            //options.add("Start Encrypted Chat");
                            options.add(lockedChats.contains(chatId)
                                ? "Unlock Chat"
                                : "Lock Chat");
                          }
                          pushAndResult(context, listDialog(options),
                              result: (_) {
                            if (!userModel.isPremium) {
                              showMessage(
                                  context,
                                  Icons.error,
                                  red0,
                                  "Opps Sorry!",
                                  "You cannot lock a message on ConvasApp until you become a Premium User",
                                  clickYesText: "Go Premium", onClicked: (_) {
                                if (_)
                                  pushAndResult(context, ShowWallet(),
                                      depend: false);
                              });
                              return;
                            }

                            if (_ == "Unlock Chat" || _ == "Lock Chat") {
                              pushAndResult(context, EnterPin(), depend: false,
                                  result: (_) {
                                if (null == _) return;

                                if (_ == userModel.getString(CODE_PIN)) {
                                  if (lockedChats.contains(chatId)) {
                                    lockedChats.remove(chatId);
                                  } else {
                                    lockedChats.add(chatId);
                                  }
                                  userModel.put(LOCKED, lockedChats);
                                  userModel.updateItems();
                                  setState(() {});
                                }
                              });
                            }
                            if (_ == "Mute Chat" || _ == "Unmute Chat") {
                              if (mutedList.contains(chatId)) {
                                mutedList.remove(chatId);
                              } else {
                                mutedList.add(chatId);
                              }
                              userModel.put(
                                  chatMode == CHAT_MODE_ENCRYPT
                                      ? MUTED_ENCRYPTED
                                      : MUTED,
                                  mutedList);
                              userModel.updateItems();
                              setState(() {});
                            }
                            if (_ == "Search") {
                              searchMode = true;
                              setState(() {});
                            }
                            if (_ == "Start Encrypted Chat") {
                              clickChat(
                                  context, CHAT_MODE_ENCRYPT, otherPerson);
                            }
                          }, depend: false);
                        },
                        child: Container(
                          width: 40,
                          height: 30,
                          child: Center(
                              child: Icon(
                            Icons.more_vert,
                            color: black,
                            size: 20,
                          )),
                        )),
                    if (otherPerson != null)
                      GestureDetector(
                        onTap: () {
                          pushAndResult(
                            context,
                            ShowProfile(
                              otherPerson,
                              chatList,
                              chatMode,
                              groupMembers: groupMembers,
                            ),
                            opaque: false,
                            //depend: false
                          );
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(22),
                          child: Container(
                            width: 45,
                            height: 45,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppConfig.appColor,
                                gradient: LinearGradient(colors: [
                                  blue4,
                                  blue2,
                                ])),
                            child: (groupChat
                                        ? (otherPerson.getString(GROUP_PHOTO))
                                        : otherPerson.userImage)
                                    .isEmpty
                                ? Center(
                                    child: Text(
                                      getInitials(groupChat
                                          ? otherPerson.getString(GROUP_NAME)
                                          : otherPerson.getString(NAME)),
                                      style: textStyle(true, 14, white),
                                    ),
                                  )
                                : CachedNetworkImage(
                                    imageUrl: groupChat
                                        ? (otherPerson.getString(GROUP_PHOTO))
                                        : otherPerson.userImage,
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                      ),
                    /*imageHolder(
                        45,
                        groupChat
                            ? (otherPerson.getString(GROUP_PHOTO))
                            : otherPerson.userImage, onImageTap: () {
                      pushAndResult(
                        context,
                        ShowProfile(
                          personModel: otherPerson,
                          fromChat: true,
                        ),
                        opaque: false,
                        //depend: false
                      );
                    }),*/
                  ],
                ),
              ],
            ),
          ),
        if (searchMode)
          Container(
            height: 45,
            margin: EdgeInsets.fromLTRB(20, 40, 20, 10),
            decoration: BoxDecoration(
              color: chat_back,
              borderRadius: BorderRadius.circular(25),
//              border: Border.all(color: AppConfig.appColor,width: 1)
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                    onTap: () {
                      setState(() {
                        searchMode = false;
                        searchController.text = "";
                        searchMessage = "";
                      });
                    },
                    child: Container(
                      width: 50,
                      height: 30,
                      child: Center(
                          child: Icon(
                        Icons.arrow_back_ios,
                        color: black,
                        size: 20,
                      )),
                    )),
                new Flexible(
                  flex: 1,
                  child: new TextField(
                    textInputAction: TextInputAction.search,
                    textCapitalization: TextCapitalization.sentences,
                    autofocus: false,
                    onSubmitted: (_) {
                      runSearch();
                    },
                    decoration: InputDecoration(
                        hintText: "Search...",
                        hintStyle: textStyle(
                          false,
                          18,
                          blue3.withOpacity(.5),
                        ),
                        border: InputBorder.none,
                        isDense: true),
                    style: textStyle(false, 16, black),
                    controller: searchController,
                    cursorColor: black,
                    cursorWidth: 1,
                    focusNode: focusSearch,
                    keyboardType: TextInputType.text,
                    onChanged: (_) {
                      if (_.trim().isEmpty) return;
                      searchMessage = _;
                      hasSearched = false;
                      setState(() {});
                    },
                  ),
                ),
                InkWell(
                    onTap: () {
                      if (!hasSearched) searchPositions.clear();
                      searchUp(canSearch: true);
                    },
                    child: Container(
                      width: 50,
                      height: 30,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_arrow_up,
                        color: black,
                        size: 20,
                      )),
                    )),
                InkWell(
                    onTap: () {
                      if (!hasSearched) searchPositions.clear();
                      searchDown(canSearch: true);
                    },
                    child: Container(
                      width: 50,
                      height: 30,
                      child: Center(
                          child: Icon(
                        Icons.keyboard_arrow_down,
                        color: black,
                        size: 20,
                      )),
                    )),
              ],
            ),
          ),
        new Expanded(
            flex: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              ),
              child: Builder(builder: (ctx) {
                if (!setup) return loadingLayout();

                if (chatList.isEmpty)
                  return Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(
                              "assets/icons/chat_bg.png",
                            ),
                            fit: BoxFit.cover)),
                  );
                return Container(
                  decoration: BoxDecoration(
                      color: chat_back,
                      image: DecorationImage(
                          image: AssetImage(
                            "assets/icons/chat_bg.png",
                          ),
                          fit: BoxFit.cover)),
                  child: ScrollablePositionedList.builder(
                    itemScrollController: messageListController,
                    padding: EdgeInsets.all(0),
                    reverse: true,
                    itemBuilder: (c, p) {
                      BaseModel chat = chatList[p];
                      bool myItem = chat.getUserId() == userModel.getUserId();
                      int type = chat.getType();
                      bool showDate =
                          timePositions.contains(chat.getObjectId()) ||
                              p == getItemCount() - 1;
                      bool showGeneral = chat.getBoolean(IS_GENERAL);

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          new Column(
                            children: <Widget>[
                              new Center(
                                child: chatList.length < shownChatsCount ||
                                        p != shownChatsCount - 1 ||
                                        chatList.length == shownChatsCount
                                    ? Container()
                                    : new GestureDetector(
                                        onTap: () {
                                          shownChatsCount =
                                              shownChatsCount + countIncrement;
                                          shownChatsCount =
                                              shownChatsCount >= chatList.length
                                                  ? chatList.length
                                                  : shownChatsCount;
                                          setState(() {});
                                          //
                                        },
                                        child: new Container(
                                          margin: EdgeInsets.fromLTRB(
                                              10, 10, 10, 0),
                                          padding:
                                              EdgeInsets.fromLTRB(10, 5, 10, 5),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.rotate_left,
                                                size: 15,
                                                color: AppConfig.appColor,
                                              ),
                                              addSpaceWidth(5),
                                              Text(
                                                "Previous messages",
                                                style: textStyle(true, 12,
                                                    AppConfig.appColor),
                                              )
                                            ],
                                          ),
                                          decoration: BoxDecoration(
//                                                  color: brown04,
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              border: Border.all(
                                                  color: AppConfig.appColor,
                                                  width: 1)),
                                        ),
                                      ),
                              ),
                              if (showDate)
                                new Center(
                                    child: Container(
                                  margin: EdgeInsets.fromLTRB(
                                      0,
                                      (p == getItemCount() - 1) ? 15 : 5,
                                      0,
                                      15),
                                  child: Text(
                                    getChatDate(chat.getTime()),
                                    style: textStyle(
                                        false, 12, black.withOpacity(0.5)),
                                  ),
                                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  decoration: BoxDecoration(
                                      color: white.withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(25),
                                      border: Border.all(
                                          width: .5,
                                          color: black.withOpacity(0.5))),
                                )),
                              if (showGeneral)
                                new Center(
                                    child: Container(
                                  margin: EdgeInsets.fromLTRB(0, 5, 0, 15),
                                  child: Text(
                                    chat.getString(MESSAGE),
                                    style: textStyle(
                                        false, 14, black.withOpacity(0.5)),
                                  ),
                                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  decoration: BoxDecoration(
                                      color: white.withOpacity(0.9),
                                      borderRadius: BorderRadius.circular(25),
                                      border: Border.all(
                                          width: .5,
                                          color: black.withOpacity(0.5))),
                                )),
                              if (!showGeneral)
                                AnimatedOpacity(
                                    opacity: blinkPosition == p ? (.3) : 1,
                                    duration: Duration(milliseconds: 500),
                                    child: Container(
                                      width: double.infinity,
                                      child: Dismissible(
                                        key: Key(getRandomId()),
                                        direction: DismissDirection.startToEnd,
                                        confirmDismiss: (_) async {
                                          if (chatRemoved(chat))
                                            return Future.value(false);
                                          replyModel = chat;
                                          FocusScope.of(context)
                                              .requestFocus(messageNode);
                                          setState(() {});
                                          return Future.value(false);
                                        },
                                        dismissThresholds: {
                                          DismissDirection.startToEnd: 0.1,
                                          DismissDirection.endToStart: 0.7
                                        },
                                        child: Column(
                                          crossAxisAlignment: myItem
                                              ? CrossAxisAlignment.end
                                              : CrossAxisAlignment.start,
                                          children: [
                                            myItem
                                                ? Builder(
                                                    builder: (ctx) {
                                                      if (type ==
                                                          CHAT_TYPE_TEXT)
                                                        return outgoingChatText(
                                                            context,
                                                            chat,
                                                            p == 0);

                                                      if (type ==
                                                          CHAT_TYPE_IMAGE)
                                                        return outgoingChatImage(
                                                            context, chat, () {
                                                          setState(() {});
                                                        }, p == 0);

                                                      if (type ==
                                                          CHAT_TYPE_REC) {
                                                        return Container(
                                                          child:
                                                              getChatAudioWidget(
                                                                  context, chat,
                                                                  (removed) {
                                                            if (removed) {
                                                              chatList
                                                                  .removeAt(p);
                                                            }
                                                            if (mounted)
                                                              setState(() {});
                                                          }, p == 0),
                                                        );
                                                      }
                                                      if (type == CHAT_TYPE_DOC)
                                                        return outgoingChatDoc(
                                                            context, chat, () {
                                                          pushChat(
                                                              "Sent a document");
                                                          setState(() {});
                                                        }, p == 0);
                                                      if (type ==
                                                          CHAT_TYPE_CONTACT)
                                                        return outgoingChatContact(
                                                            context,
                                                            chat,
                                                            p == 0);
                                                      return outgoingChatVideo(
                                                          context, chat, () {
                                                        pushChat(
                                                            "Sent a video");
                                                        setState(() {});
                                                      }, p == 0);
                                                    },
                                                  )
                                                : Builder(builder: (ctx) {
                                                    if (type == CHAT_TYPE_TEXT)
                                                      return incomingChatText(
                                                          context, chat);

                                                    if (type == CHAT_TYPE_REC) {
                                                      return Container(
                                                        child:
                                                            getChatAudioWidget(
                                                                context, chat,
                                                                (removed) {
                                                          if (removed) {
                                                            chatList
                                                                .removeAt(p);
                                                          }
                                                          if (mounted)
                                                            setState(() {});
                                                        }, p == 0),
                                                      );
                                                    }

                                                    if (type == CHAT_TYPE_IMAGE)
                                                      return incomingChatImage(
                                                          context, chat);
                                                    if (type == CHAT_TYPE_DOC)
                                                      return incomingChatDoc(
                                                          context,
                                                          chat,
                                                          fileThatExists
                                                              .contains(chat
                                                                  .getObjectId()),
                                                          () {
                                                        checkIfFileExists(
                                                            chat, true);
                                                      });

                                                    if (type ==
                                                        CHAT_TYPE_CONTACT)
                                                      return incomingChatContact(
                                                          context, chat);
                                                    return incomingChatVideo(
                                                        context, chat);
                                                  }),
                                          ],
                                        ),
                                      ),
                                    ))
                            ],
                          ),
                          if (p == 0)
                            isTyping
                                ? incomingChatTyping(context, typing: true)
                                : isRecording
                                    ? incomingChatTyping(context, typing: false)
                                    : Container()
                        ],
                      );
                    },
                    itemCount: (getItemCount()),
                  ),
                );
              }),
            )),
        addLine(.5, black.withOpacity(.2), 0, 0, 0, 0),
        if (setup && !groupChat && (blocked || iBlocked))
          Container(
            decoration: BoxDecoration(
                color: red, borderRadius: BorderRadius.circular(8)),
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: Row(
              children: [
                Icon(
                  Icons.info,
                  color: white,
                ),
                addSpaceWidth(10),
                Flexible(
                  child: Text(
                    "Oops: ${iBlocked ? "You have blocked ${otherPerson.getString(NAME)}" : "${otherPerson.getString(NAME)} has blocked you"}."
                    "You will no longer be able to receive or send messages to the user.",
                    style: textStyle(false, 14, white),
                  ),
                ),
              ],
            ),
          )
        else ...[
          if (setup && !recording)
            Container(
              width: double.infinity,
              color: chat_back,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
//                crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    if (message.isEmpty)
                      Container(
                        height: 30,
                        width: 50,
                        child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            padding: EdgeInsets.all(0),
                            onPressed: () {
                              handleOverlay();
                            },
                            child: Icon(
                              Icons.add_circle,
                              color: black.withOpacity(.5),
                              size: 20,
                            )),
                      ),
                    if (message.isNotEmpty) addSpaceWidth(10),
                    Flexible(
                      flex: 1,
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                        decoration: BoxDecoration(
//                          color: white,
                            borderRadius: replyModel != null
                                ? (BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(25),
                                    bottomLeft: Radius.circular(25)))
                                : BorderRadius.all(Radius.circular(25))),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            if (replyModel != null)
                              chatReplyWidget(
                                replyModel,
                                fitScreen: true,
                                onRemoved: () {
                                  replyModel = null;
                                  setState(() {});
                                },
                              ),
                            new ConstrainedBox(
                              constraints:
                                  BoxConstraints(maxHeight: 120, minHeight: 45),
                              child: Container(
                                child: Row(
                                  children: [
//                                  addSpaceWidth(15),
                                    Flexible(
                                      fit: FlexFit.tight,
                                      child: new TextField(
                                        onSubmitted: (_) {
                                          postChatText();
                                        },
                                        onChanged: (_) {
                                          lastTyped = DateTime.now()
                                              .millisecondsSinceEpoch;
                                          message = _.trim();
                                          setState(() {});
                                        },
                                        //textInputAction: TextInputAction.newline,
                                        textCapitalization:
                                            TextCapitalization.sentences,
                                        decoration: InputDecoration(
                                            hintText: "Type a message",
                                            isDense: true,
                                            hintStyle: textStyle(false, 17,
                                                black.withOpacity(.3)),
                                            border: InputBorder.none),
                                        style: textStyle(false, 17, black),
                                        controller: messageController,
                                        cursorColor: black,
                                        cursorWidth: 1,
                                        maxLines: null,
                                        keyboardType: TextInputType.multiline,
                                        focusNode: messageNode,
                                        scrollPadding:
                                            EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      ),
                                    ),

                                    Container(
                                      height: 40,
                                      width: 30,
                                      child: FlatButton(
                                          materialTapTargetSize:
                                              MaterialTapTargetSize.shrinkWrap,
                                          padding: EdgeInsets.all(0),
                                          onPressed: () {
                                            if (!userModel.isPremium) {
                                              showMessage(
                                                  context,
                                                  Icons.error,
                                                  red0,
                                                  "Opps Sorry!",
                                                  "You cannot lock a message on ConvasApp until you become a Premium User",
                                                  clickYesText: "Go Premium",
                                                  onClicked: (_) {
                                                if (_)
                                                  pushAndResult(
                                                      context, ShowWallet(),
                                                      depend: false);
                                              });
                                              return;
                                            }

                                            setState(() {
                                              lockChat = !lockChat;
                                            });
                                          },
                                          child: Icon(
                                            Icons.lock,
                                            color: lockChat
                                                ? red0
                                                : black.withOpacity(.5),
                                            size: 20,
                                          )),
                                    ),

                                    GestureDetector(
                                      onTap: () {
                                        if (!userModel.isPremium) {
                                          showMessage(
                                              context,
                                              Icons.error,
                                              red0,
                                              "Opps Sorry!",
                                              "You cannot send timed messages on ConvasApp until you become a Premium User",
                                              clickYesText: "Go Premium",
                                              onClicked: (_) {
                                            if (_)
                                              pushAndResult(
                                                  context, ShowWallet(),
                                                  depend: false);
                                          });
                                          return;
                                        }

                                        if (messageDelay != 0) {
                                          messageDelay = 0;
                                          setState(() {});
                                          return;
                                        }

                                        showListDialog(context, [
                                          "1 minute",
                                          "2 minutes",
                                          "3 minutes",
                                          "4 minutes",
                                          "5 minutes",
                                        ], (_) {
                                          messageDelay = _ + 1;
                                          setState(() {});
                                        }, title: "Auto Delete After");
                                      },
                                      child: Container(
                                        height: 50,
                                        width: 40,
//                                      color: black.withOpacity(.3),
                                        child: Stack(
                                          children: [
                                            Center(
                                              child: Icon(
                                                Icons.access_time,
                                                color: black.withOpacity(.5),
//                                              size: 30,
                                              ),
                                            ),
                                            if (messageDelay > 0)
                                              Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: Container(
                                                  width: 20,
                                                  height: 20,
                                                  margin: EdgeInsets.fromLTRB(
                                                      0, 0, 0, 5),
                                                  decoration: BoxDecoration(
                                                      color: blue0,
//                            borderRadius: BorderRadius.circular(5),
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                          color: white,
                                                          width: 2)),
                                                  child: Center(
                                                      child: Text(
                                                    "${messageDelay}",
                                                    style: textStyle(
                                                        true, 10, white),
                                                  )),
                                                ),
                                              )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    AnimatedContainer(
                      duration: Duration(milliseconds: 200),
                      height: 45,
                      width: 45,
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
                      child: FloatingActionButton(
                        onPressed: () {
                          if (message.isEmpty) {
                            return;
                          }
                          postChatText();
                        },
                        heroTag: "sendBut",
                        child: Icon(
                          message.isEmpty ? Icons.mic : Icons.send,
                          color: black.withOpacity(.5),
                          size: message.isNotEmpty ? 16 : null,
                        ),
                        backgroundColor: white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          if (setup && recording)
            Container(
              height: 60,
              color: (canCancelRecording()) ? red0 : white,
            ),
          if (showEmoji)
            EmojiPicker(
              onEmojiSelected: (emoji, category) {
                String text = messageController.text;
                StringBuffer sb = StringBuffer();
                sb.write(text);
                sb.write(emoji.emoji);
                messageController.text = sb.toString();
                message = sb.toString();
                setState(() {});
              },
              //recommendKeywords: ["happy", "love"],
            )
        ]
      ],
    );
  }

  bool canCancelRecording() {
    return xPosition < (getScreenWidth(context) / 1.5);
  }

  int getItemCount() {
    return chatList.length < shownChatsCount
        ? chatList.length
        : shownChatsCount;
  }

  postChatText() {
    String text = messageController.text;
    if (text.trim().isEmpty) {
      toastInAndroid("Type a message");
      return;
    }

    final String id = getRandomId();
    final BaseModel model = new BaseModel();
    model.put(CHAT_ID, chatId);

    bool sDate = showDate();
    //toastInAndroid(sDate.toString());
    if (groupChat)
      model.put(PARTIES, otherPerson.getList(PARTIES));
    else
      model.put(
          PARTIES, [userModel.getObjectId(), otherPerson.getString(USER_ID)]);
    model.put(SHOW_DATE, sDate);
    model.put(MESSAGE, text);
    model.put(TYPE, CHAT_TYPE_TEXT);
    model.put(OBJECT_ID, id);

    if (replyModel != null) {
      model.put(REPLY_DATA, replyModel.items);
      replyModel = null;
    }
    if (lockChat) {
      model.put(LOCKED, true);
      lockChat = false;
    }
    model.put(DELAY, messageDelay);
    messageDelay = 0;
    model.saveItem(chatDatabaseName, true, document: id, onComplete: () {
      pushChat(text);
    });

    messageController.text = "";
    message = "";
    sendIcon = Icons.mic;
    addChatToList(model);
    setState(() {});
    scrollToBottom();
    updateTyping(false);
  }

  postChatDoc() async {
    final path = await FilePicker.getFilePath();
    String ext = "";
    int dotPos = path.lastIndexOf('.');
    if (dotPos != -1) {
      ext = path.substring(dotPos + 1);
    }

    if (File(path).lengthSync() > 30000000) {
      toastInAndroid("Document should not be more than 30MB");
      return;
    }

    String fileExtension = ext.toLowerCase().trim();
    File document = File(path);
    String fileName = pathLib.basename(path);
    String fileSize = filesize(File(path).lengthSync());

    yesNoDialog(context, "Send File",
        "Are you sure you want to send this file \"$fileName\"", () {
      final String id = getRandomId();
      final BaseModel model = new BaseModel();
      model.put(CHAT_ID, chatId);
      model.put(SHOW_DATE, showDate());
      model.put(TYPE, CHAT_TYPE_DOC);

      if (groupChat)
        model.put(PARTIES, otherPerson.getList(PARTIES));
      else
        model.put(
            PARTIES, [userModel.getObjectId(), otherPerson.getString(USER_ID)]);
      model.put(FILE_PATH, document.path);
      model.put(FILE_EXTENSION, fileExtension.toLowerCase());
      model.put(FILE_SIZE, fileSize);
      model.put(FILE_NAME, fileName);

      model.put(OBJECT_ID, id);
      model.put(DATABASE_NAME, chatDatabaseName);

      //chatList.insert(0, model);

      upOrDown.add(model.getObjectId());
      if (replyModel != null) {
        model.put(REPLY_DATA, replyModel.items);
        replyModel = null;
      }
      if (lockChat) {
        model.put(LOCKED, true);
        lockChat = false;
      }
      model.put(DELAY, messageDelay);
      messageDelay = 0;
      model.saveItem(chatDatabaseName, true, document: id, onComplete: () {
        saveChatFile(model, FILE_PATH, FILE_URL, () {
          addChatToList(model);
          setState(() {});
          pushChat('sent a document');
        });
      });

      addChatToList(model);
      setState(() {});
      scrollToBottom();
    });
  }

  bool showDate() {
    if (chatList.isEmpty) return true;
    BaseModel lastChat = chatList[0];
    return !isSameDay(
        lastChat.getTime(), DateTime.now().millisecondsSinceEpoch);
  }

  pushChat(String message) async {
    if (!otherPerson
            .getList(chatMode == CHAT_MODE_ENCRYPT ? MUTED_ENCRYPTED : MUTED)
            .contains(chatId) &&
        otherPerson.getBoolean(PUSH_NOTIFICATION)) {
//      String messageBody = '${getFullName(userModel).trim()}: $message';
      Map data = Map();
      data[TYPE] = PUSH_TYPE_CHAT;
      data[OBJECT_ID] = chatId;
      data[TITLE] = getFullName(userModel);
      data[MESSAGE] = message;
      NotificationService.sendPush(
          token: groupChat ? chatId : otherPerson.getString(TOKEN),
          title: getFullName(userModel),
          body: message,
          tag: '${userModel.getObjectId()}chat',
          data: data);
    }
  }

  postChatImage() async {
    getSingleCroppedImage(context, onPicked: (String path) {
      final String id = getRandomId();
      final BaseModel model = new BaseModel();
      model.put(CHAT_ID, chatId);
      if (groupChat)
        model.put(PARTIES, otherPerson.getList(PARTIES));
      else
        model.put(
            PARTIES, [userModel.getObjectId(), otherPerson.getString(USER_ID)]);
      model.put(SHOW_DATE, showDate());
      model.put(TYPE, CHAT_TYPE_IMAGE);
      model.put(IMAGE_PATH, path);
      model.put(OBJECT_ID, id);
      model.put(DATABASE_NAME, chatDatabaseName);

      upOrDown.add(model.getObjectId());
      if (replyModel != null) {
        model.put(REPLY_DATA, replyModel.items);
        replyModel = null;
      }
      if (lockChat) {
        model.put(LOCKED, true);
        lockChat = false;
      }
      model.put(DELAY, messageDelay);
      messageDelay = 0;
      model.saveItem(chatDatabaseName, true, document: id, onComplete: () {
        saveChatFile(model, IMAGE_PATH, IMAGE_URL, () {
          addChatToList(model);
          setState(() {});
          pushChat('sent a photo');
        });
      });

      addChatToList(model);
      setState(() {});
      print("setting State...");
      scrollToBottom();
    });
  }

  postChatVideo(bool froCam) async {
    getSingleVideo(context, onPicked: (BaseModel videoResult) {
      pushAndResult(
          context,
          PreSendVideo(
            File(videoResult.getString(VIDEO_PATH)),
          ), result: (_) {
        String videoLength = _;

        final String id = getRandomId();
        final BaseModel model = new BaseModel();
        model.put(CHAT_ID, chatId);
        if (groupChat)
          model.put(PARTIES, otherPerson.getList(PARTIES));
        else
          model.put(PARTIES,
              [userModel.getObjectId(), otherPerson.getString(USER_ID)]);
        model.put(SHOW_DATE, showDate());
        model.put(TYPE, CHAT_TYPE_VIDEO);
        model.put(THUMBNAIL_PATH, videoResult.getString(THUMBNAIL_PATH));
        model.put(VIDEO_PATH, videoResult.getString(VIDEO_PATH));
        model.put(VIDEO_LENGTH, videoLength);
        model.put(OBJECT_ID, id);
        model.put(DATABASE_NAME, chatDatabaseName);

        upOrDown.add(model.getObjectId());
        if (replyModel != null) {
          model.put(REPLY_DATA, replyModel.items);
          replyModel = null;
        }
        if (lockChat) {
          model.put(LOCKED, true);
          lockChat = false;
        }
        model.put(DELAY, messageDelay);
        messageDelay = 0;
        model.saveItem(chatDatabaseName, true, document: id, onComplete: () {
          saveChatVideo(model, () {
            addChatToList(model);
            setState(() {});
            pushChat('sent a video');
          });
        });

        addChatToList(model);
        setState(() {});
        scrollToBottom();
      });
    });
  }

  postChatAudio() async {
    String path = await localPath;
    File newFile = await File(recordingFilePath)
        .copy("$path/${DateTime.now().millisecondsSinceEpoch}chatRec.aac");

    final String id = getRandomId();
    BaseModel chat = BaseModel();
    if (replyModel != null) {
      chat.put(REPLY_DATA, replyModel.items);
      replyModel = null;
    }

    /* String path = await localPath;
    print("rec path $recordingFilePath");
    File newFile = await File(recordingFilePath)
        .copy("$path/${DateTime.now().millisecondsSinceEpoch}chatRec.aac");
    print(newFile.path);

    final String id = getRandomId();
    BaseModel chat = BaseModel();
    if (replyModel != null) {
      chat.put(REPLY_DATA, replyModel.items);
      replyModel = null;
    }*/
    chat.put(DELAY, messageDelay);
    messageDelay = 0;
    chat
      ..put(CHAT_ID, chatId)
      ..put(
          PARTIES,
          groupChat
              ? otherPerson.getList(PARTIES)
              : [userModel.getUserId(), otherPerson.getObjectId()])
      ..put(TYPE, CHAT_TYPE_REC)
      ..put(AUDIO_LENGTH, recordTimerText)
//      ..put(AUDIO_PATH, newFile.path)
      ..put(AUDIO_PATH, recordingFilePath)
      ..put(OBJECT_ID, id)
      ..saveItem(chatDatabaseName, true, document: id, onComplete: () {
        pushChat("a voice note");
      });
    handleAudio(chat);
    addChatToList(chat);
    setState(() {});
    scrollToBottom();
  }

  scrollToBottom() {
    try {
      messageListController.jumpTo(
        index: 0,
      );
    } catch (e) {}
  }

  updateTyping(bool typing) {
    if (groupChat) {
      Map user = Map();
      user[USER_IMAGE] = userModel.getString(USER_IMAGE);
      user[USER_ID] = userModel.getString(USER_ID);
      user[NAME] = userModel.getString(NAME);
      otherPerson
        ..put(REC_USER, null)
        ..put(TYPING_USER, typing ? user : null)
        ..updateItems();
      return;
    }
    userModel
      ..put(REC_ID, null)
      ..put(TYPING_ID, typing ? chatId : null)
      ..updateItems();
  }

  updateRecording(bool recording) {
    if (groupChat) {
      Map user = Map();
      user[USER_IMAGE] = userModel.getString(USER_IMAGE);
      user[USER_ID] = userModel.getString(USER_ID);
      user[NAME] = userModel.getString(NAME);
      otherPerson
        ..put(TYPING_USER, null)
        ..put(REC_USER, recording ? user : null)
        ..updateItems();
      return;
    }
    userModel
      ..put(TYPING_ID, null)
      ..put(REC_ID, recording ? chatId : null)
      ..updateItems();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    if (state == AppLifecycleState.paused) {
      visibleChatId = "";
      updateTyping(false);
      typingSoundController.pause();
      messageSoundController.pause();
    }
    if (state == AppLifecycleState.resumed) {
      visibleChatId = chatId;
    }

    super.didChangeAppLifecycleState(state);
  }

  prepareRecording(u) async {
    if (recording) return;
    recording = true;
    xPosition = u.globalPosition.dx;
    yPosition = u.globalPosition.dy;
    cancelled = false;
    stopUpdatingPosition = false;
    recordTimer = 0;
    recordTimerText = "00:00";
    recordingOpacity = 1;
    createRecordTimer();
    startRecorder();
    updateRecording(true);
    setState(() {});
  }

  stopRecording() {
    if (!recording) return;
    recording = false;
    xPosition = -1;
    yPosition = -1;
    setState(() {});
    stopRecorder();
    updateRecording(false);
    recordTimer = 0;
    setState(() {});
    if (!cancelled && !tooShort) {
      postChatAudio();
    }
  }

  double bSize = 100;
//  double cSize = 0;
  //double cSize1 = 0;
//  double holdOpacity = 0;
  String recLength = "";
  String recordingFilePath = "";
  var flutterSoundRecorder = FlutterSoundRecorder();
  VideoPlayerController audioController;
  bool timerCounting = false;
  int timerCount = 4;
  double timerOpacity = 0;
  bool cancelled = false;
  bool stopUpdatingPosition = false;
  bool recording = false;
  int recordTimer = 0;
  int maxRecordTime = 30;
  String recordTimerText = "00:00";
  double recordingOpacity = 1;
  createRecordTimer() {
    Future.delayed(Duration(seconds: 1), () {
      if (!recording) {
        return;
      }
      recordTimer++;
      if (recordTimer > 30) {
        stopRecording();
        return;
      }
      recordingOpacity = recordingOpacity == 1 ? 0 : 1;

      int min = recordTimer ~/ 60;
      int sec = recordTimer % 60;

      String m = min.toString();
      String s = sec.toString();

      String ms = m.length == 1 ? "0$m" : m;
      String ss = s.length == 1 ? "0$s" : s;

      recordTimerText = "$ms:$ss";

      setState(() {});
      createRecordTimer();
    });
  }

  //final _codec = Codec.mp3;

  void startRecorder() async {
    try {
      PermissionStatus status = await Permission.microphone.request();
      if (status != PermissionStatus.granted) {
        throw RecordingPermissionException("Microphone permission not granted");
      }
      Directory tempDir = await getTemporaryDirectory();
      t_CODEC _codec = t_CODEC.CODEC_AAC;
      recordingFilePath = await flutterSoundRecorder.startRecorder(
        uri: '${tempDir.path}/sound.aac',
        codec: _codec,
      );

      print("Rec at $recordingFilePath");
    } catch (err) {
      print('startRecorder error: ${err.toString()}');
    }

//    try {
//      PermissionStatus status = await Permission.microphone.request();
//      if (status != PermissionStatus.granted) {
//        throw RecordingPermissionException("Microphone permission not granted");
//      }
//
//      String path =
//          "${await localPath}/${DateTime.now().millisecondsSinceEpoch}chatRec.mp3";
//      //File newFile = await getLocalFile(fileName);
//
////      Directory tempDir = await getTemporaryDirectory();
////      String path =
////          '${tempDir.path}/${flutterSoundRecorder.slotNo}-flutter_sound${sound.ext[_codec.index]}';
//
//      Directory tempDir = await getTemporaryDirectory();
//      t_CODEC _codec = t_CODEC.CODEC_AAC;
//
//      await flutterSoundRecorder.startRecorder(
//        uri: '${tempDir.path}/sound.aac',
//        //codec: _codec,
//        bitRate: 8000,
//        sampleRate: 8000,
//      );
//      recordingFilePath = path;
//      setState(() {});
//      print("Init $recordingFilePath \n\n");
//    } catch (err) {
//      print('startRecorder error: $err');
//    }
  }

  bool tooShort = false;
  void stopRecorder() async {
    recLength = recordTimerText;
    if (recordTimer < 1) {
      tooShort = true;
      setState(() {});
    }
    try {
      await flutterSoundRecorder.stopRecorder();
    } catch (err) {
      print('stopRecorder error: $err');
    }
    if (tooShort || cancelled) {
      File(recordingFilePath).delete();
      print('stopRecorder deleted: $recordingFilePath');
      Vibration.vibrate(duration: 100);
    }

    Future.delayed(Duration(seconds: 1), () {
      tooShort = false;
      cancelled = false;
      setState(() {});
    });
  }

  loadRecorder() async {
    flutterSoundRecorder = await FlutterSoundRecorder().initialize();
    //soundPlayer = await FlutterSoundPlayer().initialize();
    //flutterSound = await FlutterSoundRecorder().initialize();
    //flutterSound.setSubscriptionDuration(0.01);
    //flutterSound.setDbPeakLevelUpdate(0.8);
    //flutterSound.setDbLevelEnabled(true);
    print("Loaded Recorder");
  }

  holdToRecord(u) {
    tappingDown = true;
//    FocusScope.of(context).requestFocus(FocusNode());
    Future.delayed(Duration(milliseconds: 500), () {
      if (!tappingDown) {
        tooShort = true;
        Vibration.vibrate(duration: 100);
        setState(() {});
        Future.delayed(Duration(seconds: 1), () {
          tooShort = false;
          setState(() {});
        });
        return;
      }
      prepareRecording(u);
    });
  }

  scrollToMessage(String id) {
    if (!mounted) return;
    showProgress(true, context);
    int position = getMessagePosition(id);

    print("The position: $position");
    if (position != -1) {
      Future.delayed(Duration(milliseconds: 1000), () {
        showProgress(false, context);

        Future.delayed(Duration(seconds: 1), () {
          messageListController.jumpTo(
            index: position,
          );

          Future.delayed(Duration(milliseconds: 500), () {
            blinkPosition = position;
            setState(() {});
            Future.delayed(Duration(seconds: 1), () {
              blinkPosition = -1;
              setState(() {});
            });
          });
        });
      });
      return;
    }
    if (shownChatsCount >= chatList.length) {
      print("Ignoring...");
      Future.delayed(Duration(milliseconds: 500), () {
        showProgress(false, context);
      });
      return;
    }

    loadMorePrev();
    Future.delayed(Duration(milliseconds: 1000), () {
      scrollToMessage(id);
    });
  }

  getMessagePosition(String id) {
    print("Finding Position...");
//      if (shownChatsCount >= chatList.length) return -1;

    for (int i = 0; i < shownChatsCount; i++) {
      BaseModel bm = chatList[i];
      if (chatRemoved(bm)) continue;
      if (bm.getObjectId() == id) {
        return i;
      }
    }

    return -1;
  }

  loadMorePrev() async {
    shownChatsCount = shownChatsCount + countIncrement;
    shownChatsCount =
        shownChatsCount >= chatList.length ? chatList.length : shownChatsCount;
    setState(() {});
  }

  runSearch() {
    String text = searchController.text.trim();
    if (text.isEmpty) return;
    searchMessage = text;
    searchPositions.clear();
    searchPositionIndex = 0;
    showProgress(true, context);
    shownChatsCount = chatList.length;
    for (int i = 0; i < shownChatsCount; i++) {
      BaseModel bm = chatList[i];
      if (chatRemoved(bm)) continue;
      String message = bm.getString(MESSAGE);
      if (message.toLowerCase().contains(text.toLowerCase())) {
        searchPositions.add(i);
      }
    }

    hasSearched = true;
    Future.delayed(Duration(milliseconds: 500), () {
      showProgress(false, context);
      if (searchPositions.isEmpty) {
        showMessage(context, Icons.search, red0, "Nothing Found", "",
            delayInMilli: 500);
        return;
      }
      Future.delayed(Duration(milliseconds: 500), () {
        searchDown();
      });
    });
  }

  searchUp({bool canSearch = false}) {
    if (searchPositions.isEmpty) {
      if (canSearch) runSearch();
      return;
    }
    searchPositionIndex--;
    int messageIndex = 0;
    if (searchPositionIndex < 0) {
      searchPositionIndex = searchPositions.length - 1;
      messageIndex = searchPositions[searchPositionIndex];
    } else {
      messageIndex = searchPositions[searchPositionIndex];
    }
    messageListController.jumpTo(index: messageIndex);
    print("Mesage Positions ${searchPositions.toString()}");
    blinkMessage(messageIndex);
  }

  searchDown({bool canSearch = false}) {
    if (searchPositions.isEmpty) {
      if (canSearch) runSearch();
      return;
    }
    searchPositionIndex++;
    int messageIndex = 0;
    if (searchPositionIndex > searchPositions.length - 1) {
      searchPositionIndex = 0;
      messageIndex = searchPositions[searchPositionIndex];
    } else {
      messageIndex = searchPositions[searchPositionIndex];
    }
    messageListController.jumpTo(index: messageIndex);
    print("Mesage Positions ${searchPositions.toString()}");
    blinkMessage(messageIndex);
  }

  blinkMessage(int p) {
    Future.delayed(Duration(milliseconds: 500), () {
      blinkPosition = p;
      setState(() {});
      Future.delayed(Duration(seconds: 1), () {
        blinkPosition = -1;
        setState(() {});
      });
    });
  }

  incomingChatText(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    if (chat.getBoolean(LOCKED) && !unlockedChats.contains(chat.getObjectId()))
      return incomingChatLocked(context, chat);

    String message = chat.getString(MESSAGE);

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      children: <Widget>[
        new GestureDetector(
          onLongPress: () {
            showChatOptions(context, chat);
          },
          child: Container(
              margin: EdgeInsets.fromLTRB(60, 0, 60, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (replyData.items.isNotEmpty)
                    chatReplyWidget(
                      replyData,
                    ),
                  Card(
                    clipBehavior: Clip.antiAlias,
                    color: default_white,
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: new Container(
                      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                      decoration: BoxDecoration(
//                    color: white,
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            message,
                            style: textStyle(false, 15, black),
                          ),
                          addSpace(3),
                          Text(
                            /*timeAgo.format(
                            DateTime.fromMillisecondsSinceEpoch(
                                chat.getTime()),
                            locale: "en_short")*/
                            getChatTime(chat.getInt(TIME)),
                            style: textStyle(false, 12, black.withOpacity(.3)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
        ),
        userImageItem(context, user: groupMembers[chat.getUserId()])
      ],
    );
  }

  incomingChatDeleted(context, BaseModel chat) {
    return Container();
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
//    if (chat.getInt(DELAY) > 0) return Container();
    return new Stack(
      children: <Widget>[
        GestureDetector(
          onLongPress: () {
            showChatOptions(context, chat, deletedChat: true);
          },
          child: Container(
              margin: EdgeInsets.fromLTRB(60, 0, 60, 0),
              child: Card(
                clipBehavior: Clip.antiAlias,
                color: default_white,
                elevation: elevation,
                /*shadowColor: black.withOpacity(.3),*/
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(0),
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                )),
                child: new Container(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  decoration: BoxDecoration(
//                    color: white,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Deleted",
                            style: textStyle(false, 15, black),
                          ),
                          addSpaceWidth(5),
                          Icon(
                            Icons.info,
                            color: red0,
                            size: 17,
                          )
                        ],
                      ),
                      /*addSpace(3),
                Text(
                  */ /*timeAgo.format(
                        DateTime.fromMillisecondsSinceEpoch(
                            chat.getTime()),
                        locale: "en_short")*/ /*
                  getChatTime(chat.getInt(TIME)),
                  style: textStyle(false, 12, black.withOpacity(.3)),
                ),*/
                    ],
                  ),
                ),
              )),
        ),
        userImageItem(context)
      ],
    );
  }

  List unlockedChats = [];
  incomingChatLocked(context, BaseModel chat) {
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
//    if (chat.getInt(DELAY) > 0) return Container();
    return new Stack(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            pushAndResult(context, EnterPin(), depend: false, result: (_) {
              if (null == _) return;

              if (_ == userModel.getString(CODE_PIN)) {
                unlockedChats.add(chat.getObjectId());
                setState(() {});
              }
            });
          },
          onLongPress: () {
            showChatOptions(context, chat, deletedChat: true);
          },
          child: Container(
              margin: EdgeInsets.fromLTRB(60, 0, 60, 0),
              child: Card(
                clipBehavior: Clip.antiAlias,
                color: default_white,
                elevation: elevation,
                /*shadowColor: black.withOpacity(.3),*/
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(0),
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                )),
                child: new Container(
                  padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  decoration: BoxDecoration(
//                    color: white,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Message Locked",
                                style: textStyle(false, 15, black),
                              ),
                              Text(
                                "Tap to unlock",
                                style:
                                    textStyle(false, 12, black.withOpacity(.4)),
                              ),
                            ],
                          ),
                          addSpaceWidth(5),
                          Icon(
                            Icons.lock,
                            color: red0,
                            size: 17,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              )),
        ),
        userImageItem(context, user: groupMembers[chat.getUserId()])
      ],
    );
  }

  incomingChatTyping(context, {@required bool typing}) {
    return new Stack(
      children: <Widget>[
        Container(
            height: 40,
            margin: EdgeInsets.fromLTRB(60, 0, 60, 0),
            child: Column(
              children: [
                Card(
                  clipBehavior: Clip.antiAlias, color: default_white,
                  elevation: elevation, //shadowColor: black.withOpacity(.3),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(0),
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  )),
                  child: new Container(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    decoration: BoxDecoration(
//                  color: white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 5,
                              height: 5,
                              decoration: BoxDecoration(
                                  color: blue0, shape: BoxShape.circle),
                            ),
                            addSpaceWidth(5),
                            Container(
                              width: 4,
                              height: 4,
                              decoration: BoxDecoration(
                                  color: blue0, shape: BoxShape.circle),
                            ),
                            addSpaceWidth(5),
                            Container(
                              width: 3,
                              height: 3,
                              decoration: BoxDecoration(
                                  color: blue0, shape: BoxShape.circle),
                            ),
                            addSpaceWidth(5),
                            if (!typing)
                              Icon(
                                Icons.mic,
                                color: blue0,
                                size: 12,
                              )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )),
        userImageItem(context)
      ],
    );
  }

  outgoingChatText(context, BaseModel chat, bool firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String message = chat.getString(MESSAGE);

    String chatId = chat.getString(CHAT_ID);
    chatId = getOtherPersonId(chat);
    bool read = chat.getList(READ_BY).contains(otherPerson.getObjectId());

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        showChatOptions(context, chat);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(60, 0, 15, 5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (replyData.items.isNotEmpty)
              chatReplyWidget(
                replyData,
              ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  child: Card(
                    elevation: elevation, //shadowColor: black.withOpacity(.3),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(0),
                      topLeft: Radius.circular(25),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: new Container(
                      margin: const EdgeInsets.fromLTRB(15, 10, 20, 10),
                      child: Text(
                        message,
                        style: textStyle(false, 15, black),
                      ),
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: blue0,
                  ),
                if (chat.getBoolean(LOCKED))
                  Icon(
                    Icons.lock,
                    size: 12,
                    color: red0,
                  ),
                delayWidget(chat),
              ],
            ),
          ],
        ),
      ),
    );
  }

  incomingChatDoc(context, BaseModel chat, bool exists, onComplete) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    if (chat.getBoolean(LOCKED) && !unlockedChats.contains(chat.getObjectId()))
      return incomingChatLocked(context, chat);
//    if (chat.getInt(DELAY) > 0) return Container();

    String fileUrl = chat.getString(FILE_URL);
    String fileName = chat.getString(FILE_NAME);
    String size = chat.getString(FILE_SIZE);
    String ext = chat.getString(FILE_EXTENSION);
    bool downloading = upOrDown.contains(chat.getObjectId());
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
//  return Container();
    return new Stack(
      children: <Widget>[
        Opacity(
          opacity: fileUrl.isEmpty ? (.4) : 1,
          child: new GestureDetector(
            onLongPress: () {
              showChatOptions(context, chat);
            },
            onTap: () async {
              if (fileUrl.isEmpty || !exists) return;

              /*if (!exists) {
              downloadChatFile(chat, false, onComplete);
              return;
            }*/

              String fileName =
                  "${chat.getObjectId()}.${chat.getString(FILE_EXTENSION)}";
              File file = await getDirFile(fileName);
              await openTheFile(file.path);
            },
            child: Container(
              width: 200,
              margin: EdgeInsets.fromLTRB(60, 0, 0, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (replyData.items.isNotEmpty)
                    chatReplyWidget(
                      replyData,
                    ),
                  new Card(
                    clipBehavior: Clip.antiAlias,
                    color: default_white,
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                          width: 250,
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
//                        color: black.withOpacity(.2),
                              borderRadius: BorderRadius.circular(5)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              //addSpaceWidth(10),
                              Image.asset(
                                getExtImage(ext),
                                width: 20,
                                height: 20,
                              ),
                              addSpaceWidth(10),
                              new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      fileName,
                                      maxLines: 1,
                                      //overflow: TextOverflow.ellipsis,
                                      style: textStyle(false, 14, black),
                                    ),
                                    addSpace(3),
                                    Text(
                                      size,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: textStyle(
                                          false, 12, black.withOpacity(.5)),
                                    ),
                                  ],
                                ),
                              ),
                              //addSpaceWidth(5),
                              exists || fileUrl.isEmpty
                                  ? Container()
                                  : Container(
                                      width: 27,
                                      height: 27,
                                      margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: blue0,
                                          border: Border.all(
                                              width: 1, color: blue0)),
                                      child: GestureDetector(
                                        onTap: () {
                                          if (!downloading) {
                                            downloadChatFile(chat, onComplete);
                                          }
                                        },
                                        child: (!downloading)
                                            ? Container(
                                                width: 27,
                                                height: 27,
                                                child: Icon(
                                                  Icons.arrow_downward,
                                                  color: white,
                                                  size: 15,
                                                ),
                                              )
                                            : Container(
                                                margin: EdgeInsets.all(3),
                                                child:
                                                    CircularProgressIndicator(
                                                  //value: 20,
                                                  valueColor:
                                                      AlwaysStoppedAnimation<
                                                          Color>(white),
                                                  strokeWidth: 2,
                                                ),
                                              ),
                                      ),
                                    )
                              //addSpaceWidth(5),
                            ],
                          ),
                        ),
//                  addSpace(3),
//                  Padding(
//                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
//                    child: Text(
//                      getChatTime(chat.getInt(TIME)),
//                      style: textStyle(false, 12, black.withOpacity(.3)),
//                    ),
//                  ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        userImageItem(context, user: groupMembers[chat.getUserId()])
      ],
    );
  }

  outgoingChatDoc(context, BaseModel chat, onSaved, bool firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String fileName = chat.getString(FILE_NAME);
    String size = chat.getString(FILE_SIZE);
    String ext = chat.getString(FILE_EXTENSION);
    bool uploading = upOrDown.contains(chat.getObjectId());
    String filePath = chat.getString(FILE_PATH);
    String fileUrl = chat.getString(FILE_URL);

    String chatId = chat.getString(CHAT_ID);
    chatId = getOtherPersonId(chat);
    bool read = chat.getList(READ_BY).contains(otherPerson.getObjectId());
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        //long pressed chat...
        showChatOptions(context, chat);
      },
      onTap: () async {
        if (fileUrl.isEmpty) return;

        await openTheFile(filePath);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 15, 5),
        width: 220,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 200,
                  child: new Card(
                    color: read ? blue3 : blue0,
                    clipBehavior: Clip.antiAlias,
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: chat.myItem()
                            ? BorderRadius.only(
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25),
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(0),
                              )
                            : BorderRadius.only(
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25),
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(0),
                              )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                          width: 250,
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
//                  color: black.withOpacity(.2),
                              borderRadius: BorderRadius.circular(5)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              //addSpaceWidth(10),
                              fileUrl.isNotEmpty
                                  ? Container()
                                  : new Container(
                                      width: 27,
                                      height: 27,
                                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: blue0.withOpacity(.3),
                                          border: Border.all(
                                              width: 1, color: blue0)),
                                      child: GestureDetector(
                                        onTap: () {
                                          if (!uploading) {
                                            saveChatFile(chat, FILE_PATH,
                                                FILE_URL, onSaved);
                                            onSaved();
                                          }
                                        },
                                        child: uploading
                                            ? Container(
                                                margin: EdgeInsets.all(3),
                                                child:
                                                    CircularProgressIndicator(
                                                  //value: 20,
                                                  valueColor:
                                                      AlwaysStoppedAnimation<
                                                          Color>(white),
                                                  strokeWidth: 2,
                                                ),
                                              )
                                            : Container(
                                                width: 27,
                                                height: 27,
                                                child: Icon(
                                                  Icons.arrow_upward,
                                                  color: white,
                                                  size: 15,
                                                ),
                                              ),
                                      )),

                              new Flexible(
                                flex: 1,
                                fit: FlexFit.tight,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      fileName,
                                      maxLines: 1,
                                      //overflow: TextOverflow.ellipsis,
                                      style: textStyle(true, 14, white),
                                    ),
                                    addSpace(3),
                                    Text(
                                      size,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: textStyle(
                                          false, 12, white.withOpacity(.5)),
                                    ),
                                  ],
                                ),
                              ),
                              //addSpaceWidth(5),

                              addSpaceWidth(10),
                              Image.asset(
                                getExtImage(ext),
                                width: 20,
                                height: 20,
                              ),

                              //addSpaceWidth(5),
                            ],
                          ),
                        ),
                        //addSpace(3),
//            Padding(
//              padding: const EdgeInsets.fromLTRB(15, 0, 10, 10),
//              child: Text(
//                getChatTime(chat.getInt(TIME)),
//                style: textStyle(false, 12, white.withOpacity(.3)),
//              ),
//            ),
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: blue0,
                  ),
                if (chat.getBoolean(LOCKED))
                  Icon(
                    Icons.lock,
                    size: 12,
                    color: red0,
                  ),
                delayWidget(chat),
              ],
            ),
          ],
        ),
      ),
    );
  }

  incomingChatImage(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    if (chat.getBoolean(LOCKED) && !unlockedChats.contains(chat.getObjectId()))
      return incomingChatLocked(context, chat);
//    if (chat.getInt(DELAY) > 0) return Container();
    //List images = chat.getList(IMAGES);
    String firstImage = chat.getString(IMAGE_URL);

//  return Container();
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      children: <Widget>[
        new GestureDetector(
          onLongPress: () {
            //long pressed chat...
            showChatOptions(context, chat);
          },
          onTap: () {
            if (firstImage.isEmpty) return;
            pushAndResult(context, ViewImage([firstImage], 0));
          },
          child: Container(
            width: 250,
//            height: 200,
            margin: EdgeInsets.fromLTRB(65, 0, 40, 0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (replyData != null)
                  chatReplyWidget(
                    replyData,
                  ),
                Container(
                  width: 250,
                  height: 200,
                  child: new Card(
                    clipBehavior: Clip.antiAlias,
                    color: default_white,
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        CachedNetworkImage(
                            imageUrl: firstImage,
                            width: 200,
                            height: 200,
                            fit: BoxFit.cover,
                            placeholder: (c, p) {
                              return placeHolder(200, width: double.infinity);
                            }),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: gradientLine(height: 40),
                        ),
                        Align(
                            alignment: Alignment.bottomLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                getChatTime(chat.getInt(TIME)),
                                style:
                                    textStyle(false, 12, white.withOpacity(.3)),
                              ),
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        userImageItem(context, user: groupMembers[chat.getUserId()])
      ],
    );
  }

  outgoingChatImage(context, BaseModel chat, onSaved, firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String imageUrl = chat.getString(IMAGE_URL);
    String imagePath = chat.getString(IMAGE_PATH);
    bool uploading = upOrDown.contains(chat.getObjectId());

    String chatId = chat.getString(CHAT_ID);
    chatId = getOtherPersonId(chat);
    bool read = chat.getList(READ_BY).contains(otherPerson.getObjectId());

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        //long pressed chat...
        showChatOptions(context, chat);
      },
      onTap: () {
        if (imageUrl.isEmpty) return;
        pushAndResult(context, ViewImage([imageUrl], 0));
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 15, 5),
        width: 270,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: 230,
                  height: 200,
                  child: new Card(
                    color: blue0,
                    clipBehavior: Clip.antiAlias,
//                  margin: EdgeInsets.all(0),
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(0),
                    )),
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        imageUrl.isEmpty
                            ? Image.file(
                                File(imagePath),
                                fit: BoxFit.cover,
                              )
                            : CachedNetworkImage(
                                imageUrl: imageUrl,
                                fit: BoxFit.cover,
                                placeholder: (c, p) {
                                  return placeHolder(200,
                                      width: double.infinity);
                                }),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: gradientLine(height: 40),
                        ),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                getChatTime(chat.getInt(TIME)),
                                style:
                                    textStyle(false, 12, white.withOpacity(.3)),
                              ),
                            )),
                        imageUrl.isNotEmpty
                            ? Container()
                            : Align(
                                alignment: Alignment.center,
                                child: GestureDetector(
                                  onTap: () {
                                    if (!uploading) {
                                      saveChatFile(
                                          chat, IMAGE_PATH, IMAGE_URL, onSaved);
                                      onSaved();
                                    }
                                  },
                                  child: Container(
                                    width: 40,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        color: black.withOpacity(.9),
                                        border:
                                            Border.all(color: white, width: 1),
                                        shape: BoxShape.circle),
                                    child: uploading
                                        ? Container(
                                            margin: EdgeInsets.all(5),
                                            child: CircularProgressIndicator(
                                              //value: 20,
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      white),
                                              strokeWidth: 2,
                                            ),
                                          )
                                        : Center(
                                            child: Icon(
                                              Icons.arrow_upward,
                                              color: white,
                                              size: 20,
                                            ),
                                          ),
                                  ),
                                ),
                              )
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: blue0,
                  ),
                if (chat.getBoolean(LOCKED))
                  Icon(
                    Icons.lock,
                    size: 12,
                    color: red0,
                  ),
                delayWidget(chat),
              ],
            ),
          ],
        ),
      ),
    );
  }

  incomingChatVideo(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    if (chat.getBoolean(LOCKED) && !unlockedChats.contains(chat.getObjectId()))
      return incomingChatLocked(context, chat);
//    if (chat.getInt(DELAY) > 0) return Container();

    String videoUrl = chat.getString(VIDEO_URL);
    String thumb = chat.getString(THUMBNAIL_URL);
    String videoLenght = chat.getString(VIDEO_LENGTH);
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      children: <Widget>[
        Opacity(
          opacity: videoUrl.isEmpty ? (.4) : 1,
          child: new GestureDetector(
            onLongPress: () {
              showChatOptions(context, chat);
              //long pressed chat...
            },
            child: Container(
              width: 250,
              margin: EdgeInsets.fromLTRB(65, 0, 40, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (replyData != null)
                    chatReplyWidget(
                      replyData,
                    ),
                  new Card(
                    clipBehavior: Clip.antiAlias,
                    color: AppConfig.appColor,
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      topRight: Radius.circular(25),
                      topLeft: Radius.circular(0),
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                    )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Container(
                            color: blue0.withOpacity(.1),
                            width: 250,
                            height: 150,
                            child: new GestureDetector(
                                onTap: () {
                                  if (videoUrl.isEmpty) return;
                                  print(videoUrl);
                                  pushAndResult(context,
                                      PlayVideo(chat.getObjectId(), videoUrl),
                                      depend: false);
                                },
                                child: new Stack(
                                  children: <Widget>[
                                    thumb.isEmpty
                                        ? Container()
                                        : CachedNetworkImage(
                                            imageUrl: thumb,
                                            fit: BoxFit.cover,
                                            width: double.infinity,
                                            height: 250,
                                          ),
                                    Center(
                                      child: new Container(
                                        width: 40,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: black.withOpacity(.9),
                                            border: Border.all(
                                                color: white, width: 1),
                                            shape: BoxShape.circle),
                                        child: videoUrl.isNotEmpty
                                            ? Center(
                                                child: Icon(
                                                  Icons.play_arrow,
                                                  color: white,
                                                  size: 20,
                                                ),
                                              )
                                            : Container(),
                                      ),
                                    ),
                                    new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Expanded(flex: 1, child: Container()),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      8, 0, 0, 8),
                                              child: Text(
                                                getChatTime(chat.getInt(TIME)),
                                                style: textStyle(false, 12,
                                                    white.withOpacity(.3)),
                                              ),
                                            ),
                                            Flexible(
                                                flex: 1, child: Container()),
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                  color: black.withOpacity(.9),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        8, 4, 8, 4),
                                                child: Text(videoLenght,
                                                    style: textStyle(
                                                        false, 12, white)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ))),
                        /* addSpace(3),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 5, 15, 10),
                        child: Text(
                          getChatTime(chat.getInt(TIME)),
                          style: textStyle(false, 12, black.withOpacity(.3)),
                        ),
                      ),*/
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        userImageItem(context)
      ],
    );
  }

  outgoingChatVideo(context, BaseModel chat, onSaved, firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    String videoUrl = chat.getString(VIDEO_URL);
    String videoPath = chat.getString(VIDEO_PATH);
    String thumbPath = chat.getString(THUMBNAIL_PATH);
    String thumb = chat.getString(THUMBNAIL_URL);
    String videoLenght = chat.getString(VIDEO_LENGTH);
    bool uploading = upOrDown.contains(chat.getObjectId());

    String chatId = chat.getString(CHAT_ID);
    chatId = getOtherPersonId(chat);
    bool read = chat.getList(READ_BY).contains(otherPerson.getObjectId());

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        showChatOptions(context, chat);
        //long pressed chat...
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 20, 5),
        width: 270,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 250,
                  child: new Card(
                    color: blue0,
                    clipBehavior: Clip.antiAlias,
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(0),
                    )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Container(
                            color: blue0.withOpacity(.1),
                            width: 250,
                            height: 150,
                            child: new GestureDetector(
                                onTap: () {
                                  if (videoUrl.isNotEmpty) {
                                    pushAndResult(
                                        context,
                                        PlayVideo(
                                          chat.getObjectId(),
                                          videoUrl,
                                          videoFile: File(videoPath),
                                        ));
                                  } else {
                                    if (!uploading) {
                                      saveChatVideo(chat, onSaved);
                                      onSaved();
                                    }
                                  }
                                },
                                child: new Stack(
                                  fit: StackFit.expand,
                                  children: <Widget>[
                                    thumb.isEmpty
                                        ? Image.file(
                                            File(thumbPath),
                                            fit: BoxFit.cover,
                                          )
                                        : CachedNetworkImage(
                                            imageUrl: thumb,
                                            fit: BoxFit.cover,
                                            placeholder: (c, p) {
                                              return placeHolder(250,
                                                  width: double.infinity);
                                            }),
                                    /*
                                  thumb.isEmpty
                                      ? Container()
                                      : CachedNetworkImage(
                                          imageUrl: thumb,
                                          fit: BoxFit.cover,
                                          width: double.infinity,
                                          height: 250,
                                        ),*/
                                    Center(
                                      child: new Container(
                                        width: 40,
                                        height: 40,
                                        decoration: BoxDecoration(
                                            color: black.withOpacity(.9),
                                            border: Border.all(
                                                color: white, width: 1),
                                            shape: BoxShape.circle),
                                        child: videoUrl.isNotEmpty
                                            ? Center(
                                                child: Icon(
                                                  Icons.play_arrow,
                                                  color: white,
                                                  size: 20,
                                                ),
                                              )
                                            : (!uploading)
                                                ? Center(
                                                    child: Icon(
                                                      Icons.arrow_upward,
                                                      color: white,
                                                      size: 20,
                                                    ),
                                                  )
                                                : Container(
                                                    margin: EdgeInsets.all(5),
                                                    child:
                                                        CircularProgressIndicator(
                                                      //value: 20,
                                                      valueColor:
                                                          AlwaysStoppedAnimation<
                                                              Color>(white),
                                                      strokeWidth: 2,
                                                    ),
                                                  ),
                                      ),
                                    ),
                                    new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Expanded(flex: 1, child: Container()),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Container(
                                              margin: EdgeInsets.all(10),
                                              decoration: BoxDecoration(
                                                  color: black.withOpacity(.9),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        8, 4, 8, 4),
                                                child: Text(videoLenght,
                                                    style: textStyle(
                                                        false, 12, white)),
                                              ),
                                            ),
                                            Flexible(
                                                flex: 1, child: Container()),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0, 0, 8, 8),
                                              child: Text(
                                                getChatTime(chat.getInt(TIME)),
                                                style: textStyle(false, 12,
                                                    white.withOpacity(.3)),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ))),
                        //addSpace(3),
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: blue0,
                  ),
                if (chat.getBoolean(LOCKED))
                  Icon(
                    Icons.lock,
                    size: 12,
                    color: red0,
                  ),
                delayWidget(chat),
              ],
            ),
          ],
        ),
      ),
    );
  }

  incomingChatContact(context, BaseModel chat) {
    if (chat.getBoolean(DELETED)) {
      return incomingChatDeleted(context, chat);
    }
    if (chat.getList(HIDDEN).contains(userModel.getObjectId())) {
      return Container();
    }
    if (chat.getBoolean(LOCKED) && !unlockedChats.contains(chat.getObjectId()))
      return incomingChatLocked(context, chat);
//    if (chat.getInt(DELAY) > 0) return Container();

    BaseModel contact = chat.getModel(CONTACT);
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new Stack(
      children: <Widget>[
        new GestureDetector(
          onLongPress: () {
            showChatOptions(context, chat);
            //long pressed chat...
          },
          child: Container(
            //width: 250,
            margin: EdgeInsets.fromLTRB(65, 0, 40, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              //mainAxisSize: MainAxisSize.min,
              children: [
                if (replyData != null)
                  chatReplyWidget(
                    replyData,
                  ),
                new Card(
                  clipBehavior: Clip.antiAlias,
                  elevation: elevation,
                  color: default_white,
                  /*shadowColor: black.withOpacity(.3),*/
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    topLeft: Radius.circular(0),
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  )),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 250,
                        padding: EdgeInsets.all(3),
                        child: new Card(
                          //color: blue0,
                          clipBehavior: Clip.antiAlias,
                          elevation: elevation,
                          /*shadowColor: black.withOpacity(.3),*/
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                            topRight: Radius.circular(25),
                            topLeft: Radius.circular(0),
                            bottomLeft: Radius.circular(25),
                            bottomRight: Radius.circular(25),
                          )),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Row(
                                children: [
                                  userImageItem(context, user: contact),
                                  addSpaceWidth(10),
                                  Text(
                                    contact.getString(NAME),
                                    style: textStyle(false, 17, black),
                                  )
                                ],
                              ),
                              //addLine(.5, black.withOpacity(.5), 0, 6, 0, 0),
                              FlatButton(
                                onPressed: () {
                                  //clickChat(context, 0, contact, replace: true);
                                },
                                color: blue6,
                                child: Center(
                                  child: Text(
                                    "Message",
                                    style: textStyle(false, 17, white),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        userImageItem(context)
      ],
    );
  }

  outgoingChatContact(context, BaseModel chat, firstChat) {
    if (chat.getBoolean(DELETED)) {
      return Container();
    }
    BaseModel contact = chat.getModel(CONTACT);

    String chatId = chat.getString(CHAT_ID);
    chatId = getOtherPersonId(chat);
    bool read = chat.getList(READ_BY).contains(otherPerson.getObjectId());

    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return new GestureDetector(
      onLongPress: () {
        showChatOptions(context, chat);
        //long pressed chat...
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 0, 20, 5),
        width: 270,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (replyData != null)
              chatReplyWidget(
                replyData,
              ),
            Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 250,
                  padding: EdgeInsets.all(3),
                  child: new Card(
                    //color: blue0,
                    clipBehavior: Clip.antiAlias,
                    elevation: elevation,
                    /*shadowColor: black.withOpacity(.3),*/
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      bottomRight: Radius.circular(25),
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(0),
                    )),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          children: [
                            userImageItem(context, user: contact),
                            addSpaceWidth(10),
                            Text(
                              contact.getString(NAME),
                              style: textStyle(false, 17, black),
                            )
                          ],
                        ),
                        //addLine(.5, black.withOpacity(.5), 0, 6, 0, 0),
                        FlatButton(
                          onPressed: () {
                            clickChat(context, 0, contact, replace: true);
                          },
                          color: blue6,
                          child: Center(
                            child: Text(
                              "Message",
                              style: textStyle(false, 17, white),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                if (read && firstChat)
                  Icon(
                    Icons.remove_red_eye,
                    size: 12,
                    color: blue0,
                  ),
                if (chat.getBoolean(LOCKED))
                  Icon(
                    Icons.lock,
                    size: 12,
                    color: red0,
                  ),
                delayWidget(chat),
              ],
            ),
          ],
        ),
      ),
    );
  }

  userImageItem(context, {BaseModel user}) {
    BaseModel person = otherPerson;
    if (null != user) person = user;
    String image = person.getString(USER_IMAGE);
    //if (groupChat) image = user.getString(GROUP_PHOTO);
    //if (isTyping) image = typingUser[USER_IMAGE];
    if (isRecording) image = recordingUser[USER_IMAGE];

//    String image = !groupChat
//        ? person.getString(USER_IMAGE)
//        : (isTyping ? (typingUser[USER_IMAGE]) : (recordingUser[USER_IMAGE]));

    return new GestureDetector(
      onTap: () {
        //if (user != null) return;
        if (!groupChat) {
          pushAndResult(
              context,
              ShowProfile(
                person,
                user != null ? [] : chatList,
                0,
                shared: user != null,
              ));
        }
      },
      child: new Container(
        decoration: BoxDecoration(
          border: Border.all(width: 2, color: white),
          shape: BoxShape.circle,
        ),
        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
        width: 40,
        height: 40,
        child: Stack(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(22),
              child: Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppConfig.appColor,
                    gradient: LinearGradient(colors: [
                      blue4,
                      blue2,
                    ])),
                child: image.isEmpty
                    ? Center(
                        child: Text(
                          getInitials(person.getString(NAME)),
                          style: textStyle(true, 14, white),
                        ),
                      )
                    : CachedNetworkImage(
                        imageUrl: image,
                        fit: BoxFit.cover,
                      ),
              ),
            ),

            /*  Card(
              margin: EdgeInsets.all(0),
              shape: CircleBorder(),
              clipBehavior: Clip.antiAlias,
              color: transparent,
              elevation: .5,
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 40,
                    height: 40,
                    color: blue0,
                    child: Center(
                        child: Icon(
                      Icons.person,
                      color: white,
                      size: 15,
                    )),
                  ),
                  CachedNetworkImage(
                    width: 40,
                    height: 40,
                    imageUrl: image,
                    fit: BoxFit.cover,
                  ),
                  if (!groupChat && image.isEmpty)
                    Center(
                      child: Text(
                        getInitials(otherPerson.getString(NAME)),
                        style: textStyle(true, 14, white),
                      ),
                    )
                ],
              ),
            ),*/

            if (isOnline)
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: white, width: 2),
                    color: red0,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  saveChatFile(BaseModel model, String pathKey, String urlKey, onSaved) {
    upOrDown.add(model.getObjectId());
    String path = model.getString(pathKey);
    uploadFile(File(path), (_, error) {
      upOrDown.removeWhere((s) => s == model.getObjectId());
      if (error != null) {
        return;
      }
      model.put(urlKey, _);
      model.updateItems();
      onSaved();
    });
  }

  saveChatVideo(BaseModel model, onSaved) {
    String thumb = model.getString(THUMBNAIL_PATH);
    String videoPath = model.getString(VIDEO_PATH);
    String thumbUrl = model.getString(THUMBNAIL_URL);
    String videoUrl = model.getString(VIDEO_URL);

    bool uploadingThumb = thumbUrl.isEmpty;

    if (videoUrl.isNotEmpty) {
      onSaved();
      return;
    }

    upOrDown.add(model.getObjectId());

    uploadFile(File(uploadingThumb ? thumb : videoPath), (_, error) {
      upOrDown.removeWhere((s) => s == model.getObjectId());
      if (error != null) {
        return;
      }
      model.put(uploadingThumb ? THUMBNAIL_URL : VIDEO_URL, _);
      model.updateItems();
      saveChatVideo(model, onSaved);
    });
  }

  downloadChatFile(BaseModel model, onComplete) async {
    String fileName =
        "${model.getObjectId()}.${model.getString(FILE_EXTENSION)}";
    File file = await getDirFile(fileName);
    upOrDown.add(model.getObjectId());
    onComplete();

    QuerySnapshot shots = await Firestore.instance
        .collection(REFERENCE_BASE)
        .where(FILE_URL, isEqualTo: model.getString(FILE_URL))
        .limit(1)
        .getDocuments();
    if (shots.documents.isEmpty) {
      upOrDown.removeWhere((s) => s == model.getObjectId());
      onComplete();
    } else {
      for (DocumentSnapshot doc in shots.documents) {
        if (!doc.exists || doc.data().isEmpty) continue;
        BaseModel model = BaseModel(doc: doc);
        String ref = model.getString(REFERENCE);
        StorageReference storageReference =
            FirebaseStorage.instance.ref().child(ref);
        storageReference.writeToFile(file).future.then((_) {
          //toastInAndroid("Download Complete");
          upOrDown.removeWhere((s) => s == model.getObjectId());
          onComplete();
        }, onError: (error) {
          upOrDown.removeWhere((s) => s == model.getObjectId());
          onComplete();
        }).catchError((error) {
          upOrDown.removeWhere((s) => s == model.getObjectId());
          onComplete();
        });

        break;
      }
    }
  }

  showChatOptions(context, BaseModel chat, {bool deletedChat = false}) {
    int type = chat.getInt(TYPE);
    bool locked = chat.getBoolean(LOCKED);
    List options = ["Forward", "Reply", "Delete"];
    if (type == CHAT_TYPE_TEXT && !deletedChat) options.insert(0, "Copy");
    if (!deletedChat && chat.myItem()) options.add(!locked ? "Lock" : "Unlock");
    pushAndResult(context, listDialog(options), depend: false, opaque: false,
        result: (_) {
      if (_ == "Forward") {
        pushReplaceAndResult(
            context,
            ShareTo(
              chatMode: chatMode,
              messages: [chat],
            ),
            depend: false);
      }

      if (_ == "Reply") {
        setState(() {
          replyModel = chat;
        });
      }
      if (_ == "Copy") {
        Clipboard.setData(ClipboardData(text: chat.getString(MESSAGE)));
      }
      if (_ == "Delete") {
        if (!userModel.isPremium) {
          showMessage(context, Icons.error, red0, "Opps Sorry!",
              "You cannot delete messages on ConvasApp until you become a Premium User",
              clickYesText: "Go Premium", onClicked: (_) {
            if (_) pushAndResult(context, ShowWallet(), depend: false);
          });
          return;
        }

        if (chat.myItem()) {
          chat.put(DELETED, true);
          chat.updateItems();
        } else {
          List hidden = List.from(chat.getList(HIDDEN));
          hidden.add(userModel.getObjectId());
          chat.put(HIDDEN, hidden);
          chat.updateItems();
        }
      }
      if (_ == "Lock" || _ == "Unlock") {
        pushAndResult(context, EnterPin(), depend: false, result: (_) {
          if (null == _) return;

          if (_ == userModel.getString(CODE_PIN)) {
            chat.put(LOCKED, !locked);
            chat.updateItems();
          }
        });
      }
    });
  }

  AudioPlayer recAudioController = AudioPlayer();
  bool isPlaying = false;
  String currentPlayingAudio;
  String currentPlayingUrl;
  AudioPlayer audioPlayer;

  bool recPlayEnded = false;
  List noFileFound = [];
  getChatAudioWidget(
      context, BaseModel chat, onEdited(bool removed), bool firstChat) {
    if (chat.getBoolean(DELETED)) {
      if (!chat.myItem()) {
        return incomingChatDeleted(context, chat);
      }
      return Container();
    }
//    return Container();
    bool read = chat.getList(READ_BY).contains(otherPerson.getObjectId());
    String audioUrl = chat.getString(AUDIO_URL);
    String audioPath = chat.getString(AUDIO_PATH);
    //print(audioUrl);
    String audioLength = chat.getString(AUDIO_LENGTH);
    bool uploading = upOrDown.contains(chat.getObjectId());
    bool noFile = noFileFound.contains(chat.getObjectId());
    bool currentPlay = currentPlayingAudio == chat.getObjectId();
/*    bool isPlaying = recAudioController != null &&
        recAudioController.value.initialized &&
        recAudioController. value.isPlaying;*/
    var parts = audioPath.split("/");
    String baseFileName = parts[parts.length - 1];
    BaseModel replyData = BaseModel(items: chat.getMap(REPLY_DATA));
    return Container(
//    height: chat.myItem()?null:40,
      width: chat.myItem() ? null : 270,
      margin: EdgeInsets.fromLTRB(0, 0, 0, chat.myItem() ? 15 : 10),
      child: Stack(
        children: [
          Align(
            alignment:
                !chat.myItem() ? Alignment.centerLeft : Alignment.centerRight,
            child: Opacity(
              opacity: audioUrl.isEmpty && !chat.myItem() ? (.4) : 1,
              child: new GestureDetector(
                onTap: () async {
                  if (audioUrl.isEmpty && !chat.myItem()) return;
                  if (uploading) return;
                  if (noFile) {
                    showMessage(context, Icons.error, red0, "File not found",
                        "This file no longer exist on your device");
                    return;
                  }

                  if (!chat.myItem()) {
                    String path = await localPath;
                    File file =
                        File("$path/${chat.getObjectId()}$baseFileName");
                    print("File Path: ${file.path}");
                    bool exists = await file.exists();
                    if (!exists) {
                      upOrDown.add(chat.getObjectId());
                      onEdited(false);
                      downloadFile(file, audioUrl, (e) {
                        upOrDown.removeWhere((r) => r == chat.getObjectId());
                        fileThatExists.add(chat.getObjectId());
                        onEdited(false);
                      });
                      return;
                    } else {
                      audioPath = file.path;
                      fileThatExists.add(chat.getObjectId());
                      onEdited(false);
                    }
                  }

                  if (currentPlayingAudio == chat.getObjectId()) {
                    if (isPlaying) {
                      recAudioController.pause();
                      isPlaying = false;
                      setState(() {});
                    } else {
                      recAudioController.resume();
                      isPlaying = true;
                      setState(() {});
                    }
                  } else {
                    try {
                      await recAudioController.stop();
                    } catch (e) {}
                    recAudioController.play(
                      audioUrl,
                      //isLocal: true,
                    );
                    //recAudioController.setReleaseMode(ReleaseMode.STOP);
                    isPlaying = true;
                    currentPlayingAudio = chat.getObjectId();
                    setState(() {});
                  }
                },
                onLongPress: () {
                  showChatOptions(context, chat);
                },
                child: Container(
                  width: 200,
                  margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                  child: Column(
                    crossAxisAlignment: chat.myItem()
                        ? (CrossAxisAlignment.start)
                        : CrossAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (replyData != null)
                        chatReplyWidget(
                          replyData,
                        ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new Container(
                            height: 30,
                            width: 170,
                            color: transparent,
                            child: Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 0,
                              /*shadowColor: black.withOpacity(.3),*/
                              shape: RoundedRectangleBorder(
                                  borderRadius: chat.myItem()
                                      ? BorderRadius.only(
                                          bottomLeft: Radius.circular(25),
                                          bottomRight: Radius.circular(25),
                                          topLeft: Radius.circular(25),
                                          topRight: Radius.circular(0),
                                        )
                                      : BorderRadius.only(
                                          bottomLeft: Radius.circular(25),
                                          bottomRight: Radius.circular(25),
                                          topLeft: Radius.circular(0),
                                          topRight: Radius.circular(25),
                                        )),
                              margin: EdgeInsets.all(0),
                              color: isPlaying && currentPlay
                                  ? blue3
                                  : blue0, // isPlaying && currentPlay ? AppConfig.appColor : blue0,
                              child: Stack(
                                fit: StackFit.expand,
                                children: [
                                  /*LinearProgressIndicator(
                                    value:currentPlay?(playPosition / 100):0,
                                    backgroundColor: transparent,
                                    valueColor:
                                    AlwaysStoppedAnimation<Color>(black.withOpacity(.7)),
                                  ),*/
                                  Row(
                                    children: [
                                      addSpaceWidth(10),
                                      if (uploading)
                                        Container(
                                          width: 14,
                                          height: 14,
                                          child: CircularProgressIndicator(
                                            //value: 20,
                                            valueColor:
                                                AlwaysStoppedAnimation<Color>(
                                                    white),
                                            strokeWidth: 2,
                                          ),
                                        ),
                                      if (!uploading)
                                        chat.myItem()
                                            ? (Icon(
                                                currentPlay && isPlaying
                                                    ? (Icons.pause)
                                                    : Icons.play_circle_filled,
                                                color: white,
                                              ))
                                            : (Icon(
                                                !fileThatExists.contains(
                                                        chat.getObjectId())
                                                    ? Icons.file_download
                                                    : currentPlay && isPlaying
                                                        ? (Icons.pause)
                                                        : Icons
                                                            .play_circle_filled,
                                                color: white,
                                              )) /*FutureBuilder(
                                      builder: (c,d){
                                        if(!d.hasData)return Container();
                                        bool exists = d.data;
                                        return Icon(
                                          !exists?Icons.file_download:currentPlay && isPlaying?
                                          (Icons.pause):Icons.play_circle_filled,
                                          color: white,
                                        );
                                      },future: checkLocalFile("${chat.getObjectId()}$baseFileName"),
                                    )*/
                                      ,
                                      Flexible(
                                        fit: FlexFit.tight,
                                        child: Container(
                                          height: 2,
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              color: white,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(5))),
                                        ),
                                      ),
                                      Container(
                                        padding:
                                            EdgeInsets.fromLTRB(5, 2, 5, 2),
                                        decoration: BoxDecoration(
                                            color: white,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(25))),
                                        child: Text(
                                          audioLength,
                                          style: textStyle(
                                              false, 12, AppConfig.appColor),
                                        ),
                                      ),
                                      addSpaceWidth(5),
                                      if (noFile)
                                        Container(
                                            padding: EdgeInsets.all(1),
                                            decoration: BoxDecoration(
                                                color: white,
                                                shape: BoxShape.circle),
                                            child: Icon(
                                              Icons.error,
                                              color: red0,
                                              size: 18,
                                            )),
                                      addSpaceWidth(10),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          if (chat.myItem())
                            if (read && firstChat)
                              Icon(
                                Icons.remove_red_eye,
                                size: 12,
                                color: AppConfig.appColor,
                              ),
                          if (chat.myItem() && chat.getBoolean(LOCKED))
                            Icon(
                              Icons.lock,
                              size: 12,
                              color: red0,
                            ),
                          if (chat.myItem()) delayWidget(chat),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          if (!chat.myItem())
            userImageItem(context, user: groupMembers[chat.getUserId()])
        ],
      ),
    );
  }

  chatReplyWidget(BaseModel chat, {onRemoved, bool fitScreen = false}) {
    if (chat.items.isEmpty) return Container();
    String text = chat.getString(MESSAGE);
    int type = chat.getType();
    if (type == CHAT_TYPE_DOC) text = "Document";
    if (type == CHAT_TYPE_IMAGE) text = "Photo";
    if (type == CHAT_TYPE_VIDEO)
      text = "Video (${chat.getString(VIDEO_LENGTH)})";
    if (type == CHAT_TYPE_REC)
      text = "Voice Message (${chat.getString(AUDIO_LENGTH)})";
    var icon;
    if (type == CHAT_TYPE_DOC) icon = Icons.assignment;
    if (type == CHAT_TYPE_IMAGE) icon = Icons.photo;
    if (type == CHAT_TYPE_VIDEO) icon = Icons.videocam;
    if (type == CHAT_TYPE_REC) icon = Icons.mic;

    String image = "";
    if (type == CHAT_TYPE_IMAGE) image = chat.getString(IMAGE_URL);
    if (type == CHAT_TYPE_VIDEO) image = chat.getString(THUMBNAIL_URL);

    return GestureDetector(
      onTap: () {
        scrollToMessage(chat.getObjectId());
      },
      child: Container(
//    width: 100,
          width: fitScreen ? double.infinity : null,
          child: Card(
            clipBehavior: Clip.antiAlias,
            color: default_white,
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                side: BorderSide(color: black.withOpacity(.1), width: .5)),
            child: Container(
              decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: blue3, width: 5))),
              child: Row(
                mainAxisSize: fitScreen ? MainAxisSize.max : MainAxisSize.min,
                children: [
                  Flexible(
                    fit: fitScreen ? FlexFit.tight : FlexFit.loose,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(
                          10,
                          onRemoved == null ? 10 : 0,
                          onRemoved == null ? 10 : 0,
                          image.isEmpty ? 10 : 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Flexible(
                                  fit: !fitScreen
                                      ? FlexFit.loose
                                      : FlexFit.tight,
                                  child: Text(
                                    chat.myItem()
                                        ? "YOU"
                                        : chat.getString(NAME),
                                    style: textStyle(
                                        true, 12, black.withOpacity(.5)),
                                  )),
                              if (onRemoved != null && image.isEmpty)
                                Container(
                                  width: 30,
                                  height: 25,
                                  child: FlatButton(
                                      padding: EdgeInsets.all(0),
                                      onPressed: () {
                                        onRemoved();
                                      },
                                      child: Icon(
                                        Icons.close,
                                        size: 15,
                                        color: black.withOpacity(.5),
                                      )),
                                )
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (icon != null)
                                Icon(
                                  icon,
                                  size: 14,
                                  color: black.withOpacity(.3),
                                ),
                              addSpaceWidth(3),
                              Flexible(
                                child: Text(
                                  text,
                                  style: textStyle(false, 14, black),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              addSpaceWidth(10),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (image.isNotEmpty)
                    Container(
                      width: 50,
                      height: 50,
                      child: Stack(
                        fit: StackFit.expand,
                        children: [
                          CachedNetworkImage(
                            imageUrl: image,
                            fit: BoxFit.cover,
                          ),
                          if (onRemoved != null)
                            Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                width: 16,
                                height: 16,
                                margin: EdgeInsets.all(2),
                                child: FlatButton(
                                    padding: EdgeInsets.all(0),
                                    onPressed: () {
                                      onRemoved();
                                    },
                                    shape: CircleBorder(),
                                    color: white.withOpacity(.5),
                                    child: Icon(
                                      Icons.close,
                                      size: 12,
                                      color: black.withOpacity(.5),
                                    )),
                              ),
                            )
                        ],
                      ),
                    )
                ],
              ),
            ),
          )),
    );
  }

  delayWidget(BaseModel chat) {
    int delay = chat.getInt(DELAY);
    if (delay == 0) return Container();
    return Container(
      height: 50,
      width: 40,
//                                      color: black.withOpacity(.3),
      child: Stack(
        children: [
          Center(
            child: Icon(
              Icons.access_time,
              color: black.withOpacity(.5),
//                                              size: 30,
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              width: 20,
              height: 20,
              margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
              decoration: BoxDecoration(
                  color: red0,
//                            borderRadius: BorderRadius.circular(5),
                  shape: BoxShape.circle,
                  border: Border.all(color: white, width: 2)),
              child: Center(
                  child: Text(
                "${delay}",
                style: textStyle(true, 10, white),
              )),
            ),
          )
        ],
      ),
    );
  }

  double overlayOffset = 0;
  bool showOverlay = false;

  void handleOverlay() {
//    showOverlay = !showOverlay;
//    setState(() {});
//    return;
    showListDialog(context, ["Photo", "Video"], (p) {
      if (p == 0) postChatImage();
      if (p == 1) postChatVideo(false);
    }, showTitle: false);
  }
}
