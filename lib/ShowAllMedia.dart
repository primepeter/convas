

 import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/ViewChatMedia.dart';
import 'package:Strokes/assets.dart';
import 'package:Strokes/basemodel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ShowAllMedia extends StatefulWidget {
 List chatMedia;
 ShowAllMedia(this.chatMedia);
   @override
   _ShowAllMediaState createState() => _ShowAllMediaState();
 }

 class _ShowAllMediaState extends State<ShowAllMedia> {

  List chatMedia;
  List chatPhotos=[];
  List chatVideos=[];
  List chatDocs=[];

 @override
  void initState() {
    // TODO: implement initState
    super.initState();
    chatMedia= widget.chatMedia;
    for(BaseModel model in chatMedia){
     if(model.getType()==CHAT_TYPE_IMAGE)chatPhotos.add(model);
     if(model.getType()==CHAT_TYPE_VIDEO)chatVideos.add(model);
     if(model.getType()==CHAT_TYPE_DOC)chatDocs.add(model);
    }
  }

   @override
   Widget build(BuildContext context) {
     return Scaffold(
      backgroundColor: white,
      body: page(),
     );
   }

   int currentPage =0;
 PageController pageController = PageController();
   page(){

     return Column(
      children: [
       addSpace(40),
       Row(
        children: [
         GestureDetector(
          onTap: () async {
           Navigator.pop(context, "");
          },
          child: Container(
           //margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
              width: 50,
              height: 50,
              child: Icon(
               Icons.arrow_back,
               color: black,
               size: 30,
              )),
         ),
         Text("Chat Media",style: textStyle(true, 20, black),)
        ],
       ),
        Container(
          padding: EdgeInsets.fromLTRB(4, 4, 0, 4),
          color: white,
          child: Row(
            children: List.generate(2, (p) {
              bool active = currentPage == p;
              String title;
              if (p == 0) title = "Photos";
              if (p == 1) title = "Videos";
              if (p == 2) title = "Documents";
              bool current = p==currentPage;
              return Flexible(
                fit: FlexFit.tight,
                child: Center(
                  child: Text(title,style: textStyle(true, 18, !current?black.withOpacity(.4):blue0),),
                ),
              );
            }),
          ),
        ),
       Expanded(
         child: PageView.builder(itemBuilder: (c,p){

           List list = p==0?chatPhotos:p==1?chatVideos:chatDocs;
           if(list.isEmpty)return Center(
             child: emptyLayout(p==0?Icons.photo:p==1?Icons.video_library:Icons.library_books, "Nothing to Display", ""),
           );

          return GridView.builder(gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: ((MediaQuery.of(
                context)
                .size
                .width) /
                3),
          ), itemBuilder: (c,p){
            BaseModel model = list[p];
            bool isVideo = model.getInt(TYPE)==CHAT_TYPE_VIDEO;
            String image = isVideo?model.getString(THUMBNAIL_URL):model.getString(IMAGE_URL);
            return Hero(
              tag: model.getObjectId(),
              child: GestureDetector(
                onTap: (){
                  pushAndResult(context, ViewChatMedia(list, p));
                },
                child: Container(
                  width: 100,
                  height: 100,
                  margin: EdgeInsets.only(left: 10),
                  child: Card(
                    clipBehavior: Clip.antiAlias,color: default_white,
                    child: Stack(
                      fit: StackFit.expand,
                      children: [
                        CachedNetworkImage(imageUrl: image,fit: BoxFit.cover,),
                        if(isVideo)Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            margin: EdgeInsets.all(5),
                            padding: EdgeInsets.fromLTRB(5,2,5,2),
                            decoration: BoxDecoration(
                                color: black.withOpacity(.5),borderRadius: BorderRadius.all(Radius.circular(25))
                            ),
                            child: Text(model.getString(VIDEO_LENGTH),style: textStyle(true, 10, white),),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          },itemCount: list.length,
            padding: EdgeInsets.only(top: 5),
            physics: BouncingScrollPhysics(),);
         },
         itemCount: 2,
         onPageChanged: (p){
          setState(() {
            currentPage=p;
          });
         },controller: pageController,),
       )
      ],
     );
   }
 }
