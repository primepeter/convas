/*
import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/MainAdmin.dart';
import 'package:Strokes/assets.dart';

loadMessages(int chatMode) async {
  var lock = Lock();
  List messageList = chatMode==CHAT_MODE_ENCRYPT?lastMessagesEncrypt:lastMessages;
  List messageDot = chatMode==CHAT_MODE_ENCRYPT?showNewMessageDotEncrypt:showNewMessageDot;
  await lock.synchronized(() async {
//      List<Map> myChats = List.from(userModel.getList(MY_CHATS));
    var sub = Firestore.instance
        .collection(chatMode==CHAT_MODE_ENCRYPT?CHAT_IDS_BASE_ENCRYPT:CHAT_IDS_BASE)
        .where(PARTIES, arrayContains: userModel.getObjectId())
        .snapshots()
        .listen((shots) {
      for (DocumentSnapshot doc in shots.documents) {
        BaseModel chatIdModel = BaseModel(doc: doc);
        String chatId = chatIdModel.getObjectId();
        if (userModel.getList(DELETED_CHATS).contains(getRealChatId(chatId, chatMode))) continue;
        if (loadedIds.contains(getRealChatId(chatId, chatMode))) {
          continue;
        }
        loadedIds.add(getRealChatId(chatId, chatMode));

        var sub = Firestore.instance
            .collection(chatMode==CHAT_MODE_ENCRYPT?CHAT_BASE_ENCRYPT:CHAT_BASE)
            .where(PARTIES, arrayContains: userModel.getUserId())
            .where(CHAT_ID, isEqualTo: chatId)
            .orderBy(TIME, descending: true)
            .limit(1)
            .snapshots()
            .listen((shots) async {
          if (shots.documents.isNotEmpty) {
            BaseModel cModel = BaseModel(doc: (shots.documents[0]));
            if (isBlocked(null, userId: getOtherPersonId(cModel))) {
              messageList.removeWhere(
                      (bm) => bm.getString(CHAT_ID) == cModel.getString(CHAT_ID));
              chatMessageController.add(true);
              return;
            }
          }
          if (stopListening.contains(getRealChatId(chatId, chatMode))) return;
          for (DocumentSnapshot doc in shots.documents) {
            BaseModel model = BaseModel(doc: doc);
            String chatId = model.getString(CHAT_ID);
            int index = messageList.indexWhere(
                    (bm) => bm.getString(CHAT_ID) == model.getString(CHAT_ID));
            if (index == -1) {
              messageList.add(model);
            } else {
              messageList[index] = model;
            }

            if (!model.getList(READ_BY).contains(userModel.getObjectId()) &&
                !model.myItem() &&
                visibleChatId != model.getString(CHAT_ID)) {
              try {
                if (!messageDot.contains(chatId))
                  messageDot.add(chatId);
                setState(() {});
              } catch (E) {
                if (!messageDot.contains(chatId))
                  messageDot.add(chatId);
                setState(() {});
              }
              countUnread(chatId);
            }
          }

          String otherPersonId = getOtherPersonId(chatIdModel);
          loadOtherPerson(otherPersonId);

          try {
            messageList
                .sort((bm1, bm2) => bm2.getTime().compareTo(bm1.getTime()));
          } catch (E) {}
        });

        subs.add(sub);
      }
      if(chatMode==CHAT_MODE_REGULAR) chatSetupRegular = true;
      if(chatMode==CHAT_MODE_ENCRYPT) chatSetupEncrypt = true;
      if (mounted) setState(() {});
    });
    subs.add(sub);
  });
}*/
