import 'dart:async';

import 'package:Strokes/AppEngine.dart';
import 'package:Strokes/app_config.dart';
import 'package:Strokes/assets.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class EnterPin extends StatefulWidget {
  @override
  _EnterPinState createState() => _EnterPinState();
}

class _EnterPinState extends State<EnterPin>
    with AutomaticKeepAliveClientMixin {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final pinController = TextEditingController();
  final errorController = StreamController<ErrorAnimationType>();

  @override
  void initState() {
    super.initState();
    //pinController.text = userModel.getString(CODE_PIN);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(backgroundColor: white, key: scaffoldKey, body: page1());
  }

  page1() {
    return Column(
      children: [
        Container(
          height: 100,
          padding: EdgeInsets.only(top: 35, right: 10),
          child: Row(
            children: [BackButton()],
          ),
        ),
        Flexible(
          child: ListView(
            padding: EdgeInsets.all(25),
            children: [
              Text.rich(TextSpan(children: [
                TextSpan(
                    text: "Please enter your ",
                    style: textStyle(false, 25, black)),
                TextSpan(text: "Code Pin ", style: textStyle(true, 25, black)),
                TextSpan(text: "for ", style: textStyle(false, 25, black)),
                TextSpan(
                    text: "Encrypted Chats", style: textStyle(true, 25, black)),
              ])),
              addSpace(20),
              Container(
//          padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  //color: black.withOpacity(.05),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: PinCodeTextField(
                  length: 4,
                  obsecureText: false,
                  animationType: AnimationType.fade,
                  textInputType: TextInputType.number,
                  errorAnimationController: errorController,
                  autoDisposeControllers: false,
                  pinTheme: PinTheme(
                      shape: PinCodeFieldShape.circle,
                      borderRadius: BorderRadius.circular(25),
                      fieldHeight: 40,
                      fieldWidth: 40,
                      inactiveFillColor: black.withOpacity(.3),
                      disabledColor: black.withOpacity(.02),
                      inactiveColor: black.withOpacity(.02),
                      selectedColor: black.withOpacity(.02),
                      activeFillColor: black.withOpacity(.02),
                      activeColor: black.withOpacity(.02),
                      selectedFillColor: black.withOpacity(.5)),
                  animationDuration: Duration(milliseconds: 300),
                  backgroundColor: transparent,
                  enableActiveFill: true,
                  controller: pinController,
                  onCompleted: (v) {},
                  onChanged: (value) {},
                  beforeTextPaste: (text) {
                    return true;
                  },
                ),
              ),
              addSpace(20),
              Container(
                height: 50,
                child: FlatButton(
                    onPressed: () {
                      String pin = pinController.text;
                      String codePin = userModel.getString(CODE_PIN);
                      String failCode = userModel.getString(FAILED_CODE_PIN);
                      //print("$failCode ${pin != failCode}");
                      bool valid = (pin == codePin) || (pin == failCode);

                      if (pin.isEmpty) {
                        toast(scaffoldKey, "Please enter your code pin!");
                        errorController.add(ErrorAnimationType.shake);
                        return;
                      }

                      if (!valid) {
                        toast(scaffoldKey, "Your pin is incorrect!");
                        pinController.clear();
                        errorController.add(ErrorAnimationType.shake);
                        return;
                      }

                      Navigator.pop(context, pin);
                    },
                    padding: EdgeInsets.all(15),
                    color: AppConfig.appColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    child: Center(
                        child: Text("CONTINUE",
                            style: textStyle(true, 16, white)))),
              )
            ],
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
