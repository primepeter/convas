  
import UIKit
import Flutter
import PushKit
import flutter_voip_push_notification
//import flutter_call_kit
import flutter_callkit_voximplant
import CallKit
//import Firebase

  

@UIApplicationMain
  @objc class AppDelegate: FlutterAppDelegate, PKPushRegistryDelegate {
    
    override func application(
          _ application: UIApplication,
          didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
          ) -> Bool {
      if #available(iOS 10.0, *) {
        UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
     }
          GeneratedPluginRegistrant.register(with: self)
          return super.application(application, didFinishLaunchingWithOptions: launchOptions)
      }
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {

         FlutterVoipPushNotificationPlugin.didUpdate(pushCredentials, forType: type.rawValue);
    }
    
  
///* Add PushKit delegate method */
//    // Handle updated push credentials
//    func pushRegistry(_ registry: PKPushRegistry,
//                      didReceiveIncomingPushWith payload: PKPushPayload,
//                      for type: PKPushType,
//                      completion: @escaping () -> Void){
//
//        let uuid = NSUUID().uuidString
//        let handle = "ConvasApp"
//        let callerName = payload.dictionaryPayload["callerName"] as! String
//
//        FlutterVoipPushNotificationPlugin.didReceiveIncomingPush(with: payload, forType: type.rawValue)
//        //FlutterCallKitPlugin.reportNewIncomingCall(uuid, handle: handle, handleType: "generic", hasVideo: false, localizedCallerName: callerName, fromPushKit: true)
//        processPush(with: payload.dictionaryPayload, and: completion)
//    }

    // Handle incoming pushes
//    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
//        // Process the received push
//        FlutterVoipPushNotificationPlugin.didUpdate(pushCredentials, forType: type.rawValue);
//        processPush(with: payload.dictionaryPayload, and: nil)
//    }
    
    
 
//    // 1. override PKPushRegistryDelegate methods
//    func pushRegistry(_ registry: PKPushRegistry,
//                      didReceiveIncomingPushWith payload: PKPushPayload,
//                      for type: PKPushType
//    ) {
//          FlutterVoipPushNotificationPlugin.didReceiveIncomingPush(with: payload, forType: type.rawValue)
//        processPush(with: payload.dictionaryPayload, and: nil)
//    }
    
    func pushRegistry(_ registry: PKPushRegistry,
                      didReceiveIncomingPushWith payload: PKPushPayload,
                      for type: PKPushType,
                      completion: @escaping () -> Void
    ) {
          FlutterVoipPushNotificationPlugin.didReceiveIncomingPush(with: payload, forType: type.rawValue)
        processPush(with: payload.dictionaryPayload, and: completion)
    }
    
    

       private func processPush(with payload: Dictionary<AnyHashable, Any>,
                                 and completion: (() -> Void)?
        ) {

     
        //guard let uuid = payload["callId"] as? String else { return <#default value#> }
            //let localizedName = payload["identifier"] as? String
            let handle = "ConvasApp"
            let callerName = payload["callerName"] as! String
            let uuid = payload["callId"] as! String
            let string = String(describing: uuid)
        
            // 4. prepare call update
            let callUpdate = CXCallUpdate()
            callUpdate.localizedCallerName = callerName
        
        
        //callUpdate.
            // 5. prepare provider configuration
            let configuration = CXProviderConfiguration(localizedName: handle)
            // 6. send it to the plugin
            FlutterCallkitPlugin.reportNewIncomingCall(
                with: UUID.init(uuidString: string) ?? UUID(),
                callUpdate: callUpdate,
                providerConfiguration: configuration,
                pushProcessingCompletion: completion
            )
        }

}
