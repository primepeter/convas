package com.convas;

import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin;
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService;

//import com.transistorsoft.flutter.backgroundfetch.BackgroundFetchPlugin;

/**
 * Created by John Ebere on 6/6/2018.
 */
public class MyApplication extends FlutterApplication implements PluginRegistry.PluginRegistrantCallback {

    public static int CALL_REQUEST_CODE = 4199;
    public static String CALL_STATUS_ACCEPTED = "accepted";
    public static String ENCODED_CALL_JSON = "encodedCallJson";
    public static String CALLER = "caller";
    public static String CALL = "call";
    public static String INTENT_IS_CALL = "intentIsCall";
    public static String USER_BASE = "userBase";

    @Override
    public void onCreate() {
        super.onCreate();
        //BackgroundFetchPlugin.setPluginRegistrant(this);
        FlutterFirebaseMessagingService.setPluginRegistrant(this);
    }

    @Override
    public void registerWith(PluginRegistry registry) {
        FirebaseMessagingPlugin.registerWith(registry.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin"));
        //GeneratedPluginRegistrant.registerWith(registry);
    }

}