package com.convas;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.flutter.plugins.androidalarmmanager.AlarmService;

import static com.android.volley.VolleyLog.TAG;
import static com.convas.MyApplication.CALL;
import static com.convas.MyApplication.CALLER;
import static com.convas.MyApplication.CALL_STATUS_ACCEPTED;
import static com.convas.MyApplication.ENCODED_CALL_JSON;
import static com.convas.MyApplication.INTENT_IS_CALL;


/*class  CallReminderService extends AlarmService{

  @Override
  public void onCreate() {
    super.onCreate();
  }

  @Override
  protected void onHandleWork(Intent intent) {
    super.onHandleWork(intent);
  }
}*/

public class FirebaseCallBgService extends Service {


  @Override
  public void onCreate() {
    super.onCreate();
    Log.v("CallService","Call Service Created");
    listenForOfflineMode();
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  private void listenForOfflineMode() {
    FirebaseUser user =FirebaseAuth.getInstance().getCurrentUser();
    if(user==null)return;
    FirebaseFirestore.getInstance().collection("userBase").document(user.getUid())
            .addSnapshotListener(new EventListener<DocumentSnapshot>() {
              @Override
              public void onEvent(@Nullable DocumentSnapshot snapshot,
                                  @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                  Log.w(TAG, "Listen failed.", e);
                  return;
                }

                if (snapshot != null && snapshot.exists()) {
                  Map<String,Object> map = snapshot.getData();
                  if(null!=map){
                    boolean isOffline = Objects.equals(map.get("isOnline1"), false);
                    Log.d(TAG, "Current data: " + isOffline);
                    if(isOffline){
                      listenForMyCalls(user.getUid());
                    }
                  }
                } else {
                  Log.d(TAG, "Current data: null");
                }
              }
            });
  }


  private void listenForMyCalls(String userId) {

    FirebaseFirestore.getInstance().collection("callsIdsBase")
            .whereArrayContains("parties",userId).limit(1)
            .addSnapshotListener(new EventListener<QuerySnapshot>() {
              @Override
              public void onEvent(@Nullable QuerySnapshot snapshots,
                                  @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                  Log.w(TAG, "listen:error", e);
                  return;
                }

                if(snapshots == null)return;


                for (DocumentChange dc : snapshots.getDocumentChanges()) {
                  if(dc.getType()== DocumentChange.Type.REMOVED||dc.getType()== DocumentChange.Type.MODIFIED)continue;
                  Map<String,Object> map = dc.getDocument().getData();
                  Log.d(TAG, "Call Info: " + map);
                  boolean myItem = Objects.equals(map.get("userId"), userId);
                  if(myItem)continue;
                  String status = String.valueOf(map.get("status"));
                  boolean  callAnswered = status.equals(CALL_STATUS_ACCEPTED);
                  if(callAnswered)continue;
                  List parties = (List) map.get("parties");
                  if (parties == null)continue;
                  parties.remove(userId);
                  String callerUserId = String.valueOf(parties.get(0));
                  loadCallerProfile(callerUserId, new ProfileInterface() {
                    @java.lang.Override
                    public void getProfile(Map<String, Object> callerProfile) {
                      Map<String,Object> call = new HashMap<>();
                      call.put(CALLER,callerProfile);
                      call.put(CALL,map);
                      //String msg = String.valueOf(dc.getDocument().getData());
                      Gson gson = new Gson();
                      String callEncoded = gson.toJson(call);
                      //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                      Intent i = new Intent(FirebaseCallBgService.this,MainActivity.class);
                      i.putExtra(ENCODED_CALL_JSON, callEncoded);
                      i.putExtra(INTENT_IS_CALL, true);
                      i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                      getApplicationContext().startActivity(i);
                    }
                  });
                }

              }
            });
  }
  private void loadCallerProfile(String callerUserId,ProfileInterface profileInterface) {
    FirebaseFirestore.getInstance().collection("userBase").document(callerUserId)
            .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
      @Override
      public void onComplete(@NonNull Task<DocumentSnapshot> task) {
        if (task.isSuccessful()) {
          DocumentSnapshot document = task.getResult();
          if (document != null) {
            if (document.exists()) {
              //Log.d(TAG, "DocumentSnapshot data: " + document.getData());
              profileInterface.getProfile(document.getData());
            } else {
              Log.d(TAG, "No such document");
            }
          }
        } else {
          Log.d(TAG, "get failed with ", task.getException());
        }
      }
    });
  }

  private interface ProfileInterface{
    void getProfile(Map<String, Object> map);
  }
}

