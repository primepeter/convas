const functions = require('firebase-functions');
const apn = require('apn');
const jwt = require('jsonwebtoken');
const { APNS } = require('apns2')
const { BasicNotification } = require('apns2')
const { SilentNotification } = require('apns2')
const fs = require('fs');



exports.makeCall = functions.https.onCall(async (data, context) => {
  let callToken = data.callToken;
  let callId = data.callId;
  let callerId = data.callerId;
  let callerName = data.callerName;
  let isVideo = data.isVideo;



  const config = {
    production: false, /* change this when in production */
    token: {
      key: "./AuthKey_33K97MAVW2.p8",
      keyId: "33K97MAVW2",
      teamId: "JXXWNS7S3X"
    }
  };
  let apnProvider = new apn.Provider(config);
  var note = new apn.Notification();
  // Expires 1 hour from now.
  note.expiry = Math.floor(Date.now() / 1000) + 3600;
  //note.badge = 3;
  //note.sound = "ping.aiff";
  note.alert = "\uD83D\uDCE7 \u2709 You have a new message";
  note.payload = {
    "callToken": String(callToken),
    "callId": String(callId),
    "callerId": String(callerId),
    "callerName": String(callerName),
    "isVideo": String(isVideo) === 'true'
  };

  note.topic = "com.convas.voip";
  return apnProvider.send(note, callToken).then((result) => {
    // see documentation for an explanation of result
    console.log(result);
    return "Call Sent. " + result + data;
  }).catch(error => {
    console.log(error);
    return error;
  });
  // apnProvider.shutdown();
  // return "Call Initialized! " + callToken;
});
//openssl x509 -in voip.cer -inform DER -outform PEM -out cert.pem
//openssl pkcs12 -in voip.p12 -out voip.pem -nodes -clcerts
//curl -v -d '{"callerID":"remote@example.com"}' --http2 --cert voip.pem:123456 https://api.development.push.apple.com/3/device/0d2006103a9b54b7899a98c45a31ac516eb46f8e4a588c0e8f590dc28827de77